"""mindcure URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views

from django_socketio.views import socketio

from app import views


admin.site.site_header = settings.ADMIN_SITE_HEADER

urlpatterns = [
    # user authentication
    url(r'^accounts/login/$', views.login,  name='login'),
    url(r'^logout/$', views.logout, {'next_page': '/'}, name='logout'),
    url(r'^accounts/register/$', views.RegistrationView.as_view(), name='register'),
    url(r'^accounts/verify$', views.verify, name='verify'),
    url(r'^password_reset/$', views.password_reset, {'template_name': 'registration/password_reset.html'},
        name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done,
        {'template_name': 'registration/password_reset_done.html'}, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.password_reset_confirm, {'template_name': 'registration/password_reset_confirm.html'},
        name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete,
        {'template_name': 'registration/password_reset_complete.html'}, name='password_reset_complete'),

    # therapist verification
    url(r'^therapists/verify$', views.therapist_verify, name='therapist_verify'),
    url(r'^therapists/redirect$', views.therapist_login_redirect,  name='therapist_login_redirect'),

    url(r'^', include('app.urls')),
    url(r'^api/', include('app.api_routes')),
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^admin/', admin.site.urls),
    url(r'^s3direct/', include('s3direct.urls')),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^admin/django-ses/', include('django_ses.urls')),
    url("^socket\.io", socketio, name="socketio"),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
