
from __future__ import absolute_import

import os

from django.conf import settings
from celery import Celery

# os.environ.setdefault('DJANGO_SETTINGS_MODULE', "mindcure.settings")

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mindcure.settings.production")
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mindcure.settings.{}".format(os.getenv('DJANGO_SETTINGS_NAME')))

app = Celery('mindcure', broker=settings.BROKER_URL)
app.config_from_object('django.conf:settings')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()
