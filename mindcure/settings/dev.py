from .basic import *

ALLOWED_HOSTS = ['localhost']

CSRF_COOKIE_SECURE = False
SESSION_COOKIE_SECURE = False
