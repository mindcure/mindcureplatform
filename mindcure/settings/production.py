from .basic import *

ALLOWED_HOSTS = ['.mindcureglobal.com']

FORUM_URL = 'https://forum.mindcureglobal.com'

DEBUG = False

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'mindcure',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': 5433
    }
}

THERAPIST_URL = 'https://therapists.mindcureglobal.com'

DOMAIN_BASE = 'mindcureglobal.com'

DOMAIN = 'mindcureglobal.com'

SCHEME = 'https://'

BASE_DOMAIN = SCHEME + DOMAIN

SESSION_COOKIE_DOMAIN = ".mindcureglobal.com"

SITE_URL = "https://mindcureglobal.com"
