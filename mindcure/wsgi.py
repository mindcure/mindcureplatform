"""
WSGI config for mindcure project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application

# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mindcure.settings")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mindcure.settings.{}".format(os.getenv('DJANGO_SETTINGS_NAME')))

sys.dont_write_bytecode = True

application = get_wsgi_application()
