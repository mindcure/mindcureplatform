const http = require('http');
const io = require('socket.io')();
const socketAuth = require('socketio-auth');

const PORT = process.env.PORT || 5060;
const server = http.createServer();
const bluebird = require('bluebird');
const redis = require('redis'),
    redisClient = redis.createClient();
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

io.attach(server);

// user verification
async function verifyUser (token) {
  return new Promise((resolve, reject) => {
    // setTimeout to mock a cache or database call
    setTimeout(() => {
      // this information should come from your cache or database

      const user = redisClient.getAsync(token);

      if (!user) {
        return reject('USER_NOT_FOUND');
      }

      return resolve(user);
    });
  });
}

socketAuth(io, {
  authenticate: async (socket, data, callback) => {

    console.log('data: ', data)

    const { token } = data;

    try {
      const user = await verifyUser(token);

      console.log('user: ', user);

      socket.user = user;

      return callback(null, true);
    } catch (e) {
      console.log(`Socket ${socket.id} unauthorized.`);
      return callback({ message: 'UNAUTHORIZED' });
    }
  },
  postAuthenticate: (socket) => {
    console.log(`Socket ${socket.id} authenticated.`);
  },
  disconnect: (socket) => {
    console.log(`Socket ${socket.id} disconnected.`);
  },
})

io.on('connection', (socket) => {
  console.log(`Socket ${socket.id} connected.`);

  socket.on('disconnect', () => {
    console.log(`Socket ${socket.id} disconnected.`);
  });
});

server.listen(PORT);