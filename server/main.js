
    // Handles remote MediaStream success by adding it as the remoteVideo src.
    function gotRemoteMediaStream(event) {
      const mediaStream = event.stream;
      localVideo.srcObject = mediaStream;
      remoteStream = mediaStream;
      trace('Remote peer connection received remote stream.');
    }

    // voice listeners
    // Connects with new peer candidate.
    function handleConnection(event) {
      const peerConnection = event.target;
      const iceCandidate = event.candidate;

      trace('iceCandidate: ', iceCandidate);

      if (iceCandidate) {
        const newIceCandidate = new RTCIceCandidate(iceCandidate);
        const otherPeer = getOtherPeer(peerConnection);

        otherPeer.addIceCandidate(newIceCandidate)
          .then(() => {
            handleConnectionSuccess(peerConnection);
          }).catch((error) => {
            handleConnectionFailure(peerConnection, error);
          });

        trace(`${getPeerName(peerConnection)} ICE candidate:\n` +
              `${event.candidate.candidate}.`);
      }
    }

    // Logs that the connection succeeded.
    function handleConnectionSuccess(peerConnection) {
      trace(`${getPeerName(peerConnection)} addIceCandidate success.`);
    };

    // Logs that the connection failed.
    function handleConnectionFailure(peerConnection, error) {
      trace(`${getPeerName(peerConnection)} failed to add ICE Candidate:\n`+
            `${error.toString()}.`);
    }

    // Logs changes to the connection state.
    function handleConnectionChange(event) {
      const peerConnection = event.target;
      console.log('ICE state change event: ', event);
      trace(`${getPeerName(peerConnection)} ICE state: ` +
            `${peerConnection.iceConnectionState}.`);
    }

    function answer() {
        trace('answering remote connection request')
        localPeerConnection.createAnswer().then(createdAnswer).catch(setSessionDescriptionError);
    }

    function createdAnswer() {

    }


    function call() {
        $scope.voice.calling = true;
        trace('Starting call.');
        startTime = window.performance.now();

        // Get local media stream tracks.
        const videoTracks = localStream.getVideoTracks();
        const audioTracks = localStream.getAudioTracks();
        if (videoTracks.length > 0) {
            trace(`Using video device: ${videoTracks[0].label}.`);
        }
        if (audioTracks.length > 0) {
            trace(`Using audio device: ${audioTracks[0].label}.`);
        }

        const servers = { iceServers: [{
                  urls: "turn:mindcureglobal.com:3478",
                  username: "mindcure",
                  credential: "e@volution1ary"
              }]
        };  // Allows for RTC server configuration.
        // Create peer connections and add behavior.
        localPeerConnection = new RTCPeerConnection(servers);
        trace('Created local peer connection object localPeerConnection.');

        // listen for ice candidate
        localPeerConnection.addEventListener('icecandidate', handleConnection);
        localPeerConnection.addEventListener(
            'iceconnectionstatechange', handleConnectionChange);

        // create remote peer connection
        remotePeerConnection = new RTCPeerConnection(servers);
        trace('Created remote peer connection object remotePeerConnection.');

        // listen for ice candidate
        remotePeerConnection.addEventListener('icecandidate', handleConnection);
        remotePeerConnection.addEventListener(
            'iceconnectionstatechange', handleConnectionChange);
        remotePeerConnection.addEventListener('addstream', gotRemoteMediaStream);

        // Add local stream to connection and create offer to connect.
        localPeerConnection.addStream(localStream);
        trace('Added local stream to localPeerConnection.');

        // create offer
        trace('localPeerConnection createOffer start.');
        localPeerConnection.createOffer($scope.voice.offerOptions)
            .then(createdOffer).catch(setSessionDescriptionError);
    }

    function gotLocalMediaStream(stream) {
        localVideo = document.querySelector('video');
        console.log('Got stream with constraints:', constraints);

        modal.style.display = "block";
        localStream = stream; // make variable available to browser console
//        video.srcObject = stream;
    }

    function handleLocalMediaStreamError(error) {
      if (error.name === 'ConstraintNotSatisfiedError') {
        let v = constraints.video;
        errorMsg(`The resolution ${v.width.exact}x${v.height.exact} px is not supported by your device.`);
      } else if (error.name === 'PermissionDeniedError') {
        errorMsg('Permissions have not been granted to use your camera and ' +
          'microphone, you need to allow the page access to your devices in ' +
          'order for the demo to work.');
      }
      errorMsg(`getUserMedia error: ${error.name}`, error);
    }

    async function init(e) {
        e.disabled = true;
        const stream = await navigator.mediaDevices.getUserMedia(constraints);
        gotLocalMediaStream(stream);
        call();
//      try {
//        const stream = await navigator.mediaDevices.getUserMedia(constraints);
//        gotLocalMediaStream(stream);
//        call();
//      } catch (e) {
//        console.warn(e);
//        handleLocalMediaStreamError(e);
//      }
    }

function hangupCall() {
    localPeerConnection.close();
    remotePeerConnection.close();
    localPeerConnection = null;
    remotePeerConnection = null;
    hangupButton.disabled = true;
    callButton.disabled = false;
    trace('Ending call.');
}