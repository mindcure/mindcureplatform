var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var _ = require('lodash-node');
var bluebird = require('bluebird');
var redis = require('redis'),
    redisClient = redis.createClient();
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const serialize = require('node-serialize');

var users = [];

async function getAccount(identifier) {
    return await redisClient.getAsync(identifier).catch(()=> {
        socket.disconnect();
    });
}

function authenticate(socket, token, user_type) {

    redisClient.get(`${user_type}:${token}`, (err, result) => {

        if (err || !result ) {
            return socket.emit('login_error', 'Invalid token');
        }

        session_account = serialize.unserialize(result);

        socket.emit('login_successful', 'true');
        socket.broadcast.emit('online', session_account.username);

        // attach user to redis user
        session_account.socket = socket.id;
        session_account.user_type = user_type
        users.push(session_account);
    })
}

function disconnect(socket) {

    var index = _.findIndex(users, { socket: socket.id, user_type: 'user' });
    if (index !== -1) {
        var user_obj = users[index];
        socket.broadcast.emit('offline', users[index].username);
        users.splice(index, 1);
        console.log(`${user_obj.username} disconnected.`);
        return
    }

    var t_index = _.findIndex(users, { socket: socket.id, user_type: 'therapist' });
    if (t_index !== -1) {
        var therapist = users[t_index];
        socket.broadcast.emit('offline', users[t_index].email);
        users.splice(t_index, 1);
        console.log(`${therapist.email} disconnected.`);
        return
    }
}

function offerType(socket, email, user_type, token, message) {

    var currentUser = _.find(users, { socket: socket.id });

    if (!currentUser) {
        return getAccount(`user:${token}`).then(function(user) {

            if(!user) { 
                socket.emit('logged_out');
                return; 
            }
           
            // save user details
            currentUser = serialize.unserialize(user);
            currentUser.socket = socket.id;
            users.push(currentUser);

            var contact = _.find(users, { email: email, user_type: user_type });

            if (!contact) {
                socket.emit('contact_status', false);
                return;
            }

            if (!contact.onboarding.chat_step_completed || !contact.onboarding.payment_step_completed) {
                socket.emit('contact_status', false);
                return;
            }

            io.to(contact.socket).emit('messageReceived', currentUser.username, message, socket);
            socket.emit('contact_status', true, contact.socket);

            return;
        });
    }

    var contact = _.find(users, { email: email, user_type: user_type });

    if (!contact) {
        socket.emit('contact_status', false);
        return;
    }

    if (!contact.onboarding.chat_step_completed || !contact.onboarding.payment_step_completed) {
        socket.emit('contact_status', false);
        return;
    }

    io.to(contact.socket).emit('messageReceived', currentUser.username, message);
    socket.emit('contact_status', true, contact.socket);
    return;
}


io.on('connection', function (socket) {

   console.log(`Socket ${socket.id} connected.`);

  // login listener
  socket.on('login', function (token, user_type) {
    authenticate(socket, token, user_type)
  });

  socket.on('message', function(message) {

    console.log('message: ', message);

    if (message.type == 'offer') {
        offerType(socket, message.email, message.user_type, message.token, message.data);
    }    

    else if (message.type === 'bye') {
        console.log('bye received');
        io.to(message.socket).emit('bye');
    }

    else if (message.type === 'answer') {
        // io.to(message.socket).emit('answered');
    }

    socket.broadcast.emit('message', message);

  });

  socket.on('disconnect', () => {
        disconnect(socket);
  });
  ///////////////////

  socket.on('bye', function(){
    console.log('received bye');
  });

});

http.listen(5060, function(){
  console.log('listening on *:5060');
});
