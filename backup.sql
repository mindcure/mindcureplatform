--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.12
-- Dumped by pg_dump version 9.5.12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_account (
    user_ptr_id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    date_of_birth date,
    address_id integer,
    bio text,
    headline character varying(255)
);


ALTER TABLE public.app_account OWNER TO postgres;

--
-- Name: app_address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_address (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    phone character varying(50),
    line1 text NOT NULL,
    line2 text,
    country_id integer NOT NULL,
    state_id integer NOT NULL,
    city_id integer
);


ALTER TABLE public.app_address OWNER TO postgres;

--
-- Name: app_address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_address_id_seq OWNER TO postgres;

--
-- Name: app_address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_address_id_seq OWNED BY public.app_address.id;


--
-- Name: app_assessmenttest; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_assessmenttest (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    title text NOT NULL,
    slug character varying(50) NOT NULL,
    banner_image character varying(100) NOT NULL,
    description text
);


ALTER TABLE public.app_assessmenttest OWNER TO postgres;

--
-- Name: app_assessmenttest_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_assessmenttest_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_assessmenttest_id_seq OWNER TO postgres;

--
-- Name: app_assessmenttest_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_assessmenttest_id_seq OWNED BY public.app_assessmenttest.id;


--
-- Name: app_cart; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_cart (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    checked_out boolean NOT NULL,
    user_id integer,
    checkout_date date
);


ALTER TABLE public.app_cart OWNER TO postgres;

--
-- Name: app_cart_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_cart_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_cart_id_seq OWNER TO postgres;

--
-- Name: app_cart_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_cart_id_seq OWNED BY public.app_cart.id;


--
-- Name: app_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_category (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name character varying(100) NOT NULL,
    slug character varying(50) NOT NULL
);


ALTER TABLE public.app_category OWNER TO postgres;

--
-- Name: app_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_category_id_seq OWNER TO postgres;

--
-- Name: app_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_category_id_seq OWNED BY public.app_category.id;


--
-- Name: app_certification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_certification (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    issuer text NOT NULL,
    date date NOT NULL,
    description text,
    name text NOT NULL,
    therapist_id integer NOT NULL
);


ALTER TABLE public.app_certification OWNER TO postgres;

--
-- Name: app_certification_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_certification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_certification_id_seq OWNER TO postgres;

--
-- Name: app_certification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_certification_id_seq OWNED BY public.app_certification.id;


--
-- Name: app_city; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_city (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name text NOT NULL,
    code character varying(150) NOT NULL,
    state_id integer NOT NULL
);


ALTER TABLE public.app_city OWNER TO postgres;

--
-- Name: app_city_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_city_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_city_id_seq OWNER TO postgres;

--
-- Name: app_city_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_city_id_seq OWNED BY public.app_city.id;


--
-- Name: app_country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_country (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name text NOT NULL,
    code character varying(150) NOT NULL,
    is_enabled boolean NOT NULL
);


ALTER TABLE public.app_country OWNER TO postgres;

--
-- Name: app_country_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_country_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_country_id_seq OWNER TO postgres;

--
-- Name: app_country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_country_id_seq OWNED BY public.app_country.id;


--
-- Name: app_education; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_education (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    year integer NOT NULL,
    degree text NOT NULL,
    study_field text NOT NULL,
    grade character varying(255) NOT NULL,
    school_id integer NOT NULL
);


ALTER TABLE public.app_education OWNER TO postgres;

--
-- Name: app_education_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_education_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_education_id_seq OWNER TO postgres;

--
-- Name: app_education_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_education_id_seq OWNED BY public.app_education.id;


--
-- Name: app_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_item (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    quantity integer NOT NULL,
    unit_price numeric(18,2) NOT NULL,
    cart_id integer NOT NULL,
    user_id integer,
    product_id integer NOT NULL,
    CONSTRAINT app_item_quantity_check CHECK ((quantity >= 0))
);


ALTER TABLE public.app_item OWNER TO postgres;

--
-- Name: app_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_item_id_seq OWNER TO postgres;

--
-- Name: app_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_item_id_seq OWNED BY public.app_item.id;


--
-- Name: app_onboarding; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_onboarding (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    assessment_step_completed boolean NOT NULL,
    step_two_completed boolean NOT NULL,
    step_three_completed boolean NOT NULL,
    step_four_completed boolean NOT NULL,
    step_five_completed boolean NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.app_onboarding OWNER TO postgres;

--
-- Name: app_onboarding_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_onboarding_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_onboarding_id_seq OWNER TO postgres;

--
-- Name: app_onboarding_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_onboarding_id_seq OWNED BY public.app_onboarding.id;


--
-- Name: app_order; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_order (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    delivery_address text NOT NULL,
    delivery_name text NOT NULL,
    order_status_id integer NOT NULL,
    reference_code character varying(150) NOT NULL,
    payment_status_id integer NOT NULL
);


ALTER TABLE public.app_order OWNER TO postgres;

--
-- Name: app_order_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_order_id_seq OWNER TO postgres;

--
-- Name: app_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_order_id_seq OWNED BY public.app_order.id;


--
-- Name: app_orderitem; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_orderitem (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    quantity integer NOT NULL,
    unit_price numeric(18,2) NOT NULL,
    total_price numeric(18,2) NOT NULL,
    order_id integer NOT NULL,
    product_id integer NOT NULL,
    user_id integer NOT NULL,
    CONSTRAINT app_orderitem_quantity_check CHECK ((quantity >= 0))
);


ALTER TABLE public.app_orderitem OWNER TO postgres;

--
-- Name: app_orderitem_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_orderitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_orderitem_id_seq OWNER TO postgres;

--
-- Name: app_orderitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_orderitem_id_seq OWNED BY public.app_orderitem.id;


--
-- Name: app_orderstatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_orderstatus (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(50) NOT NULL
);


ALTER TABLE public.app_orderstatus OWNER TO postgres;

--
-- Name: app_orderstatus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_orderstatus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_orderstatus_id_seq OWNER TO postgres;

--
-- Name: app_orderstatus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_orderstatus_id_seq OWNED BY public.app_orderstatus.id;


--
-- Name: app_paymentstatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_paymentstatus (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(50) NOT NULL
);


ALTER TABLE public.app_paymentstatus OWNER TO postgres;

--
-- Name: app_paymentstatus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_paymentstatus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_paymentstatus_id_seq OWNER TO postgres;

--
-- Name: app_paymentstatus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_paymentstatus_id_seq OWNED BY public.app_paymentstatus.id;


--
-- Name: app_plantype; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_plantype (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(150) NOT NULL,
    description text
);


ALTER TABLE public.app_plantype OWNER TO postgres;

--
-- Name: app_plantype_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_plantype_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_plantype_id_seq OWNER TO postgres;

--
-- Name: app_plantype_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_plantype_id_seq OWNED BY public.app_plantype.id;


--
-- Name: app_product; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_product (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name text NOT NULL,
    sku text NOT NULL,
    unit_price numeric(10,2) NOT NULL,
    available_quantity integer NOT NULL,
    cover_image character varying(100) NOT NULL,
    description text
);


ALTER TABLE public.app_product OWNER TO postgres;

--
-- Name: app_product_categories; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_product_categories (
    id integer NOT NULL,
    product_id integer NOT NULL,
    category_id integer NOT NULL
);


ALTER TABLE public.app_product_categories OWNER TO postgres;

--
-- Name: app_product_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_product_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_product_categories_id_seq OWNER TO postgres;

--
-- Name: app_product_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_product_categories_id_seq OWNED BY public.app_product_categories.id;


--
-- Name: app_product_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_product_id_seq OWNER TO postgres;

--
-- Name: app_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_product_id_seq OWNED BY public.app_product.id;


--
-- Name: app_qualification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_qualification (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name text NOT NULL,
    code character varying(255) NOT NULL
);


ALTER TABLE public.app_qualification OWNER TO postgres;

--
-- Name: app_qualification_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_qualification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_qualification_id_seq OWNER TO postgres;

--
-- Name: app_qualification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_qualification_id_seq OWNED BY public.app_qualification.id;


--
-- Name: app_question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_question (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    number smallint NOT NULL,
    body text NOT NULL,
    is_active boolean NOT NULL,
    assessment_test_id integer NOT NULL,
    CONSTRAINT app_question_number_check CHECK ((number >= 0))
);


ALTER TABLE public.app_question OWNER TO postgres;

--
-- Name: app_question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_question_id_seq OWNER TO postgres;

--
-- Name: app_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_question_id_seq OWNED BY public.app_question.id;


--
-- Name: app_question_responses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_question_responses (
    id integer NOT NULL,
    question_id integer NOT NULL,
    questionresponse_id integer NOT NULL
);


ALTER TABLE public.app_question_responses OWNER TO postgres;

--
-- Name: app_question_responses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_question_responses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_question_responses_id_seq OWNER TO postgres;

--
-- Name: app_question_responses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_question_responses_id_seq OWNED BY public.app_question_responses.id;


--
-- Name: app_questionresponse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_questionresponse (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.app_questionresponse OWNER TO postgres;

--
-- Name: app_questionresponse_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_questionresponse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_questionresponse_id_seq OWNER TO postgres;

--
-- Name: app_questionresponse_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_questionresponse_id_seq OWNED BY public.app_questionresponse.id;


--
-- Name: app_ratingentry; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_ratingentry (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    score integer NOT NULL,
    therapist_id integer NOT NULL,
    CONSTRAINT app_ratingentry_score_check CHECK ((score >= 0))
);


ALTER TABLE public.app_ratingentry OWNER TO postgres;

--
-- Name: app_ratingentry_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_ratingentry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_ratingentry_id_seq OWNER TO postgres;

--
-- Name: app_ratingentry_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_ratingentry_id_seq OWNED BY public.app_ratingentry.id;


--
-- Name: app_resources; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_resources (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    resource_url character varying(100) NOT NULL,
    order_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.app_resources OWNER TO postgres;

--
-- Name: app_resources_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_resources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_resources_id_seq OWNER TO postgres;

--
-- Name: app_resources_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_resources_id_seq OWNED BY public.app_resources.id;


--
-- Name: app_review; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_review (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    body text NOT NULL,
    therapist_id integer NOT NULL
);


ALTER TABLE public.app_review OWNER TO postgres;

--
-- Name: app_review_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_review_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_review_id_seq OWNER TO postgres;

--
-- Name: app_review_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_review_id_seq OWNED BY public.app_review.id;


--
-- Name: app_school; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_school (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name text NOT NULL,
    code character varying(255) NOT NULL,
    city text,
    country_id integer NOT NULL,
    state_id integer NOT NULL
);


ALTER TABLE public.app_school OWNER TO postgres;

--
-- Name: app_school_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_school_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_school_id_seq OWNER TO postgres;

--
-- Name: app_school_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_school_id_seq OWNED BY public.app_school.id;


--
-- Name: app_sessions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_sessions (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    last_active_date timestamp with time zone NOT NULL,
    therapist_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.app_sessions OWNER TO postgres;

--
-- Name: app_sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_sessions_id_seq OWNER TO postgres;

--
-- Name: app_sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_sessions_id_seq OWNED BY public.app_sessions.id;


--
-- Name: app_state; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_state (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name text NOT NULL,
    code character varying(150) NOT NULL,
    is_enabled boolean NOT NULL,
    country_id integer NOT NULL
);


ALTER TABLE public.app_state OWNER TO postgres;

--
-- Name: app_state_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_state_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_state_id_seq OWNER TO postgres;

--
-- Name: app_state_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_state_id_seq OWNED BY public.app_state.id;


--
-- Name: app_testimonial; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_testimonial (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    image character varying(100) NOT NULL,
    body text NOT NULL,
    is_active boolean NOT NULL,
    headline text NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.app_testimonial OWNER TO postgres;

--
-- Name: app_testimonial_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_testimonial_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_testimonial_id_seq OWNER TO postgres;

--
-- Name: app_testimonial_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_testimonial_id_seq OWNED BY public.app_testimonial.id;


--
-- Name: app_therapist; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_therapist (
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    profile_image character varying(100) NOT NULL,
    address_id integer,
    is_activated boolean NOT NULL,
    bio text NOT NULL,
    qualification_id integer NOT NULL,
    resume text NOT NULL,
    email character varying(254) NOT NULL,
    password character varying(255) NOT NULL,
    name text NOT NULL,
    id integer NOT NULL,
    rating integer NOT NULL,
    reviews_count integer NOT NULL,
    CONSTRAINT app_therapist_rating_check CHECK ((rating >= 0))
);


ALTER TABLE public.app_therapist OWNER TO postgres;

--
-- Name: app_therapist_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_therapist_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_therapist_id_seq OWNER TO postgres;

--
-- Name: app_therapist_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_therapist_id_seq OWNED BY public.app_therapist.id;


--
-- Name: app_transaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_transaction (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    reference_code text NOT NULL,
    "txRef" text,
    amount double precision NOT NULL,
    order_id integer NOT NULL,
    transaction_status_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.app_transaction OWNER TO postgres;

--
-- Name: app_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_transaction_id_seq OWNER TO postgres;

--
-- Name: app_transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_transaction_id_seq OWNED BY public.app_transaction.id;


--
-- Name: app_transactionstatus; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_transactionstatus (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    name character varying(200) NOT NULL,
    code character varying(50) NOT NULL
);


ALTER TABLE public.app_transactionstatus OWNER TO postgres;

--
-- Name: app_transactionstatus_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_transactionstatus_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_transactionstatus_id_seq OWNER TO postgres;

--
-- Name: app_transactionstatus_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_transactionstatus_id_seq OWNED BY public.app_transactionstatus.id;


--
-- Name: app_userassessment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_userassessment (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    code character varying(50) NOT NULL,
    is_completed boolean NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.app_userassessment OWNER TO postgres;

--
-- Name: app_userassessment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_userassessment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_userassessment_id_seq OWNER TO postgres;

--
-- Name: app_userassessment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_userassessment_id_seq OWNED BY public.app_userassessment.id;


--
-- Name: app_userresponse; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.app_userresponse (
    id integer NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    response text NOT NULL,
    question text NOT NULL,
    assessment_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.app_userresponse OWNER TO postgres;

--
-- Name: app_userresponse_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.app_userresponse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_userresponse_id_seq OWNER TO postgres;

--
-- Name: app_userresponse_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.app_userresponse_id_seq OWNED BY public.app_userresponse.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: authtoken_token; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.authtoken_token (
    key character varying(40) NOT NULL,
    created timestamp with time zone NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.authtoken_token OWNER TO postgres;

--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- Name: jet_bookmark; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jet_bookmark (
    id integer NOT NULL,
    url character varying(200) NOT NULL,
    title character varying(255) NOT NULL,
    "user" integer NOT NULL,
    date_add timestamp with time zone NOT NULL,
    CONSTRAINT jet_bookmark_user_check CHECK (("user" >= 0))
);


ALTER TABLE public.jet_bookmark OWNER TO postgres;

--
-- Name: jet_bookmark_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jet_bookmark_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jet_bookmark_id_seq OWNER TO postgres;

--
-- Name: jet_bookmark_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jet_bookmark_id_seq OWNED BY public.jet_bookmark.id;


--
-- Name: jet_pinnedapplication; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jet_pinnedapplication (
    id integer NOT NULL,
    app_label character varying(255) NOT NULL,
    "user" integer NOT NULL,
    date_add timestamp with time zone NOT NULL,
    CONSTRAINT jet_pinnedapplication_user_check CHECK (("user" >= 0))
);


ALTER TABLE public.jet_pinnedapplication OWNER TO postgres;

--
-- Name: jet_pinnedapplication_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jet_pinnedapplication_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jet_pinnedapplication_id_seq OWNER TO postgres;

--
-- Name: jet_pinnedapplication_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.jet_pinnedapplication_id_seq OWNED BY public.jet_pinnedapplication.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_address ALTER COLUMN id SET DEFAULT nextval('public.app_address_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_assessmenttest ALTER COLUMN id SET DEFAULT nextval('public.app_assessmenttest_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_cart ALTER COLUMN id SET DEFAULT nextval('public.app_cart_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_category ALTER COLUMN id SET DEFAULT nextval('public.app_category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_certification ALTER COLUMN id SET DEFAULT nextval('public.app_certification_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_city ALTER COLUMN id SET DEFAULT nextval('public.app_city_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_country ALTER COLUMN id SET DEFAULT nextval('public.app_country_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_education ALTER COLUMN id SET DEFAULT nextval('public.app_education_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_item ALTER COLUMN id SET DEFAULT nextval('public.app_item_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_onboarding ALTER COLUMN id SET DEFAULT nextval('public.app_onboarding_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_order ALTER COLUMN id SET DEFAULT nextval('public.app_order_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_orderitem ALTER COLUMN id SET DEFAULT nextval('public.app_orderitem_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_orderstatus ALTER COLUMN id SET DEFAULT nextval('public.app_orderstatus_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_paymentstatus ALTER COLUMN id SET DEFAULT nextval('public.app_paymentstatus_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_plantype ALTER COLUMN id SET DEFAULT nextval('public.app_plantype_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_product ALTER COLUMN id SET DEFAULT nextval('public.app_product_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_product_categories ALTER COLUMN id SET DEFAULT nextval('public.app_product_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_qualification ALTER COLUMN id SET DEFAULT nextval('public.app_qualification_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_question ALTER COLUMN id SET DEFAULT nextval('public.app_question_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_question_responses ALTER COLUMN id SET DEFAULT nextval('public.app_question_responses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_questionresponse ALTER COLUMN id SET DEFAULT nextval('public.app_questionresponse_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_ratingentry ALTER COLUMN id SET DEFAULT nextval('public.app_ratingentry_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_resources ALTER COLUMN id SET DEFAULT nextval('public.app_resources_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_review ALTER COLUMN id SET DEFAULT nextval('public.app_review_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_school ALTER COLUMN id SET DEFAULT nextval('public.app_school_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_sessions ALTER COLUMN id SET DEFAULT nextval('public.app_sessions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_state ALTER COLUMN id SET DEFAULT nextval('public.app_state_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_testimonial ALTER COLUMN id SET DEFAULT nextval('public.app_testimonial_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_therapist ALTER COLUMN id SET DEFAULT nextval('public.app_therapist_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_transaction ALTER COLUMN id SET DEFAULT nextval('public.app_transaction_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_transactionstatus ALTER COLUMN id SET DEFAULT nextval('public.app_transactionstatus_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_userassessment ALTER COLUMN id SET DEFAULT nextval('public.app_userassessment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_userresponse ALTER COLUMN id SET DEFAULT nextval('public.app_userresponse_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jet_bookmark ALTER COLUMN id SET DEFAULT nextval('public.jet_bookmark_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jet_pinnedapplication ALTER COLUMN id SET DEFAULT nextval('public.jet_pinnedapplication_id_seq'::regclass);


--
-- Data for Name: app_account; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_account (user_ptr_id, created_at, updated_at, date_of_birth, address_id, bio, headline) FROM stdin;
1	2018-03-18 23:28:43.16872+01	2018-03-18 23:28:43.168737+01	\N	\N	Tell them my story	\N
\.


--
-- Data for Name: app_address; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_address (id, created_at, updated_at, phone, line1, line2, country_id, state_id, city_id) FROM stdin;
1	2018-03-18 23:16:08.246526+01	2018-03-18 23:16:08.24655+01	7055784398	14, Ebinpejo Lane		1	25	\N
\.


--
-- Name: app_address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_address_id_seq', 1, true);


--
-- Data for Name: app_assessmenttest; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_assessmenttest (id, created_at, updated_at, title, slug, banner_image, description) FROM stdin;
2	2018-03-28 10:40:38+01	2018-03-28 10:40:38+01	Emotional Wellness Test for Students	emotional-wellness-test-for-students	images/c1_aeaDBJx.jpg	As a student, you would agree that tests are a major stress-inducers. But all tests don't have to be that way. Here's a test that won't stress you but will tell you just how stressed you are.
\.


--
-- Name: app_assessmenttest_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_assessmenttest_id_seq', 2, true);


--
-- Data for Name: app_cart; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_cart (id, created_at, updated_at, checked_out, user_id, checkout_date) FROM stdin;
19	2018-03-27 23:21:58.871449+01	2018-03-27 23:21:58.871468+01	f	\N	\N
\.


--
-- Name: app_cart_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_cart_id_seq', 19, true);


--
-- Data for Name: app_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_category (id, created_at, updated_at, name, slug) FROM stdin;
1	2018-03-27 23:55:05+01	2018-03-27 23:55:05+01	Books	books
\.


--
-- Name: app_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_category_id_seq', 1, true);


--
-- Data for Name: app_certification; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_certification (id, created_at, updated_at, issuer, date, description, name, therapist_id) FROM stdin;
\.


--
-- Name: app_certification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_certification_id_seq', 1, false);


--
-- Data for Name: app_city; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_city (id, created_at, updated_at, name, code, state_id) FROM stdin;
1	2018-03-18 23:14:09.897162+01	2018-03-18 23:14:09.897215+01	Aba South	aba south	1
2	2018-03-18 23:14:09.908012+01	2018-03-18 23:14:09.908049+01	Arochukwu	arochukwu	1
3	2018-03-18 23:14:09.919111+01	2018-03-18 23:14:09.919151+01	Bende	bende	1
4	2018-03-18 23:14:09.930033+01	2018-03-18 23:14:09.930077+01	Ikwuano	ikwuano	1
5	2018-03-18 23:14:09.940839+01	2018-03-18 23:14:09.940866+01	Isiala Ngwa North	isiala ngwa north	1
6	2018-03-18 23:14:09.951845+01	2018-03-18 23:14:09.951873+01	Isiala Ngwa South	isiala ngwa south	1
7	2018-03-18 23:14:09.96291+01	2018-03-18 23:14:09.962943+01	Isuikwuato	isuikwuato	1
8	2018-03-18 23:14:09.973971+01	2018-03-18 23:14:09.974009+01	Obi Ngwa	obi ngwa	1
9	2018-03-18 23:14:09.98504+01	2018-03-18 23:14:09.985081+01	Ohafia	ohafia	1
10	2018-03-18 23:14:09.996035+01	2018-03-18 23:14:09.996079+01	Osisioma	osisioma	1
11	2018-03-18 23:14:10.007043+01	2018-03-18 23:14:10.007085+01	Ugwunagbo	ugwunagbo	1
12	2018-03-18 23:14:10.01803+01	2018-03-18 23:14:10.01807+01	Ukwa East	ukwa east	1
13	2018-03-18 23:14:10.029111+01	2018-03-18 23:14:10.029157+01	Ukwa West	ukwa west	1
14	2018-03-18 23:14:10.039814+01	2018-03-18 23:14:10.039849+01	Umuahia North	umuahia north	1
15	2018-03-18 23:14:10.051083+01	2018-03-18 23:14:10.051128+01	Umuahia South	umuahia south	1
16	2018-03-18 23:14:10.062076+01	2018-03-18 23:14:10.06212+01	Umu Nneochi	umu nneochi	1
17	2018-03-18 23:14:10.083725+01	2018-03-18 23:14:10.08374+01	Fufure	fufure	2
18	2018-03-18 23:14:10.094815+01	2018-03-18 23:14:10.094833+01	Ganye	ganye	2
19	2018-03-18 23:14:10.106034+01	2018-03-18 23:14:10.106057+01	Gayuk	gayuk	2
20	2018-03-18 23:14:10.117155+01	2018-03-18 23:14:10.117185+01	Gombi	gombi	2
21	2018-03-18 23:14:10.12807+01	2018-03-18 23:14:10.128111+01	Grie	grie	2
22	2018-03-18 23:14:10.13897+01	2018-03-18 23:14:10.139007+01	Hong	hong	2
23	2018-03-18 23:14:10.149919+01	2018-03-18 23:14:10.149951+01	Jada	jada	2
24	2018-03-18 23:14:10.160983+01	2018-03-18 23:14:10.161019+01	Lamurde	lamurde	2
25	2018-03-18 23:14:10.171936+01	2018-03-18 23:14:10.171971+01	Madagali	madagali	2
26	2018-03-18 23:14:10.182942+01	2018-03-18 23:14:10.182976+01	Maiha	maiha	2
27	2018-03-18 23:14:10.193951+01	2018-03-18 23:14:10.193987+01	Mayo Belwa	mayo belwa	2
28	2018-03-18 23:14:10.205019+01	2018-03-18 23:14:10.205054+01	Michika	michika	2
29	2018-03-18 23:14:10.215989+01	2018-03-18 23:14:10.216025+01	Mubi North	mubi north	2
30	2018-03-18 23:14:10.227024+01	2018-03-18 23:14:10.227059+01	Mubi South	mubi south	2
31	2018-03-18 23:14:10.23955+01	2018-03-18 23:14:10.239587+01	Numan	numan	2
32	2018-03-18 23:14:10.24928+01	2018-03-18 23:14:10.249325+01	Shelleng	shelleng	2
33	2018-03-18 23:14:10.260281+01	2018-03-18 23:14:10.260326+01	Song	song	2
34	2018-03-18 23:14:10.271224+01	2018-03-18 23:14:10.27127+01	Toungo	toungo	2
35	2018-03-18 23:14:10.282231+01	2018-03-18 23:14:10.282269+01	Yola North	yola north	2
36	2018-03-18 23:14:10.293109+01	2018-03-18 23:14:10.293148+01	Yola South	yola south	2
37	2018-03-18 23:14:10.315185+01	2018-03-18 23:14:10.31522+01	Eastern Obolo	eastern obolo	3
38	2018-03-18 23:14:10.326156+01	2018-03-18 23:14:10.32619+01	Eket	eket	3
39	2018-03-18 23:14:10.337146+01	2018-03-18 23:14:10.33718+01	Esit Eket	esit eket	3
40	2018-03-18 23:14:10.348102+01	2018-03-18 23:14:10.348137+01	Essien Udim	essien udim	3
41	2018-03-18 23:14:10.359627+01	2018-03-18 23:14:10.359672+01	Etim Ekpo	etim ekpo	3
42	2018-03-18 23:14:10.370666+01	2018-03-18 23:14:10.370707+01	Etinan	etinan	3
43	2018-03-18 23:14:10.381445+01	2018-03-18 23:14:10.381489+01	Ibeno	ibeno	3
44	2018-03-18 23:14:10.392407+01	2018-03-18 23:14:10.392451+01	Ibesikpo Asutan	ibesikpo asutan	3
45	2018-03-18 23:14:10.403351+01	2018-03-18 23:14:10.403392+01	Ibiono-Ibom	ibiono-ibom	3
46	2018-03-18 23:14:10.414471+01	2018-03-18 23:14:10.414511+01	Ika	ika	3
47	2018-03-18 23:14:10.42541+01	2018-03-18 23:14:10.425449+01	Ikono	ikono	3
48	2018-03-18 23:14:10.43641+01	2018-03-18 23:14:10.436451+01	Ikot Abasi	ikot abasi	3
49	2018-03-18 23:14:10.447399+01	2018-03-18 23:14:10.44744+01	Ikot Ekpene	ikot ekpene	3
50	2018-03-18 23:14:10.458407+01	2018-03-18 23:14:10.458446+01	Ini	ini	3
51	2018-03-18 23:14:10.469466+01	2018-03-18 23:14:10.469511+01	Itu	itu	3
52	2018-03-18 23:14:10.480417+01	2018-03-18 23:14:10.480462+01	Mbo	mbo	3
53	2018-03-18 23:14:10.491435+01	2018-03-18 23:14:10.491476+01	Mkpat-Enin	mkpat-enin	3
54	2018-03-18 23:14:10.502403+01	2018-03-18 23:14:10.502439+01	Nsit-Atai	nsit-atai	3
55	2018-03-18 23:14:10.513438+01	2018-03-18 23:14:10.513478+01	Nsit-Ibom	nsit-ibom	3
56	2018-03-18 23:14:10.524435+01	2018-03-18 23:14:10.524475+01	Nsit-Ubium	nsit-ubium	3
57	2018-03-18 23:14:10.535114+01	2018-03-18 23:14:10.535146+01	Obot Akara	obot akara	3
58	2018-03-18 23:14:10.546981+01	2018-03-18 23:14:10.547025+01	Okobo	okobo	3
59	2018-03-18 23:14:10.557493+01	2018-03-18 23:14:10.557536+01	Onna	onna	3
60	2018-03-18 23:14:10.568531+01	2018-03-18 23:14:10.568576+01	Oron	oron	3
61	2018-03-18 23:14:10.579462+01	2018-03-18 23:14:10.579501+01	Oruk Anam	oruk anam	3
62	2018-03-18 23:14:10.590441+01	2018-03-18 23:14:10.590478+01	Udung-Uko	udung-uko	3
63	2018-03-18 23:14:10.601477+01	2018-03-18 23:14:10.601516+01	Ukanafun	ukanafun	3
64	2018-03-18 23:14:10.612474+01	2018-03-18 23:14:10.612514+01	Uruan	uruan	3
65	2018-03-18 23:14:10.623486+01	2018-03-18 23:14:10.623527+01	Urue-Offong/Oruko	urue-offong/oruko	3
66	2018-03-18 23:14:10.634622+01	2018-03-18 23:14:10.634662+01	Uyo	uyo	3
67	2018-03-18 23:14:10.656604+01	2018-03-18 23:14:10.656644+01	Anambra East	anambra east	4
68	2018-03-18 23:14:10.667614+01	2018-03-18 23:14:10.667651+01	Anambra West	anambra west	4
69	2018-03-18 23:14:10.678643+01	2018-03-18 23:14:10.678683+01	Anaocha	anaocha	4
70	2018-03-18 23:14:10.689673+01	2018-03-18 23:14:10.689715+01	Awka North	awka north	4
71	2018-03-18 23:14:10.700594+01	2018-03-18 23:14:10.70064+01	Awka South	awka south	4
72	2018-03-18 23:14:10.711693+01	2018-03-18 23:14:10.711738+01	Ayamelum	ayamelum	4
73	2018-03-18 23:14:10.722554+01	2018-03-18 23:14:10.722591+01	Dunukofia	dunukofia	4
74	2018-03-18 23:14:10.733648+01	2018-03-18 23:14:10.733688+01	Ekwusigo	ekwusigo	4
75	2018-03-18 23:14:10.744673+01	2018-03-18 23:14:10.744713+01	Idemili North	idemili north	4
76	2018-03-18 23:14:10.755635+01	2018-03-18 23:14:10.755672+01	Idemili South	idemili south	4
77	2018-03-18 23:14:10.766622+01	2018-03-18 23:14:10.766663+01	Ihiala	ihiala	4
78	2018-03-18 23:14:10.777691+01	2018-03-18 23:14:10.777731+01	Njikoka	njikoka	4
79	2018-03-18 23:14:10.788726+01	2018-03-18 23:14:10.78877+01	Nnewi North	nnewi north	4
80	2018-03-18 23:14:10.799708+01	2018-03-18 23:14:10.799748+01	Nnewi South	nnewi south	4
81	2018-03-18 23:14:10.810579+01	2018-03-18 23:14:10.810612+01	Ogbaru	ogbaru	4
82	2018-03-18 23:14:10.82166+01	2018-03-18 23:14:10.821695+01	Onitsha North	onitsha north	4
83	2018-03-18 23:14:10.832644+01	2018-03-18 23:14:10.832683+01	Onitsha South	onitsha south	4
84	2018-03-18 23:14:10.84365+01	2018-03-18 23:14:10.843685+01	Orumba North	orumba north	4
85	2018-03-18 23:14:10.854648+01	2018-03-18 23:14:10.854688+01	Orumba South	orumba south	4
86	2018-03-18 23:14:10.865684+01	2018-03-18 23:14:10.865725+01	Oyi	oyi	4
87	2018-03-18 23:14:10.887745+01	2018-03-18 23:14:10.887786+01	Bauchi	bauchi	5
88	2018-03-18 23:14:10.898859+01	2018-03-18 23:14:10.898895+01	Bogoro	bogoro	5
89	2018-03-18 23:14:10.909793+01	2018-03-18 23:14:10.909834+01	Damban	damban	5
90	2018-03-18 23:14:10.92087+01	2018-03-18 23:14:10.920912+01	Darazo	darazo	5
91	2018-03-18 23:14:10.931885+01	2018-03-18 23:14:10.931931+01	Dass	dass	5
92	2018-03-18 23:14:10.942729+01	2018-03-18 23:14:10.942754+01	Gamawa	gamawa	5
93	2018-03-18 23:14:10.953809+01	2018-03-18 23:14:10.953843+01	Ganjuwa	ganjuwa	5
94	2018-03-18 23:14:10.96484+01	2018-03-18 23:14:10.964872+01	Giade	giade	5
95	2018-03-18 23:14:10.975935+01	2018-03-18 23:14:10.975968+01	Itas/Gadau	itas/gadau	5
96	2018-03-18 23:14:10.986877+01	2018-03-18 23:14:10.98691+01	Jama'are	jama'are	5
97	2018-03-18 23:14:10.997928+01	2018-03-18 23:14:10.997968+01	Katagum	katagum	5
98	2018-03-18 23:14:11.009172+01	2018-03-18 23:14:11.009217+01	Kirfi	kirfi	5
99	2018-03-18 23:14:11.020104+01	2018-03-18 23:14:11.02015+01	Misau	misau	5
100	2018-03-18 23:14:11.031065+01	2018-03-18 23:14:11.031108+01	Ningi	ningi	5
101	2018-03-18 23:14:11.042026+01	2018-03-18 23:14:11.042063+01	Shira	shira	5
102	2018-03-18 23:14:11.053123+01	2018-03-18 23:14:11.053169+01	Tafawa Balewa	tafawa balewa	5
103	2018-03-18 23:14:11.064084+01	2018-03-18 23:14:11.064129+01	Toro	toro	5
104	2018-03-18 23:14:11.074943+01	2018-03-18 23:14:11.074971+01	Warji	warji	5
105	2018-03-18 23:14:11.085924+01	2018-03-18 23:14:11.085988+01	Zaki	zaki	5
106	2018-03-18 23:14:11.107943+01	2018-03-18 23:14:11.107984+01	Ekeremor	ekeremor	6
107	2018-03-18 23:14:11.11849+01	2018-03-18 23:14:11.118512+01	Kolokuma/Opokuma	kolokuma/opokuma	6
108	2018-03-18 23:14:11.129643+01	2018-03-18 23:14:11.129657+01	Nembe	nembe	6
109	2018-03-18 23:14:11.14077+01	2018-03-18 23:14:11.140822+01	Ogbia	ogbia	6
110	2018-03-18 23:14:11.15156+01	2018-03-18 23:14:11.151583+01	Sagbama	sagbama	6
111	2018-03-18 23:14:11.162656+01	2018-03-18 23:14:11.162684+01	Southern Ijaw	southern ijaw	6
112	2018-03-18 23:14:11.174075+01	2018-03-18 23:14:11.174121+01	Yenagoa	yenagoa	6
113	2018-03-18 23:14:11.196086+01	2018-03-18 23:14:11.196116+01	Apa	apa	7
114	2018-03-18 23:14:11.207175+01	2018-03-18 23:14:11.207213+01	Ado	ado	7
115	2018-03-18 23:14:11.218246+01	2018-03-18 23:14:11.218293+01	Buruku	buruku	7
116	2018-03-18 23:14:11.22933+01	2018-03-18 23:14:11.229376+01	Gboko	gboko	7
117	2018-03-18 23:14:11.240272+01	2018-03-18 23:14:11.240313+01	Guma	guma	7
118	2018-03-18 23:14:11.25131+01	2018-03-18 23:14:11.251356+01	Gwer East	gwer east	7
119	2018-03-18 23:14:11.263489+01	2018-03-18 23:14:11.263524+01	Gwer West	gwer west	7
120	2018-03-18 23:14:11.272906+01	2018-03-18 23:14:11.272938+01	Katsina-Ala	katsina-ala	7
121	2018-03-18 23:14:11.284186+01	2018-03-18 23:14:11.284218+01	Konshisha	konshisha	7
122	2018-03-18 23:14:11.295272+01	2018-03-18 23:14:11.295313+01	Kwande	kwande	7
123	2018-03-18 23:14:11.306331+01	2018-03-18 23:14:11.306376+01	Logo	logo	7
124	2018-03-18 23:14:11.31733+01	2018-03-18 23:14:11.317376+01	Makurdi	makurdi	7
125	2018-03-18 23:14:11.32822+01	2018-03-18 23:14:11.328266+01	Obi	obi	7
126	2018-03-18 23:14:11.339145+01	2018-03-18 23:14:11.339187+01	Ogbadibo	ogbadibo	7
127	2018-03-18 23:14:11.35013+01	2018-03-18 23:14:11.350172+01	Ohimini	ohimini	7
128	2018-03-18 23:14:11.361262+01	2018-03-18 23:14:11.361308+01	Oju	oju	7
129	2018-03-18 23:14:11.372169+01	2018-03-18 23:14:11.372211+01	Okpokwu	okpokwu	7
130	2018-03-18 23:14:11.383241+01	2018-03-18 23:14:11.383287+01	Oturkpo	oturkpo	7
131	2018-03-18 23:14:11.394131+01	2018-03-18 23:14:11.394168+01	Tarka	tarka	7
132	2018-03-18 23:14:11.405179+01	2018-03-18 23:14:11.405219+01	Ukum	ukum	7
133	2018-03-18 23:14:11.416144+01	2018-03-18 23:14:11.416186+01	Ushongo	ushongo	7
134	2018-03-18 23:14:11.427181+01	2018-03-18 23:14:11.427225+01	Vandeikya	vandeikya	7
135	2018-03-18 23:14:11.449336+01	2018-03-18 23:14:11.449378+01	Askira/Uba	askira/uba	8
136	2018-03-18 23:14:11.460335+01	2018-03-18 23:14:11.46038+01	Bama	bama	8
137	2018-03-18 23:14:11.471294+01	2018-03-18 23:14:11.471339+01	Bayo	bayo	8
138	2018-03-18 23:14:11.482268+01	2018-03-18 23:14:11.482302+01	Biu	biu	8
139	2018-03-18 23:14:11.493369+01	2018-03-18 23:14:11.49341+01	Chibok	chibok	8
140	2018-03-18 23:14:11.504259+01	2018-03-18 23:14:11.504289+01	Damboa	damboa	8
141	2018-03-18 23:14:11.515336+01	2018-03-18 23:14:11.515386+01	Dikwa	dikwa	8
142	2018-03-18 23:14:11.526289+01	2018-03-18 23:14:11.526328+01	Gubio	gubio	8
143	2018-03-18 23:14:11.537536+01	2018-03-18 23:14:11.537575+01	Guzamala	guzamala	8
144	2018-03-18 23:14:11.548427+01	2018-03-18 23:14:11.548473+01	Gwoza	gwoza	8
145	2018-03-18 23:14:11.559374+01	2018-03-18 23:14:11.559415+01	Hawul	hawul	8
146	2018-03-18 23:14:11.57115+01	2018-03-18 23:14:11.571196+01	Jere	jere	8
147	2018-03-18 23:14:11.581422+01	2018-03-18 23:14:11.581467+01	Kaga	kaga	8
148	2018-03-18 23:14:11.592362+01	2018-03-18 23:14:11.592403+01	Kala/Balge	kala/balge	8
149	2018-03-18 23:14:11.603405+01	2018-03-18 23:14:11.60345+01	Konduga	konduga	8
150	2018-03-18 23:14:11.614331+01	2018-03-18 23:14:11.614374+01	Kukawa	kukawa	8
151	2018-03-18 23:14:11.625417+01	2018-03-18 23:14:11.625462+01	Kwaya Kusar	kwaya kusar	8
152	2018-03-18 23:14:11.636652+01	2018-03-18 23:14:11.636698+01	Mafa	mafa	8
153	2018-03-18 23:14:11.647613+01	2018-03-18 23:14:11.64766+01	Magumeri	magumeri	8
154	2018-03-18 23:14:11.658563+01	2018-03-18 23:14:11.658603+01	Maiduguri	maiduguri	8
155	2018-03-18 23:14:11.66964+01	2018-03-18 23:14:11.669685+01	Marte	marte	8
156	2018-03-18 23:14:11.680617+01	2018-03-18 23:14:11.680663+01	Mobbar	mobbar	8
157	2018-03-18 23:14:11.691571+01	2018-03-18 23:14:11.691608+01	Monguno	monguno	8
158	2018-03-18 23:14:11.70258+01	2018-03-18 23:14:11.702618+01	Ngala	ngala	8
159	2018-03-18 23:14:11.713751+01	2018-03-18 23:14:11.713795+01	Nganzai	nganzai	8
160	2018-03-18 23:14:11.724648+01	2018-03-18 23:14:11.724695+01	Shani	shani	8
161	2018-03-18 23:14:11.746702+01	2018-03-18 23:14:11.746741+01	Akamkpa	akamkpa	9
162	2018-03-18 23:14:11.75777+01	2018-03-18 23:14:11.757815+01	Akpabuyo	akpabuyo	9
163	2018-03-18 23:14:11.768753+01	2018-03-18 23:14:11.768852+01	Bakassi	bakassi	9
164	2018-03-18 23:14:11.779701+01	2018-03-18 23:14:11.779741+01	Bekwarra	bekwarra	9
165	2018-03-18 23:14:11.790758+01	2018-03-18 23:14:11.790803+01	Biase	biase	9
166	2018-03-18 23:14:11.801637+01	2018-03-18 23:14:11.801674+01	Boki	boki	9
167	2018-03-18 23:14:11.812736+01	2018-03-18 23:14:11.812777+01	Calabar Municipal	calabar municipal	9
168	2018-03-18 23:14:11.823784+01	2018-03-18 23:14:11.823825+01	Calabar South	calabar south	9
169	2018-03-18 23:14:11.834759+01	2018-03-18 23:14:11.834798+01	Etung	etung	9
170	2018-03-18 23:14:11.845825+01	2018-03-18 23:14:11.845871+01	Ikom	ikom	9
171	2018-03-18 23:14:11.856761+01	2018-03-18 23:14:11.85683+01	Obanliku	obanliku	9
172	2018-03-18 23:14:11.867777+01	2018-03-18 23:14:11.867818+01	Obubra	obubra	9
173	2018-03-18 23:14:11.878825+01	2018-03-18 23:14:11.878866+01	Obudu	obudu	9
174	2018-03-18 23:14:11.889825+01	2018-03-18 23:14:11.889865+01	Odukpani	odukpani	9
175	2018-03-18 23:14:11.900774+01	2018-03-18 23:14:11.900853+01	Ogoja	ogoja	9
176	2018-03-18 23:14:11.911788+01	2018-03-18 23:14:11.911829+01	Yakuur	yakuur	9
177	2018-03-18 23:14:11.922685+01	2018-03-18 23:14:11.922726+01	Yala	yala	9
178	2018-03-18 23:14:11.944779+01	2018-03-18 23:14:11.944856+01	Aniocha South	aniocha south	10
179	2018-03-18 23:14:11.955852+01	2018-03-18 23:14:11.955899+01	Bomadi	bomadi	10
180	2018-03-18 23:14:11.966843+01	2018-03-18 23:14:11.966886+01	Burutu	burutu	10
181	2018-03-18 23:14:11.977762+01	2018-03-18 23:14:11.977799+01	Ethiope East	ethiope east	10
182	2018-03-18 23:14:11.988964+01	2018-03-18 23:14:11.98901+01	Ethiope West	ethiope west	10
183	2018-03-18 23:14:11.999827+01	2018-03-18 23:14:11.999864+01	Ika North East	ika north east	10
184	2018-03-18 23:14:12.01097+01	2018-03-18 23:14:12.011017+01	Ika South	ika south	10
185	2018-03-18 23:14:12.021912+01	2018-03-18 23:14:12.021951+01	Isoko North	isoko north	10
186	2018-03-18 23:14:12.033012+01	2018-03-18 23:14:12.033058+01	Isoko South	isoko south	10
187	2018-03-18 23:14:12.043913+01	2018-03-18 23:14:12.043951+01	Ndokwa East	ndokwa east	10
188	2018-03-18 23:14:12.054993+01	2018-03-18 23:14:12.055039+01	Ndokwa West	ndokwa west	10
189	2018-03-18 23:14:12.065893+01	2018-03-18 23:14:12.06593+01	Okpe	okpe	10
190	2018-03-18 23:14:12.077079+01	2018-03-18 23:14:12.077126+01	Oshimili North	oshimili north	10
191	2018-03-18 23:14:12.087967+01	2018-03-18 23:14:12.088022+01	Oshimili South	oshimili south	10
192	2018-03-18 23:14:12.098506+01	2018-03-18 23:14:12.098534+01	Patani	patani	10
193	2018-03-18 23:14:12.109743+01	2018-03-18 23:14:12.109778+01	Sapele	sapele	10
194	2018-03-18 23:14:12.12095+01	2018-03-18 23:14:12.120996+01	Udu	udu	10
195	2018-03-18 23:14:12.131678+01	2018-03-18 23:14:12.131728+01	Ughelli North	ughelli north	10
196	2018-03-18 23:14:12.142386+01	2018-03-18 23:14:12.142409+01	Ughelli South	ughelli south	10
197	2018-03-18 23:14:12.153476+01	2018-03-18 23:14:12.153495+01	Ukwuani	ukwuani	10
198	2018-03-18 23:14:12.16434+01	2018-03-18 23:14:12.164357+01	Uvwie	uvwie	10
199	2018-03-18 23:14:12.175789+01	2018-03-18 23:14:12.175821+01	Warri North	warri north	10
200	2018-03-18 23:14:12.186935+01	2018-03-18 23:14:12.186977+01	Warri South	warri south	10
201	2018-03-18 23:14:12.197897+01	2018-03-18 23:14:12.197938+01	Warri South West	warri south west	10
202	2018-03-18 23:14:12.219988+01	2018-03-18 23:14:12.220029+01	Afikpo North	afikpo north	11
203	2018-03-18 23:14:12.231092+01	2018-03-18 23:14:12.231136+01	Afikpo South	afikpo south	11
204	2018-03-18 23:14:12.241836+01	2018-03-18 23:14:12.241865+01	Ebonyi	ebonyi	11
205	2018-03-18 23:14:12.253221+01	2018-03-18 23:14:12.253266+01	Ezza North	ezza north	11
206	2018-03-18 23:14:12.264207+01	2018-03-18 23:14:12.264252+01	Ezza South	ezza south	11
207	2018-03-18 23:14:12.275109+01	2018-03-18 23:14:12.275154+01	Ikwo	ikwo	11
208	2018-03-18 23:14:12.287858+01	2018-03-18 23:14:12.287892+01	Ishielu	ishielu	11
209	2018-03-18 23:14:12.297094+01	2018-03-18 23:14:12.297125+01	Ivo	ivo	11
210	2018-03-18 23:14:12.308142+01	2018-03-18 23:14:12.308183+01	Izzi	izzi	11
211	2018-03-18 23:14:12.319185+01	2018-03-18 23:14:12.319225+01	Ohaozara	ohaozara	11
212	2018-03-18 23:14:12.330175+01	2018-03-18 23:14:12.330218+01	Ohaukwu	ohaukwu	11
213	2018-03-18 23:14:12.341147+01	2018-03-18 23:14:12.341181+01	Onicha	onicha	11
214	2018-03-18 23:14:12.363156+01	2018-03-18 23:14:12.3632+01	Egor	egor	12
215	2018-03-18 23:14:12.37416+01	2018-03-18 23:14:12.374197+01	Esan Central	esan central	12
216	2018-03-18 23:14:12.385279+01	2018-03-18 23:14:12.385324+01	Esan North-East	esan north-east	12
217	2018-03-18 23:14:12.396252+01	2018-03-18 23:14:12.396292+01	Esan South-East	esan south-east	12
218	2018-03-18 23:14:12.407246+01	2018-03-18 23:14:12.407291+01	Esan West	esan west	12
219	2018-03-18 23:14:12.418209+01	2018-03-18 23:14:12.418247+01	Etsako Central	etsako central	12
220	2018-03-18 23:14:12.429293+01	2018-03-18 23:14:12.429338+01	Etsako East	etsako east	12
221	2018-03-18 23:14:12.440268+01	2018-03-18 23:14:12.440314+01	Etsako West	etsako west	12
222	2018-03-18 23:14:12.451268+01	2018-03-18 23:14:12.451314+01	Igueben	igueben	12
223	2018-03-18 23:14:12.462232+01	2018-03-18 23:14:12.462269+01	Ikpoba Okha	ikpoba okha	12
224	2018-03-18 23:14:12.473316+01	2018-03-18 23:14:12.473362+01	Orhionmwon	orhionmwon	12
225	2018-03-18 23:14:12.484285+01	2018-03-18 23:14:12.48433+01	Oredo	oredo	12
226	2018-03-18 23:14:12.495216+01	2018-03-18 23:14:12.495261+01	Ovia North-East	ovia north-east	12
227	2018-03-18 23:14:12.50607+01	2018-03-18 23:14:12.506093+01	Ovia South-West	ovia south-west	12
228	2018-03-18 23:14:12.517174+01	2018-03-18 23:14:12.517206+01	Owan East	owan east	12
229	2018-03-18 23:14:12.528334+01	2018-03-18 23:14:12.528375+01	Owan West	owan west	12
230	2018-03-18 23:14:12.539229+01	2018-03-18 23:14:12.539261+01	Uhunmwonde	uhunmwonde	12
231	2018-03-18 23:14:12.561398+01	2018-03-18 23:14:12.561441+01	Efon	efon	13
232	2018-03-18 23:14:12.572426+01	2018-03-18 23:14:12.57247+01	Ekiti East	ekiti east	13
233	2018-03-18 23:14:12.583446+01	2018-03-18 23:14:12.583491+01	Ekiti South-West	ekiti south-west	13
234	2018-03-18 23:14:12.595172+01	2018-03-18 23:14:12.595214+01	Ekiti West	ekiti west	13
235	2018-03-18 23:14:12.605464+01	2018-03-18 23:14:12.605506+01	Emure	emure	13
236	2018-03-18 23:14:12.616424+01	2018-03-18 23:14:12.616467+01	Gbonyin	gbonyin	13
237	2018-03-18 23:14:12.627471+01	2018-03-18 23:14:12.627516+01	Ido Osi	ido osi	13
238	2018-03-18 23:14:12.638376+01	2018-03-18 23:14:12.63841+01	Ijero	ijero	13
239	2018-03-18 23:14:12.649279+01	2018-03-18 23:14:12.649321+01	Ikere	ikere	13
240	2018-03-18 23:14:12.660477+01	2018-03-18 23:14:12.660524+01	Ikole	ikole	13
241	2018-03-18 23:14:12.671451+01	2018-03-18 23:14:12.671496+01	Ilejemeje	ilejemeje	13
242	2018-03-18 23:14:12.682296+01	2018-03-18 23:14:12.682329+01	Irepodun/Ifelodun	irepodun/ifelodun	13
243	2018-03-18 23:14:12.693445+01	2018-03-18 23:14:12.693485+01	Ise/Orun	ise/orun	13
244	2018-03-18 23:14:12.704238+01	2018-03-18 23:14:12.704282+01	Moba	moba	13
245	2018-03-18 23:14:12.715218+01	2018-03-18 23:14:12.71526+01	Oye	oye	13
246	2018-03-18 23:14:12.737329+01	2018-03-18 23:14:12.737371+01	Awgu	awgu	14
247	2018-03-18 23:14:12.748275+01	2018-03-18 23:14:12.748316+01	Enugu East	enugu east	14
248	2018-03-18 23:14:12.759276+01	2018-03-18 23:14:12.759317+01	Enugu North	enugu north	14
249	2018-03-18 23:14:12.77029+01	2018-03-18 23:14:12.770332+01	Enugu South	enugu south	14
250	2018-03-18 23:14:12.78134+01	2018-03-18 23:14:12.781379+01	Ezeagu	ezeagu	14
251	2018-03-18 23:14:12.792345+01	2018-03-18 23:14:12.792385+01	Igbo Etiti	igbo etiti	14
252	2018-03-18 23:14:12.803343+01	2018-03-18 23:14:12.803381+01	Igbo Eze North	igbo eze north	14
253	2018-03-18 23:14:12.814372+01	2018-03-18 23:14:12.814413+01	Igbo Eze South	igbo eze south	14
254	2018-03-18 23:14:12.825393+01	2018-03-18 23:14:12.825434+01	Isi Uzo	isi uzo	14
255	2018-03-18 23:14:12.836407+01	2018-03-18 23:14:12.836449+01	Nkanu East	nkanu east	14
256	2018-03-18 23:14:12.847407+01	2018-03-18 23:14:12.847447+01	Nkanu West	nkanu west	14
257	2018-03-18 23:14:12.85841+01	2018-03-18 23:14:12.858452+01	Nsukka	nsukka	14
258	2018-03-18 23:14:12.869424+01	2018-03-18 23:14:12.869466+01	Oji River	oji river	14
259	2018-03-18 23:14:12.880405+01	2018-03-18 23:14:12.880446+01	Udenu	udenu	14
260	2018-03-18 23:14:12.891374+01	2018-03-18 23:14:12.891414+01	Udi	udi	14
261	2018-03-18 23:14:12.902397+01	2018-03-18 23:14:12.902438+01	Uzo Uwani	uzo uwani	14
262	2018-03-18 23:14:12.924413+01	2018-03-18 23:14:12.924453+01	Bwari	bwari	15
263	2018-03-18 23:14:12.935446+01	2018-03-18 23:14:12.935489+01	Gwagwalada	gwagwalada	15
264	2018-03-18 23:14:12.946449+01	2018-03-18 23:14:12.946489+01	Kuje	kuje	15
265	2018-03-18 23:14:12.957481+01	2018-03-18 23:14:12.957521+01	Kwali	kwali	15
266	2018-03-18 23:14:12.968395+01	2018-03-18 23:14:12.968436+01	Municipal Area Council	municipal area council	15
267	2018-03-18 23:14:12.990473+01	2018-03-18 23:14:12.990514+01	Balanga	balanga	16
268	2018-03-18 23:14:13.001459+01	2018-03-18 23:14:13.001499+01	Billiri	billiri	16
269	2018-03-18 23:14:13.01252+01	2018-03-18 23:14:13.01256+01	Dukku	dukku	16
270	2018-03-18 23:14:13.023486+01	2018-03-18 23:14:13.02354+01	Funakaye	funakaye	16
271	2018-03-18 23:14:13.034495+01	2018-03-18 23:14:13.034537+01	Gombe	gombe	16
272	2018-03-18 23:14:13.063715+01	2018-03-18 23:14:13.063748+01	Kaltungo	kaltungo	16
273	2018-03-18 23:14:13.089666+01	2018-03-18 23:14:13.089705+01	Kwami	kwami	16
274	2018-03-18 23:14:13.100669+01	2018-03-18 23:14:13.100709+01	Nafada	nafada	16
275	2018-03-18 23:14:13.111694+01	2018-03-18 23:14:13.111738+01	Shongom	shongom	16
276	2018-03-18 23:14:13.122564+01	2018-03-18 23:14:13.122601+01	Yamaltu/Deba	yamaltu/deba	16
277	2018-03-18 23:14:13.144667+01	2018-03-18 23:14:13.144708+01	Ahiazu Mbaise	ahiazu mbaise	17
278	2018-03-18 23:14:13.155322+01	2018-03-18 23:14:13.155341+01	Ehime Mbano	ehime mbano	17
279	2018-03-18 23:14:13.166312+01	2018-03-18 23:14:13.166335+01	Ezinihitte	ezinihitte	17
280	2018-03-18 23:14:13.177369+01	2018-03-18 23:14:13.177387+01	Ideato North	ideato north	17
281	2018-03-18 23:14:13.188283+01	2018-03-18 23:14:13.188301+01	Ideato South	ideato south	17
282	2018-03-18 23:14:13.199334+01	2018-03-18 23:14:13.19935+01	Ihitte/Uboma	ihitte/uboma	17
283	2018-03-18 23:14:13.21074+01	2018-03-18 23:14:13.210772+01	Ikeduru	ikeduru	17
284	2018-03-18 23:14:13.221488+01	2018-03-18 23:14:13.221523+01	Isiala Mbano	isiala mbano	17
285	2018-03-18 23:14:13.232712+01	2018-03-18 23:14:13.232757+01	Isu	isu	17
286	2018-03-18 23:14:13.243743+01	2018-03-18 23:14:13.243788+01	Mbaitoli	mbaitoli	17
287	2018-03-18 23:14:13.254731+01	2018-03-18 23:14:13.254776+01	Ngor Okpala	ngor okpala	17
288	2018-03-18 23:14:13.265882+01	2018-03-18 23:14:13.265927+01	Njaba	njaba	17
289	2018-03-18 23:14:13.27695+01	2018-03-18 23:14:13.276991+01	Nkwerre	nkwerre	17
290	2018-03-18 23:14:13.287968+01	2018-03-18 23:14:13.288015+01	Nwangele	nwangele	17
291	2018-03-18 23:14:13.298818+01	2018-03-18 23:14:13.298862+01	Obowo	obowo	17
292	2018-03-18 23:14:13.31171+01	2018-03-18 23:14:13.311751+01	Oguta	oguta	17
293	2018-03-18 23:14:13.320547+01	2018-03-18 23:14:13.320591+01	Ohaji/Egbema	ohaji/egbema	17
294	2018-03-18 23:14:13.331734+01	2018-03-18 23:14:13.33176+01	Okigwe	okigwe	17
295	2018-03-18 23:14:13.342583+01	2018-03-18 23:14:13.342609+01	Orlu	orlu	17
296	2018-03-18 23:14:13.353959+01	2018-03-18 23:14:13.354018+01	Orsu	orsu	17
297	2018-03-18 23:14:13.364541+01	2018-03-18 23:14:13.36457+01	Oru East	oru east	17
298	2018-03-18 23:14:13.375898+01	2018-03-18 23:14:13.375933+01	Oru West	oru west	17
299	2018-03-18 23:14:13.386861+01	2018-03-18 23:14:13.386901+01	Owerri Municipal	owerri municipal	17
300	2018-03-18 23:14:13.39806+01	2018-03-18 23:14:13.398105+01	Owerri North	owerri north	17
301	2018-03-18 23:14:13.409027+01	2018-03-18 23:14:13.409061+01	Owerri West	owerri west	17
302	2018-03-18 23:14:13.419949+01	2018-03-18 23:14:13.419989+01	Unuimo	unuimo	17
303	2018-03-18 23:14:13.441839+01	2018-03-18 23:14:13.441874+01	Babura	babura	18
304	2018-03-18 23:14:13.45297+01	2018-03-18 23:14:13.453012+01	Biriniwa	biriniwa	18
305	2018-03-18 23:14:13.463966+01	2018-03-18 23:14:13.464011+01	Birnin Kudu	birnin kudu	18
306	2018-03-18 23:14:13.474922+01	2018-03-18 23:14:13.474966+01	Buji	buji	18
307	2018-03-18 23:14:13.48576+01	2018-03-18 23:14:13.485797+01	Dutse	dutse	18
308	2018-03-18 23:14:13.496982+01	2018-03-18 23:14:13.497022+01	Gagarawa	gagarawa	18
309	2018-03-18 23:14:13.507993+01	2018-03-18 23:14:13.508038+01	Garki	garki	18
310	2018-03-18 23:14:13.518913+01	2018-03-18 23:14:13.518957+01	Gumel	gumel	18
311	2018-03-18 23:14:13.529806+01	2018-03-18 23:14:13.529829+01	Guri	guri	18
312	2018-03-18 23:14:13.540893+01	2018-03-18 23:14:13.540923+01	Gwaram	gwaram	18
313	2018-03-18 23:14:13.551942+01	2018-03-18 23:14:13.551978+01	Gwiwa	gwiwa	18
314	2018-03-18 23:14:13.562937+01	2018-03-18 23:14:13.562977+01	Hadejia	hadejia	18
315	2018-03-18 23:14:13.573986+01	2018-03-18 23:14:13.574031+01	Jahun	jahun	18
316	2018-03-18 23:14:13.585059+01	2018-03-18 23:14:13.585105+01	Kafin Hausa	kafin hausa	18
317	2018-03-18 23:14:13.595996+01	2018-03-18 23:14:13.59604+01	Kazaure	kazaure	18
318	2018-03-18 23:14:13.606997+01	2018-03-18 23:14:13.607042+01	Kiri Kasama	kiri kasama	18
429	2018-03-18 23:14:14.873283+01	2018-03-18 23:14:14.873325+01	Augie	augie	22
319	2018-03-18 23:14:13.61913+01	2018-03-18 23:14:13.619171+01	Kiyawa	kiyawa	18
320	2018-03-18 23:14:13.629017+01	2018-03-18 23:14:13.629047+01	Kaugama	kaugama	18
321	2018-03-18 23:14:13.639967+01	2018-03-18 23:14:13.639997+01	Maigatari	maigatari	18
322	2018-03-18 23:14:13.651031+01	2018-03-18 23:14:13.651067+01	Malam Madori	malam madori	18
323	2018-03-18 23:14:13.662068+01	2018-03-18 23:14:13.662109+01	Miga	miga	18
324	2018-03-18 23:14:13.673085+01	2018-03-18 23:14:13.673101+01	Ringim	ringim	18
325	2018-03-18 23:14:13.684141+01	2018-03-18 23:14:13.684186+01	Roni	roni	18
326	2018-03-18 23:14:13.695133+01	2018-03-18 23:14:13.695178+01	Sule Tankarkar	sule tankarkar	18
327	2018-03-18 23:14:13.705911+01	2018-03-18 23:14:13.705933+01	Taura	taura	18
328	2018-03-18 23:14:13.71707+01	2018-03-18 23:14:13.7171+01	Yankwashi	yankwashi	18
329	2018-03-18 23:14:13.739154+01	2018-03-18 23:14:13.739184+01	Chikun	chikun	19
330	2018-03-18 23:14:13.750252+01	2018-03-18 23:14:13.75029+01	Giwa	giwa	19
331	2018-03-18 23:14:13.76136+01	2018-03-18 23:14:13.761405+01	Igabi	igabi	19
332	2018-03-18 23:14:13.77232+01	2018-03-18 23:14:13.772365+01	Ikara	ikara	19
333	2018-03-18 23:14:13.783275+01	2018-03-18 23:14:13.783312+01	Jaba	jaba	19
334	2018-03-18 23:14:13.794442+01	2018-03-18 23:14:13.794485+01	Jema'a	jema'a	19
335	2018-03-18 23:14:13.805334+01	2018-03-18 23:14:13.805372+01	Kachia	kachia	19
336	2018-03-18 23:14:13.816248+01	2018-03-18 23:14:13.816292+01	Kaduna North	kaduna north	19
337	2018-03-18 23:14:13.827213+01	2018-03-18 23:14:13.827258+01	Kaduna South	kaduna south	19
338	2018-03-18 23:14:13.838117+01	2018-03-18 23:14:13.83815+01	Kagarko	kagarko	19
339	2018-03-18 23:14:13.849245+01	2018-03-18 23:14:13.849286+01	Kajuru	kajuru	19
340	2018-03-18 23:14:13.86026+01	2018-03-18 23:14:13.860305+01	Kaura	kaura	19
341	2018-03-18 23:14:13.871409+01	2018-03-18 23:14:13.871454+01	Kauru	kauru	19
342	2018-03-18 23:14:13.882358+01	2018-03-18 23:14:13.882399+01	Kubau	kubau	19
343	2018-03-18 23:14:13.893541+01	2018-03-18 23:14:13.893585+01	Kudan	kudan	19
344	2018-03-18 23:14:13.904534+01	2018-03-18 23:14:13.904599+01	Lere	lere	19
345	2018-03-18 23:14:13.915398+01	2018-03-18 23:14:13.91543+01	Makarfi	makarfi	19
346	2018-03-18 23:14:13.926468+01	2018-03-18 23:14:13.926509+01	Sabon Gari	sabon gari	19
347	2018-03-18 23:14:13.937632+01	2018-03-18 23:14:13.937671+01	Sanga	sanga	19
348	2018-03-18 23:14:13.948334+01	2018-03-18 23:14:13.948366+01	Soba	soba	19
349	2018-03-18 23:14:13.959366+01	2018-03-18 23:14:13.959403+01	Zangon Kataf	zangon kataf	19
350	2018-03-18 23:14:13.970392+01	2018-03-18 23:14:13.970432+01	Zaria	zaria	19
351	2018-03-18 23:14:13.992476+01	2018-03-18 23:14:13.992521+01	Albasu	albasu	20
352	2018-03-18 23:14:14.003422+01	2018-03-18 23:14:14.003468+01	Bagwai	bagwai	20
353	2018-03-18 23:14:14.0143+01	2018-03-18 23:14:14.014334+01	Bebeji	bebeji	20
354	2018-03-18 23:14:14.025375+01	2018-03-18 23:14:14.025411+01	Bichi	bichi	20
355	2018-03-18 23:14:14.036331+01	2018-03-18 23:14:14.036362+01	Bunkure	bunkure	20
356	2018-03-18 23:14:14.047336+01	2018-03-18 23:14:14.047367+01	Dala	dala	20
357	2018-03-18 23:14:14.058386+01	2018-03-18 23:14:14.058422+01	Dambatta	dambatta	20
358	2018-03-18 23:14:14.069421+01	2018-03-18 23:14:14.069462+01	Dawakin Kudu	dawakin kudu	20
359	2018-03-18 23:14:14.080458+01	2018-03-18 23:14:14.080504+01	Dawakin Tofa	dawakin tofa	20
360	2018-03-18 23:14:14.091395+01	2018-03-18 23:14:14.091436+01	Doguwa	doguwa	20
361	2018-03-18 23:14:14.102391+01	2018-03-18 23:14:14.102431+01	Fagge	fagge	20
362	2018-03-18 23:14:14.113439+01	2018-03-18 23:14:14.113483+01	Gabasawa	gabasawa	20
363	2018-03-18 23:14:14.124413+01	2018-03-18 23:14:14.124454+01	Garko	garko	20
364	2018-03-18 23:14:14.135378+01	2018-03-18 23:14:14.135419+01	Garun Mallam	garun mallam	20
365	2018-03-18 23:14:14.146332+01	2018-03-18 23:14:14.146368+01	Gaya	gaya	20
366	2018-03-18 23:14:14.157737+01	2018-03-18 23:14:14.157778+01	Gezawa	gezawa	20
367	2018-03-18 23:14:14.168773+01	2018-03-18 23:14:14.168865+01	Gwale	gwale	20
368	2018-03-18 23:14:14.179235+01	2018-03-18 23:14:14.17926+01	Gwarzo	gwarzo	20
369	2018-03-18 23:14:14.190153+01	2018-03-18 23:14:14.190175+01	Kabo	kabo	20
370	2018-03-18 23:14:14.201244+01	2018-03-18 23:14:14.201262+01	Kano Municipal	kano municipal	20
371	2018-03-18 23:14:14.212388+01	2018-03-18 23:14:14.212407+01	Karaye	karaye	20
372	2018-03-18 23:14:14.22342+01	2018-03-18 23:14:14.223447+01	Kibiya	kibiya	20
373	2018-03-18 23:14:14.234481+01	2018-03-18 23:14:14.234514+01	Kiru	kiru	20
374	2018-03-18 23:14:14.245122+01	2018-03-18 23:14:14.245149+01	Kumbotso	kumbotso	20
375	2018-03-18 23:14:14.256827+01	2018-03-18 23:14:14.25687+01	Kunchi	kunchi	20
376	2018-03-18 23:14:14.267685+01	2018-03-18 23:14:14.267724+01	Kura	kura	20
377	2018-03-18 23:14:14.27859+01	2018-03-18 23:14:14.278616+01	Madobi	madobi	20
378	2018-03-18 23:14:14.28975+01	2018-03-18 23:14:14.289786+01	Makoda	makoda	20
379	2018-03-18 23:14:14.300869+01	2018-03-18 23:14:14.300915+01	Minjibir	minjibir	20
380	2018-03-18 23:14:14.311775+01	2018-03-18 23:14:14.311815+01	Nasarawa	nasarawa	20
381	2018-03-18 23:14:14.32265+01	2018-03-18 23:14:14.322695+01	Rano	rano	20
382	2018-03-18 23:14:14.335506+01	2018-03-18 23:14:14.335613+01	Rimin Gado	rimin gado	20
383	2018-03-18 23:14:14.344717+01	2018-03-18 23:14:14.344749+01	Rogo	rogo	20
384	2018-03-18 23:14:14.35575+01	2018-03-18 23:14:14.355791+01	Shanono	shanono	20
385	2018-03-18 23:14:14.366807+01	2018-03-18 23:14:14.366848+01	Sumaila	sumaila	20
386	2018-03-18 23:14:14.377702+01	2018-03-18 23:14:14.377733+01	Takai	takai	20
387	2018-03-18 23:14:14.388885+01	2018-03-18 23:14:14.388932+01	Tarauni	tarauni	20
388	2018-03-18 23:14:14.399673+01	2018-03-18 23:14:14.399718+01	Tofa	tofa	20
389	2018-03-18 23:14:14.410577+01	2018-03-18 23:14:14.410608+01	Tsanyawa	tsanyawa	20
390	2018-03-18 23:14:14.421712+01	2018-03-18 23:14:14.421743+01	Tudun Wada	tudun wada	20
391	2018-03-18 23:14:14.43275+01	2018-03-18 23:14:14.432838+01	Ungogo	ungogo	20
392	2018-03-18 23:14:14.443796+01	2018-03-18 23:14:14.44384+01	Warawa	warawa	20
393	2018-03-18 23:14:14.454621+01	2018-03-18 23:14:14.454646+01	Wudil	wudil	20
394	2018-03-18 23:14:14.476826+01	2018-03-18 23:14:14.476871+01	Batagarawa	batagarawa	21
395	2018-03-18 23:14:14.487786+01	2018-03-18 23:14:14.487826+01	Batsari	batsari	21
396	2018-03-18 23:14:14.498729+01	2018-03-18 23:14:14.498763+01	Baure	baure	21
397	2018-03-18 23:14:14.509662+01	2018-03-18 23:14:14.5097+01	Bindawa	bindawa	21
398	2018-03-18 23:14:14.520485+01	2018-03-18 23:14:14.52051+01	Charanchi	charanchi	21
399	2018-03-18 23:14:14.531819+01	2018-03-18 23:14:14.53186+01	Dandume	dandume	21
400	2018-03-18 23:14:14.542756+01	2018-03-18 23:14:14.542796+01	Danja	danja	21
401	2018-03-18 23:14:14.553823+01	2018-03-18 23:14:14.553863+01	Dan Musa	dan musa	21
402	2018-03-18 23:14:14.564829+01	2018-03-18 23:14:14.564873+01	Daura	daura	21
403	2018-03-18 23:14:14.575853+01	2018-03-18 23:14:14.575899+01	Dutsi	dutsi	21
404	2018-03-18 23:14:14.586888+01	2018-03-18 23:14:14.586933+01	Dutsin Ma	dutsin ma	21
405	2018-03-18 23:14:14.59781+01	2018-03-18 23:14:14.597851+01	Faskari	faskari	21
406	2018-03-18 23:14:14.608904+01	2018-03-18 23:14:14.60895+01	Funtua	funtua	21
407	2018-03-18 23:14:14.619886+01	2018-03-18 23:14:14.619931+01	Ingawa	ingawa	21
408	2018-03-18 23:14:14.630837+01	2018-03-18 23:14:14.630882+01	Jibia	jibia	21
409	2018-03-18 23:14:14.643246+01	2018-03-18 23:14:14.643281+01	Kafur	kafur	21
410	2018-03-18 23:14:14.652993+01	2018-03-18 23:14:14.653028+01	Kaita	kaita	21
411	2018-03-18 23:14:14.663904+01	2018-03-18 23:14:14.663936+01	Kankara	kankara	21
412	2018-03-18 23:14:14.6749+01	2018-03-18 23:14:14.674937+01	Kankia	kankia	21
413	2018-03-18 23:14:14.685949+01	2018-03-18 23:14:14.685988+01	Katsina	katsina	21
414	2018-03-18 23:14:14.696965+01	2018-03-18 23:14:14.697006+01	Kurfi	kurfi	21
415	2018-03-18 23:14:14.70799+01	2018-03-18 23:14:14.70803+01	Kusada	kusada	21
416	2018-03-18 23:14:14.718991+01	2018-03-18 23:14:14.719031+01	Mai'Adua	mai'adua	21
417	2018-03-18 23:14:14.730004+01	2018-03-18 23:14:14.730043+01	Malumfashi	malumfashi	21
418	2018-03-18 23:14:14.741047+01	2018-03-18 23:14:14.741089+01	Mani	mani	21
419	2018-03-18 23:14:14.752008+01	2018-03-18 23:14:14.752048+01	Mashi	mashi	21
420	2018-03-18 23:14:14.763013+01	2018-03-18 23:14:14.763053+01	Matazu	matazu	21
421	2018-03-18 23:14:14.774156+01	2018-03-18 23:14:14.774196+01	Musawa	musawa	21
422	2018-03-18 23:14:14.785268+01	2018-03-18 23:14:14.785308+01	Rimi	rimi	21
423	2018-03-18 23:14:14.796215+01	2018-03-18 23:14:14.796261+01	Sabuwa	sabuwa	21
424	2018-03-18 23:14:14.807254+01	2018-03-18 23:14:14.8073+01	Safana	safana	21
425	2018-03-18 23:14:14.818153+01	2018-03-18 23:14:14.818188+01	Sandamu	sandamu	21
426	2018-03-18 23:14:14.829276+01	2018-03-18 23:14:14.829316+01	Zango	zango	21
427	2018-03-18 23:14:14.851256+01	2018-03-18 23:14:14.851302+01	Arewa Dandi	arewa dandi	22
428	2018-03-18 23:14:14.862173+01	2018-03-18 23:14:14.862207+01	Argungu	argungu	22
430	2018-03-18 23:14:14.884284+01	2018-03-18 23:14:14.88433+01	Bagudo	bagudo	22
431	2018-03-18 23:14:14.895278+01	2018-03-18 23:14:14.895323+01	Birnin Kebbi	birnin kebbi	22
432	2018-03-18 23:14:14.906097+01	2018-03-18 23:14:14.906131+01	Bunza	bunza	22
433	2018-03-18 23:14:14.917372+01	2018-03-18 23:14:14.917413+01	Dandi	dandi	22
434	2018-03-18 23:14:14.928405+01	2018-03-18 23:14:14.928449+01	Fakai	fakai	22
435	2018-03-18 23:14:14.939179+01	2018-03-18 23:14:14.939207+01	Gwandu	gwandu	22
436	2018-03-18 23:14:14.950491+01	2018-03-18 23:14:14.950532+01	Jega	jega	22
437	2018-03-18 23:14:14.961373+01	2018-03-18 23:14:14.961419+01	Kalgo	kalgo	22
438	2018-03-18 23:14:14.972318+01	2018-03-18 23:14:14.972355+01	Koko/Besse	koko/besse	22
439	2018-03-18 23:14:14.983432+01	2018-03-18 23:14:14.983477+01	Maiyama	maiyama	22
440	2018-03-18 23:14:14.994445+01	2018-03-18 23:14:14.994491+01	Ngaski	ngaski	22
441	2018-03-18 23:14:15.005351+01	2018-03-18 23:14:15.005381+01	Sakaba	sakaba	22
442	2018-03-18 23:14:15.016416+01	2018-03-18 23:14:15.016452+01	Shanga	shanga	22
443	2018-03-18 23:14:15.027464+01	2018-03-18 23:14:15.027509+01	Suru	suru	22
444	2018-03-18 23:14:15.03846+01	2018-03-18 23:14:15.038505+01	Wasagu/Danko	wasagu/danko	22
445	2018-03-18 23:14:15.049341+01	2018-03-18 23:14:15.049369+01	Yauri	yauri	22
446	2018-03-18 23:14:15.060399+01	2018-03-18 23:14:15.060437+01	Zuru	zuru	22
447	2018-03-18 23:14:15.082347+01	2018-03-18 23:14:15.082381+01	Ajaokuta	ajaokuta	23
448	2018-03-18 23:14:15.093472+01	2018-03-18 23:14:15.093512+01	Ankpa	ankpa	23
449	2018-03-18 23:14:15.104501+01	2018-03-18 23:14:15.104542+01	Bassa	bassa	23
450	2018-03-18 23:14:15.115396+01	2018-03-18 23:14:15.115439+01	Dekina	dekina	23
451	2018-03-18 23:14:15.126338+01	2018-03-18 23:14:15.12637+01	Ibaji	ibaji	23
452	2018-03-18 23:14:15.137458+01	2018-03-18 23:14:15.137498+01	Idah	idah	23
453	2018-03-18 23:14:15.148496+01	2018-03-18 23:14:15.148541+01	Igalamela Odolu	igalamela odolu	23
454	2018-03-18 23:14:15.159512+01	2018-03-18 23:14:15.159557+01	Ijumu	ijumu	23
455	2018-03-18 23:14:15.170413+01	2018-03-18 23:14:15.170449+01	Kabba/Bunu	kabba/bunu	23
456	2018-03-18 23:14:15.181528+01	2018-03-18 23:14:15.181568+01	Kogi	kogi	23
457	2018-03-18 23:14:15.192494+01	2018-03-18 23:14:15.192537+01	Lokoja	lokoja	23
458	2018-03-18 23:14:15.203576+01	2018-03-18 23:14:15.203643+01	Mopa Muro	mopa muro	23
459	2018-03-18 23:14:15.214033+01	2018-03-18 23:14:15.214054+01	Ofu	ofu	23
460	2018-03-18 23:14:15.224991+01	2018-03-18 23:14:15.225006+01	Ogori/Magongo	ogori/magongo	23
461	2018-03-18 23:14:15.236239+01	2018-03-18 23:14:15.236259+01	Okehi	okehi	23
462	2018-03-18 23:14:15.247035+01	2018-03-18 23:14:15.24705+01	Okene	okene	23
463	2018-03-18 23:14:15.258312+01	2018-03-18 23:14:15.258341+01	Olamaboro	olamaboro	23
464	2018-03-18 23:14:15.269367+01	2018-03-18 23:14:15.269404+01	Omala	omala	23
465	2018-03-18 23:14:15.280535+01	2018-03-18 23:14:15.280575+01	Yagba East	yagba east	23
466	2018-03-18 23:14:15.291499+01	2018-03-18 23:14:15.29154+01	Yagba West	yagba west	23
467	2018-03-18 23:14:15.313545+01	2018-03-18 23:14:15.313586+01	Baruten	baruten	24
468	2018-03-18 23:14:15.324558+01	2018-03-18 23:14:15.324603+01	Edu	edu	24
469	2018-03-18 23:14:15.335575+01	2018-03-18 23:14:15.335619+01	Ekiti	ekiti	24
470	2018-03-18 23:14:15.34646+01	2018-03-18 23:14:15.346494+01	Ifelodun	ifelodun	24
471	2018-03-18 23:14:15.359249+01	2018-03-18 23:14:15.359291+01	Ilorin East	ilorin east	24
472	2018-03-18 23:14:15.368403+01	2018-03-18 23:14:15.368433+01	Ilorin South	ilorin south	24
473	2018-03-18 23:14:15.379459+01	2018-03-18 23:14:15.379489+01	Ilorin West	ilorin west	24
474	2018-03-18 23:14:15.390285+01	2018-03-18 23:14:15.390315+01	Irepodun	irepodun	24
475	2018-03-18 23:14:15.712365+01	2018-03-18 23:14:15.712407+01	Isin	isin	24
476	2018-03-18 23:14:15.753783+01	2018-03-18 23:14:15.753829+01	Kaiama	kaiama	24
477	2018-03-18 23:14:15.764729+01	2018-03-18 23:14:15.764773+01	Moro	moro	24
478	2018-03-18 23:14:15.775668+01	2018-03-18 23:14:15.775709+01	Offa	offa	24
479	2018-03-18 23:14:15.786713+01	2018-03-18 23:14:15.786757+01	Oke Ero	oke ero	24
480	2018-03-18 23:14:15.797726+01	2018-03-18 23:14:15.797769+01	Oyun	oyun	24
481	2018-03-18 23:14:15.808468+01	2018-03-18 23:14:15.808499+01	Pategi	pategi	24
482	2018-03-18 23:14:15.830824+01	2018-03-18 23:14:15.830868+01	Ajeromi-Ifelodun	ajeromi-ifelodun	25
483	2018-03-18 23:14:15.841819+01	2018-03-18 23:14:15.84186+01	Alimosho	alimosho	25
484	2018-03-18 23:14:15.852918+01	2018-03-18 23:14:15.852963+01	Amuwo-Odofin	amuwo-odofin	25
485	2018-03-18 23:14:15.863909+01	2018-03-18 23:14:15.863953+01	Apapa	apapa	25
486	2018-03-18 23:14:15.874835+01	2018-03-18 23:14:15.874881+01	Badagry	badagry	25
487	2018-03-18 23:14:15.885837+01	2018-03-18 23:14:15.885875+01	Epe	epe	25
488	2018-03-18 23:14:15.896937+01	2018-03-18 23:14:15.896978+01	Eti Osa	eti osa	25
489	2018-03-18 23:14:15.907894+01	2018-03-18 23:14:15.907939+01	Ibeju-Lekki	ibeju-lekki	25
490	2018-03-18 23:14:15.91888+01	2018-03-18 23:14:15.918925+01	Ifako-Ijaiye	ifako-ijaiye	25
491	2018-03-18 23:14:15.929727+01	2018-03-18 23:14:15.929759+01	Ikeja	ikeja	25
492	2018-03-18 23:14:15.940878+01	2018-03-18 23:14:15.940916+01	Ikorodu	ikorodu	25
493	2018-03-18 23:14:15.951861+01	2018-03-18 23:14:15.951901+01	Kosofe	kosofe	25
494	2018-03-18 23:14:15.962854+01	2018-03-18 23:14:15.962895+01	Lagos Island	lagos island	25
495	2018-03-18 23:14:15.974248+01	2018-03-18 23:14:15.974288+01	Lagos Mainland	lagos mainland	25
496	2018-03-18 23:14:15.984891+01	2018-03-18 23:14:15.984932+01	Mushin	mushin	25
497	2018-03-18 23:14:15.99589+01	2018-03-18 23:14:15.995929+01	Ojo	ojo	25
498	2018-03-18 23:14:16.00683+01	2018-03-18 23:14:16.006871+01	Oshodi-Isolo	oshodi-isolo	25
499	2018-03-18 23:14:16.017853+01	2018-03-18 23:14:16.017893+01	Shomolu	shomolu	25
500	2018-03-18 23:14:16.028917+01	2018-03-18 23:14:16.028957+01	Surulere	surulere	25
501	2018-03-18 23:14:16.05086+01	2018-03-18 23:14:16.050902+01	Awe	awe	26
502	2018-03-18 23:14:16.061863+01	2018-03-18 23:14:16.061904+01	Doma	doma	26
503	2018-03-18 23:14:16.072993+01	2018-03-18 23:14:16.073034+01	Karu	karu	26
504	2018-03-18 23:14:16.083958+01	2018-03-18 23:14:16.083999+01	Keana	keana	26
505	2018-03-18 23:14:16.094963+01	2018-03-18 23:14:16.094987+01	Keffi	keffi	26
506	2018-03-18 23:14:16.106057+01	2018-03-18 23:14:16.106087+01	Kokona	kokona	26
507	2018-03-18 23:14:16.11718+01	2018-03-18 23:14:16.117217+01	Lafia	lafia	26
508	2018-03-18 23:14:16.128164+01	2018-03-18 23:14:16.12821+01	Nasarawa	nasarawa	26
509	2018-03-18 23:14:16.139192+01	2018-03-18 23:14:16.139237+01	Nasarawa Egon	nasarawa egon	26
510	2018-03-18 23:14:16.15019+01	2018-03-18 23:14:16.150236+01	Obi	obi	26
511	2018-03-18 23:14:16.161247+01	2018-03-18 23:14:16.161293+01	Toto	toto	26
512	2018-03-18 23:14:16.172239+01	2018-03-18 23:14:16.172284+01	Wamba	wamba	26
513	2018-03-18 23:14:16.194122+01	2018-03-18 23:14:16.194157+01	Agwara	agwara	27
514	2018-03-18 23:14:16.205248+01	2018-03-18 23:14:16.205289+01	Bida	bida	27
515	2018-03-18 23:14:16.21631+01	2018-03-18 23:14:16.216356+01	Borgu	borgu	27
516	2018-03-18 23:14:16.227221+01	2018-03-18 23:14:16.227267+01	Bosso	bosso	27
517	2018-03-18 23:14:16.237816+01	2018-03-18 23:14:16.237856+01	Chanchaga	chanchaga	27
518	2018-03-18 23:14:16.248599+01	2018-03-18 23:14:16.248614+01	Edati	edati	27
519	2018-03-18 23:14:16.2597+01	2018-03-18 23:14:16.259717+01	Gbako	gbako	27
520	2018-03-18 23:14:16.270645+01	2018-03-18 23:14:16.270664+01	Gurara	gurara	27
521	2018-03-18 23:14:16.282075+01	2018-03-18 23:14:16.282115+01	Katcha	katcha	27
522	2018-03-18 23:14:16.292926+01	2018-03-18 23:14:16.292961+01	Kontagora	kontagora	27
523	2018-03-18 23:14:16.304127+01	2018-03-18 23:14:16.304167+01	Lapai	lapai	27
524	2018-03-18 23:14:16.315227+01	2018-03-18 23:14:16.315264+01	Lavun	lavun	27
525	2018-03-18 23:14:16.326268+01	2018-03-18 23:14:16.326312+01	Magama	magama	27
526	2018-03-18 23:14:16.336744+01	2018-03-18 23:14:16.336772+01	Mariga	mariga	27
527	2018-03-18 23:14:16.348483+01	2018-03-18 23:14:16.348528+01	Mashegu	mashegu	27
528	2018-03-18 23:14:16.359349+01	2018-03-18 23:14:16.359383+01	Mokwa	mokwa	27
529	2018-03-18 23:14:16.370359+01	2018-03-18 23:14:16.370399+01	Moya	moya	27
530	2018-03-18 23:14:16.383225+01	2018-03-18 23:14:16.383266+01	Paikoro	paikoro	27
531	2018-03-18 23:14:16.392274+01	2018-03-18 23:14:16.392298+01	Rafi	rafi	27
532	2018-03-18 23:14:16.403368+01	2018-03-18 23:14:16.403403+01	Rijau	rijau	27
533	2018-03-18 23:14:16.414422+01	2018-03-18 23:14:16.414462+01	Shiroro	shiroro	27
534	2018-03-18 23:14:16.425458+01	2018-03-18 23:14:16.425499+01	Suleja	suleja	27
535	2018-03-18 23:14:16.436526+01	2018-03-18 23:14:16.436571+01	Tafa	tafa	27
536	2018-03-18 23:14:16.447419+01	2018-03-18 23:14:16.447459+01	Wushishi	wushishi	27
537	2018-03-18 23:14:16.469462+01	2018-03-18 23:14:16.4695+01	Abeokuta South	abeokuta south	28
538	2018-03-18 23:14:16.480526+01	2018-03-18 23:14:16.480572+01	Ado-Odo/Ota	ado-odo/ota	28
539	2018-03-18 23:14:16.491281+01	2018-03-18 23:14:16.491305+01	Egbado North	egbado north	28
540	2018-03-18 23:14:16.502317+01	2018-03-18 23:14:16.50235+01	Egbado South	egbado south	28
541	2018-03-18 23:14:16.513458+01	2018-03-18 23:14:16.513499+01	Ewekoro	ewekoro	28
542	2018-03-18 23:14:16.524434+01	2018-03-18 23:14:16.524472+01	Ifo	ifo	28
543	2018-03-18 23:14:16.535501+01	2018-03-18 23:14:16.535548+01	Ijebu East	ijebu east	28
544	2018-03-18 23:14:16.546539+01	2018-03-18 23:14:16.54658+01	Ijebu North	ijebu north	28
545	2018-03-18 23:14:16.557492+01	2018-03-18 23:14:16.55753+01	Ijebu North East	ijebu north east	28
546	2018-03-18 23:14:16.568475+01	2018-03-18 23:14:16.568514+01	Ijebu Ode	ijebu ode	28
547	2018-03-18 23:14:16.579535+01	2018-03-18 23:14:16.579576+01	Ikenne	ikenne	28
548	2018-03-18 23:14:16.590288+01	2018-03-18 23:14:16.590314+01	Imeko Afon	imeko afon	28
549	2018-03-18 23:14:16.601465+01	2018-03-18 23:14:16.601499+01	Ipokia	ipokia	28
550	2018-03-18 23:14:16.612584+01	2018-03-18 23:14:16.612626+01	Obafemi Owode	obafemi owode	28
551	2018-03-18 23:14:16.623648+01	2018-03-18 23:14:16.623688+01	Odeda	odeda	28
552	2018-03-18 23:14:16.634654+01	2018-03-18 23:14:16.634695+01	Odogbolu	odogbolu	28
553	2018-03-18 23:14:16.645657+01	2018-03-18 23:14:16.645699+01	Ogun Waterside	ogun waterside	28
554	2018-03-18 23:14:16.65666+01	2018-03-18 23:14:16.6567+01	Remo North	remo north	28
555	2018-03-18 23:14:16.667664+01	2018-03-18 23:14:16.667705+01	Shagamu	shagamu	28
556	2018-03-18 23:14:16.691081+01	2018-03-18 23:14:16.691108+01	Akoko North-West	akoko north-west	29
557	2018-03-18 23:14:16.700508+01	2018-03-18 23:14:16.700536+01	Akoko South-West	akoko south-west	29
558	2018-03-18 23:14:16.711684+01	2018-03-18 23:14:16.711725+01	Akoko South-East	akoko south-east	29
559	2018-03-18 23:14:16.722488+01	2018-03-18 23:14:16.722523+01	Akure North	akure north	29
560	2018-03-18 23:14:16.733731+01	2018-03-18 23:14:16.733772+01	Akure South	akure south	29
561	2018-03-18 23:14:16.744578+01	2018-03-18 23:14:16.744623+01	Ese Odo	ese odo	29
562	2018-03-18 23:14:16.755612+01	2018-03-18 23:14:16.755656+01	Idanre	idanre	29
563	2018-03-18 23:14:16.76646+01	2018-03-18 23:14:16.766491+01	Ifedore	ifedore	29
564	2018-03-18 23:14:16.777521+01	2018-03-18 23:14:16.777559+01	Ilaje	ilaje	29
565	2018-03-18 23:14:16.78856+01	2018-03-18 23:14:16.788601+01	Ile Oluji/Okeigbo	ile oluji/okeigbo	29
566	2018-03-18 23:14:16.799571+01	2018-03-18 23:14:16.799611+01	Irele	irele	29
567	2018-03-18 23:14:16.810531+01	2018-03-18 23:14:16.810569+01	Odigbo	odigbo	29
568	2018-03-18 23:14:16.821592+01	2018-03-18 23:14:16.821634+01	Okitipupa	okitipupa	29
569	2018-03-18 23:14:16.832619+01	2018-03-18 23:14:16.832665+01	Ondo East	ondo east	29
570	2018-03-18 23:14:16.843622+01	2018-03-18 23:14:16.843667+01	Ondo West	ondo west	29
571	2018-03-18 23:14:16.854555+01	2018-03-18 23:14:16.854588+01	Ose	ose	29
572	2018-03-18 23:14:16.865617+01	2018-03-18 23:14:16.865658+01	Owo	owo	29
573	2018-03-18 23:14:16.887689+01	2018-03-18 23:14:16.887729+01	Atakunmosa West	atakunmosa west	30
574	2018-03-18 23:14:16.898578+01	2018-03-18 23:14:16.898612+01	Aiyedaade	aiyedaade	30
575	2018-03-18 23:14:16.909592+01	2018-03-18 23:14:16.909633+01	Aiyedire	aiyedire	30
576	2018-03-18 23:14:16.9205+01	2018-03-18 23:14:16.920527+01	Boluwaduro	boluwaduro	30
577	2018-03-18 23:14:16.931648+01	2018-03-18 23:14:16.931687+01	Boripe	boripe	30
578	2018-03-18 23:14:16.942623+01	2018-03-18 23:14:16.94266+01	Ede North	ede north	30
579	2018-03-18 23:14:16.953602+01	2018-03-18 23:14:16.953642+01	Ede South	ede south	30
580	2018-03-18 23:14:16.964701+01	2018-03-18 23:14:16.964743+01	Ife Central	ife central	30
581	2018-03-18 23:14:16.975672+01	2018-03-18 23:14:16.975709+01	Ife East	ife east	30
582	2018-03-18 23:14:16.986666+01	2018-03-18 23:14:16.986704+01	Ife North	ife north	30
583	2018-03-18 23:14:16.998419+01	2018-03-18 23:14:16.99846+01	Ife South	ife south	30
584	2018-03-18 23:14:17.008561+01	2018-03-18 23:14:17.008597+01	Egbedore	egbedore	30
585	2018-03-18 23:14:17.019576+01	2018-03-18 23:14:17.019607+01	Ejigbo	ejigbo	30
586	2018-03-18 23:14:17.030937+01	2018-03-18 23:14:17.030982+01	Ifedayo	ifedayo	30
587	2018-03-18 23:14:17.041922+01	2018-03-18 23:14:17.041969+01	Ifelodun	ifelodun	30
588	2018-03-18 23:14:17.052951+01	2018-03-18 23:14:17.052993+01	Ila	ila	30
589	2018-03-18 23:14:17.063943+01	2018-03-18 23:14:17.063989+01	Ilesa East	ilesa east	30
590	2018-03-18 23:14:17.074953+01	2018-03-18 23:14:17.075+01	Ilesa West	ilesa west	30
591	2018-03-18 23:14:17.085761+01	2018-03-18 23:14:17.085798+01	Irepodun	irepodun	30
592	2018-03-18 23:14:17.09701+01	2018-03-18 23:14:17.097055+01	Irewole	irewole	30
593	2018-03-18 23:14:17.107967+01	2018-03-18 23:14:17.108012+01	Isokan	isokan	30
594	2018-03-18 23:14:17.118812+01	2018-03-18 23:14:17.118835+01	Iwo	iwo	30
595	2018-03-18 23:14:17.129879+01	2018-03-18 23:14:17.129914+01	Obokun	obokun	30
596	2018-03-18 23:14:17.140855+01	2018-03-18 23:14:17.140897+01	Odo Otin	odo otin	30
597	2018-03-18 23:14:17.1519+01	2018-03-18 23:14:17.151941+01	Ola Oluwa	ola oluwa	30
598	2018-03-18 23:14:17.162878+01	2018-03-18 23:14:17.162915+01	Olorunda	olorunda	30
599	2018-03-18 23:14:17.173914+01	2018-03-18 23:14:17.173955+01	Oriade	oriade	30
600	2018-03-18 23:14:17.184961+01	2018-03-18 23:14:17.185001+01	Orolu	orolu	30
601	2018-03-18 23:14:17.195911+01	2018-03-18 23:14:17.195951+01	Osogbo	osogbo	30
602	2018-03-18 23:14:17.2179+01	2018-03-18 23:14:17.21794+01	Akinyele	akinyele	31
603	2018-03-18 23:14:17.228934+01	2018-03-18 23:14:17.228974+01	Atiba	atiba	31
604	2018-03-18 23:14:17.239886+01	2018-03-18 23:14:17.239927+01	Atisbo	atisbo	31
605	2018-03-18 23:14:17.250923+01	2018-03-18 23:14:17.250963+01	Egbeda	egbeda	31
606	2018-03-18 23:14:17.266679+01	2018-03-18 23:14:17.2667+01	Ibadan North	ibadan north	31
607	2018-03-18 23:14:17.272565+01	2018-03-18 23:14:17.272586+01	Ibadan North-East	ibadan north-east	31
608	2018-03-18 23:14:17.283773+01	2018-03-18 23:14:17.28379+01	Ibadan North-West	ibadan north-west	31
609	2018-03-18 23:14:17.294865+01	2018-03-18 23:14:17.294893+01	Ibadan South-East	ibadan south-east	31
610	2018-03-18 23:14:17.305881+01	2018-03-18 23:14:17.305907+01	Ibadan South-West	ibadan south-west	31
611	2018-03-18 23:14:17.317079+01	2018-03-18 23:14:17.317116+01	Ibarapa Central	ibarapa central	31
612	2018-03-18 23:14:17.327882+01	2018-03-18 23:14:17.327916+01	Ibarapa East	ibarapa east	31
613	2018-03-18 23:14:17.338724+01	2018-03-18 23:14:17.338754+01	Ibarapa North	ibarapa north	31
614	2018-03-18 23:14:17.349723+01	2018-03-18 23:14:17.349752+01	Ido	ido	31
615	2018-03-18 23:14:17.361248+01	2018-03-18 23:14:17.361293+01	Irepo	irepo	31
616	2018-03-18 23:14:17.372244+01	2018-03-18 23:14:17.372289+01	Iseyin	iseyin	31
617	2018-03-18 23:14:17.383225+01	2018-03-18 23:14:17.383268+01	Itesiwaju	itesiwaju	31
618	2018-03-18 23:14:17.394129+01	2018-03-18 23:14:17.394163+01	Iwajowa	iwajowa	31
619	2018-03-18 23:14:17.405256+01	2018-03-18 23:14:17.405296+01	Kajola	kajola	31
620	2018-03-18 23:14:17.416262+01	2018-03-18 23:14:17.416307+01	Lagelu	lagelu	31
621	2018-03-18 23:14:17.42736+01	2018-03-18 23:14:17.427405+01	Ogbomosho North	ogbomosho north	31
622	2018-03-18 23:14:17.438254+01	2018-03-18 23:14:17.438288+01	Ogbomosho South	ogbomosho south	31
623	2018-03-18 23:14:17.449367+01	2018-03-18 23:14:17.449408+01	Ogo Oluwa	ogo oluwa	31
624	2018-03-18 23:14:17.460408+01	2018-03-18 23:14:17.460454+01	Olorunsogo	olorunsogo	31
625	2018-03-18 23:14:17.471327+01	2018-03-18 23:14:17.471372+01	Oluyole	oluyole	31
626	2018-03-18 23:14:17.482306+01	2018-03-18 23:14:17.482347+01	Ona Ara	ona ara	31
627	2018-03-18 23:14:17.493418+01	2018-03-18 23:14:17.493463+01	Orelope	orelope	31
628	2018-03-18 23:14:17.504351+01	2018-03-18 23:14:17.504391+01	Ori Ire	ori ire	31
629	2018-03-18 23:14:17.515172+01	2018-03-18 23:14:17.515198+01	Oyo	oyo	31
630	2018-03-18 23:14:17.526244+01	2018-03-18 23:14:17.526275+01	Oyo East	oyo east	31
631	2018-03-18 23:14:17.537383+01	2018-03-18 23:14:17.537423+01	Saki East	saki east	31
632	2018-03-18 23:14:17.548328+01	2018-03-18 23:14:17.548369+01	Saki West	saki west	31
633	2018-03-18 23:14:17.559378+01	2018-03-18 23:14:17.559418+01	Surulere	surulere	31
634	2018-03-18 23:14:17.581415+01	2018-03-18 23:14:17.581457+01	Barkin Ladi	barkin ladi	32
635	2018-03-18 23:14:17.592386+01	2018-03-18 23:14:17.592428+01	Bassa	bassa	32
636	2018-03-18 23:14:17.603394+01	2018-03-18 23:14:17.603434+01	Jos East	jos east	32
637	2018-03-18 23:14:17.614234+01	2018-03-18 23:14:17.614274+01	Jos North	jos north	32
638	2018-03-18 23:14:17.625143+01	2018-03-18 23:14:17.625163+01	Jos South	jos south	32
639	2018-03-18 23:14:17.636278+01	2018-03-18 23:14:17.63631+01	Kanam	kanam	32
640	2018-03-18 23:14:17.64742+01	2018-03-18 23:14:17.64746+01	Kanke	kanke	32
641	2018-03-18 23:14:17.658366+01	2018-03-18 23:14:17.658403+01	Langtang South	langtang south	32
642	2018-03-18 23:14:17.669484+01	2018-03-18 23:14:17.669529+01	Langtang North	langtang north	32
643	2018-03-18 23:14:17.680428+01	2018-03-18 23:14:17.680469+01	Mangu	mangu	32
644	2018-03-18 23:14:17.691548+01	2018-03-18 23:14:17.691592+01	Mikang	mikang	32
645	2018-03-18 23:14:17.702409+01	2018-03-18 23:14:17.702442+01	Pankshin	pankshin	32
646	2018-03-18 23:14:17.715224+01	2018-03-18 23:14:17.715264+01	Qua'an Pan	qua'an pan	32
647	2018-03-18 23:14:17.724514+01	2018-03-18 23:14:17.724555+01	Riyom	riyom	32
648	2018-03-18 23:14:17.73532+01	2018-03-18 23:14:17.735339+01	Shendam	shendam	32
649	2018-03-18 23:14:17.74656+01	2018-03-18 23:14:17.746605+01	Wase	wase	32
650	2018-03-18 23:14:17.768478+01	2018-03-18 23:14:17.768519+01	Ahoada East	ahoada east	33
651	2018-03-18 23:14:17.779472+01	2018-03-18 23:14:17.779514+01	Ahoada West	ahoada west	33
652	2018-03-18 23:14:17.790511+01	2018-03-18 23:14:17.790548+01	Akuku-Toru	akuku-toru	33
653	2018-03-18 23:14:17.801544+01	2018-03-18 23:14:17.801586+01	Andoni	andoni	33
654	2018-03-18 23:14:17.812494+01	2018-03-18 23:14:17.812534+01	Asari-Toru	asari-toru	33
655	2018-03-18 23:14:17.823596+01	2018-03-18 23:14:17.823641+01	Bonny	bonny	33
656	2018-03-18 23:14:17.834571+01	2018-03-18 23:14:17.834612+01	Degema	degema	33
657	2018-03-18 23:14:17.845564+01	2018-03-18 23:14:17.845609+01	Eleme	eleme	33
658	2018-03-18 23:14:17.856599+01	2018-03-18 23:14:17.856644+01	Emuoha	emuoha	33
659	2018-03-18 23:14:17.867534+01	2018-03-18 23:14:17.867577+01	Etche	etche	33
660	2018-03-18 23:14:17.878551+01	2018-03-18 23:14:17.878591+01	Gokana	gokana	33
661	2018-03-18 23:14:17.889578+01	2018-03-18 23:14:17.889619+01	Ikwerre	ikwerre	33
662	2018-03-18 23:14:17.900605+01	2018-03-18 23:14:17.900648+01	Khana	khana	33
663	2018-03-18 23:14:17.911597+01	2018-03-18 23:14:17.911638+01	Obio/Akpor	obio/akpor	33
664	2018-03-18 23:14:17.922476+01	2018-03-18 23:14:17.922521+01	Ogba/Egbema/Ndoni	ogba/egbema/ndoni	33
665	2018-03-18 23:14:17.933508+01	2018-03-18 23:14:17.933544+01	Ogu/Bolo	ogu/bolo	33
666	2018-03-18 23:14:17.944639+01	2018-03-18 23:14:17.944684+01	Okrika	okrika	33
667	2018-03-18 23:14:17.955751+01	2018-03-18 23:14:17.955796+01	Omuma	omuma	33
668	2018-03-18 23:14:17.966764+01	2018-03-18 23:14:17.96681+01	Opobo/Nkoro	opobo/nkoro	33
669	2018-03-18 23:14:17.977725+01	2018-03-18 23:14:17.977765+01	Oyigbo	oyigbo	33
670	2018-03-18 23:14:17.988764+01	2018-03-18 23:14:17.988854+01	Port Harcourt	port harcourt	33
671	2018-03-18 23:14:17.999712+01	2018-03-18 23:14:17.999756+01	Tai	tai	33
672	2018-03-18 23:14:18.022405+01	2018-03-18 23:14:18.022445+01	Bodinga	bodinga	34
673	2018-03-18 23:14:18.03286+01	2018-03-18 23:14:18.032907+01	Dange Shuni	dange shuni	34
674	2018-03-18 23:14:18.073923+01	2018-03-18 23:14:18.073968+01	Gada	gada	34
675	2018-03-18 23:14:18.10964+01	2018-03-18 23:14:18.109687+01	Goronyo	goronyo	34
676	2018-03-18 23:14:18.120662+01	2018-03-18 23:14:18.120713+01	Gudu	gudu	34
677	2018-03-18 23:14:18.131765+01	2018-03-18 23:14:18.131795+01	Gwadabawa	gwadabawa	34
678	2018-03-18 23:14:18.142704+01	2018-03-18 23:14:18.142741+01	Illela	illela	34
679	2018-03-18 23:14:18.153821+01	2018-03-18 23:14:18.153862+01	Isa	isa	34
680	2018-03-18 23:14:18.164954+01	2018-03-18 23:14:18.165+01	Kebbe	kebbe	34
681	2018-03-18 23:14:18.175876+01	2018-03-18 23:14:18.175917+01	Kware	kware	34
682	2018-03-18 23:14:18.186886+01	2018-03-18 23:14:18.186931+01	Rabah	rabah	34
683	2018-03-18 23:14:18.197878+01	2018-03-18 23:14:18.197921+01	Sabon Birni	sabon birni	34
684	2018-03-18 23:14:18.208959+01	2018-03-18 23:14:18.209004+01	Shagari	shagari	34
685	2018-03-18 23:14:18.219833+01	2018-03-18 23:14:18.219873+01	Silame	silame	34
686	2018-03-18 23:14:18.230584+01	2018-03-18 23:14:18.230619+01	Sokoto North	sokoto north	34
687	2018-03-18 23:14:18.241826+01	2018-03-18 23:14:18.241863+01	Sokoto South	sokoto south	34
688	2018-03-18 23:14:18.25305+01	2018-03-18 23:14:18.253095+01	Tambuwal	tambuwal	34
689	2018-03-18 23:14:18.26351+01	2018-03-18 23:14:18.263538+01	Tangaza	tangaza	34
690	2018-03-18 23:14:18.274997+01	2018-03-18 23:14:18.275042+01	Tureta	tureta	34
691	2018-03-18 23:14:18.285585+01	2018-03-18 23:14:18.285619+01	Wamako	wamako	34
692	2018-03-18 23:14:18.296571+01	2018-03-18 23:14:18.296597+01	Wurno	wurno	34
693	2018-03-18 23:14:18.307474+01	2018-03-18 23:14:18.307494+01	Yabo	yabo	34
694	2018-03-18 23:14:18.329517+01	2018-03-18 23:14:18.329534+01	Bali	bali	35
695	2018-03-18 23:14:18.340659+01	2018-03-18 23:14:18.340676+01	Donga	donga	35
696	2018-03-18 23:14:18.351633+01	2018-03-18 23:14:18.351655+01	Gashaka	gashaka	35
697	2018-03-18 23:14:18.362414+01	2018-03-18 23:14:18.362427+01	Gassol	gassol	35
698	2018-03-18 23:14:18.373688+01	2018-03-18 23:14:18.373711+01	Ibi	ibi	35
699	2018-03-18 23:14:18.384909+01	2018-03-18 23:14:18.384942+01	Jalingo	jalingo	35
700	2018-03-18 23:14:18.395962+01	2018-03-18 23:14:18.396+01	Karim Lamido	karim lamido	35
701	2018-03-18 23:14:18.40705+01	2018-03-18 23:14:18.40709+01	Kumi	kumi	35
702	2018-03-18 23:14:18.417588+01	2018-03-18 23:14:18.417617+01	Lau	lau	35
703	2018-03-18 23:14:18.429132+01	2018-03-18 23:14:18.429176+01	Sardauna	sardauna	35
704	2018-03-18 23:14:18.440057+01	2018-03-18 23:14:18.440101+01	Takum	takum	35
705	2018-03-18 23:14:18.451103+01	2018-03-18 23:14:18.451148+01	Ussa	ussa	35
706	2018-03-18 23:14:18.461977+01	2018-03-18 23:14:18.462014+01	Wukari	wukari	35
707	2018-03-18 23:14:18.473077+01	2018-03-18 23:14:18.473122+01	Yorro	yorro	35
708	2018-03-18 23:14:18.484067+01	2018-03-18 23:14:18.484106+01	Zing	zing	35
709	2018-03-18 23:14:18.506075+01	2018-03-18 23:14:18.506112+01	Bursari	bursari	36
710	2018-03-18 23:14:18.517163+01	2018-03-18 23:14:18.517209+01	Damaturu	damaturu	36
711	2018-03-18 23:14:18.528128+01	2018-03-18 23:14:18.528173+01	Fika	fika	36
712	2018-03-18 23:14:18.538974+01	2018-03-18 23:14:18.538998+01	Fune	fune	36
713	2018-03-18 23:14:18.550158+01	2018-03-18 23:14:18.550192+01	Geidam	geidam	36
714	2018-03-18 23:14:18.561225+01	2018-03-18 23:14:18.561266+01	Gujba	gujba	36
715	2018-03-18 23:14:18.572205+01	2018-03-18 23:14:18.572246+01	Gulani	gulani	36
716	2018-03-18 23:14:18.583212+01	2018-03-18 23:14:18.583253+01	Jakusko	jakusko	36
717	2018-03-18 23:14:18.59423+01	2018-03-18 23:14:18.594271+01	Karasuwa	karasuwa	36
718	2018-03-18 23:14:18.60526+01	2018-03-18 23:14:18.605301+01	Machina	machina	36
719	2018-03-18 23:14:18.616241+01	2018-03-18 23:14:18.616282+01	Nangere	nangere	36
720	2018-03-18 23:14:18.62723+01	2018-03-18 23:14:18.627272+01	Nguru	nguru	36
721	2018-03-18 23:14:18.638104+01	2018-03-18 23:14:18.63813+01	Potiskum	potiskum	36
722	2018-03-18 23:14:18.649174+01	2018-03-18 23:14:18.649207+01	Tarmuwa	tarmuwa	36
723	2018-03-18 23:14:18.660259+01	2018-03-18 23:14:18.660299+01	Yunusari	yunusari	36
724	2018-03-18 23:14:18.671262+01	2018-03-18 23:14:18.671303+01	Yusufari	yusufari	36
725	2018-03-18 23:14:18.693314+01	2018-03-18 23:14:18.693355+01	Bakura	bakura	37
726	2018-03-18 23:14:18.704311+01	2018-03-18 23:14:18.704352+01	Birnin Magaji/Kiyaw	birnin magaji/kiyaw	37
727	2018-03-18 23:14:18.715204+01	2018-03-18 23:14:18.715245+01	Bukkuyum	bukkuyum	37
728	2018-03-18 23:14:18.726201+01	2018-03-18 23:14:18.726242+01	Bungudu	bungudu	37
729	2018-03-18 23:14:18.738886+01	2018-03-18 23:14:18.738921+01	Gummi	gummi	37
730	2018-03-18 23:14:18.74782+01	2018-03-18 23:14:18.747838+01	Gusau	gusau	37
731	2018-03-18 23:14:18.758999+01	2018-03-18 23:14:18.759034+01	Kaura Namoda	kaura namoda	37
732	2018-03-18 23:14:18.770327+01	2018-03-18 23:14:18.770372+01	Maradun	maradun	37
733	2018-03-18 23:14:18.781392+01	2018-03-18 23:14:18.781437+01	Maru	maru	37
734	2018-03-18 23:14:18.792335+01	2018-03-18 23:14:18.792379+01	Shinkafi	shinkafi	37
735	2018-03-18 23:14:18.803281+01	2018-03-18 23:14:18.803326+01	Talata Mafara	talata mafara	37
736	2018-03-18 23:14:18.814371+01	2018-03-18 23:14:18.814402+01	Chafe	chafe	37
737	2018-03-18 23:14:18.825464+01	2018-03-18 23:14:18.825505+01	Zurmi	zurmi	37
\.


--
-- Name: app_city_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_city_id_seq', 737, true);


--
-- Data for Name: app_country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_country (id, created_at, updated_at, name, code, is_enabled) FROM stdin;
1	2018-03-18 23:14:09.848994+01	2018-03-18 23:14:09.849005+01	Nigeria	nigeria	f
\.


--
-- Name: app_country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_country_id_seq', 1, true);


--
-- Data for Name: app_education; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_education (id, created_at, updated_at, year, degree, study_field, grade, school_id) FROM stdin;
\.


--
-- Name: app_education_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_education_id_seq', 1, false);


--
-- Data for Name: app_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_item (id, created_at, updated_at, quantity, unit_price, cart_id, user_id, product_id) FROM stdin;
\.


--
-- Name: app_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_item_id_seq', 3, true);


--
-- Data for Name: app_onboarding; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_onboarding (id, created_at, updated_at, assessment_step_completed, step_two_completed, step_three_completed, step_four_completed, step_five_completed, user_id) FROM stdin;
\.


--
-- Name: app_onboarding_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_onboarding_id_seq', 1, false);


--
-- Data for Name: app_order; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_order (id, created_at, updated_at, delivery_address, delivery_name, order_status_id, reference_code, payment_status_id) FROM stdin;
\.


--
-- Name: app_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_order_id_seq', 1, false);


--
-- Data for Name: app_orderitem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_orderitem (id, created_at, updated_at, quantity, unit_price, total_price, order_id, product_id, user_id) FROM stdin;
\.


--
-- Name: app_orderitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_orderitem_id_seq', 1, false);


--
-- Data for Name: app_orderstatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_orderstatus (id, created_at, updated_at, name, code) FROM stdin;
\.


--
-- Name: app_orderstatus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_orderstatus_id_seq', 1, false);


--
-- Data for Name: app_paymentstatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_paymentstatus (id, created_at, updated_at, name, code) FROM stdin;
\.


--
-- Name: app_paymentstatus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_paymentstatus_id_seq', 1, false);


--
-- Data for Name: app_plantype; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_plantype (id, created_at, updated_at, name, code, description) FROM stdin;
\.


--
-- Name: app_plantype_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_plantype_id_seq', 1, false);


--
-- Data for Name: app_product; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_product (id, created_at, updated_at, name, sku, unit_price, available_quantity, cover_image, description) FROM stdin;
1	2018-03-28 00:16:12+01	2018-03-28 00:16:12+01	Norwegian Wood	XOIRASHFRF	2000.00	7	products/coollogo_com-22286304.png	Author: Haruki Murakami
\.


--
-- Data for Name: app_product_categories; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_product_categories (id, product_id, category_id) FROM stdin;
1	1	1
\.


--
-- Name: app_product_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_product_categories_id_seq', 1, true);


--
-- Name: app_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_product_id_seq', 1, true);


--
-- Data for Name: app_qualification; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_qualification (id, created_at, updated_at, name, code) FROM stdin;
1	2018-03-18 23:14:22.118396+01	2018-03-18 23:14:22.118406+01	Masters in Psychology	masters_psychology
2	2018-03-18 23:14:22.148751+01	2018-03-18 23:14:22.148824+01	Diploma in Psychology	diploma_psychology
3	2018-03-18 23:14:22.159898+01	2018-03-18 23:14:22.159938+01	Certification in Psychology	certification_psychology
4	2018-03-18 23:14:22.170866+01	2018-03-18 23:14:22.170907+01	Masters in Social Work	masters_social
5	2018-03-18 23:14:22.181926+01	2018-03-18 23:14:22.181971+01	Neurolinguistic Programming Certification	neurolinguist
6	2018-03-18 23:14:22.192996+01	2018-03-18 23:14:22.193042+01	Hypnotherapy	hypnotherapy
7	2018-03-18 23:14:22.20395+01	2018-03-18 23:14:22.203997+01	Life Coaching Certification	life
8	2018-03-18 23:14:22.214991+01	2018-03-18 23:14:22.215036+01	Career Coaching/Training Certification	career
9	2018-03-18 23:14:22.225983+01	2018-03-18 23:14:22.226029+01	Human Resource Professionals with experience in areas of emotional wellness	human_resources
\.


--
-- Name: app_qualification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_qualification_id_seq', 9, true);


--
-- Data for Name: app_question; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_question (id, created_at, updated_at, number, body, is_active, assessment_test_id) FROM stdin;
5	2018-03-28 12:05:33+01	2018-03-28 12:05:33+01	1	I feel I’m living a well-balanced life (academic, relationships, personal growth, interests/hobbies)	t	2
\.


--
-- Name: app_question_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_question_id_seq', 5, true);


--
-- Data for Name: app_question_responses; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_question_responses (id, question_id, questionresponse_id) FROM stdin;
13	5	9
14	5	10
\.


--
-- Name: app_question_responses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_question_responses_id_seq', 14, true);


--
-- Data for Name: app_questionresponse; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_questionresponse (id, created_at, updated_at, name) FROM stdin;
9	2018-03-28 12:06:02+01	2018-03-28 12:06:02+01	Never
10	2018-03-28 12:06:12+01	2018-03-28 12:06:12+01	Always
\.


--
-- Name: app_questionresponse_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_questionresponse_id_seq', 10, true);


--
-- Data for Name: app_ratingentry; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_ratingentry (id, created_at, updated_at, score, therapist_id) FROM stdin;
\.


--
-- Name: app_ratingentry_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_ratingentry_id_seq', 5, true);


--
-- Data for Name: app_resources; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_resources (id, created_at, updated_at, resource_url, order_id, user_id) FROM stdin;
\.


--
-- Name: app_resources_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_resources_id_seq', 1, false);


--
-- Data for Name: app_review; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_review (id, created_at, updated_at, body, therapist_id) FROM stdin;
\.


--
-- Name: app_review_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_review_id_seq', 1, false);


--
-- Data for Name: app_school; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_school (id, created_at, updated_at, name, code, city, country_id, state_id) FROM stdin;
\.


--
-- Name: app_school_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_school_id_seq', 1, false);


--
-- Data for Name: app_sessions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_sessions (id, created_at, updated_at, last_active_date, therapist_id, user_id) FROM stdin;
\.


--
-- Name: app_sessions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_sessions_id_seq', 1, false);


--
-- Data for Name: app_state; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_state (id, created_at, updated_at, name, code, is_enabled, country_id) FROM stdin;
1	2018-03-18 23:14:09.886493+01	2018-03-18 23:14:09.886542+01	Abia	abia	f	1
2	2018-03-18 23:14:10.073495+01	2018-03-18 23:14:10.073552+01	Adamawa	adamawa	f	1
3	2018-03-18 23:14:10.304177+01	2018-03-18 23:14:10.304213+01	Akwa Ibom	akwa ibom	f	1
4	2018-03-18 23:14:10.645596+01	2018-03-18 23:14:10.645637+01	Anambra	anambra	f	1
5	2018-03-18 23:14:10.876817+01	2018-03-18 23:14:10.876864+01	Bauchi	bauchi	f	1
6	2018-03-18 23:14:11.097032+01	2018-03-18 23:14:11.097067+01	Bayelsa	bayelsa	f	1
7	2018-03-18 23:14:11.185264+01	2018-03-18 23:14:11.185309+01	Benue	benue	f	1
8	2018-03-18 23:14:11.438261+01	2018-03-18 23:14:11.438297+01	Borno	borno	f	1
9	2018-03-18 23:14:11.735602+01	2018-03-18 23:14:11.735637+01	Cross River	cross river	f	1
10	2018-03-18 23:14:11.933673+01	2018-03-18 23:14:11.9337+01	Delta	delta	f	1
11	2018-03-18 23:14:12.208946+01	2018-03-18 23:14:12.208979+01	Ebonyi	ebonyi	f	1
12	2018-03-18 23:14:12.352175+01	2018-03-18 23:14:12.352216+01	Edo	edo	f	1
13	2018-03-18 23:14:12.55036+01	2018-03-18 23:14:12.550401+01	Ekiti	ekiti	f	1
14	2018-03-18 23:14:12.7262+01	2018-03-18 23:14:12.726239+01	Enugu	enugu	f	1
15	2018-03-18 23:14:12.913409+01	2018-03-18 23:14:12.913449+01	FCT	fct	f	1
16	2018-03-18 23:14:12.979473+01	2018-03-18 23:14:12.979513+01	Gombe	gombe	f	1
17	2018-03-18 23:14:13.133656+01	2018-03-18 23:14:13.133699+01	Imo	imo	f	1
18	2018-03-18 23:14:13.430806+01	2018-03-18 23:14:13.430834+01	Jigawa	jigawa	f	1
19	2018-03-18 23:14:13.728145+01	2018-03-18 23:14:13.728182+01	Kaduna	kaduna	f	1
20	2018-03-18 23:14:13.981427+01	2018-03-18 23:14:13.981471+01	Kano	kano	f	1
21	2018-03-18 23:14:14.465666+01	2018-03-18 23:14:14.465698+01	Katsina	katsina	f	1
22	2018-03-18 23:14:14.840265+01	2018-03-18 23:14:14.840311+01	Kebbi	kebbi	f	1
23	2018-03-18 23:14:15.071487+01	2018-03-18 23:14:15.071532+01	Kogi	kogi	f	1
24	2018-03-18 23:14:15.302474+01	2018-03-18 23:14:15.302511+01	Kwara	kwara	f	1
25	2018-03-18 23:14:15.819855+01	2018-03-18 23:14:15.8199+01	Lagos	lagos	f	1
26	2018-03-18 23:14:16.039855+01	2018-03-18 23:14:16.039895+01	Nasarawa	nasarawa	f	1
27	2018-03-18 23:14:16.183249+01	2018-03-18 23:14:16.183293+01	Niger	niger	f	1
28	2018-03-18 23:14:16.458527+01	2018-03-18 23:14:16.458571+01	Ogun	ogun	f	1
29	2018-03-18 23:14:16.678676+01	2018-03-18 23:14:16.678716+01	Ondo	ondo	f	1
30	2018-03-18 23:14:16.876602+01	2018-03-18 23:14:16.876643+01	Osun	osun	f	1
31	2018-03-18 23:14:17.206923+01	2018-03-18 23:14:17.206965+01	Oyo	oyo	f	1
32	2018-03-18 23:14:17.570344+01	2018-03-18 23:14:17.570385+01	Plateau	plateau	f	1
33	2018-03-18 23:14:17.757583+01	2018-03-18 23:14:17.757627+01	Rivers	rivers	f	1
34	2018-03-18 23:14:18.010821+01	2018-03-18 23:14:18.010866+01	Sokoto	sokoto	f	1
35	2018-03-18 23:14:18.318463+01	2018-03-18 23:14:18.318482+01	Taraba	taraba	f	1
36	2018-03-18 23:14:18.495152+01	2018-03-18 23:14:18.495197+01	Yobe	yobe	f	1
37	2018-03-18 23:14:18.682295+01	2018-03-18 23:14:18.682336+01	Zamfara	zamfara	f	1
\.


--
-- Name: app_state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_state_id_seq', 37, true);


--
-- Data for Name: app_testimonial; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_testimonial (id, created_at, updated_at, image, body, is_active, headline, name) FROM stdin;
\.


--
-- Name: app_testimonial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_testimonial_id_seq', 1, false);


--
-- Data for Name: app_therapist; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_therapist (created_at, updated_at, profile_image, address_id, is_activated, bio, qualification_id, resume, email, password, name, id, rating, reviews_count) FROM stdin;
\.


--
-- Name: app_therapist_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_therapist_id_seq', 1, true);


--
-- Data for Name: app_transaction; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_transaction (id, created_at, updated_at, reference_code, "txRef", amount, order_id, transaction_status_id, user_id) FROM stdin;
\.


--
-- Name: app_transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_transaction_id_seq', 1, false);


--
-- Data for Name: app_transactionstatus; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_transactionstatus (id, created_at, updated_at, name, code) FROM stdin;
\.


--
-- Name: app_transactionstatus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_transactionstatus_id_seq', 1, false);


--
-- Data for Name: app_userassessment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_userassessment (id, created_at, updated_at, code, is_completed, user_id) FROM stdin;
\.


--
-- Name: app_userassessment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_userassessment_id_seq', 1, false);


--
-- Data for Name: app_userresponse; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.app_userresponse (id, created_at, updated_at, response, question, assessment_id, user_id) FROM stdin;
\.


--
-- Name: app_userresponse_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.app_userresponse_id_seq', 1, false);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add pinned application	1	add_pinnedapplication
2	Can change pinned application	1	change_pinnedapplication
3	Can delete pinned application	1	delete_pinnedapplication
4	Can add bookmark	2	add_bookmark
5	Can change bookmark	2	change_bookmark
6	Can delete bookmark	2	delete_bookmark
7	Can add log entry	3	add_logentry
8	Can change log entry	3	change_logentry
9	Can delete log entry	3	delete_logentry
10	Can add group	4	add_group
11	Can change group	4	change_group
12	Can delete group	4	delete_group
13	Can add user	5	add_user
14	Can change user	5	change_user
15	Can delete user	5	delete_user
16	Can add permission	6	add_permission
17	Can change permission	6	change_permission
18	Can delete permission	6	delete_permission
19	Can add content type	7	add_contenttype
20	Can change content type	7	change_contenttype
21	Can delete content type	7	delete_contenttype
22	Can add session	8	add_session
23	Can change session	8	change_session
24	Can delete session	8	delete_session
25	Can add site	9	add_site
26	Can change site	9	change_site
27	Can delete site	9	delete_site
28	Can add Token	10	add_token
29	Can change Token	10	change_token
30	Can delete Token	10	delete_token
31	Can add on boarding	11	add_onboarding
32	Can change on boarding	11	change_onboarding
33	Can delete on boarding	11	delete_onboarding
34	Can add address	12	add_address
35	Can change address	12	change_address
36	Can delete address	12	delete_address
37	Can add qualification	13	add_qualification
38	Can change qualification	13	change_qualification
39	Can delete qualification	13	delete_qualification
40	Can add country	14	add_country
41	Can change country	14	change_country
42	Can delete country	14	delete_country
43	Can add order item	15	add_orderitem
44	Can change order item	15	change_orderitem
45	Can delete order item	15	delete_orderitem
46	Can add question	16	add_question
47	Can change question	16	change_question
48	Can delete question	16	delete_question
49	Can add certification	17	add_certification
50	Can change certification	17	change_certification
51	Can delete certification	17	delete_certification
52	Can add testimonial	18	add_testimonial
53	Can change testimonial	18	change_testimonial
54	Can delete testimonial	18	delete_testimonial
55	Can add user assessment	19	add_userassessment
56	Can change user assessment	19	change_userassessment
57	Can delete user assessment	19	delete_userassessment
58	Can add cart	20	add_cart
59	Can change cart	20	change_cart
60	Can delete cart	20	delete_cart
61	Can add education	21	add_education
62	Can change education	21	change_education
63	Can delete education	21	delete_education
64	Can add product	22	add_product
65	Can change product	22	change_product
66	Can delete product	22	delete_product
67	Can add transaction status	23	add_transactionstatus
68	Can change transaction status	23	change_transactionstatus
69	Can delete transaction status	23	delete_transactionstatus
70	Can add resource	24	add_resources
71	Can change resource	24	change_resources
72	Can delete resource	24	delete_resources
73	Can add payment status	25	add_paymentstatus
74	Can change payment status	25	change_paymentstatus
75	Can delete payment status	25	delete_paymentstatus
76	Can add therapist	26	add_therapist
77	Can change therapist	26	change_therapist
78	Can delete therapist	26	delete_therapist
79	Can add user response	27	add_userresponse
80	Can change user response	27	change_userresponse
81	Can delete user response	27	delete_userresponse
82	Can add state	28	add_state
83	Can change state	28	change_state
84	Can delete state	28	delete_state
85	Can add category	29	add_category
86	Can change category	29	change_category
87	Can delete category	29	delete_category
88	Can add order	30	add_order
89	Can change order	30	change_order
90	Can delete order	30	delete_order
91	Can add school	31	add_school
92	Can change school	31	change_school
93	Can delete school	31	delete_school
94	Can add item	32	add_item
95	Can change item	32	change_item
96	Can delete item	32	delete_item
97	Can add sessions	33	add_sessions
98	Can change sessions	33	change_sessions
99	Can delete sessions	33	delete_sessions
100	Can add variant	34	add_variant
101	Can change variant	34	change_variant
102	Can delete variant	34	delete_variant
103	Can add order status	35	add_orderstatus
104	Can change order status	35	change_orderstatus
105	Can delete order status	35	delete_orderstatus
106	Can add city	36	add_city
107	Can change city	36	change_city
108	Can delete city	36	delete_city
109	Can add transaction	37	add_transaction
110	Can change transaction	37	change_transaction
111	Can delete transaction	37	delete_transaction
112	Can add account	38	add_account
113	Can change account	38	change_account
114	Can delete account	38	delete_account
115	Can add review	39	add_review
116	Can change review	39	change_review
117	Can delete review	39	delete_review
118	Can add plan type	40	add_plantype
119	Can change plan type	40	change_plantype
120	Can delete plan type	40	delete_plantype
121	Can add rating entry	41	add_ratingentry
122	Can change rating entry	41	change_ratingentry
123	Can delete rating entry	41	delete_ratingentry
124	Can add assessment test	42	add_assessmenttest
125	Can change assessment test	42	change_assessmenttest
126	Can delete assessment test	42	delete_assessmenttest
127	Can add question response	43	add_questionresponse
128	Can change question response	43	change_questionresponse
129	Can delete question response	43	delete_questionresponse
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 129, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$36000$7Vxu2o0H92uQ$jNTLGs5qw0WXbDw0DBqvh4X4y6QafYT83f6ZUCeQ9Kg=	2018-03-27 17:49:57.179379+01	f	stikks			styccs@gmail.com	f	t	2018-03-18 23:28:43.16866+01
2	pbkdf2_sha256$36000$kWfNQqLj5lfo$JUZ31Y0pQClXIXHbaIyPWWbBblNwb7hfhom40yYlGxc=	2018-03-27 23:54:27.843012+01	t	mindcure			mindcure@gmail.com	t	t	2018-03-27 12:42:53.130678+01
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 2, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: authtoken_token; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.authtoken_token (key, created, user_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-03-27 12:43:32.342809+01	1	Question object	1	[{"added": {}}]	16	2
2	2018-03-27 23:55:13.549629+01	1	Category object	1	[{"added": {}}]	29	2
3	2018-03-28 00:16:40.464863+01	1	Product object	1	[{"added": {}}]	22	2
4	2018-03-28 00:18:42.166333+01	1	Norwegian Wood	2	[{"changed": {"fields": ["description"]}}]	22	2
5	2018-03-28 10:40:56.428245+01	2	Emotional Wellness Test for Students	1	[{"added": {}}]	42	2
6	2018-03-28 11:51:28.048054+01	1	QuestionResponse object	1	[{"added": {}}]	43	2
7	2018-03-28 11:53:24.648738+01	2	mostly	1	[{"added": {}}]	43	2
8	2018-03-28 11:53:36.061284+01	3	sometimes	1	[{"added": {}}]	43	2
9	2018-03-28 11:53:41.594618+01	4	never	1	[{"added": {}}]	43	2
10	2018-03-28 11:59:47.679648+01	3	I feel I’m living a well-balanced life (academic, relationships, personal growth, interests/hobbies)	1	[{"added": {}}]	16	2
11	2018-03-28 12:01:49.91092+01	5	Always	1	[{"added": {}}]	43	2
12	2018-03-28 12:01:55.860851+01	6	Mostly	1	[{"added": {}}]	43	2
13	2018-03-28 12:02:02.595499+01	7	Sometimes/Rarely	1	[{"added": {}}]	43	2
14	2018-03-28 12:02:09.72934+01	8	Never	1	[{"added": {}}]	43	2
15	2018-03-28 12:02:16.342133+01	4	I feel I’m living a well-balanced life (academic, relationships, personal growth, interests/hobbies)	1	[{"added": {}}]	16	2
16	2018-03-28 12:06:09.896646+01	9	Never	1	[{"added": {}}]	43	2
17	2018-03-28 12:06:17.451029+01	10	Always	1	[{"added": {}}]	43	2
18	2018-03-28 12:06:20.76145+01	5	I feel I’m living a well-balanced life (academic, relationships, personal growth, interests/hobbies)	1	[{"added": {}}]	16	2
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 18, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	jet	pinnedapplication
2	jet	bookmark
3	admin	logentry
4	auth	group
5	auth	user
6	auth	permission
7	contenttypes	contenttype
8	sessions	session
9	sites	site
10	authtoken	token
11	app	onboarding
12	app	address
13	app	qualification
14	app	country
15	app	orderitem
16	app	question
17	app	certification
18	app	testimonial
19	app	userassessment
20	app	cart
21	app	education
22	app	product
23	app	transactionstatus
24	app	resources
25	app	paymentstatus
26	app	therapist
27	app	userresponse
28	app	state
29	app	category
30	app	order
31	app	school
32	app	item
33	app	sessions
34	app	variant
35	app	orderstatus
36	app	city
37	app	transaction
38	app	account
39	app	review
40	app	plantype
41	app	ratingentry
42	app	assessmenttest
43	app	questionresponse
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 43, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-03-18 23:13:49.118262+01
2	auth	0001_initial	2018-03-18 23:13:50.208664+01
3	admin	0001_initial	2018-03-18 23:13:50.462323+01
4	admin	0002_logentry_remove_auto_add	2018-03-18 23:13:50.5064+01
5	contenttypes	0002_remove_content_type_name	2018-03-18 23:13:50.560978+01
6	auth	0002_alter_permission_name_max_length	2018-03-18 23:13:50.593687+01
7	auth	0003_alter_user_email_max_length	2018-03-18 23:13:50.626787+01
8	auth	0004_alter_user_username_opts	2018-03-18 23:13:50.651037+01
9	auth	0005_alter_user_last_login_null	2018-03-18 23:13:50.682051+01
10	auth	0006_require_contenttypes_0002	2018-03-18 23:13:50.693657+01
11	auth	0007_alter_validators_add_error_messages	2018-03-18 23:13:50.719993+01
12	auth	0008_alter_user_username_max_length	2018-03-18 23:13:50.836328+01
13	app	0001_initial	2018-03-18 23:13:52.404432+01
14	app	0002_auto_20180123_2214	2018-03-18 23:13:52.965084+01
15	app	0003_auto_20180217_0226	2018-03-18 23:13:55.227838+01
16	app	0004_auto_20180217_1107	2018-03-18 23:13:56.93136+01
17	app	0005_auto_20180220_0854	2018-03-18 23:13:57.519781+01
18	app	0006_auto_20180317_2106	2018-03-18 23:13:58.137491+01
19	app	0007_auto_20180317_2256	2018-03-18 23:13:58.179808+01
20	app	0008_auto_20180317_2309	2018-03-18 23:13:58.213057+01
21	app	0009_qualification	2018-03-18 23:13:58.423189+01
22	app	0010_auto_20180318_1422	2018-03-18 23:13:58.753519+01
23	app	0011_auto_20180318_1442	2018-03-18 23:13:59.195091+01
24	app	0012_address_city	2018-03-18 23:13:59.571706+01
25	app	0013_auto_20180318_1725	2018-03-18 23:14:00.267925+01
26	app	0014_auto_20180318_2053	2018-03-18 23:14:00.315534+01
27	app	0015_auto_20180318_2152	2018-03-18 23:14:00.373021+01
28	app	0016_auto_20180318_2238	2018-03-18 23:14:01.360031+01
29	app	0017_auto_20180318_2306	2018-03-18 23:14:01.589727+01
30	app	0018_therapist_id	2018-03-18 23:14:01.832528+01
31	authtoken	0001_initial	2018-03-18 23:14:02.04038+01
32	authtoken	0002_auto_20160226_1747	2018-03-18 23:14:02.137984+01
33	jet	0001_initial	2018-03-18 23:14:02.393306+01
34	jet	0002_delete_userdashboardmodule	2018-03-18 23:14:02.471602+01
35	sessions	0001_initial	2018-03-18 23:14:02.656587+01
36	sites	0001_initial	2018-03-18 23:14:02.75574+01
37	sites	0002_alter_domain_unique	2018-03-18 23:14:02.878925+01
38	app	0019_auto_20180320_2314	2018-03-20 23:14:33.597471+01
39	app	0020_question_is_active	2018-03-27 12:45:51.673881+01
40	app	0021_auto_20180327_1805	2018-03-27 18:05:39.660451+01
41	app	0022_auto_20180327_1812	2018-03-27 18:12:44.476768+01
42	app	0023_auto_20180327_2353	2018-03-27 23:53:47.63972+01
43	app	0024_auto_20180328_0018	2018-03-28 00:18:15.343394+01
44	app	0025_auto_20180328_0021	2018-03-28 00:22:31.46148+01
45	app	0026_auto_20180328_0024	2018-03-28 00:24:53.545018+01
46	app	0027_auto_20180328_0028	2018-03-28 00:28:08.905561+01
47	app	0028_auto_20180328_1034	2018-03-28 10:34:46.869962+01
48	app	0029_auto_20180328_1035	2018-03-28 10:35:31.732624+01
49	app	0030_auto_20180328_1137	2018-03-28 11:37:46.379769+01
50	app	0031_auto_20180328_1150	2018-03-28 11:50:24.263952+01
51	app	0032_auto_20180328_1153	2018-03-28 11:53:13.575949+01
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 51, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
0bngitoovi31tqy4pn22jba1d7w22lw0	MDdmZTI2YzVkZWYxNDM5MTljZWEwNzYxYjc1MTg1ZmI3YjRhZTcxMDp7ImNhcnQiOiJ7XCJjaGVja2VkX291dFwiOiBmYWxzZSwgXCJjcmVhdGVkX2F0XCI6IFwiMjAxOC0wMy0yN1QyMjoxMjozNS4zMTg5MDkrMDA6MDBcIiwgXCJ1cGRhdGVkX2F0XCI6IFwiMjAxOC0wMy0yN1QyMjoxMjozNS4zMTg5MzQrMDA6MDBcIiwgXCJ1c2VyXCI6IG51bGwsIFwiY2hlY2tvdXRfZGF0ZVwiOiBudWxsLCBcImlkXCI6IDEzfSJ9	2018-04-10 23:12:36.563559+01
vn84dby29rveiwpm1slpzgbezzqm0i65	NmNjOTA5MTA1MjY4MjAzMzBjMzA5ZGRlNzVlODU1MTBiYTExMGM3MTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiOWExNWJiODRmZWJlY2E4ZWRhN2ZlYzc5OTRhYjZmNmYxMDg4NjcwMSIsIl9hdXRoX3VzZXJfaWQiOiIyIiwiY2FydCI6IntcImNoZWNrZWRfb3V0XCI6IGZhbHNlLCBcImNyZWF0ZWRfYXRcIjogXCIyMDE4LTAzLTI3VDIyOjIxOjU4Ljg3MTQ0OSswMDowMFwiLCBcInVwZGF0ZWRfYXRcIjogXCIyMDE4LTAzLTI3VDIyOjIxOjU4Ljg3MTQ2OCswMDowMFwiLCBcInVzZXJcIjogbnVsbCwgXCJjaGVja291dF9kYXRlXCI6IG51bGwsIFwiaWRcIjogMTl9In0=	2018-04-10 23:54:27.854044+01
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_site_id_seq', 1, true);


--
-- Data for Name: jet_bookmark; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jet_bookmark (id, url, title, "user", date_add) FROM stdin;
\.


--
-- Name: jet_bookmark_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jet_bookmark_id_seq', 1, false);


--
-- Data for Name: jet_pinnedapplication; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jet_pinnedapplication (id, app_label, "user", date_add) FROM stdin;
\.


--
-- Name: jet_pinnedapplication_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jet_pinnedapplication_id_seq', 1, false);


--
-- Name: app_account_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_account
    ADD CONSTRAINT app_account_pkey PRIMARY KEY (user_ptr_id);


--
-- Name: app_address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_address
    ADD CONSTRAINT app_address_pkey PRIMARY KEY (id);


--
-- Name: app_assessmenttest_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_assessmenttest
    ADD CONSTRAINT app_assessmenttest_pkey PRIMARY KEY (id);


--
-- Name: app_cart_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_cart
    ADD CONSTRAINT app_cart_pkey PRIMARY KEY (id);


--
-- Name: app_category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_category
    ADD CONSTRAINT app_category_pkey PRIMARY KEY (id);


--
-- Name: app_certification_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_certification
    ADD CONSTRAINT app_certification_pkey PRIMARY KEY (id);


--
-- Name: app_city_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_city
    ADD CONSTRAINT app_city_pkey PRIMARY KEY (id);


--
-- Name: app_country_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_country
    ADD CONSTRAINT app_country_pkey PRIMARY KEY (id);


--
-- Name: app_education_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_education
    ADD CONSTRAINT app_education_pkey PRIMARY KEY (id);


--
-- Name: app_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_item
    ADD CONSTRAINT app_item_pkey PRIMARY KEY (id);


--
-- Name: app_onboarding_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_onboarding
    ADD CONSTRAINT app_onboarding_pkey PRIMARY KEY (id);


--
-- Name: app_order_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_order
    ADD CONSTRAINT app_order_pkey PRIMARY KEY (id);


--
-- Name: app_order_reference_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_order
    ADD CONSTRAINT app_order_reference_code_key UNIQUE (reference_code);


--
-- Name: app_orderitem_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_orderitem
    ADD CONSTRAINT app_orderitem_pkey PRIMARY KEY (id);


--
-- Name: app_orderstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_orderstatus
    ADD CONSTRAINT app_orderstatus_pkey PRIMARY KEY (id);


--
-- Name: app_paymentstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_paymentstatus
    ADD CONSTRAINT app_paymentstatus_pkey PRIMARY KEY (id);


--
-- Name: app_plantype_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_plantype
    ADD CONSTRAINT app_plantype_pkey PRIMARY KEY (id);


--
-- Name: app_product_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_product_categories
    ADD CONSTRAINT app_product_categories_pkey PRIMARY KEY (id);


--
-- Name: app_product_categories_product_id_category_id_a24a34e5_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_product_categories
    ADD CONSTRAINT app_product_categories_product_id_category_id_a24a34e5_uniq UNIQUE (product_id, category_id);


--
-- Name: app_product_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_product
    ADD CONSTRAINT app_product_pkey PRIMARY KEY (id);


--
-- Name: app_qualification_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_qualification
    ADD CONSTRAINT app_qualification_code_key UNIQUE (code);


--
-- Name: app_qualification_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_qualification
    ADD CONSTRAINT app_qualification_pkey PRIMARY KEY (id);


--
-- Name: app_question_number_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_question
    ADD CONSTRAINT app_question_number_key UNIQUE (number);


--
-- Name: app_question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_question
    ADD CONSTRAINT app_question_pkey PRIMARY KEY (id);


--
-- Name: app_question_responses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_question_responses
    ADD CONSTRAINT app_question_responses_pkey PRIMARY KEY (id);


--
-- Name: app_question_responses_question_id_questionresp_267decfa_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_question_responses
    ADD CONSTRAINT app_question_responses_question_id_questionresp_267decfa_uniq UNIQUE (question_id, questionresponse_id);


--
-- Name: app_questionresponse_name_3f504edd_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_questionresponse
    ADD CONSTRAINT app_questionresponse_name_3f504edd_uniq UNIQUE (name);


--
-- Name: app_questionresponse_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_questionresponse
    ADD CONSTRAINT app_questionresponse_pkey PRIMARY KEY (id);


--
-- Name: app_ratingentry_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_ratingentry
    ADD CONSTRAINT app_ratingentry_pkey PRIMARY KEY (id);


--
-- Name: app_resources_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_resources
    ADD CONSTRAINT app_resources_pkey PRIMARY KEY (id);


--
-- Name: app_review_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_review
    ADD CONSTRAINT app_review_pkey PRIMARY KEY (id);


--
-- Name: app_school_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_school
    ADD CONSTRAINT app_school_pkey PRIMARY KEY (id);


--
-- Name: app_sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_sessions
    ADD CONSTRAINT app_sessions_pkey PRIMARY KEY (id);


--
-- Name: app_state_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_state
    ADD CONSTRAINT app_state_pkey PRIMARY KEY (id);


--
-- Name: app_testimonial_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_testimonial
    ADD CONSTRAINT app_testimonial_pkey PRIMARY KEY (id);


--
-- Name: app_therapist_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_therapist
    ADD CONSTRAINT app_therapist_pkey PRIMARY KEY (id);


--
-- Name: app_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_transaction
    ADD CONSTRAINT app_transaction_pkey PRIMARY KEY (id);


--
-- Name: app_transactionstatus_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_transactionstatus
    ADD CONSTRAINT app_transactionstatus_pkey PRIMARY KEY (id);


--
-- Name: app_userassessment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_userassessment
    ADD CONSTRAINT app_userassessment_pkey PRIMARY KEY (id);


--
-- Name: app_userresponse_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_userresponse
    ADD CONSTRAINT app_userresponse_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: authtoken_token_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_pkey PRIMARY KEY (key);


--
-- Name: authtoken_token_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_key UNIQUE (user_id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: jet_bookmark_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jet_bookmark
    ADD CONSTRAINT jet_bookmark_pkey PRIMARY KEY (id);


--
-- Name: jet_pinnedapplication_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jet_pinnedapplication
    ADD CONSTRAINT jet_pinnedapplication_pkey PRIMARY KEY (id);


--
-- Name: app_account_address_id_b9866ce8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_account_address_id_b9866ce8 ON public.app_account USING btree (address_id);


--
-- Name: app_address_city_id_8f9cbaf4; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_address_city_id_8f9cbaf4 ON public.app_address USING btree (city_id);


--
-- Name: app_address_country_id_b94a1d67; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_address_country_id_b94a1d67 ON public.app_address USING btree (country_id);


--
-- Name: app_address_state_id_bdd54851; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_address_state_id_bdd54851 ON public.app_address USING btree (state_id);


--
-- Name: app_assessmenttest_slug_34aee1e8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_assessmenttest_slug_34aee1e8 ON public.app_assessmenttest USING btree (slug);


--
-- Name: app_assessmenttest_slug_34aee1e8_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_assessmenttest_slug_34aee1e8_like ON public.app_assessmenttest USING btree (slug varchar_pattern_ops);


--
-- Name: app_cart_user_id_2bf07879; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_cart_user_id_2bf07879 ON public.app_cart USING btree (user_id);


--
-- Name: app_category_slug_b059cea8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_category_slug_b059cea8 ON public.app_category USING btree (slug);


--
-- Name: app_category_slug_b059cea8_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_category_slug_b059cea8_like ON public.app_category USING btree (slug varchar_pattern_ops);


--
-- Name: app_certification_therapist_id_314b8774; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_certification_therapist_id_314b8774 ON public.app_certification USING btree (therapist_id);


--
-- Name: app_city_state_id_1840ad3e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_city_state_id_1840ad3e ON public.app_city USING btree (state_id);


--
-- Name: app_education_school_id_0e096800; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_education_school_id_0e096800 ON public.app_education USING btree (school_id);


--
-- Name: app_item_cart_id_ba6a442d; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_item_cart_id_ba6a442d ON public.app_item USING btree (cart_id);


--
-- Name: app_item_product_id_f0f250e6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_item_product_id_f0f250e6 ON public.app_item USING btree (product_id);


--
-- Name: app_item_user_id_cfd2d514; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_item_user_id_cfd2d514 ON public.app_item USING btree (user_id);


--
-- Name: app_onboarding_user_id_6fa4e5c6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_onboarding_user_id_6fa4e5c6 ON public.app_onboarding USING btree (user_id);


--
-- Name: app_order_order_status_id_e6d81983; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_order_order_status_id_e6d81983 ON public.app_order USING btree (order_status_id);


--
-- Name: app_order_payment_status_id_49bd7ca2; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_order_payment_status_id_49bd7ca2 ON public.app_order USING btree (payment_status_id);


--
-- Name: app_order_reference_code_da5d3687_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_order_reference_code_da5d3687_like ON public.app_order USING btree (reference_code varchar_pattern_ops);


--
-- Name: app_orderitem_order_id_41257a1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_orderitem_order_id_41257a1b ON public.app_orderitem USING btree (order_id);


--
-- Name: app_orderitem_product_id_5f40ddb0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_orderitem_product_id_5f40ddb0 ON public.app_orderitem USING btree (product_id);


--
-- Name: app_orderitem_user_id_32e31a66; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_orderitem_user_id_32e31a66 ON public.app_orderitem USING btree (user_id);


--
-- Name: app_product_categories_category_id_ccb5369c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_product_categories_category_id_ccb5369c ON public.app_product_categories USING btree (category_id);


--
-- Name: app_product_categories_product_id_6af6455e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_product_categories_product_id_6af6455e ON public.app_product_categories USING btree (product_id);


--
-- Name: app_qualification_code_3487f524_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_qualification_code_3487f524_like ON public.app_qualification USING btree (code varchar_pattern_ops);


--
-- Name: app_question_assessment_test_id_bd8be2da; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_question_assessment_test_id_bd8be2da ON public.app_question USING btree (assessment_test_id);


--
-- Name: app_question_responses_question_id_8ff5bbf8; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_question_responses_question_id_8ff5bbf8 ON public.app_question_responses USING btree (question_id);


--
-- Name: app_question_responses_questionresponse_id_9c811b09; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_question_responses_questionresponse_id_9c811b09 ON public.app_question_responses USING btree (questionresponse_id);


--
-- Name: app_questionresponse_name_3f504edd_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_questionresponse_name_3f504edd_like ON public.app_questionresponse USING btree (name varchar_pattern_ops);


--
-- Name: app_ratingentry_therapist_id_839000c5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_ratingentry_therapist_id_839000c5 ON public.app_ratingentry USING btree (therapist_id);


--
-- Name: app_resources_order_id_74919c9f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_resources_order_id_74919c9f ON public.app_resources USING btree (order_id);


--
-- Name: app_resources_user_id_af71e052; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_resources_user_id_af71e052 ON public.app_resources USING btree (user_id);


--
-- Name: app_review_therapist_id_0fbd0f9f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_review_therapist_id_0fbd0f9f ON public.app_review USING btree (therapist_id);


--
-- Name: app_school_country_id_8879331c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_school_country_id_8879331c ON public.app_school USING btree (country_id);


--
-- Name: app_school_state_id_628d1bdf; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_school_state_id_628d1bdf ON public.app_school USING btree (state_id);


--
-- Name: app_sessions_therapist_id_98a50410; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_sessions_therapist_id_98a50410 ON public.app_sessions USING btree (therapist_id);


--
-- Name: app_sessions_user_id_5b1d1a4f; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_sessions_user_id_5b1d1a4f ON public.app_sessions USING btree (user_id);


--
-- Name: app_state_country_id_fc8513d5; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_state_country_id_fc8513d5 ON public.app_state USING btree (country_id);


--
-- Name: app_therapist_address_id_05ee1cb1; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_therapist_address_id_05ee1cb1 ON public.app_therapist USING btree (address_id);


--
-- Name: app_therapist_qualification_id_21386ac0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_therapist_qualification_id_21386ac0 ON public.app_therapist USING btree (qualification_id);


--
-- Name: app_transaction_order_id_6bfb692a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_transaction_order_id_6bfb692a ON public.app_transaction USING btree (order_id);


--
-- Name: app_transaction_transaction_status_id_a3a07c09; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_transaction_transaction_status_id_a3a07c09 ON public.app_transaction USING btree (transaction_status_id);


--
-- Name: app_transaction_user_id_489f1ae7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_transaction_user_id_489f1ae7 ON public.app_transaction USING btree (user_id);


--
-- Name: app_userassessment_user_id_60754308; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_userassessment_user_id_60754308 ON public.app_userassessment USING btree (user_id);


--
-- Name: app_userresponse_assessment_id_cc386139; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_userresponse_assessment_id_cc386139 ON public.app_userresponse USING btree (assessment_id);


--
-- Name: app_userresponse_user_id_88d9b04a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX app_userresponse_user_id_88d9b04a ON public.app_userresponse USING btree (user_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: authtoken_token_key_10f0b77e_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX authtoken_token_key_10f0b77e_like ON public.authtoken_token USING btree (key varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_site_domain_a2e37b91_like ON public.django_site USING btree (domain varchar_pattern_ops);


--
-- Name: app_account_address_id_b9866ce8_fk_app_address_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_account
    ADD CONSTRAINT app_account_address_id_b9866ce8_fk_app_address_id FOREIGN KEY (address_id) REFERENCES public.app_address(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_account_user_ptr_id_19984d3e_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_account
    ADD CONSTRAINT app_account_user_ptr_id_19984d3e_fk_auth_user_id FOREIGN KEY (user_ptr_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_address_city_id_8f9cbaf4_fk_app_city_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_address
    ADD CONSTRAINT app_address_city_id_8f9cbaf4_fk_app_city_id FOREIGN KEY (city_id) REFERENCES public.app_city(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_address_country_id_b94a1d67_fk_app_country_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_address
    ADD CONSTRAINT app_address_country_id_b94a1d67_fk_app_country_id FOREIGN KEY (country_id) REFERENCES public.app_country(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_address_state_id_bdd54851_fk_app_state_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_address
    ADD CONSTRAINT app_address_state_id_bdd54851_fk_app_state_id FOREIGN KEY (state_id) REFERENCES public.app_state(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_cart_user_id_2bf07879_fk_app_account_user_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_cart
    ADD CONSTRAINT app_cart_user_id_2bf07879_fk_app_account_user_ptr_id FOREIGN KEY (user_id) REFERENCES public.app_account(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_city_state_id_1840ad3e_fk_app_state_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_city
    ADD CONSTRAINT app_city_state_id_1840ad3e_fk_app_state_id FOREIGN KEY (state_id) REFERENCES public.app_state(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_education_school_id_0e096800_fk_app_school_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_education
    ADD CONSTRAINT app_education_school_id_0e096800_fk_app_school_id FOREIGN KEY (school_id) REFERENCES public.app_school(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_item_cart_id_ba6a442d_fk_app_cart_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_item
    ADD CONSTRAINT app_item_cart_id_ba6a442d_fk_app_cart_id FOREIGN KEY (cart_id) REFERENCES public.app_cart(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_item_product_id_f0f250e6_fk_app_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_item
    ADD CONSTRAINT app_item_product_id_f0f250e6_fk_app_product_id FOREIGN KEY (product_id) REFERENCES public.app_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_item_user_id_cfd2d514_fk_app_account_user_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_item
    ADD CONSTRAINT app_item_user_id_cfd2d514_fk_app_account_user_ptr_id FOREIGN KEY (user_id) REFERENCES public.app_account(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_onboarding_user_id_6fa4e5c6_fk_app_account_user_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_onboarding
    ADD CONSTRAINT app_onboarding_user_id_6fa4e5c6_fk_app_account_user_ptr_id FOREIGN KEY (user_id) REFERENCES public.app_account(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_order_order_status_id_e6d81983_fk_app_orderstatus_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_order
    ADD CONSTRAINT app_order_order_status_id_e6d81983_fk_app_orderstatus_id FOREIGN KEY (order_status_id) REFERENCES public.app_orderstatus(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_order_payment_status_id_49bd7ca2_fk_app_paymentstatus_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_order
    ADD CONSTRAINT app_order_payment_status_id_49bd7ca2_fk_app_paymentstatus_id FOREIGN KEY (payment_status_id) REFERENCES public.app_paymentstatus(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_orderitem_order_id_41257a1b_fk_app_order_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_orderitem
    ADD CONSTRAINT app_orderitem_order_id_41257a1b_fk_app_order_id FOREIGN KEY (order_id) REFERENCES public.app_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_orderitem_product_id_5f40ddb0_fk_app_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_orderitem
    ADD CONSTRAINT app_orderitem_product_id_5f40ddb0_fk_app_product_id FOREIGN KEY (product_id) REFERENCES public.app_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_orderitem_user_id_32e31a66_fk_app_account_user_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_orderitem
    ADD CONSTRAINT app_orderitem_user_id_32e31a66_fk_app_account_user_ptr_id FOREIGN KEY (user_id) REFERENCES public.app_account(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_product_categories_category_id_ccb5369c_fk_app_category_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_product_categories
    ADD CONSTRAINT app_product_categories_category_id_ccb5369c_fk_app_category_id FOREIGN KEY (category_id) REFERENCES public.app_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_product_categories_product_id_6af6455e_fk_app_product_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_product_categories
    ADD CONSTRAINT app_product_categories_product_id_6af6455e_fk_app_product_id FOREIGN KEY (product_id) REFERENCES public.app_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_question_assessment_test_id_bd8be2da_fk_app_asses; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_question
    ADD CONSTRAINT app_question_assessment_test_id_bd8be2da_fk_app_asses FOREIGN KEY (assessment_test_id) REFERENCES public.app_assessmenttest(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_question_respons_questionresponse_id_9c811b09_fk_app_quest; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_question_responses
    ADD CONSTRAINT app_question_respons_questionresponse_id_9c811b09_fk_app_quest FOREIGN KEY (questionresponse_id) REFERENCES public.app_questionresponse(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_question_responses_question_id_8ff5bbf8_fk_app_question_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_question_responses
    ADD CONSTRAINT app_question_responses_question_id_8ff5bbf8_fk_app_question_id FOREIGN KEY (question_id) REFERENCES public.app_question(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_ratingentry_therapist_id_839000c5_fk_app_therapist_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_ratingentry
    ADD CONSTRAINT app_ratingentry_therapist_id_839000c5_fk_app_therapist_id FOREIGN KEY (therapist_id) REFERENCES public.app_therapist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_resources_order_id_74919c9f_fk_app_order_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_resources
    ADD CONSTRAINT app_resources_order_id_74919c9f_fk_app_order_id FOREIGN KEY (order_id) REFERENCES public.app_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_resources_user_id_af71e052_fk_app_account_user_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_resources
    ADD CONSTRAINT app_resources_user_id_af71e052_fk_app_account_user_ptr_id FOREIGN KEY (user_id) REFERENCES public.app_account(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_review_therapist_id_0fbd0f9f_fk_app_therapist_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_review
    ADD CONSTRAINT app_review_therapist_id_0fbd0f9f_fk_app_therapist_id FOREIGN KEY (therapist_id) REFERENCES public.app_therapist(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_school_country_id_8879331c_fk_app_country_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_school
    ADD CONSTRAINT app_school_country_id_8879331c_fk_app_country_id FOREIGN KEY (country_id) REFERENCES public.app_country(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_school_state_id_628d1bdf_fk_app_state_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_school
    ADD CONSTRAINT app_school_state_id_628d1bdf_fk_app_state_id FOREIGN KEY (state_id) REFERENCES public.app_state(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_sessions_user_id_5b1d1a4f_fk_app_account_user_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_sessions
    ADD CONSTRAINT app_sessions_user_id_5b1d1a4f_fk_app_account_user_ptr_id FOREIGN KEY (user_id) REFERENCES public.app_account(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_state_country_id_fc8513d5_fk_app_country_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_state
    ADD CONSTRAINT app_state_country_id_fc8513d5_fk_app_country_id FOREIGN KEY (country_id) REFERENCES public.app_country(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_therapist_address_id_05ee1cb1_fk_app_address_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_therapist
    ADD CONSTRAINT app_therapist_address_id_05ee1cb1_fk_app_address_id FOREIGN KEY (address_id) REFERENCES public.app_address(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_therapist_qualification_id_21386ac0_fk_app_qualification_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_therapist
    ADD CONSTRAINT app_therapist_qualification_id_21386ac0_fk_app_qualification_id FOREIGN KEY (qualification_id) REFERENCES public.app_qualification(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_transaction_order_id_6bfb692a_fk_app_order_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_transaction
    ADD CONSTRAINT app_transaction_order_id_6bfb692a_fk_app_order_id FOREIGN KEY (order_id) REFERENCES public.app_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_transaction_transaction_status_i_a3a07c09_fk_app_trans; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_transaction
    ADD CONSTRAINT app_transaction_transaction_status_i_a3a07c09_fk_app_trans FOREIGN KEY (transaction_status_id) REFERENCES public.app_transactionstatus(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_transaction_user_id_489f1ae7_fk_app_account_user_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_transaction
    ADD CONSTRAINT app_transaction_user_id_489f1ae7_fk_app_account_user_ptr_id FOREIGN KEY (user_id) REFERENCES public.app_account(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_userassessment_user_id_60754308_fk_app_account_user_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_userassessment
    ADD CONSTRAINT app_userassessment_user_id_60754308_fk_app_account_user_ptr_id FOREIGN KEY (user_id) REFERENCES public.app_account(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_userresponse_assessment_id_cc386139_fk_app_usera; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_userresponse
    ADD CONSTRAINT app_userresponse_assessment_id_cc386139_fk_app_usera FOREIGN KEY (assessment_id) REFERENCES public.app_userassessment(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: app_userresponse_user_id_88d9b04a_fk_app_account_user_ptr_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.app_userresponse
    ADD CONSTRAINT app_userresponse_user_id_88d9b04a_fk_app_account_user_ptr_id FOREIGN KEY (user_id) REFERENCES public.app_account(user_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: authtoken_token_user_id_35299eff_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.authtoken_token
    ADD CONSTRAINT authtoken_token_user_id_35299eff_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

