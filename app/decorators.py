from django.http import HttpResponseForbidden
from . import models


def is_authenticated_therapist(fxn):
    def wrap(request, *args, **kwargs):
        key = request.META.get('HTTP_X_AUTH_TOKEN')

        if not key:
            return HttpResponseForbidden()

        token = models.Token.objects.filter(key=key).first()

        if token and not token.is_expired:
            return fxn(request, token.therapist.id, token, *args, **kwargs)

        return HttpResponseForbidden()

    wrap.__name__ = fxn.__name__
    wrap.__doc__ = fxn.__doc__

    return wrap
