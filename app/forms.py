from django.forms import Form, ModelForm, EmailField, URLField, PasswordInput, CharField, FileField, \
    MultipleChoiceField, CheckboxSelectMultiple, IntegerField, ImageField, BooleanField, FloatField, DateField
from django.db.models import Q

from s3direct.widgets import S3DirectWidget

from . import models


class LoginForm(Form):
    """
    user authentication form
    """
    username = CharField(required=True)
    password = CharField(required=True, widget=PasswordInput)


class RegistrationForm(Form):
    """
    user registration form
    """
    # full_name = CharField(required=True)
    username = CharField(required=True)
    email = EmailField(required=True)
    password = CharField(required=True, widget=PasswordInput)

    def clean(self):
        """
        validate email address and username
        """
        cleaned_data = super(RegistrationForm, self).clean()

        if self.is_valid():
            username_exists = models.Account.objects.filter(Q(username=cleaned_data['username'])).exists()
            if username_exists:
                self._errors['username'] = self.error_class(['A user with this username already exists'])

            email_exists = models.Account.objects.filter(Q(email=cleaned_data['email'])).exists()
            if email_exists:
                self._errors['email'] = self.error_class(['A user with this email already exists'])

        return cleaned_data


class TherapistForm(Form):
    """
    therapist registration form
    """
    name = CharField(required=True)
    email = EmailField(required=True)
    password = CharField(required=True, widget=PasswordInput)
    bio = CharField(required=True)
    phone = CharField(required=True)
    line1 = CharField(required=True)
    line2 = CharField(required=False)
    # state = CharField(required=False)
    country = CharField(required=True)
    qualification = CharField(required=True)
    resume = FileField(required=True)
    profile_image = ImageField(required=True)
    cover_letter = CharField(required=True)
    que_1 = CharField(required=True)
    que_2 = CharField(required=True)
    que_3 = CharField(required=True)
    que_4 = CharField(required=True)
    que_5 = CharField(required=True)
    que_6 = CharField(required=True)
    que_7 = CharField(required=True)
    que_8 = CharField(required=True)
    que_9 = CharField(required=True)
    que_10 = CharField(required=True)
    que_11 = CharField(required=True)

    def clean(self):
        """
        validate email address and username
        """
        cleaned_data = super(TherapistForm, self).clean()

        if self.is_valid():

            if models.Therapist.objects.filter(email=cleaned_data['email']).exists():
                self._errors['email'] = self.error_class(['An account with this email already exists'])

            if models.Address.objects.filter(phone=cleaned_data['phone']).exists():
                self._errors['phone'] = self.error_class(['An address with this phone already exists'])

            # if not models.State.objects.filter(code=cleaned_data['state']).exists():
            #     self._errors['state'] = self.error_class(['Invalid state'])

            if not models.Country.objects.filter(code=cleaned_data['country']).exists():
                self._errors['country'] = self.error_class(['Invalid country'])

            if not models.Qualification.objects.filter(code=cleaned_data['qualification']).exists():
                self._errors['qualification'] = self.error_class(['Invalid qualification'])

        return cleaned_data


class EditTherapistForm(Form):
    """
    therapist registration form
    """
    name = CharField(required=True)
    bio = CharField(required=True)
    phone = CharField(required=True)
    line1 = CharField(required=True)
    line2 = CharField(required=False)
    city = CharField(required=False)
    state = CharField(required=False)
    country = CharField(required=True)
    qualification = CharField(required=True)
    profile_image = FileField(required=False)

    def clean(self):
        """
        validate email address and username
        """
        cleaned_data = super(EditTherapistForm, self).clean()

        if self.is_valid():

            if not models.State.objects.filter(name=cleaned_data['state']).exists():
                self._errors['state'] = self.error_class(['Invalid state'])

            if not models.Country.objects.filter(name=cleaned_data['country']).exists():
                self._errors['country'] = self.error_class(['Invalid country'])

            if not models.Qualification.objects.filter(code=cleaned_data['qualification']).exists():
                self._errors['qualification'] = self.error_class(['Invalid qualification'])

        return cleaned_data


class ProfileForm(Form):
    bio = CharField(required=True)


class QuestionForm(ModelForm):
    responses = MultipleChoiceField(choices=models.QuestionResponse.objects.all(), widget=CheckboxSelectMultiple(),
                                    required=True)


class CartItemForm(Form):
    product_id = IntegerField(required=True)
    quantity = IntegerField(required=True)
    cart_id = IntegerField(required=True)


class RemoveCartItemForm(Form):
    item_id = IntegerField(required=True)
    cart_id = IntegerField(required=True)


class SelectPlanForm(Form):
    """
    user therapy plan
    """
    plan_id = IntegerField(required=True)

    def clean(self):
        """
        validate email address and username
        """
        cleaned_data = super(SelectPlanForm, self).clean()

        if self.is_valid():
            if not models.Plan.objects.get(pk=cleaned_data['plan_id']):
                self._errors['plan_id'] = self.error_class(['Invalid plan'])

        return cleaned_data


class SelectTherapistForm(Form):
    """
    selecting therapist form
    """
    therapist_id = IntegerField(required=True)

    def clean(self):
        """
        validate email address and username
        """
        cleaned_data = super(SelectTherapistForm, self).clean()

        if self.is_valid():
            if not models.Therapist.objects.get(pk=cleaned_data['therapist_id']):
                self._errors['therapist_id'] = self.error_class(['Invalid therapist'])

        return cleaned_data


class TherapistLoginForm(Form):
    """
    user authentication form
    """
    email = CharField(required=True)
    password = CharField(required=True, widget=PasswordInput)


class ComposeMessageForm(Form):
    subject = CharField(required=True)
    body = CharField(required=True)
    therapist_id = IntegerField(required=True)
    sender = CharField(required=True)
    do_not_reply = BooleanField(required=False)
    recipient = CharField(required=True)
    user_id = IntegerField(required=True)
    username = CharField(required=True)
    thread_id = CharField(required=True)


class MessageProcessingForm(Form):
    """ Schema class for handling messages to clients from customers """
    user_id = IntegerField(required=True)
    therapist_id = IntegerField(required=False)
    username = CharField(required=True)
    subject = CharField(required=True)
    user_read = BooleanField(required=False)
    therapist_read = BooleanField(required=False)
    user_replied = BooleanField(required=False)
    therapist_replied = BooleanField(required=False)
    body = CharField(required=True)


class PreTherapyForm(Form):
    """ Schema class for handling messages to clients from customers """
    gender = CharField(required=True)
    age = IntegerField(required=True)
    previous_sessions = CharField(required=True)
    medication = CharField(required=True)
    struggles = CharField(required=True)
    suicide = CharField(required=True)
    connecting = CharField(required=True)
    reason = CharField(required=True)
    qualities = CharField(required=True)
    country = CharField(required=True)


QUALITIES_CHOICES = (
    ("Depression", "Depression"),
    ("Stress Disorders", "Stress Disorders"),
    ("Anxiety", "Anxiety"),
    ("Addiction", "Addiction"),
    ("Personality issues", "Personality issues"),
    ("Relationship and Dating issues", "Relationship and Dating issues"),
    ("Family and Parenting", "Family and Parenting"),
    ("Trauma and Abuse", "Trauma and Abuse"),
    ("Coping with Grief and Loss", "Coping with Grief and Loss"),
    ("Intimacy related issues", "Intimacy related issues"),
    ("Eating disorders", "Eating disorders"),
    ("Motivation, self-esteem and confidence", "Motivation, self-esteem and confidence"),
    ("Career difficulties", "Career difficulties"),
    ("Anger management", "Anger management"),
    ("Bipolar disorders", "Bipolar disorders"),
    ("Emotional wellness", "Emotional wellness")
)


class WebPreTherapyForm(PreTherapyForm):
    qualities = MultipleChoiceField(required=True, choices=QUALITIES_CHOICES)


class ChangePlanForm(Form):
    duration = IntegerField(required=True)


class RatingForm(Form):
    therapist_id = IntegerField(required=True)
    rating = IntegerField(required=True)


class ReviewForm(Form):
    therapist_id = IntegerField(required=True)
    title = CharField(required=True)
    body = CharField(required=True)


class TherapistBillingSettingsForm(Form):
    subaccount_code = CharField(required=True)
    settlement_schedule = CharField(required=True)
    settlement_bank = CharField(required=True)
    payment_gateway_id = IntegerField(required=True)
    payment_gateway_integration = IntegerField(required=True)
    business_name = CharField(required=True)
    percentage_charge = FloatField(required=True)


class ContactForm(Form):
    subject = CharField(required=True)
    name = CharField(required=True)
    email = EmailField(required=True)
    phone = CharField(required=False)
    message = CharField(required=True)


class SubscriptionTransactionForm(Form):
    amount = FloatField(required=True)
    duration = IntegerField(required=True)
    start_date = DateField(required=True)


class CloseSubTransactionForm(Form):
    transaction_reference = CharField(required=True)
    tx_ref = CharField(required=True)
    status = CharField(required=True)


class CloseTransactionForm(Form):
    transaction_reference = CharField(required=True)
    status = CharField(required=True)
    tx_ref = CharField(required=True)
    cart_id = IntegerField(required=True)


class AssessmentTestForm(Form):
    assessment_test_id = IntegerField(required=True)
    responses = CharField(required=True)


class AddHabitForm(Form):
    name = CharField(required=True)
    start_date = DateField(required=True)
    target_days = IntegerField(required=True)
    positive_response = CharField(required=True)
    negative_response = CharField(required=True)


class RestartHabitForm(Form):
    start_date = DateField(required=True)
    target_days = IntegerField(required=True)


class HabitCheckInForm(Form):
    habit_id = IntegerField(required=True)
    positive = CharField(required=True)
    negative = CharField(required=True)


class NotificationSettingsForm(Form):
    player_id = CharField(required=True)
    push_token = CharField(required=True)


class FeedBackForm(Form):
    pass


class HabitCompanionForm(Form):
    email = EmailField(required=True)


class QuitTherapyForm(Form):
    body = CharField(required=False)
    send_therapist = BooleanField(required=False)


class ChangeTherapistForm(Form):
    body = CharField(required=False)
    send_therapist = BooleanField(required=False)
