from django_assets import Bundle, register
css = Bundle('static/css/*', filters='cssmin', output='gen/packed.css')
register('css_all', css)