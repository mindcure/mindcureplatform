from datetime import date, timedelta, datetime
import random
import os

from django.utils.crypto import get_random_string
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings
from django.db.models import Q

import celery
from celery.schedules import crontab

import models

from services import common, mongo
from mindcure import celery_app


@celery_app.task
def send_mail_method(subject, message, from_email, recipient_list, fail_silently=False, html_message=None):
    """
    send mail helper function
    :param subject:
    :param message:
    :param from_email:
    :param recipient_list:
    :param fail_silently:
    :param html_message:
    :return:
    """
    # send email
    return send_mail(subject, message, from_email, recipient_list, html_message=html_message)


# celery task
@celery_app.task(name='contact_new_user')
def contact_new_user(user_id):
    """
    send welcome mail
    send new message to new user
    :param user_id:
    :return:
    """

    try:
        user = models.Account.objects.get(pk=user_id)

        # send welcome email to new user
        send_verification_mail(user.id)

        # send message to newly registered user
        send_message_new_user(user_id=user.id)

    except models.Account.DoesNotExist:
        raise


def send_message_new_user(user_id):
    """
    send message to new user
    :param user_id:
    :return:
    """
    body = ''
    common.send_message(subject='Welcome to MiNDCuRE', body=body, user_id=user_id, user_read=False)

    return True


@celery_app.task(name='contact_new_therapist')
def send_mail_new_therapist(therapist_id):
    """
    :param therapist_id
    :return:
    """
    try:
        therapist = models.Therapist.objects.get(pk=therapist_id)

        token = models.Token.objects.filter(expires__gte=datetime.today(), is_verification=True,
                                            therapist_id=therapist.id).first()

        if not token:
            expires = datetime.today() + timedelta(days=2)
            token = models.Token.objects.create(key=get_random_string(40), therapist=therapist, expires=expires,
                                                is_verification=True)

        year = date.today().year

        domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

        welcome_message = render_to_string('emails/therapists/welcome.html', locals())

        to_email = [therapist.email]

        # if settings.DEBUG is True:
        #     to_email = [settings.TEST_EMAIL]

        send_mail('MiNDCuRE Therapist Registration', welcome_message, settings.TEAM_EMAIL, to_email,
                  html_message=welcome_message)

        return True

    except models.Therapist.DoesNotExist:
        raise


@celery_app.task(name='contact_verified_therapist')
def send_mail_therapist_verified(therapist_id):
    """
    send mail to verified therapist
    :param therapist_id:
    :return:
    """
    try:
        user = models.Therapist.objects.get(pk=therapist_id)

        year = date.today().year

        domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

        message = render_to_string('emails/therapists/verified.html', locals())

        to_email = [user.email]

        # if settings.DEBUG is True:
        #     to_email = [settings.TEST_EMAIL]

        send_mail('Your MiNDCuRE Therapist Dashboard', message, settings.TEAM_EMAIL, to_email, html_message=message)

        return True

    except models.Therapist.DoesNotExist:
        raise


def send_mail_therapist_first_reminder(therapist_id):
    """
    send first reminder to unverified therapist
    :param therapist_id:
    :return:
    """
    try:
        user = models.Therapist.objects.get(pk=therapist_id)

        year = date.today().year

        domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

        testimonies = models.Testimonial.objects.filter_by(is_therapist=True).all()

        message = render_to_string('emails/therapists/first_reminder.html', locals())

        to_email = [user.email]

        if settings.DEBUG is True:
            to_email = [settings.TEST_EMAIL]

        send_mail('Other therapists want you in!', message, settings.TEAM_EMAIL, to_email, html_message=message)

        return True

    except models.Therapist.DoesNotExist:
        raise


def send_verification_mail(user_id):
    """
    send verification mail to user
    :param user_id:
    :return:
    """
    try:
        user = models.Account.objects.get(pk=user_id)

        token = models.AuthorizationToken.objects.filter(is_expired=False, is_verification=True,
                                                         user_id=user.id).first()

        if not token:
            token = models.AuthorizationToken.objects.create(code=get_random_string(40), user=user)

        year = date.today().year

        domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

        verification_message = render_to_string('emails/users/verification.html', locals())

        to_email = [user.email]

        # if settings.DEBUG is True:
        #     to_email = [settings.TEST_EMAIL]

        send_mail('Your MiNDCuRE Registration', verification_message, settings.TEAM_EMAIL, to_email,
                  html_message=verification_message)

        return True

    except models.Account.DoesNotExist:
        raise


@celery.decorators.periodic_task(run_every=crontab(hour=00, minute=00), name="send_pre_therapy_reminder", ignore_result=True)
def send_pre_therapy_reminder(user_id):
    """
    send pre-therapy mail to user
    :param user_id:
    :return:
    """
    try:
        profiles = models.Profile.objects.filter((~Q(plan_step_completed=False) | ~Q(therapist_step_completed=False)),
                                                 pre_therapy_step_completed=True).all()

        user = models.Account.objects.get(pk=user_id)

        pre_therapy_responses = models.UserPreTherapyResponse.objects.filter(user=user).all()

        if pre_therapy_responses:

            quality_response = next(c for c in pre_therapy_responses if c.code == 'qualities')
            country_response = next(c for c in pre_therapy_responses if c.code == 'country')

            qualities = quality_response.response.split(',')

            therapist = models.Therapist.objects.filter(address__country__name=country_response.response,
                                                        categories__title__in=qualities).order_by('?').first()

            if not therapist:
                therapist = models.Therapist.objects.filter(address__country__name=country_response.response).order_by(
                    '?').first()

                if not therapist:
                    therapist = models.Therapist.objects.order_by('rating').first()
        else:
            therapist = models.Therapist.objects.order_by('rating').first()

        year = date.today().year

        domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

        message = render_to_string('emails/users/pretherapy.html', locals())

        to_email = [user.email]

        if settings.DEBUG is True:
            to_email = [settings.TEST_EMAIL]

        send_mail('{} sent you a message'.format(therapist.name), message, settings.TEAM_EMAIL, to_email,
                  html_message=message)

        return True

    except models.Account.DoesNotExist:
        raise
    except models.Therapist.DoesNotExist:
        raise
    except Exception:
        raise


# @celery.decorators.periodic_task(run_every=crontab(hour=00, minute=30), name="send_first_reminder", ignore_result=True)
def send_first_reminder():
    """
    send first reminder mail to user
    :param user_id:
    :return:
    """
    try:
        week_ago = date.today() - timedelta(days=7)
        onboardings = models.OnBoarding.objects.filter((~Q(pre_therapy_step_completed=False) |
                                                        ~Q(plan_step_completed=False) |
                                                        ~Q(therapist_step_completed=False)),
                                                       created_at__date=week_ago).all()

        for profile in onboardings:

            last_sent = mongo.MongoService.retrieve_item('reminders', **{'reminder_type': 3,
                                                                         'user_id': profile.user_id})

            if not last_sent or (date.today() - last_sent.date()).days <= 7:
                continue

            user = models.Account.objects.get(pk=profile.user_id)

            year = date.today().year

            domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

            message = render_to_string('emails/users/first_reminder.html', locals())

            to_email = [user.email]

            if settings.DEBUG is True:
                to_email = [settings.TEST_EMAIL]

            send_mail('MiNDCuRE Listens. Get Help Today', message, settings.TEAM_EMAIL, to_email,
                      html_message=message)

            mongo.MongoService.record_data('reminders', profile.user_id, datetime.now(),
                                           **{'reminder_type': 1, 'user_id': profile.user_id})

        return True

    except models.Account.DoesNotExist:
        raise
    except Exception:
        raise


# @celery.decorators.periodic_task(run_every=crontab(hour=1, minute=00), name="send_second_reminder", ignore_result=True)
def send_second_reminder():
    """
    send second reminder mail to user
    :param user_id:
    :return:
    """
    try:
        week_start = date.today() - timedelta(days=14)
        week_end = date.today() - timedelta(days=8)
        onboardings = models.OnBoarding.objects.filter((~Q(pre_therapy_step_completed=False) |
                                                        ~Q(plan_step_completed=False) |
                                                        ~Q(therapist_step_completed=False)),
                                                       created_at__date_gte=week_start).all()

        for profile in onboardings:

            last_sent = mongo.MongoService.retrieve_item('reminders', **{'reminder_type': 2,
                                                                         'user_id': profile.user_id})

            if not last_sent or (date.today() - last_sent.date()).days <= 14:
                continue

            user = models.Account.objects.get(pk=profile.user_id)

            year = date.today().year

            domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

            testimonies = models.Testimonial.objects.all()

            message = render_to_string('emails/users/second_reminder.html', locals())

            to_email = [user.email]

            if settings.DEBUG is True:
                to_email = [settings.TEST_EMAIL]

            send_mail('Hear what others say!', message, settings.TEAM_EMAIL, to_email,
                      html_message=message)

            mongo.MongoService.record_data('reminders', profile.user_id, datetime.now(),
                                           **{'reminder_type': 2, 'user_id': profile.user_id})

        return True

    except models.Account.DoesNotExist:
        raise
    except Exception:
        raise


# @celery.decorators.periodic_task(run_every=crontab(hour=1, minute=30), name="send_third_reminder", ignore_result=True)
def send_third_reminder():
    """
    send third reminder mail to user
    :return:
    """
    try:
        week_start = date.today() - timedelta(days=21)
        week_end = date.today() - timedelta(days=15)
        onboardings = models.OnBoarding.objects.filter((~Q(pre_therapy_step_completed=False) |
                                                        ~Q(plan_step_completed=False) |
                                                        ~Q(therapist_step_completed=False)),
                                                       created_at__date_gte=week_start,
                                                       created_at__date_lte=week_end).all()

        for profile in onboardings:

            last_sent = mongo.MongoService.retrieve_item('reminders', **{'reminder_type': 3,
                                                                         'user_id': profile.user_id})

            if not last_sent or (date.today() - last_sent.date()).days <= 30:
                continue

            user = models.Account.objects.get(pk=profile.user_id)

            year = date.today().year

            domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

            random.sample(list(models.Therapist.objects.all()), k=3)

            message = render_to_string('emails/users/third_reminder.html', locals())

            to_email = [user.email]

            if settings.DEBUG is True:
                to_email = [settings.TEST_EMAIL]

            send_mail('Your therapist is interested in You!', message, settings.TEAM_EMAIL, to_email,
                      html_message=message)

            mongo.MongoService.record_data('reminders', profile.user_id, datetime.now(),
                                           **{'reminder_type': 3, 'user_id': profile.user_id})

        return True

    except models.Account.DoesNotExist:
        raise
    except Exception:
        raise


@celery_app.task(name='contact_therapist_therapy_termination')
def send_mail_therapist_therapy_terminated(therapist_id, user_id, feedback_body, send_to_therapist=False):
    """
    send mail to therapist
    after user terminates therapy
    :param therapist_id:
    :param feedback_id:
    :return:
    """
    try:
        therapist = models.Therapist.objects.get(pk=therapist_id)

        user = models.Account.objects.get(pk=user_id)

        year = date.today().year

        domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

        message = render_to_string('emails/therapists/therapy_terminated.html', locals())

        to_email = [therapist.email]

        send_mail('{} Therapy Session Terminated'.format(user.username), message, settings.TEAM_EMAIL, to_email, html_message=message)

        # send to admin
        admin_message = render_to_string('emails/admin_therapy_terminated.html', locals())
        send_mail('{} Therapy Session Terminated'.format(user.username), admin_message, settings.TEAM_EMAIL, [settings.TEAM_EMAIL], html_message=admin_message)

        return True

    except models.Therapist.DoesNotExist:
        raise e
    except models.TherapyFeedback.DoesNotExist as e:
        raise e 


@celery_app.task(name='contact_therapist_therapy_termination')
def add_habit_companion(habit_id, email):
    """
    add habit companion
    :param habit_id:
    :param email:
    :return:
    """
    try:
        habit = models.Habit.objects.get(pk=habit_id)

        user = habit.user

        year = date.today().year

        domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

        expires = datetime.today() + timedelta(days=2)
        token = models.CompanionToken.objects.create(key=get_random_string(40), email=email, expires=expires, habit=habit)

        message = render_to_string('emails/users/add_habit_companion.html', locals())

        to_email = [email]

        send_mail('Habit Companion Request by {}'.format(user.username), message, settings.TEAM_EMAIL, to_email, html_message=message)
        return True

    except models.Habit.DoesNotExist as e:
        raise e


@celery_app.task(name='companion_request_declined')
def companion_request_declined(habit_id, email):
    """
    companion request declined
    :param habit_id:
    :param companion_id:
    :return:
    """
    try:
        habit = models.Habit.objects.get(pk=habit_id)

        user = habit.user

        year = date.today().year

        domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

        message = render_to_string('emails/users/companion_request_declined.html', locals())

        to_email = [user.email]

        send_mail('Habit Companion Request Declined by {}'.format(email), message, settings.TEAM_EMAIL, to_email, html_message=message)
        return True

    except models.Habit.DoesNotExist as e:
        raise e
    except models.Account.DoesNotExist as e:
        raise e


@celery_app.task(name='companion_request_accepted')
def companion_request_accepted(habit_id, companion_id):
    """
    companion request accepted
    :param habit_id:
    :param companion_id:
    :return:
    """
    try:
        habit = models.Habit.objects.get(pk=habit_id)

        user = habit.user

        companion = models.Account.objects.get(pk=companion_id)

        year = date.today().year

        domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

        message = render_to_string('emails/users/companion_request_accepted.html', locals())

        to_email = [user.email]

        send_mail('Habit Companion Request Accepted by {}'.format(companion.username), message, settings.TEAM_EMAIL, to_email, html_message=message)
        return True

    except models.Habit.DoesNotExist as e:
        raise e
    except models.Account.DoesNotExist as e:
        raise e


@celery.task(name='send_habit_report')
def send_habit_report(habit_id):
    """
    send habit tracking report to compnaion
    :param habit_id:
    :return:
    """
    try:
        habit = models.Habit.objects.get(pk=habit_id)

        today = date.today()

        year = today.year

        domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

        target_days = (habit.target_date - habit.start_date).days + 1

        negative_count = models.CheckIn.objects.filter(habit_id=habit.id, negative=True).count()
        positive_count = models.CheckIn.objects.filter(habit_id=habit.id, positive=True).count()

        total_days = (today - habit.start_date).days + 1 if habit.target_date > today else target_days

        not_checked = total_days - (positive_count + negative_count)

        success_percentage = positive_count / total_days * 100

        message = render_to_string('emails/users/habit_report.html', locals())

        to_email = [habit.companion.email]

        send_mail_method('{} Tracking progress report for {}'.format(habit.name, habit.user.username),
                                     message, settings.TEAM_EMAIL, to_email, html_message=message)

        return habit

    except models.Habit.DoesNotExist as e:
        raise e


@celery_app.task(name='send_mail_therapist_changed')
def send_mail_therapist_changed(therapist_id, user_id, feedback_body, send_to_therapist=False, old_therapist_id=None):
    """
    send mail to therapist
    after user terminates therapy
    :param therapist_id:
    :param old_therapist_id:
    :param user_id:
    :param feedback_body:
    :param send_to_therapist:
    :return:
    """
    try:
        therapist = models.Therapist.objects.get(pk=therapist_id)

        old_therapist = None

        if old_therapist_id:
            old_therapist = models.Therapist.objects.get(pk=old_therapist_id)

        user = models.Account.objects.get(pk=user_id)

        year = date.today().year

        domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

        # send to previous therapist
        if old_therapist:
            message = render_to_string('emails/therapists/therapist_changed.html', locals())
            send_mail('Therapist Changed by {}'.format(user.username), message, settings.TEAM_EMAIL, [old_therapist.email], html_message=message)

        # send to new therapist
        message = render_to_string('emails/therapists/new_user.html', locals())
        send_mail('{} has assigned you as his/her new Therapist'.format(user.username), message, settings.TEAM_EMAIL, [therapist.email], html_message=message)

        # send to admin
        admin_message = render_to_string('emails/therapist_changed.html', locals())
        send_mail('Therapist Changed by {}'.format(user.username), admin_message, settings.TEAM_EMAIL, [settings.TEAM_EMAIL], html_message=admin_message)

        return True

    except models.Therapist.DoesNotExist:
        raise e
    except models.TherapyFeedback.DoesNotExist as e:
        raise e 