from datetime import date, datetime, timedelta
import json

from django.contrib.auth import authenticate, views as auth_views
from django.core.paginator import PageNotAnInteger, Paginator, EmptyPage
from django.db.models import Q
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404

from rest_framework import status, response, decorators, authentication, permissions
from rest_framework.authtoken.models import Token

from . import forms, serializers, decorators as app_decorators, models
from .services import account, therapists, mongo, encoders, cart, assessment as assessment_service, \
    habits as habit_service, therapy as therapy_service

from . import tasks


@decorators.api_view(['POST'])
def authenticate_user(request):
    """
        authenticates user and generate authorization token
    """
    form = forms.LoginForm(request.data)

    if form.is_valid():
        form.cleaned_data['username'] = form.cleaned_data['username'].lower()
        user = authenticate(**form.cleaned_data)
        if not user:
            return response.Response({'error': {'message': 'Invalid username/password combination'}},
                                     status=status.HTTP_401_UNAUTHORIZED)

        if not user.account.is_verified:
            tasks.contact_new_user.delay(user.id)
            return response.Response({'error': {'message': 'Account not verified. Check your email for verification '
                                                           'link'}},
                                     status=status.HTTP_401_UNAUTHORIZED)

        if not models.Profile.objects.filter(user_id=user.account.id).exists():
            models.Profile.objects.create(user=user.account)

        if not user.account.onboarding.first():
            models.OnBoarding.objects.create(user=user.account)

        token, _ = Token.objects.get_or_create(user=user)

        api_response = response.Response({'token': token.key}, status=status.HTTP_200_OK)

        cookie_value = account.encode_jwt_token(user.account.id)
        max_age = 14 * 24 * 60 * 60  # two weeks
        expires = datetime.strftime(datetime.utcnow() + timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
        api_response.set_cookie('mindcure_community', cookie_value, httponly=True, max_age=max_age, expires=expires,
                                domain='mindcureglobal.com')

        # save user data in redis
        account.save_mobile_redis_user(token.key, user.account.id)

        return api_response

    return response.Response({'error': form.errors}, status=status.HTTP_400_BAD_REQUEST)


@decorators.api_view(['POST'])
def register_user(request):
    """
        register new user account
    """
    form = forms.RegistrationForm(request.data)

    if form.is_valid():
        user = account.register_account(**form.cleaned_data)

        if not models.Profile.objects.filter(user_id=user.id).exists():
            models.Profile.objects.create(user=user)

        if not user.onboarding.first():
            models.OnBoarding.objects.create(user=user)

        return response.Response({'data': {'message': "User with id - {} has been created".format(user.id)}},
                                 status=status.HTTP_201_CREATED)

    return response.Response({'error': form.errors}, status=status.HTTP_400_BAD_REQUEST)


@decorators.api_view(['DELETE'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def logout_user(request):
    """
    logout user via api
    :param request:
    :return:
    """
    token = request.auth
    account.clear_redis_user('user:m_{}'.format(request.user.username), 'user_m:{}'.format(token.key))
    token.delete()
    return response.Response(status=status.HTTP_204_NO_CONTENT)


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def me(request):
    """
    current user
    :param request:
    :return:
    """
    profile = request.user.account.profile
    cart_obj = cart.get_active_cart(profile.user.id)
    return response.Response({'data': {
        'user': serializers.ProfileSerializer(profile).data,
        'cart': serializers.ApiCartSerializer(cart_obj).data
    }})


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.SessionAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def get_profile(request):
    """
    current user
    :param request:
    :return:
    """
    profile = request.user.account.profile
    return response.Response({'data': serializers.ProfileSerializer(profile).data})


@decorators.api_view(['POST'])
def authenticate_therapist(request):
    """
        authenticates user and generate authorization token
    """
    form = forms.TherapistLoginForm(request.POST)

    if form.is_valid():
        therapist = therapists.authenticate(**form.cleaned_data)
        if not therapist:
            return response.Response({'error': 'Invalid username/password combination'},
                                     status=status.HTTP_401_UNAUTHORIZED)

        if not therapist.is_verified:
            tasks.send_mail_new_therapist(therapist.id)
            return response.Response({'error': 'Account not verified, check your email for verification mail'},
                                     status=status.HTTP_401_UNAUTHORIZED)

        onboarding = models.TherapistOnBoarding.objects.filter(therapist_id=therapist.id).first()

        # # create if not found
        if not onboarding:
            models.TherapistOnBoarding.objects.create(therapist=therapist)

        data = serializers.TherapistSerializer(therapist).data
        token = models.Token.objects.create(therapist=therapist)
        data['token'] = token.key

        therapists.save_redis_therapist(token, therapist.id)

        return response.Response({'therapist': data}, status=status.HTTP_200_OK)

    return response.Response(form.errors)


@decorators.api_view(['GET'])
@app_decorators.is_authenticated_therapist
def therapist_billing(request, therapist_id, token):
    """
        retrieve therapy billing information
    """
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', '-created_at')

    query = models.Billing.objects.order_by(order_by)
    filtered_query = query.filter(therapist_id=therapist_id).all()
    paginator = Paginator(filtered_query, 18)

    try:
        billing_list = paginator.page(page)
    except PageNotAnInteger:
        billing_list = paginator.page(1)
    except EmptyPage:
        billing_list = paginator.page(paginator.num_pages)

    return encoders.JSONResponse({'data': [serializers.BillingSerializer(c).data for c in
                                           billing_list.object_list], 'count': paginator.count,
                                  'page_range': list(billing_list.paginator.page_range),
                                  'start_index': billing_list.start_index(),
                                  'has_other_pages': billing_list.has_other_pages(),
                                  'end_index': billing_list.end_index()
                                  })


@decorators.api_view(['GET'])
@app_decorators.is_authenticated_therapist
def therapist_messages(request, therapist_id, token):
    """
    retrieve therapist's messages
    :param request:
    :param therapist_id:
    :param token:
    :return:
    """

    page = request.GET.get('page', 1)

    data = mongo.MongoService.aggregate_collection('messages', page=int(page), key='thread_id',
                                                   sort_key='timestamp',
                                                   fields=['user_id', 'therapist_id', 'sender', 'recipient',
                                                           'timestamp', 'username', 'user_read', 'user_replied',
                                                           'therapist_replied', 'therapist_read', 'subject', 'body'],
                                                   **{'therapist_id': therapist_id})

    return encoders.JSONResponse({'data': data})


@decorators.api_view(['GET'])
@app_decorators.is_authenticated_therapist
def therapist_reviews(request):
    """
    reviews
    :param request:
    :return:
    """
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', '-created_at')

    query = models.Order.objects.order_by(order_by)
    filtered_query = query.filter(user=request.user.account).all()
    paginator = Paginator(filtered_query, 18)

    try:
        order_list = paginator.page(page)
    except PageNotAnInteger:
        order_list = paginator.page(1)
    except EmptyPage:
        order_list = paginator.page(paginator.num_pages)

    return encoders.JSONResponse({'data': [serializers.OrderSerializer(c).data for c in order_list.object_list],
                                  'count': paginator.count,
                                  'page_range': list(order_list.paginator.page_range),
                                  'start_index': order_list.start_index(),
                                  'has_other_pages': order_list.has_other_pages(), 'end_index': order_list.end_index()
                                  })


@decorators.api_view(['GET'])
@app_decorators.is_authenticated_therapist
def therapist_sessions(request, therapist_id, token):
    """
    reviews
    :param request:
    :return:
    """
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', '-created_at')

    query = models.Sessions.objects.order_by(order_by)
    filtered_query = query.filter(therapist_id=therapist_id, expiration_date__gte=date.today(),
                                  termination_date__isnull=True).all()
    paginator = Paginator(filtered_query, 18)

    try:
        sessions_list = paginator.page(page)
    except PageNotAnInteger:
        sessions_list = paginator.page(1)
    except EmptyPage:
        sessions_list = paginator.page(paginator.num_pages)

    return encoders.JSONResponse({'data': [serializers.TherapySessionSerializer(c).data for c in
                                           sessions_list.object_list], 'count': paginator.count,
                                  'page_range': list(sessions_list.paginator.page_range),
                                  'start_index': sessions_list.start_index(),
                                  'has_other_pages': sessions_list.has_other_pages(),
                                  'end_index': sessions_list.end_index()
                                  })


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def reviews(request, pk):
    """
    reviews
    :param request:
    :param pk
    :return:
    """
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', '-created_at')

    query = models.Review.objects.order_by(order_by)
    filtered_query = query.filter(therapist_id=pk).all()
    paginator = Paginator(filtered_query, 18)

    try:
        review_list = paginator.page(page)
    except PageNotAnInteger:
        review_list = paginator.page(1)
    except EmptyPage:
        review_list = paginator.page(paginator.num_pages)

    return encoders.JSONResponse({'data': [serializers.ReviewSerializer(c).data for c in review_list.object_list],
                                  'count': paginator.count,
                                  'start_index': review_list.start_index(),
                                  'has_other_pages': review_list.has_other_pages(), 'end_index': review_list.end_index()
                                  })


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def user_inbox(request):
    """
        retrieve user sessions
    """
    page = request.GET.get('page', 1)
    data = mongo.MongoService.query_collection('messages', page=int(page),
                                               excludes=[{'sender': request.user.account.username}],
                                               **{'user_id': request.user.account.pk,
                                                  'sender': request.user.account.username})

    return encoders.JSONResponse({'data': data})


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def user_sent(request):
    """
        retrieve user sessions
    """
    page = request.GET.get('page', 1)
    data = mongo.MongoService.query_collection('messages', page=int(page), **{'user_id': request.user.account.pk,
                                                                              'sender': request.user.account.username})

    return encoders.JSONResponse({'data': data})


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def user_messages(request):
    """
        retrieve user sessions
    """
    page = request.GET.get('page', 1)
    data = mongo.MongoService.aggregate_collection('messages', page=int(page), key='thread_id', sort_key='timestamp',
                                                   fields=['user_id', 'therapist_id', 'sender', 'recipient',
                                                           'timestamp', 'username', 'user_read', 'user_replied',
                                                           'therapist_replied', 'therapist_read', 'subject', 'body'],
                                                   **{'user_id': request.user.account.pk})

    return encoders.JSONResponse({'data': data})


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def user_message(request, pk):
    """
    retrieve therapist messages
    :param request
    :param pk
    """
    therapist = models.Therapist.objects.get(pk=pk)

    if not therapist:
        return encoders.JSONResponse({'error': {'message': 'Invalid therapist'}}, status=404)

    page = request.GET.get('page', 1)
    data = mongo.MongoService.query_collection('messages', page=int(page), **{
        'user_id': request.user.account.pk,
        'therapist_id': therapist.id
    })

    return encoders.JSONResponse({'data': data})


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def user_notifications(request):
    """
        retrieve user sessions
    """
    page = request.GET.get('page', 1)

    data = mongo.MongoService.query_collection('notifications', page=int(page), **{'user_id': request.user.account.pk})

    return encoders.JSONResponse({'data': data})


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def orders(request):
    """
    orders
    :param request:
    :return:
    """
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', '-created_at')

    query = models.Order.objects.order_by(order_by)
    filtered_query = query.filter(user=request.user.account).all()
    paginator = Paginator(filtered_query, 18)

    try:
        order_list = paginator.page(page)
    except PageNotAnInteger:
        order_list = paginator.page(1)
    except EmptyPage:
        order_list = paginator.page(paginator.num_pages)

    return encoders.JSONResponse({'data': [serializers.OrderSerializer(c).data for c in order_list.object_list],
                                  'count': paginator.count,
                                  'page_range': list(order_list.paginator.page_range),
                                  'start_index': order_list.start_index(),
                                  'has_other_pages': order_list.has_other_pages(), 'end_index': order_list.end_index()
                                  })


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def order(request, pk):
    """
    order matching pk
    :param request:
    :param pk
    :return:
    """
    order_item = models.Order.objects.get(pk=pk)

    return encoders.JSONResponse({'data':  serializers.OrderItemSerializer(order_item).data})


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def resources(request):
    """
    resources
    :param request:
    :return:
    """
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', '-created_at')

    query = models.Resources.objects.order_by(order_by)
    filtered_query = query.filter(user=request.user.account).all()
    paginator = Paginator(filtered_query, 18)

    try:
        resource_list = paginator.page(page)
    except PageNotAnInteger:
        resource_list = paginator.page(1)
    except EmptyPage:
        resource_list = paginator.page(paginator.num_pages)

    return encoders.JSONResponse({'data': [serializers.ResourceSerializer(c).data for c in resource_list.object_list],
                                  'count': paginator.count,
                                  'page_range': list(resource_list.paginator.page_range),
                                  'start_index': resource_list.start_index(),
                                  'has_other_pages': resource_list.has_other_pages(),
                                  'end_index': resource_list.end_index()
                                  })


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def assessments(request):
    """
    assessments
    :param request:
    :return:
    """
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', '-created_at')

    query = models.AssessmentTest.objects.order_by(order_by)
    paginator = Paginator(query.all(), 18)

    try:
        assessment_list = paginator.page(page)
    except PageNotAnInteger:
        assessment_list = paginator.page(1)
    except EmptyPage:
        assessment_list = paginator.page(paginator.num_pages)

    return encoders.JSONResponse(
        {'data': [serializers.AssessmentSerializer(c).data for c in assessment_list.object_list],
         'count': paginator.count, 'page_range': list(assessment_list.paginator.page_range),
         'start_index': assessment_list.start_index(),
         'has_other_pages': assessment_list.has_other_pages(),
         'end_index': assessment_list.end_index()
         })


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def user_assessments(request):
    """
    assessments
    :param request:
    :return:
    """
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', '-created_at')

    query = models.UserAssessment.objects.order_by(order_by)
    filtered_query = query.filter(user=request.user.account).all()
    paginator = Paginator(filtered_query, 20)

    try:
        assessment_list = paginator.page(page)
    except PageNotAnInteger:
        assessment_list = paginator.page(1)
    except EmptyPage:
        assessment_list = paginator.page(paginator.num_pages)

    return encoders.JSONResponse(
        {'data': {'items': [serializers.UserAssessmentSerializer(c).data for c in assessment_list.object_list],
                  'count': paginator.count, 'page_range': list(assessment_list.paginator.page_range),
                  'start_index': assessment_list.start_index(),
                  'has_other_pages': assessment_list.has_other_pages(),
                  'end_index': assessment_list.end_index()
                  }})


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def user_assessments_create(request):
    """
    submit user assessment response
    :param request:
    :return:
    """

    form = forms.AssessmentTestForm(request.data)

    if form.is_valid():

        assessment_test = models.AssessmentTest.objects.get(pk=form.cleaned_data['assessment_test_id'])

        if not assessment_test:
            return response.Response({'error': {
                'message': 'AssessmentTest not found'
            }}, status=400)

        data = json.loads(form.cleaned_data['responses'])

        user_assessment = assessment_service.save_user_response(request.user.account.id, assessment_test.id, **data)

        return response.Response({'data': serializers.UserAssessmentSerializer(user_assessment).data})

    return response.Response({'error': form.errors}, status=400)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def quit_therapy(request):
    """
    terminate current session
    :param request:
    :return:
    """
    form = forms.QuitTherapyForm(request.data)

    if form.is_valid():
        try:
            # terminate current session
            profile = therapy_service.quit_therapy(request.user.account.id, **form.cleaned_data)

            return response.Response({'data': serializers.ProfileSerializer(profile).data})
        except Exception as e:
            return response.Response({'error': 'Error occurred'}, status=400)

    return response.Response({'error': form.errors}, status=400)            


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def select_therapist(request, pk):
    """
    select therapist
    :param request:
    :param pk
    :return:
    """
    form = forms.ChangeTherapistForm(request.data)

    if form.is_valid():
        try:
            # retrieve therapist
            therapist = models.Therapist.objects.get(pk=pk)

            user = therapy_service.change_therapist(request.user.account.id, therapist.id, **form.cleaned_data)

            return response.Response({'data': serializers.ProfileSerializer(user.profile).data})
        except models.Therapist.DoesNotExist:
            return response.Response(status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            return response.Response({'error': 'Error occurred'}, status=400)

    return response.Response({'error': form.errors}, status=400)  


@decorators.api_view(['GET'])
def plans(request):
    """
    select therapist
    :param request:
    :return:
    """
    # retrieve therapy plans
    plans_ = models.Plan.objects.all()

    return response.Response({'data': [serializers.PlanSerializer(c).data for c in plans_]})


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def change_plan(request, pk):
    """
    select therapist
    :param request:
    :param pk
    :return:
    """
    # retrieve therapist
    plan = models.Plan.objects.get(pk=pk)

    if not plan:
        return response.Response(status=status.HTTP_404_NOT_FOUND)

    on_board = models.OnBoarding.objects.filter(user=request.user.account).first()

    if not on_board:
        on_board = models.OnBoarding.objects.create(user=request.user.account)

    # form = forms.ChangePlanForm(request.data)

    # if form.is_valid():

    user = therapy_service.change_plan(request.user.account.id, plan.id)

    on_board.plan_step_completed = True
    on_board.save()

    return response.Response({'data': serializers.ProfileSerializer(user.profile).data})

    # return response.Response({'error': form.errors}, status=400)


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def get_current_session(request):
    """
    returns current session
    :param request:
    :return:
    """
    return response.Response({'data': therapy_service.get_current_session(request.user.id)})


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def submit_pre_therapy(request):
    """
    returns current session
    :param request:
    :return:
    """
    form = forms.PreTherapyForm(request.data)

    if form.is_valid():

        if models.UserPreTherapy.objects.filter(user=request.user.account).exists():
            return response.Response({'error': {'message': 'Pre-therapy assessment already exists'}}, status=400)

        # record pre_therapy responses
        account.update_user_pre_therapy_responses(request.user.account.id, **form.cleaned_data)

        on_board = models.OnBoarding.objects.filter(user=request.user.account).first()

        if not on_board:
            on_board = models.OnBoarding.objects.create(user=request.user.account)

        on_board.pre_therapy_step_completed = True
        on_board.save()

        return response.Response({'data': serializers.ProfileSerializer(request.user.account.profile).data})

    return response.Response({'error': form.errors}, status=400)


@decorators.api_view(['GET'])
def qualifications(request):
    """
    select qualifications
    :param request:
    :return:
    """
    # retrieve therapy plans
    qualifications_ = models.Qualification.objects.all()

    return response.Response({'data': [serializers.QualificationSerializer(c).data for c in qualifications_]})


@decorators.api_view(['GET'])
@app_decorators.is_authenticated_therapist
def therapist_profile(request, therapist_id, token):
    """
    update therapist
    :param request:
    :param therapist_id
    :param token
    :return:
    """
    therapist = models.Therapist.objects.get(pk=therapist_id)

    return response.Response({'data': serializers.TherapistSerializer(therapist).data})


@decorators.api_view(['POST'])
@app_decorators.is_authenticated_therapist
def therapist_edit(request, therapist_id, token):
    """
    update therapist
    :param request:
    :param therapist_id
    :param tokenap
    :return:
    """
    form = forms.EditTherapistForm(request.data)

    if form.is_valid():

        therapist = models.Therapist.objects.get(pk=therapist_id)

        data = form.cleaned_data.copy()
        data.update(request.FILES)

        therapists.update_therapist(therapist.id, **data)

        return response.Response({'data': serializers.TherapistSerializer(therapist).data})

    return response.Response({'error': form.errors}, status=400)


@decorators.api_view(['DELETE'])
@app_decorators.is_authenticated_therapist
def logout_therapist(request, therapist_id, token):
    """
    logout user via api
    :param request:
    :param therapist_id
    :param token
    :return:
    """
    therapists.clear_redis_user(token.key, therapist_id)
    token.expire()
    return response.Response(status=status.HTTP_204_NO_CONTENT)


@decorators.api_view(['POST'])
@app_decorators.is_authenticated_therapist
def deactivate_therapist(request, therapist_id, token):
    """
    logout user via api
    :param request:
    :param therapist_id
    :param token
    :return:
    """
    therapist = therapists.deactivate_account(therapist_id)

    return response.Response({'data': serializers.TherapistSerializer(therapist).data}, status=status.HTTP_200_OK)


@decorators.api_view(['POST'])
@app_decorators.is_authenticated_therapist
def activate_therapist(request, therapist_id, token):
    """
    logout user via api
    :param request:
    :param therapist_id
    :param token
    :return:
    """
    therapist = therapists.request_activation(therapist_id)
    return response.Response({'data': serializers.TherapistSerializer(therapist).data}, status=status.HTTP_200_OK)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def select_plan(request):
    """
    select therapist
    :param request:
    :return:
    """
    form = forms.SelectPlanForm(request.data)

    if form.is_valid():

        # retrieve therapist
        plan = models.Plan.objects.get(pk=form.cleaned_data['plan_id'])

        if not plan:
            return response.Response(status=status.HTTP_404_NOT_FOUND)

        on_board = models.OnBoarding.objects.filter(user=request.user.account).first()

        if not on_board:
            return response.Response({'error': {'message': 'Invalid onboarding process'}}, status=400)

        on_board.plan_step_completed = True
        on_board.save()

        # update profile
        profile = request.user.account.profile
        profile.current_plan = plan
        profile.save()

        return response.Response({'data': serializers.ProfileSerializer(profile).data})

    return response.Response({'error': form.errors}, status=400)


@decorators.api_view(['POST'])
@app_decorators.is_authenticated_therapist
def therapist_billing_settings_update(request, therapist_id, token):
    """
    update therapist onboarding
    :param request:
    :param therapist_id
    :param token
    :return:
    """
    form = forms.TherapistBillingSettingsForm(request.data)

    if form.is_valid():

        therapist = models.Therapist.objects.get(pk=therapist_id)

        therapists.update_therapist_billing_settings(therapist.id, **form.cleaned_data)

        return response.Response({'data': serializers.TherapistSerializer(therapist).data})

    return response.Response({'error': form.errors}, status=400)


@decorators.api_view(['POST'])
@app_decorators.is_authenticated_therapist
def therapist_sessions_setup(request, therapist_id, token):
    """
    update therapist onboarding
    :param request:
    :param therapist_id
    :param token
    :return:
    """

    therapist = models.Therapist.objects.get(pk=therapist_id)

    therapists.update_chat_step_completed(therapist.id)

    return response.Response({'data': serializers.TherapistSerializer(therapist).data})


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def search_product_and_therapist(request):
    """
    search product or therapist matching
    GET request parameter
    :param request:
    :return:
    """
    query_therapist = models.Therapist.objects.filter(is_activated=True, is_verified=True)
    query_product = models.Product.objects.filter(available_quantity__gt=0)

    q = request.GET.get('q', None)
    if q:
        # retrieve therapists
        query_therapist = query_therapist.filter(Q(name__icontains=q) | Q(email__icontains=q)).order_by(
            '-created_at').all()

        # retrieve products
        query_product = query_product.filter(Q(name__icontains=q) | Q(sku__icontains=q)).order_by('-created_at').all()

    else:
        # select random 10 therapists and products
        query_therapist = query_therapist.order_by('?').all()[:10]
        query_product = query_product.order_by('?').all()[:10]

    return response.Response({
        'data': {
            'products': [serializers.ProductSerializer(c).data for c in query_product],
            'therapists': [serializers.TherapistSerializer(d).data for d in query_therapist]
        }
    })


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def daily_tips(request):
    """
    GET request endpoint for daily tips
    :param request:
    :return:
    """
    tip_today = models.DailyTips.objects.filter(target_date=date.today()).first()

    if not tip_today:
        try:
            tip_today = models.DailyTips.objects.latest('created_at')
        except models.DailyTips.DoesNotExist:
            return response.Response({'data': None})

    return response.Response({
        'data': serializers.DailyTipSerializer(tip_today).data
    })


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def user_billing(request):
    """
    retrieve user billing records
    :param request
    """
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', '-created_at')

    filtered_query = models.Billing.objects.filter(user_id=request.user.account.id).order_by(order_by).all()
    paginator = Paginator(filtered_query, 18)

    try:
        billing_list = paginator.page(page)
    except PageNotAnInteger:
        billing_list = paginator.page(1)
    except EmptyPage:
        billing_list = paginator.page(paginator.num_pages)

    return encoders.JSONResponse({'data': [serializers.BillingSerializer(c).data for c in
                                           billing_list.object_list], 'count': paginator.count,
                                  'page_range': list(billing_list.paginator.page_range),
                                  'start_index': billing_list.start_index(),
                                  'has_other_pages': billing_list.has_other_pages(),
                                  'end_index': billing_list.end_index()
                                  })


@decorators.api_view(['GET'])
def categories(request):
    """
    retrieve product categories
    :param request:
    :return:
    """
    order_by = request.GET.get('order_by', 'name')

    list_categories = models.Category.objects.order_by(order_by)

    return encoders.JSONResponse(
        {'data': [serializers.CategorySerializer(c).data for c in list_categories.all()],
         'total': list_categories.count(),
         })


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def get_cart(request):
    """
    retrieve cart
    :param request:
    :return:
    """
    sess_cart = cart.get_active_cart(request.user.account.id)
    return encoders.JSONResponse({'data': serializers.ApiCartSerializer(sess_cart).data}, status=status.HTTP_200_OK)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def remove_cart_item(request):
    """
    remove item from cart
    :param request:
    :return:
    """
    try:
        form = forms.RemoveCartItemForm(request.data)

        if form.is_valid():

            item = models.Item.objects.get(pk=form.cleaned_data['item_id'])
            item.delete()
            session_cart = models.Cart.objects.get(pk=form.cleaned_data['cart_id'])

            json_cart = json.dumps(serializers.CartSerializer(session_cart).data, cls=encoders.DateTimeEncoder)
            request.session['cart'] = json_cart
            return encoders.JSONResponse(
                {'message': "Successfully removed item", 'data': json_cart}, status=200)

        return response.Response({'error': form.errors}, status=status.HTTP_400_BAD_REQUEST)

    except Exception as e:
        return encoders.JSONResponse({'message': e}, status=400)


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def habits(request):
    """
    habits endpoint
    :param request:
    :return:
    """
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', 'start_date')

    query = models.Habit.objects.order_by(order_by)
    filtered_query = query.filter(user=request.user.account).all()
    paginator = Paginator(filtered_query, 18)

    try:
        habit_list = paginator.page(page)
    except PageNotAnInteger:
        habit_list = paginator.page(1)
    except EmptyPage:
        habit_list = paginator.page(paginator.num_pages)

    return encoders.JSONResponse({'data': [serializers.HabitSerializer(c).data for c in habit_list.object_list],
                                  'count': paginator.count,
                                  'page_range': list(habit_list.paginator.page_range),
                                  'start_index': habit_list.start_index(),
                                  'has_other_pages': habit_list.has_other_pages(), 'end_index': habit_list.end_index()
                                  })


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def habit_check_ins(request, pk):
    """
    habit check ins
    :param request:
    :param pk
    :return:
    """
    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')

    habit, check_ins, negative_count, positive_count = habit_service.load_checkins(pk, start_date, end_date)

    return encoders.JSONResponse({'data': {
        'habit': serializers.HabitSerializer(habit).data,
        'check_ins': [serializers.CheckInSerializer(c).data for c in check_ins],
        'negative_count': negative_count,
        'positive_count': positive_count
    }})


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def add_habit(request):
    """
    add habit
    :param request:
    :return:
    """
    form = forms.AddHabitForm(request.data)

    if form.is_valid():
        habit = habit_service.create(request.user.account.id, **form.cleaned_data)

        return response.Response({'data': serializers.HabitSerializer(habit).data})

    return response.Response({'error': form.errors}, status=status.HTTP_400_BAD_REQUEST)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def send_habit_report(request, pk):
    """
    add habit
    :param request:
    :return:
    """
    try:
        habit = habit_service.get_habit(pk)
        tasks.send_habit_report.delay(habit.id)
        return response.Response({'data': serializers.HabitSerializer(habit).data})

    except Exception as e:
        return response.Response({'error': e.message}, status=status.HTTP_400_BAD_REQUEST)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def restart_habit(request, pk):
    """
    restart habit
    :param pk
    :param request:
    :return:
    """
    form = forms.RestartHabitForm(request.data)

    if form.is_valid():

        if not habit_service.check_owner(request.user.account.id, pk):
            return response.Response({'error': 'No access rights'}, status=status.HTTP_400_BAD_REQUEST)

        habit = habit_service.restart_habit(pk, **form.cleaned_data)

        return response.Response({'data': serializers.HabitSerializer(habit).data})

    return response.Response({'error': form.errors}, status=status.HTTP_400_BAD_REQUEST)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def add_habit_companion(request, pk):
    """
    restart habit
    :param pk
    :param request:
    :return:
    """
    form = forms.HabitCompanionForm(request.data)

    if form.is_valid():

        if not habit_service.check_owner(request.user.account.id, pk):
            return response.Response({'error': 'No access rights'}, status=status.HTTP_400_BAD_REQUEST)

        # habit = habit_service.add_companion(pk, **form.cleaned_data)
        habit = habit_service.get_habit(pk)

        companion = habit_service.get_companion(habit.id)

        if companion:
            return response.Response({'error': 'Habit Companion already exists'}, status=status.HTTP_400_BAD_REQUEST)

        tasks.add_habit_companion.delay(pk, **form.cleaned_data)

        return response.Response({'data': serializers.HabitSerializer(habit).data})

    return response.Response({'error': form.errors}, status=status.HTTP_400_BAD_REQUEST)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def check_in(request):
    """
    habit progress check in
    :param request:
    :return:
    """
    form = forms.HabitCheckInForm(request.data)

    if form.is_valid():
        obj = habit_service.record_check_in(request.user.account.id, **form.cleaned_data)

        return response.Response({'data': serializers.CheckInSerializer(obj).data})

    return response.Response({'error': form.errors}, status=status.HTTP_400_BAD_REQUEST)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def notification_settings(request):
    """
    habit progress check in
    :param request:
    :return:
    """
    form = forms.NotificationSettingsForm(request.data)

    if form.is_valid():
        obj = account.save_notification_settings(request.user.account.id, **form.cleaned_data)

        return response.Response({'data': serializers.AccountNotificationSerializer(obj).data})

    return response.Response({'error': form.errors}, status=status.HTTP_400_BAD_REQUEST)


@decorators.api_view(['GET'])
@decorators.authentication_classes((authentication.TokenAuthentication,))
@decorators.permission_classes((permissions.IsAuthenticated,))
def therapist_categories(request):
    """
    retrieve therapist categories
    :param request:
    :return:
    """
    order_by = request.GET.get('order_by', 'title')

    list_categories = models.TherapistCategory.objects.order_by(order_by)

    return encoders.JSONResponse(
        {'data': [serializers.TherapistCategorySerializer(c).data for c in list_categories.all()],
         'total': list_categories.count(),
         })


@decorators.api_view(['GET'])
def languages(request):
    """
    retrieve languages
    :param request:
    :return:
    """
    order_by = request.GET.get('order_by', '-created_at')

    list_languages = models.Language.objects.order_by(order_by)

    return encoders.JSONResponse(
        {'data': [serializers.LanguageSerializer(c).data for c in list_languages.all()],
         'total': list_languages.count(),
         })


@decorators.api_view(['POST'])
def password_reset(request, email_template_name='registration/password_reset_email.html',
                   subject_template_name='registration/password_reset_subject.txt',
                   password_reset_form=auth_views.PasswordResetForm,
                   token_generator=auth_views.default_token_generator, from_email=None, html_email_template_name=None,
                   extra_email_context=None):

    form = password_reset_form(request.data)
    if form.is_valid():
        opts = {
            'use_https': request.is_secure(),
            'token_generator': token_generator,
            'from_email': from_email,
            'email_template_name': email_template_name,
            'subject_template_name': subject_template_name,
            'request': request,
            'html_email_template_name': html_email_template_name,
            'extra_email_context': extra_email_context,
        }
        form.save(**opts)
        return response.Response({'data': {'message': 'Successfully sent reset instructions to your provided email.'}})

    return response.Response({'error': form.errors}, status=status.HTTP_400_BAD_REQUEST)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def increment_call_counter(request):
    """
    increment call counter POST endpoint
    :param request:
    :return:
    """
    profile = therapy_service.increment_call_counter(request.user.account.profile.id)
    return response.Response({'data': serializers.ProfileSerializer(profile).data})


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def increment_conversation_counter(request):
    """
    increment call counter POST endpoint
    :param request:
    :return:
    """
    profile = therapy_service.increment_conversation_counter(request.user.account.profile.id)
    return response.Response({'data': serializers.ProfileSerializer(profile).data})


@decorators.api_view(['GET'])
@app_decorators.is_authenticated_therapist
def user_details(request, therapist_id, token, username, **kwargs):
    """
    user details
    :param request:
    :param username:
    :param therapist_id:
    :param token:
    :return:
    """
    user_account = models.Account.objects.get(username=username)
    return response.Response({'data': serializers.ApiAccountSerializer(user_account).data})


@decorators.api_view(['GET'])
@app_decorators.is_authenticated_therapist
def therapist_user_assessments(request, therapist_id, token, username, **kwargs):
    """
    user details
    :param request:
    :param username:
    :param therapist_id:
    :param token:
    :return:
    """
    user_account = models.Account.objects.get(username=username)
    user_tests = user_account.user_assessments.all()
    return response.Response({'data': [serializers.UserAssessmentSerializer(c).data for c in user_tests]})


@decorators.api_view(['GET'])
@app_decorators.is_authenticated_therapist
def therapist_user_assessment(request, therapist_id, token, username, pk, **kwargs):
    """
    user details
    :param request:
    :param username:
    :param therapist_id:
    :param token:
    :return:
    """
    assessment = models.UserAssessment.objects.get(pk=pk)
    return response.Response({'data': serializers.UserAssessmentSerializer(assessment).data})


@decorators.api_view(['DELETE'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def cancel_habit(request, pk):
    """
    cancel habit
    :param request:
    :param pk:
    :return:
    """
    habit = get_object_or_404(models.Habit, pk=pk)
    habit.delete()

    return response.Response(status=status.HTTP_204_NO_CONTENT)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def assign_therapist(request):
    """
    assign therapist
    :param request:
    :return:
    """

    pre_therapy_responses = models.UserPreTherapyResponse.objects.filter(user=request.user.account).all()

    if not pre_therapy_responses:
        return encoders.JSONResponse({'message': 'failed'}, status=400)

    quality_response = next(c for c in pre_therapy_responses if c.code == 'qualities')
    country_response = next(c for c in pre_therapy_responses if c.code == 'country')

    qualities = quality_response.response.split(',')

    therapist = models.Therapist.objects.filter(address__country__name=country_response.response,
                                                categories__title__in=qualities, billing_settings__isnull=False)\
                                                .order_by('?').first()

    if not therapist:
        therapist = models.Therapist.objects.filter(address__country__name=country_response.response, 
                                                        billing_settings__isnull=False).order_by('?').first()

        if not therapist:
            therapist = models.Therapist.objects.filter(billing_settings__isnull=False).order_by('rating').first()

            if not therapist:
                therapist = models.Therapist.objects.order_by('rating').first()

    if not therapist:
        return encoders.JSONResponse({'message': 'failed'}, status=400)

    form = forms.ChangeTherapistForm(request.POST)

    if form.is_valid():
        therapy_service.change_therapist(request.user.account.id, therapist.id, **form.cleaned_data)

        # update onboarding steps
        on_board = models.OnBoarding.objects.filter(user=request.user).first()

        if not on_board.plan_step_completed and request.is_ajax():
            messages.success(request, "You have selected a suitable therapist. Select a plan below to start "
                                    "counseling.")

        return encoders.JSONResponse({'data': serializers.ProfileSerializer(on_board.user.profile).data})
    return encoders.JSONResponse({'message': form.errors}, status=400)