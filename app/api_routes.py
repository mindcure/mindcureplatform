from django.conf.urls import include, url

from rest_framework.routers import SimpleRouter

from . import viewsets, api, ajax_view

router = SimpleRouter()
router.register("accounts", viewsets.AccountViewSet)

urlpatterns = [
    # mobile api authentication endpoints
    url(r'^auth/login$', api.authenticate_user, name='api_login'),
    url(r'^auth/register$', api.register_user, name='api_register'),
    url(r'^auth/logout$', api.logout_user, name='api_logout'),
    url(r'^auth/reset$', api.password_reset),

    url(r'^me$', api.me, name='me'),
    url(r'^profile', api.get_profile),
    url(r'^orders$', api.orders, name='api_orders'),
    url(r'^orders/(?P<pk>[-\d]+)$', api.order, name='api_order'),
    url(r'^resources$', api.resources, name='api_resources'),
    url(r'^assessments$', api.assessments, name='api_assessments'),

    # web api endpoints
    url(r'^web/login$', ajax_view.login_user, name='web_login'),
    url(r'^web/register$', ajax_view.register_user, name='web_register'),

    # ajax routes
    url(r'^check/username$', ajax_view.check_username, name='check_username'),
    url(r'^check/email$', ajax_view.check_email, name='check_email'),
    url(r'^check/therapist$', ajax_view.check_therapist, name='check_therapist'),

    # api endpoints
    url(r'^products$', ajax_view.filter_products, name='filter_products'),
    url(r'^assessments$', api.assessments),
    url(r'^therapists$', ajax_view.paginate_therapists, name='paginate_therapists'),
    url(r'^carts/add$', ajax_view.add_to_cart, name='add_to_cart'),
    url(r'^carts/get$', api.get_cart, name='get_cart'),
    url(r'^items/(?P<pk>[-\d]+)/remove$', ajax_view.remove_item, name='remove_item'),

    # api cart
    # url(r'^carts/item/add$', api.add_to_cart),
    url(r'^carts/item/remove$', api.remove_cart_item),
    # url(r'^transactions/initiate$', api.initiate_transaction, name='initiate_transaction'),

    # therapist api endpoints
    url(r'^therapists/login$', api.authenticate_therapist, name='therapist_login'),
    url(r'^therapists/billings$', api.therapist_billing, name='therapist_billing'),
    url(r'^therapists/messages$', api.therapist_messages, name='therapist_messages'),
    url(r'^therapists/reviews$', api.therapist_reviews, name='therapist_reviews'),
    url(r'^therapists/edit$', api.therapist_edit, name='therapist_edit'),
    url(r'^therapists/(?P<pk>[-\d]+)/select$', api.select_therapist, name='select_therapist'),
    url(r'^therapists/logout$', api.logout_therapist, name='logout_therapist'),
    url(r'^therapists/quit$', api.quit_therapy, name='therapy_termination'),
    url(r'^therapists/deactivate$', api.deactivate_therapist, name='deactivate_therapist'),
    url(r'^therapists/activate$', api.activate_therapist, name='activate_therapist'),
    url(r'^therapists/messages/send$', ajax_view.therapist_send_message, name='therapist_send_message'),
    url(r'^therapists/profile$', api.therapist_profile, name='therapist_profile'),
    url(r'^therapists/billings/settings$', api.therapist_billing_settings_update, name='billing_settings'),
    url(r'^therapists/sessions/setup$', api.therapist_sessions_setup, name='therapist_sessions_setup'),

    url(r'^therapists/(?P<pk>[-\d]+)/reviews$', api.reviews),
    url(r'^therapists/sessions$', api.therapist_sessions, name='therapist_sessions'),

    # user api endpoints
    url(r'^user/inbox$', api.user_inbox, name='user_inbox'),
    url(r'^user/sent$', api.user_inbox, name='user_sent'),
    url(r'^user/messages$', api.user_messages, name='user_messages'),
    url(r'^user/(?P<pk>[-\d]+)/messages$', api.user_message, name='user_message'),

    url(r'^users/(?P<username>[-\w]+)$', api.user_details),
    url(r'^users/(?P<username>[-\w]+)/assessments$', api.therapist_user_assessments),
    url(r'^users/(?P<username>[-\w]+)/assessments/(?P<pk>[-\d]+)$', api.therapist_user_assessment),

    url(r'^user/notifications$', api.user_notifications, name='user_notifications'),
    url(r'^user/assessments$', api.user_assessments),
    url(r'^assessments/respond$', api.user_assessments_create, name='user_assessments_create'),
    url(r'^user/pre-therapy$', api.submit_pre_therapy, name='submit_pre_therapy'),

    # therapy session and payments
    url(r'^sessions/current$', api.get_current_session, name='current_session'),
    url(r'^subscribe/user$', ajax_view.subscribe_user, name='subscribe_user'),
    url(r'^subscribe/close$', ajax_view.complete_subscription, name='complete_subscription'),
    url(r'^user/billings$', api.user_billing, name='user_billing'),

    url(r'^initiate/transaction$', ajax_view.initiate_transaction, name='initiate_transaction'),
    url(r'^close/transaction$', ajax_view.close_transaction, name='close_transaction'),

    url(r'^therapists/assign$', api.assign_therapist, name='assign_therapist'),
    url(r'^messages/send$', ajax_view.create_message, name='send_message'),

    url(r'^qualifications$', api.qualifications, name='qualifications'),
    url(r'^plans$', api.plans, name='plans'),
    url(r'^plans/(?P<pk>[-\d]+)/change$', api.change_plan, name='change_plan'),
    url(r'^plans/select$', api.select_plan, name='select_plan'),

    # search for products and therapists
    url(r'^search$', api.search_product_and_therapist),
    url(r'^daily$', api.daily_tips),
    url(r'^categories$', api.categories),
    url(r'^therapist-categories$', api.therapist_categories),
    url(r'^languages$', api.languages),
    url(r'^habits$', api.habits),
    url(r'^habits/add$', api.add_habit),
    url(r'^habits/check-in$', api.check_in),
    url(r'^habits/(?P<pk>[-\d]+)/check-ins$', api.habit_check_ins),
    url(r'^habits/(?P<pk>[-\d]+)/restart$', api.restart_habit),
    url(r'^habits/(?P<pk>[-\d]+)/companion$', api.add_habit_companion),
    url(r'^habits/(?P<pk>[-\d]+)/report$', api.send_habit_report),
    url(r'^habits/(?P<pk>[-\d]+)/cancel$', api.cancel_habit),
    url(r'^notifications/settings$', api.notification_settings),
    url(r'^increment/call$', api.increment_call_counter),
    url(r'^increment/conversation$', api.increment_conversation_counter),
    # url(r'^therapists/assign$', api.assign_therapist),

    # viewsets
    url(r'^', include(router.urls))
]
