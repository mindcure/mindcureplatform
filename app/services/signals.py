from django.db.models import Avg, signals as model_signals

from django.dispatch import receiver

from app import models


@receiver(model_signals.post_save, sender=models.RatingEntry)
def update_rating(sender, instance, created, **kwargs):
    """
    update therapist rating
    :param sender:
    :param instance:
    :param created:
    :param kwargs:
    :return:
    """
    print(created)
    exit()
    therapist = instance.therapist
    if created:
        therapist.rating = models.RatingEntry.objects.filter(therapist_id=therapist.id).all().aggregate(Avg('score'))
        therapist.save()

    return therapist
