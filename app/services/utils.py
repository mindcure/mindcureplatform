import os
import random
import string
import datetime
from mimetypes import guess_type

from django.core.files.storage import default_storage
from django.conf import settings
from django.shortcuts import render

def handle_uploaded_file(file_path):
    save_path = os.path.join(settings.MEDIA_ROOT, 'uploads', file_path)
    path = default_storage.save(save_path, file_path)
    return default_storage.path(path)


def validate_file_uploaded(file_path):
    """
    checks if uploaded file is a permitted file
    :param file_path:
    :return:
    """
    try:
        _mime = guess_type(file_path)
        return _mime[0] in settings.ACCEPTED_MIME_TYPES
    except:
        return False


def render_with_locals(template_name, context, **kwargs):
    """
    render response with context updated
    :param request:
    :param template_name
    :param context:
    :param locals:
    :return:
    """
    request = kwargs.get('request')
    context.update(kwargs)
    return render(request, template_name, context)


def random_generator(length=10):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(length))


def number_generator(length=10):
    return ''.join(random.choice(string.digits) for _ in range(length))


def add_working_days(current_date, add_days):
    """
    add working days to fro
    :param current_date:
    :param add_days:
    :return:
    """
    business_days_to_add = add_days
    while business_days_to_add > 0:
        current_date += datetime.timedelta(days=1)
        weekday = current_date.weekday()

        # skip satutday and sunday
        if weekday >= 5:
            continue
        business_days_to_add -= 1

    return current_date


def flatten_data(data):
    """
    flatten dictionary values
    """
    for key, value in data.iteritems():
        data[key] = ''.join(value)
    
    return data
