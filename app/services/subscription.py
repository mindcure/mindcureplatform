from __future__ import division

from datetime import date

from django.conf import settings

from app import models
from app.services import utils, account, therapy as therapy_service


def record_billing(sub_transaction_id, therapist_id):
    """
    record billings
    :param sub_transaction_id:
    :param therapist_id
    :return:
    """
    try:
        sub_transaction = models.SubscriptionTransaction.objects.get(pk=sub_transaction_id)

        therapist = models.Therapist.objects.get(pk=therapist_id)

        # user billing record
        return models.Billing.objects.create(user=sub_transaction.user, therapist=therapist,
                                             sub_transaction=sub_transaction)

    except models.SubscriptionTransaction.DoesNotExist as e:
        raise e
    except models.Therapist.DoesNotExist as e:
        raise e


def complete_session_payment(sub_transaction_id, duration, **kwargs):
    """
    complete session payment
    :param sub_transaction_id:
    :param duration
    :param kwargs:
    :return:
    """
    try:
        sub_transaction = models.SubscriptionTransaction.objects.get(pk=sub_transaction_id)

        user = sub_transaction.user

        current_session = therapy_service.retrieve_subscribe_session(user.profile.current_therapist.id, user.id)
        # current_session = therapy_service.get_current_session(sub_transaction.user.id)

        billing = record_billing(sub_transaction.id, current_session.therapist.id)

        if billing:
            current_session.duration = duration
            current_session.start_date = sub_transaction.start_date
            expiration_date = utils.add_working_days(current_session.start_date,
                                                     current_session.duration + current_session.rolled_over_duration)

            current_session.expiration_date = expiration_date
            current_session.billed_date = date.today()
            current_session.save()

        return current_session

    except models.SubscriptionTransaction.DoesNotExist as e:
        raise e


def create_transaction(user_id, amount, duration, start_date):
    """
    create transactions
    :param user_id:
    :param amount
    :param duration
    :param start_date
    :return:
    """
    try:
        user = models.Account.objects.get(pk=user_id)

        # current_session = therapy_service.retrieve_subscribe_session(user.profile.current_therapist.id, user.id)
        current_session = therapy_service.get_current_session(user.id)

        transaction_status = models.TransactionStatus.objects.filter(code='pending').first()

        reference_code = utils.random_generator(18)
        while models.SubscriptionTransaction.objects.filter(reference_code=reference_code).exists():
            reference_code = utils.random_generator(18)

        deducted_amount = (settings.DEDUCTION_PERCENT/100) * amount
        _amount = amount - deducted_amount

        # retrieve pending transaction for session and user
        # sub_transaction = models.SubscriptionTransaction.objects.filter(session=current_session, user=user,
        #                                                                 transaction_status=transaction_status).first()
        #
        # if sub_transaction:
        #     sub_transaction.amount = _amount
        #     sub_transaction.total_amount = amount
        #     sub_transaction.deducted_amount = deducted_amount
        #     sub_transaction.save()
        #     return sub_transaction

        return models.SubscriptionTransaction.objects.create(user=user, transaction_status=transaction_status,
                                                             amount=_amount, total_amount=amount,
                                                             reference_code=reference_code, session=current_session,
                                                             duration=duration, deducted_amount=deducted_amount,
                                                             start_date=start_date)

    except models.Account.DoesNotExist as e:
        raise e
    except models.Profile.DoesNotExist as e:
        raise e
    except models.TransactionStatus.DoesNotExist as e:
        raise e


def close_transaction(transaction_reference, tx_ref, status='success'):
    """
    complete transaction
    :param transaction_reference:
    :param tx_ref
    :param status:
    :return:
    """
    transaction = models.SubscriptionTransaction.objects.filter(reference_code=transaction_reference).first()

    if not transaction:
        raise models.Transaction.DoesNotExist

    tx_status_code = 'successful' if status == 'success' else 'failed'
    transaction_status = models.TransactionStatus.objects.filter(code=tx_status_code).first()

    if not transaction_status:
        raise models.TransactionStatus.DoesNotExist

    # update transaction status
    transaction.transaction_status = transaction_status

    # save gateway transaction reference
    transaction.txRef = tx_ref
    transaction.save()

    # create/update therapy session
    complete_session_payment(transaction.id, transaction.duration)

    return transaction
