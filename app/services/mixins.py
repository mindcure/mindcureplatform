from django.contrib.auth.mixins import AccessMixin
from django.shortcuts import redirect
from django.conf import settings


class AnonymousMixin(AccessMixin):
    """
    Custom mixin to prevent access to routes by an authenticated user
    """

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(settings.HOME_URL)
        return super(AnonymousMixin, self).dispatch(request, *args, **kwargs)
