from os.path import dirname, isfile, basename
import glob

modules = glob.glob('{}/*.py'.format(dirname(__file__)))

__all__ = [basename(c)[:-3] for c in modules if isfile(c) and not c.endswith('__init__.py')]