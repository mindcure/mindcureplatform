from app import models


def save_user_response(user_id, assessment_test_id, **kwargs):
    """
    save user assessment response
    :param user_id:
    :param assessment_test_id:
    :param kwargs
    :return:
    """
    assessment = models.AssessmentTest.objects.get(pk=assessment_test_id)

    if not assessment:
        raise models.AssessmentTest.DoesNotExist

    user = models.Account.objects.get(pk=user_id)

    if not user:
        raise models.Account.DoesNotExist

    scores = 0
    user_assessment = models.UserAssessment.objects.create(user=user, assessment_test=assessment)

    for question_id, response_name in kwargs.iteritems():
        question = models.Question.objects.get(pk=question_id)
        response = models.QuestionResponse.objects.filter(name=response_name).first()
        models.UserResponse.objects.create(user=user, question=question.body, response=response.name,
                                           assessment=user_assessment)
        scores += response.score

    user_assessment.is_completed = True
    user_assessment.score = scores
    user_assessment.save()

    return user_assessment
