import json

from django.conf import settings
import jwt
import redis

from app import models, serializers


def check_username(username):
    """
    :param username
    :return boolean
    :returns true if username found
    """
    try:
        return models.Account.objects.filter(username=username.lower()).exists()
    except Exception as e:
        raise e


def check_email(email):
    """
    :param email
    :return boolean
    :returns true if username found
    """
    try:
        return models.Account.objects.filter(email=email.lower()).exists()
    except Exception as e:
        raise e


def register_account(username, email, password, **kwargs):
    """
    :param username
    :param email
    :param password
    :returns user object
    """
    try:
        acct = models.Account.objects.create_user(username=username.lower(), email=email.lower(), password=password)
        return acct
    except Exception as e:
        raise e


def update_profile(user_id, bio):
    """
    update user bio information
    :param user_id:
    :param bio:
    :return:
    """
    try:
        user = models.Account.objects.get(pk=user_id)
        user.bio = bio
        user.save()
        return user
    except Exception as e:
        raise e


def encode_jwt_token(user_id):
    """
    encode jwt
    :return:
    """
    user = models.Account.objects.get(pk=user_id)

    if not user:
        raise models.Account.DoesNotExist

    serialized_user = serializers.AccountSerializer(user).data

    return jwt.encode(serialized_user, settings.NODEBB_JWT_TOKEN, algorithm='HS256')


def update_user_pre_therapy_responses(user_id, **kwargs):
    """
    updates user account information using pre therapy information
    :param user_id:
    :param kwargs
    :return:
    """

    user = models.Account.objects.get(pk=user_id)

    if not user:
        raise models.Account.DoesNotExist

    _file = open('setup/questions.json')
    questions = json.loads(_file.read())

    user_pre_assessment = models.UserPreTherapy.objects.create(user=user)

    for key, value in kwargs.iteritems():
        question = next((c for c in questions if c['key'] == key), None)
        if question:
            models.UserPreTherapyResponse.objects.create(user=user, question=question['value'], response=value,
                                                         pre_assessment=user_pre_assessment, code=key)
    # update pre_therapy step completed
    user_pre_assessment.is_completed = True
    user_pre_assessment.save()

    responses = user_pre_assessment.responses.all()

    # update age information
    age_question = next(i for i in responses if i.question == settings.PT_QUESTIONS_AGE)
    user.age = age_question.response

    # update gender information
    gender_question = next(i for i in responses if i.question == settings.PT_QUESTION_GENDER)
    user.gender = gender_question.response

    # update country information
    country_question = next(i for i in responses if i.question == settings.PT_QUESTIONS_COUNTRY)
    user.country = country_question.response
    user.save()

    return user


def save_notification_settings(user_id, player_id, push_token):
    """
    save notification settings
    :param user_id
    :param player_id:
    :param push_token:
    :return:
    """
    try:
        user = models.Account.objects.get(pk=user_id)

        account_notification = models.AccountNotification.objects.filter(user_id=user.id).first()

        if account_notification:
            account_notification.update(player_id=player_id, push_token=push_token)
            return account_notification

        return models.AccountNotification.objects.create(user=user, player_id=player_id, push_token=push_token)
    except models.Account.DoesNotExist as e:
        raise e


def save_redis_user(token, username_token, user_id):
    """
    save redis user for website
    :param token:
    :param username_token:
    :param user_id
    :return:
    """
    try:
        user = models.Account.objects.get(pk=user_id)

        redis_obj = redis.StrictRedis()
        serialized_user = serializers.AccountSerializer(user).data

        # save redis username
        # redis_obj.set(username_token, json.dumps(serialized_user))

        # save redis with token
        return redis_obj.set('user:{}'.format(token), json.dumps(serialized_user))
    except models.Account.DoesNotExist as e:
        raise e


def save_web_redis_user(token, user_id):
    """
    save redis user for website
    :param token:
    :param user_id:
    :return:
    """
    try:
        print('token: %s' % token)
        user = models.Account.objects.get(pk=user_id)

        redis_obj = redis.StrictRedis()
        serialized_user = serializers.AccountSerializer(user).data

        # save redis username
        redis_obj.set('user:w_%s' % user.username, json.dumps(serialized_user))

        # save redis with token
        return redis_obj.set('user:w_%s' % token, json.dumps(serialized_user))
    except models.Account.DoesNotExist as e:
        raise e


def save_mobile_redis_user(token, user_id):
    """
    save user data in redis
    :param user_id:
    :param token
    :return:
    """
    try:
        user = models.Account.objects.get(pk=user_id)

        redis_obj = redis.StrictRedis()
        serialized_user = serializers.AccountSerializer(user).data

        # save redis username
        redis_obj.set('user:m_%s' % user.username, json.dumps(serialized_user))

        return redis_obj.set('user:m_%s' % token, json.dumps(serialized_user))
    except models.Account.DoesNotExist as e:
        raise e


def clear_redis_user(username_token, token):
    """
    clear user record in redis
    :param username_token
    :param token:
    :return:
    """
    try:
        redis_obj = redis.StrictRedis()

        # delete mobile redis user
        redis_obj.delete(username_token)

        return redis_obj.delete(token)
    except Exception as e:
        raise e
