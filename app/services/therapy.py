from datetime import date

import numpy as np

from app import models, tasks


def update_session_plan(user_id, plan_id):
    """
    update session plan
    :param therapist_id
    :param user_id
    :param plan_id
    """

    try:
        user = models.Account.objects.get(pk=user_id)

        profile = user.profile

        plan = models.Plan.objects.get(pk=plan_id)

        current_session = get_current_session(user.id)

        if not current_session:
            # update profile current plan
            profile.current_plan = plan
            profile.save()

            return user

        # if session new
        if current_session.termination_date is None and current_session.billed_date is None:
            current_session.duration = plan.duration
            current_session.save()

             # update profile current plan
            profile.current_plan = plan
            profile.save()

            return user

        # add new plan information to current session
        current_session.new_plan = plan
        current_session.save()

        return user

    except models.Account.DoesNotExist as e:
        raise e
    except models.Plan.DoesNotExist as e:
        raise e
    except Exception as e:
        raise e


def change_plan(user_id, plan_id):
    """
    update user plan
    :param user_id:
    :param plan_id:
    :param duration
    :return:
    """
    try:
        user = models.Account.objects.get(pk=user_id)

        plan = models.Plan.objects.get(pk=plan_id)

        # update user's therapy session
        update_session_plan(user.id, plan.id)

        return user
    except models.Plan.DoesNotExist as e:
        raise e
    except models.Account.DoesNotExist as e:
        raise e      
    except Exception as e:
        raise e


def change_therapist(user_id, therapist_id, body, send_therapist=False):
    try:
        user = models.Account.objects.get(pk=user_id)

        old_therapist_id = None

        profile = user.profile

        # update profile
        therapist = models.Therapist.objects.get(pk=therapist_id)

        if profile.current_therapist:
            old_therapist_id = profile.current_therapist.id

        profile.current_therapist = therapist
        profile.save()

        # update user's therapy session
        if profile.current_plan:
            current_session = update_session_details(therapist.id, user.id)

        # update on_board defaults
        on_board = models.OnBoarding.objects.filter(user=user).first()

        if not on_board:
            on_board = models.OnBoarding.objects.create(user=user)

        on_board.therapist_step_completed = True
        on_board.save()

        # save reason for termination
        if body:
            models.TherapyFeedback.objects.create(therapy_session=current_session, body=body, subject='Change Therapist')

        # mail therapist and admin
        tasks.send_mail_therapist_changed.delay(therapist.id, user.id, body, send_therapist, old_therapist_id=old_therapist_id)

        return user
    except models.Account.DoesNotExist as e:
        raise e
    except models.Therapist.DoesNotExist as e:
        raise e
    except Exception as e:
        raise e


# def update_therapy_session(therapist_id, user_id, plan_duration=0):
#     """
#     update user's therapy session
#     :param therapist_id:
#     :param user_id:
#     :param plan_duration
#     :return:
#     """
#     # therapy terminated - extendable
#     # Billed
#     # - not expired
#     # not terminated and not expired
#     # not terminated, expired not billed
#
#     user = models.Account.objects.get(pk=user_id)
#
#     if not user:
#         raise models.Account.DoesNotExist
#
#     therapist = models.Therapist.objects.get(pk=therapist_id)
#
#     if not user:
#         raise models.Therapist.DoesNotExist
#
#     # check for terminated sessions
#     # not extended yet
#     current_session = models.Sessions.objects.filter(user=user, can_extend=True, termination_date__isnull=False).first()
#
#     # if session found
#     # create new session
#     # set duration as remaining period coupled with plan's duration
#     if current_session:
#         duration = plan_duration
#         # compute new expiration date
#         # if previously billed
#         if current_session.billed_date:
#             leftover_duration = current_session.duration - np.busday_count(current_session.termination_date,
#                                                                   current_session.billed_date)
#             duration = plan_duration + leftover_duration
#
#         # expiration_date = utils.add_working_days(date.today(), add_days=duration + plan_duration)
#
#         # create new session
#         # set expiration date as that above
#         # set today as billed date
#         models.Sessions.objects.create(user=user, therapist=therapist, duration=duration,
#                                        extended_session=current_session)
#
#         # terminate previous session extendability
#         current_session.can_extend = False
#         current_session.save()
#     else:
#         # check for active sessions not terminated yet
#         current_session = models.Sessions.objects.filter(user=user, expiration_date__gte=date.today(),
#                                                          termination_date__isnull=True).first()
#
#         if current_session:
#             # create new session
#             # compute new expiration date
#             duration = plan_duration
#             if current_session.billed_date:
#                 leftover_duration = current_session.duration - np.busday_count(current_session.billed_date,
#                                                                                date.today())
#                 duration = plan_duration + leftover_duration
#
#             # expiration_date = utils.add_working_days(date.today(), add_days=duration + plan_duration)
#             models.Sessions.objects.create(user=user, therapist=therapist, duration=duration,
#                                            extended_session=current_session)
#
#             # terminate current session
#             # set as non extendable
#             current_session.termination_date = date.today()
#             current_session.can_extend = False
#             current_session.save()
#
#         else:
#             # check for expired sessions not terminated yet
#             current_session = models.Sessions.objects.filter(user=user, expiration_date__lt=date.today(),
#                                                              termination_date__isnull=True,
#                                                              billed_date__isnull=True).first()
#
#             if current_session:
#                 models.Sessions.objects.create(user=user, therapist=therapist, duration=plan_duration,
#                                                extended_session=current_session)
#
#                 # terminate current session
#                 # set as non extendable
#                 current_session.termination_date = date.today()
#                 current_session.can_extend = False
#                 current_session.save()
#
#             else:
#                 current_session = models.Sessions.objects.filter(user=user, expiration_date__isnull=True,
#                                                                  termination_date__isnull=True,
#                                                                  billed_date__isnull=True).first()
#
#                 if current_session:
#                     models.Sessions.objects.create(user=user, therapist=therapist, duration=plan_duration,
#                                                    extended_session=current_session)
#
#                     # terminate current session
#                     # set as non extendable
#                     current_session.termination_date = date.today()
#                     current_session.can_extend = False
#                     current_session.save()
#                 else:
#                     # expiration_date = utils.add_working_days(date.today(), plan_duration)
#                     models.Sessions.objects.create(user=user, therapist=therapist, duration=plan_duration)
#
#     return therapist


def replace_session(therapist_id, user_id, rolled_over_duration, current_session_id, **session_data):
    """
    replace session information
    :param therapist_id
    :param user_id
    :param rolled_over_duration
    :param current_session_id
    :param session_data
    :return:
    """
    try:
        current_session = models.Sessions.objects.get(pk=current_session_id)

        user = models.Account.objects.get(pk=user_id)

        # add new plan duration
        if current_session.new_plan:
            rolled_over_duration += current_session.new_plan.duration

            # update profile current plan
            user.profile.current_plan = current_session.new_plan
            user.profile.save()

        new_session = models.Sessions.objects.create(
            user_id=user.id,
            therapist_id=therapist_id,
            rolled_over_duration=rolled_over_duration,
            extended_session=current_session
        )

        current_session.update(**session_data)
        current_session.save()

        return new_session
    except models.Account.DoesNotExist as e:
        raise e
    except models.Sessions.DoesNotExist as e:
        raise e
    except Exception as e:
        raise e


def get_current_session(user_id):
    """
    returns current user session
    :param user_id:
    :return:
    """
    # query current active session using the following rules
    # terminated
    #   - paid for and extendable
    # billed
    #   - terminated (above)
    #   - not expired
    #   - expired (not applicable)
    # not started yet
    # - not terminated, not billed
    try:
        user = models.Account.objects.get(pk=user_id)

        today = date.today()

        # billed but terminated and extendable
        if models.Sessions.objects.filter(user=user, billed_date__isnull=False, termination_date__isnull=False,
                                          can_extend=True).first():
            return models.Sessions.objects.filter(user=user, billed_date__isnull=False, can_extend=True,
                                                  termination_date__isnull=False).first()

        # billed, not expired, not terminated
        elif models.Sessions.objects.filter(user=user, billed_date__isnull=False, expiration_date__gte=today,
                                            termination_date__isnull=True).first():
            return models.Sessions.objects.filter(user=user, billed_date__isnull=False, expiration_date__gte=today,
                                                  termination_date__isnull=True).first()

        # not started and not terminated
        elif models.Sessions.objects.filter(user=user, termination_date__isnull=True, expiration_date__isnull=True,
                                            billed_date__isnull=True).first():
            return models.Sessions.objects.filter(user=user, termination_date__isnull=True,
                                                  expiration_date__isnull=True, billed_date__isnull=True).first()

        return None

    except models.Account.DoesNotExist as e:
        raise e


def update_session_details(therapist_id, user_id,  **kwargs):
    """
    change session if query resolves
    :param therapist_id:
    :param user_id:
    :param kwargs:
    :return:
    """

    try:
        therapist = models.Therapist.objects.get(pk=therapist_id)

        user = models.Account.objects.get(pk=user_id)

        today = date.today()

        current_session = get_current_session(user.id)

        if not current_session:
            return models.Sessions.objects.create(user=user, therapist=therapist)

        # billed not expired
        if current_session.billed_date is not None and current_session.expiration_date > today:

            # compute new duration
            # new_duration = plan_duration + np.busday_count(today, current_session.expiration_date)
            new_duration = np.busday_count(today, current_session.expiration_date)
            return replace_session(therapist_id=therapist.id, user_id=user.id, rolled_over_duration=new_duration,
                                   current_session_id=current_session.id, **dict(can_extend=False,
                                                                                 termination_date=today))

        # billed but extendable
        elif current_session.billed_date is not None and current_session.can_extend is True:
            # new_duration = plan_duration + np.busday_count(today, current_session.termination_date)
            new_duration = np.busday_count(today, current_session.termination_date)
            return replace_session(therapist_id=therapist.id, user_id=user.id, rolled_over_duration=new_duration,
                                   current_session_id=current_session.id,
                                   **dict(can_extend=False, termination_date=today))

        # new_duration = plan_duration if plan_duration > 0 else user.profile.current_plan.duration
        return replace_session(therapist_id=therapist.id, user_id=user.id, rolled_over_duration=0,
                               current_session_id=current_session.id,
                               **dict(can_extend=False, termination_date=today))

    except models.Therapist.DoesNotExist as e:
        raise e
    except models.Account.DoesNotExist as e:
        raise e


def create_update_therapy_session(user_id, duration):
    """
    creates therapy session with therapist
    :param user_id:
    :param duration
    :return:
    """
    try:
        user = models.Account.objects.get(pk=user_id)

        profile = user.profile

        therapist = profile.current_therapist

        update_session_details(therapist_id=therapist.id, user_id=user.id)

        return user

    except models.Account.DoesNotExist as e:
        raise e
    except models.Therapist.DoesNotExist as e:
        raise e


def quit_therapy(user_id, body, send_to_therapist=False):
    """
    quit therapy
    :param user_id:
    :return:
    """
    try:
        user = models.Account.objects.get(pk=user_id)

        current_session = get_current_session(user.id)
        current_session.termination_date = date.today()

        # make extendable if expiration > today
        if current_session.expiration_date > date.today():
            current_session.can_extend = True

        # save current session
        current_session.save()

        # reset conversation and call counter limit
        profile = user.profile
        profile.counter_conversation = 0
        profile.counter_call = 0
        profile.save()

        # save reason for termination
        if body:
            models.TherapyFeedback.objects.create(therapy_session=current_session, body=body, subject='Quit Therapy')

        # mail therapist and admin
        tasks.send_mail_therapist_therapy_terminated.delay(current_session.therapist.id, user.id, body, send_to_therapist)

        return profile

    except models.Account.DoesNotExist as e:
        raise e


def retrieve_subscribe_session(therapist_id, user_id,  **kwargs):
    """
    retrievable subscribable session
    :param therapist_id
    :param user_id:
    :param kwargs:
    :return:
    """

    try:
        therapist = models.Therapist.objects.get(pk=therapist_id)

        user = models.Account.objects.get(pk=user_id)

        today = date.today()

        current_session = get_current_session(user.id)

        if not current_session:
            return models.Sessions.objects.create(user=user, therapist=therapist)

        if current_session.termination_date is None and current_session.billed_date is None:
            return current_session

        # billed but extendable
        if current_session.billed_date is not None and current_session.can_extend is True:
            # new_duration = plan_duration + np.busday_count(today, current_session.termination_date)
            if current_session.start_date > current_session.termination_date:
                new_duration = np.busday_count(current_session.start_date, current_session.expiration_date)
            else:
                new_duration = np.busday_count(current_session.termination_date, current_session.expiration_date)

            return replace_session(therapist_id=therapist.id, user_id=user.id, rolled_over_duration=new_duration,
                                   current_session_id=current_session.id,
                                   **dict(can_extend=False, termination_date=today))

        # billed not expired
        elif current_session.billed_date is not None and current_session.expiration_date > today:

            # compute new duration
            # new_duration = plan_duration + np.busday_count(today, current_session.expiration_date)
            new_duration = np.busday_count(today, current_session.expiration_date)
            return replace_session(therapist_id=therapist.id, user_id=user.id, rolled_over_duration=new_duration,
                                   current_session_id=current_session.id, **dict(can_extend=False,
                                                                                 termination_date=today))

        # new_duration = plan_duration if plan_duration > 0 else user.profile.current_plan.duration
        return replace_session(therapist_id=therapist.id, user_id=user.id, rolled_over_duration=0,
                               current_session_id=current_session.id,
                               **dict(can_extend=False, termination_date=today))

    except models.Therapist.DoesNotExist as e:
        raise e
    except models.Account.DoesNotExist as e:
        raise e


def increment_conversation_counter(profile_id):
    """
    increment profile conversation counter
    :param profile_id:
    :return:
    """
    try:
        profile = models.Profile.objects.get(pk=profile_id)
        profile.counter_conversation += 1
        profile.save()

        return profile
    except models.Profile.DoesNotExist as e:
        raise e


def increment_call_counter(profile_id):
    """
    increment profile call counter
    :param profile_id:
    :return:
    """
    try:
        profile = models.Profile.objects.get(pk=profile_id)
        profile.counter_call += 1
        profile.save()

        return profile
    except models.Profile.DoesNotExist as e:
        raise e

