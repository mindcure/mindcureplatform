from django.db import models as db_models
from django.core.files.base import ContentFile

from app import models
from app.services import utils


def create_transaction(user_id, order_id):
    """
    create transactions
    :param user_id:
    :param order_id:
    :return:
    """
    user = models.Account.objects.get(pk=user_id)

    if not user:
        raise db_models.ObjectDoesNotExist

    order = models.Order.objects.get(pk=order_id)

    if not order:
        raise db_models.ObjectDoesNotExist

    transaction_status = models.TransactionStatus.objects.filter(code='pending').first()

    if not transaction_status:
        raise db_models.ObjectDoesNotExist

    reference_code = utils.random_generator(12)
    while models.Order.objects.filter(reference_code=reference_code).exists():
        reference_code = utils.random_generator(12)

    transaction = models.Transaction.objects.create(user=user, order=order, transaction_status=transaction_status,
                                                    amount=order.amount, reference_code=reference_code)

    return transaction


def process_order(user_id, cart_id):
    """
    create order for item
    :param user_id:
    :param cart_id:
    :return:
    """
    cart = models.Cart.objects.get(pk=cart_id)

    if not cart:
        raise db_models.ObjectDoesNotExist

    user = models.Account.objects.get(pk=user_id)

    if not user:
        raise db_models.ObjectDoesNotExist

    order_status = models.OrderStatus.objects.filter(code='pending').first()
    payment_status = models.PaymentStatus.objects.filter(code='pending').first()

    reference_code = utils.random_generator(12)
    while models.Order.objects.filter(reference_code=reference_code).exists():
        reference_code = utils.random_generator(12)

    order = models.Order.objects.filter(user_id=user.id, cart_id=cart.id).first()

    if not order:
        order = models.Order.objects.create(order_status=order_status, payment_status=payment_status, user=user,
                                            reference_code=reference_code, amount=cart.total_price, cart=cart)

        for item in cart.items.all():
            models.OrderItem.objects.create(order=order, user=user, product=item.product, quantity=item.quantity,
                                            unit_price=item.unit_price, total_price=item.total_price)

    return create_transaction(user.id, order.id)


def generate_resource(order_item_id):
    """
    generate resource matching order item
    :param order_item_id:
    :return:
    """
    order_item = models.OrderItem.objects.get(pk=order_item_id)

    if not order_item:
        raise db_models.ObjectDoesNotExist

    # create a copy of file in resources directory
    resource_file = ContentFile(order_item.product.resource.read())
    resource_name = order_item.product.resource.name.split('/')
    resource_file.name = resource_name[1]

    resource = models.Resources.objects.create(order_item=order_item, name=order_item.product.name,
                                               user=order_item.user, resource_item=resource_file)
    resource.save()

    return resource


def complete_order(order_id, status='delivered'):
    """

    :param order_id:
    :param status:
    :return:
    """
    order = models.Order.objects.get(pk=order_id)

    if not order:
        raise db_models.ObjectDoesNotExist

    order_status = models.OrderStatus.objects.filter(code=status).first()

    if not order_status:
        raise db_models.ObjectDoesNotExist

    order.order_status = order_status
    order.save()

    # generate resource for each item
    for item in order.order_items.all():
        generate_resource(item.id)

    return order


def close_transaction(transaction_reference, tx_ref, status='successful'):
    """
    complete transaction
    :param transaction_reference:
    :param tx_ref
    :param status:
    :return:
    """
    transaction = models.Transaction.objects.get(reference_code=transaction_reference)

    if not transaction:
        raise db_models.ObjectDoesNotExist

    status = 'successful' if status == 'success' else status

    transaction_status = models.TransactionStatus.objects.filter(code=status).first()

    if not transaction_status:
        raise db_models.ObjectDoesNotExist

    # update order status
    if status == 'successful':
        order_status = 'delivered'
    else:
        order_status = 'cancelled'

    complete_order(transaction.order_id, order_status)

    # update transaction if order updated
    transaction.transaction_status = transaction_status
    transaction.txRef = tx_ref
    transaction.save()

    return transaction
