from datetime import datetime

from django.template.loader import render_to_string
from premailer import transform

from app import models
import mongo


def send_message(subject, body, user_read=True, user_replied=False, therapist_read=False, therapist_replied=False,
                 username=None, user_id=None, therapist_id=None, **kwargs):
    """
    send message
    :param subject:
    :param body:
    :param user_read:
    :param user_replied:
    :param therapist_read:
    :param therapist_replied:
    :param user_id:
    :param therapist_id:
    :param username
    :param kwargs:
    :return:
    """
    data = dict(user_read=user_read, user_replied=user_replied, user_id=user_id,  therapist_id=therapist_id,
                therapist_read=therapist_read, therapist_replied=therapist_replied, subject=subject, body=body,
                username=username)

    timestamp = (datetime.today() - datetime(1970, 1, 1)).total_seconds()
    identifier = mongo.MongoService.get_next_sequence('messages')
    resp = mongo.MongoService.record_data('messages', identifier, timestamp, **data)
    return resp.inserted_id


def build_html_message(sender, domain, link, image_url, thumbnail_url, tag='general', recipient=''):
    """
    build sms message based on recipient information and message tag
    :return:
    """
    custom_message = models.SystemMessage.objects.filter(tag=tag).order_by('?').first()
    message = custom_message.html_text % sender
    html = render_to_string('emails/default.html', locals())
    return transform(''' %s ''' % html)