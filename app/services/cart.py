import json
from datetime import date

from app import models, serializers

from app.services import encoders


def get_session_cart(request):
    """
    get or create session cart id for current user
    :return:
    """
    # if request.user.is_superuser:
    #     raise Exception('Admin user cannot access cart, login as regular user')

    if 'cart' not in request.session:
        cart = get_active_cart(request.user.account.id) if request.user.is_authenticated() \
            else models.Cart.objects.create()

        request.session['cart'] = json.dumps(serializers.CartSerializer(cart).data, cls=encoders.DateTimeEncoder)

    return request.session['cart']


def get_current_cart(request):
    """
    retrieve current cart
    :param request:
    :return:
    """
    sess = json.loads(get_session_cart(request))
    try:
        session_cart = models.Cart.objects.get(pk=sess['id'])
    except Exception as e:
        if hasattr(request.session, 'cart'):
            del request.session['cart']
        raise e
        # sesh_cart = json.loads(get_session_cart(request))
        # session_cart = models.Cart.objects.get(pk=sess['id'])

    return session_cart


def get_active_cart(user_id):
    """
    retrieve active cart of user
    :param user_id:
    :return:
    """
    return models.Cart.objects.filter(user_id=user_id, checked_out=False).first() if models.Cart.objects.filter(
        user_id=user_id, checked_out=False).exists() else models.Cart.objects.create(user_id=user_id)


def merge_cart(request):
    """
    merge current cart with user's existing cart after user logs on
    :param request:
    :return:
    """
    session_cart = json.loads(request.session['cart'])
    # print(session_cart)
    # exit()

    try:
        current_cart = models.Cart.objects.get(pk=session_cart['id'])
    except Exception as e:
        del request.session['cart']
        sesh_cart = json.loads(get_session_cart(request))
        current_cart = models.Cart.objects.get(pk=sesh_cart['id'])

    current_user = request.user.account
    user_cart = models.Cart.objects.filter(user_id=current_user.id, checked_out=True).first()

    if user_cart:
        user_products = map(lambda x: x.id, user_cart.items.select_related('product'))
        for item in current_cart.items.all():
            if item.product.id in user_products:
                user_item = next(i for i in user_cart.items.all() if i == item.product.id)
                user_item.quantity += item.quantity
                user_item.save()

                item.delete()
                continue
            item.cart = user_cart
            item.user = current_user

        request.session['cart'] = json.dumps(serializers.CartSerializer(user_cart).data, cls=encoders.DateTimeEncoder)
        return user_cart

    current_cart.user_id = current_user.id
    map(lambda x: change_item_owner(x, current_user), current_cart.items.all())
    current_cart.save()
    request.session['cart'] = json.dumps(serializers.CartSerializer(current_cart).data, cls=encoders.DateTimeEncoder)
    return current_cart


def add_to_cart(cart_id, product_id, quantity):
    """
    add item to cart
    :param cart_id:
    :param product_id:
    :param quantity:
    :return:
    """
    cart = models.Cart.objects.get(pk=cart_id)

    if not cart:
        raise Exception('Cart not found')

    product = models.Product.objects.get(pk=product_id)

    if not product:
        raise Exception('Product not found')

    if quantity > product.available_quantity:
        raise Exception("Requested quantity not available")

    try:
        item = models.Item.objects.filter(cart_id=cart.id, product_id=product.id)

        if not item:
            item = models.Item.objects.create(cart=cart, product=product, quantity=quantity, unit_price=product.unit_price)

        item.quantity += quantity
        item.save()
        return True
    except Exception:
        raise Exception("Unable to add to cart")


def change_item_owner(item, user):
    """
    change owner of cart item
    :param item:
    :param user:
    :return:
    """
    item.user_id = user.id
    item.save()
    return item


def checkout_cart(request, cart_id):
    """
    checkout cart
    :param request
    :param cart_id:
    :return:
    """
    # current_cart = json.loads(request.session['cart'])
    session_cart = models.Cart.objects.get(pk=cart_id)

    if not session_cart:
        raise models.Cart.DoesNotExist

    session_cart.checked_out = True
    session_cart.checkout_date = date.today()
    session_cart.save()

    if request.session.get('cart'):
        del request.session['cart']

    return get_current_cart(request)
