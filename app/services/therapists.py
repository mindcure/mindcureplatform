import bcrypt
import json

from django.core.files.base import ContentFile

import redis

from app import models, serializers


def check_email(email):
    """
    check if therapist account matching email exists
    :param email:
    :return:
    """
    try:
        return models.Therapist.objects.filter(email=email).exists()
    except Exception as e:
        raise e


def create_practitioner_request(therapist_id, **kwargs):
    """
    create practitioner request
    :return
    """
    try:
        therapist = models.Therapist.objects.get(pk=therapist_id)
    except models.Therapist.DoesNotExist:
        raise

    if models.RequestInfo.objects.filter(therapist_id=therapist.pk).first():
        raise Exception(u'Therapist request info matching this user already exists')

    cover_letter = kwargs.pop('cover_letter')
    request_info = models.RequestInfo.objects.create(cover_letter=cover_letter, therapist=therapist)

    for question_id, body in kwargs.iteritems():
        q = question_id.replace('que_', '')
        question = models.RequestInfoQuestion.objects.get(number=int(q))
        models.RequestInfoResponse.objects.create(request_info=request_info, request_info_question=question, body=body)

    return request_info


def create_therapist_account(email, phone, line1, line2, **kwargs):
    """
    create therapist account
    :return:
    """
    if models.Therapist.objects.filter(email=email).exists():
        raise Exception(u'Therapist account matching this user already exists')

    # state = models.State.objects.filter(code=kwargs.pop('state')).first()
    #
    # if not state:
    #     raise Exception('State not Found')

    country = models.Country.objects.filter(code=kwargs.pop('country')).first()

    if not country:
        raise Exception('Country not Found')

    qualification = models.Qualification.objects.filter(code=kwargs.pop('qualification')).first()

    if not qualification:
        raise Exception('Qualification not Found')

    address = models.Address.objects.create(phone=phone, line1=line1, line2=line2, country=country)

    password = bcrypt.hashpw(kwargs.pop('password').encode('utf-8'), bcrypt.gensalt(12))

    data = {
        'name': kwargs.pop('name'),
        'email': email,
        'password': password,
        'bio': kwargs.pop('bio'),
        'address': address,
        'qualification': qualification,
        'resume': kwargs.pop('resume'),
        'profile_image': kwargs.pop('profile_image')
    }

    therapist = models.Therapist.objects.create(**data)

    if therapist:
        create_practitioner_request(therapist.id, **kwargs)

    return therapist


def authenticate(email, password):
    """
    authenticate therapist
    """
    therapist = models.Therapist.objects.filter(email=email).first()

    if not therapist or not bcrypt.checkpw(password, therapist.password):
        return None

    return therapist


def update_therapist(therapist_id, qualification, state, country, profile_image, **kwargs):
    """
    update therapist
    :todo update profile image logic
    :param therapist_id:
    :param qualification
    :param state
    :param country
    :param kwargs:
    :return:
    """
    therapist = models.Therapist.objects.filter(pk=therapist_id).first()

    if not therapist:
        raise models.Therapist.DoesNotExist

    data = kwargs.copy()

    data['qualification'] = models.Qualification.objects.get(code=qualification)

    # update address info
    address_data = dict()

    if state:
        address_data['state'] = models.State.objects.get(name=state)

    address_data['country'] = models.Country.objects.get(name=country)

    address_data['phone'] = data.pop('phone')
    address_data['city'] = data.pop('city')
    address_data['line1'] = data.pop('line1')
    address_data['line2'] = data.pop('line2')

    for a_key, a_value in address_data.iteritems():
        if hasattr(therapist.address, a_key):
            setattr(therapist.address, a_key, a_value)

    therapist.address.save()

    # update profile
    if profile_image:
        if len(profile_image):
            # create a copy of file in resources directory
            resource_file = ContentFile(profile_image[0].read())
            resource_name = profile_image[0].name.split('/')
            resource_file.name = resource_name[0]

            # update profile image
            data.update({
                'profile_image': resource_file
            })

    # update therapist info
    for key, value in data.iteritems():
        if hasattr(therapist, key):
            setattr(therapist, key, value)

    therapist.save()

    return therapist


def deactivate_account(therapist_id):
    """
    deactivate therapist account
    :param therapist_id:
    :return:
    """
    try:
        therapist = models.Therapist.objects.get(pk=therapist_id)

        therapist.is_activated = False
        therapist.save()

        return therapist

    except models.Therapist.DoesNotExist:
        raise
    except Exception:
        raise


def request_activation(therapist_id):
    """
    deactivate therapist account
    :param therapist_id:
    :todo add send email implementation
    :return:
    """
    try:
        therapist = models.Therapist.objects.get(pk=therapist_id)

        return therapist

    except models.Therapist.DoesNotExist:
        raise
    except Exception:
        raise


def add_rating(therapist_id, rating, **kwargs):
    """
    add new rating for therapist
    :param therapist_id:
    :param rating:
    :return:
    """
    try:
        therapist = models.Therapist.objects.get(pk=therapist_id)

        # create new rating entry
        rating_entry = models.RatingEntry.objects.create(therapist=therapist, rating=rating)

        # find new average rating
        # update ratings
        therapist.rating = models.RatingEntry.objects.filter(therapist_id=therapist.id).all().aggregate(
            models.Avg('rating'))
        therapist.save()

        return rating_entry
    except models.Therapist.DoesNotExist:
        raise
    except models.Account.DoesNotExist:
        raise
    except Exception:
        raise


def add_review(user_id, therapist_id, title, body, **kwargs):
    """
    add new rating for therapist
    :param user_id
    :param therapist_id:
    :param title:
    :param body
    :return:
    """
    try:
        therapist = models.Therapist.objects.get(pk=therapist_id)

        user = models.Account.objects.get(pk=user_id)

        # create new review entry
        review = models.Review.objects.create(therapist=therapist, title=title, body=body, user=user)

        return review
    except models.Therapist.DoesNotExist:
        raise
    except models.Account.DoesNotExist:
        raise
    except Exception:
        raise


def update_therapist_billing_settings(therapist_id, **kwargs):
    """
    update therapist billing settings
    :param therapist_id
    :param kwargs:
    :return:
    """
    therapist = models.Therapist.objects.filter(pk=therapist_id).first()

    if not therapist:
        raise models.Therapist.DoesNotExist

    data = kwargs.copy()

    billing_settings = models.TherapistBillingSettings.objects.filter(therapist_id=therapist_id).first()

    if not billing_settings:
        data['therapist_id'] = therapist.id
        billing_settings = models.TherapistBillingSettings.objects.create(**data)

    for key, value in kwargs.iteritems():
        setattr(billing_settings, key, value)

    billing_settings.save()

    # check payment settings onboarding if not checked
    if not therapist.onboarding.payment_step_completed:
        therapist.onboarding.payment_step_completed = True
        therapist.onboarding.save()

    return therapist


def update_chat_step_completed(therapist_id):
    """
    update therapist onboarding chat_step_completed
    :param therapist_id:
    :return:
    """
    therapist = models.Therapist.objects.filter(pk=therapist_id).first()

    if not therapist:
        raise models.Therapist.DoesNotExist

    therapist.onboarding.chat_step_completed = True
    therapist.onboarding.save()

    return therapist


def get_therapist(pk):
    """
    get therapist matching pk
    :param pk:
    :return:
    """
    try:
        return models.Therapist.objects.get(pk=pk)
    except models.Therapist.DoesNotExist as e:
        raise e


def save_redis_therapist(token, therapist_id):
    """
    save redis user for website
    :param token:
    :param username_token:
    :param therapist_id
    :return:
    """
    try:
        therapist_obj = models.Therapist.objects.get(pk=therapist_id)

        redis_obj = redis.StrictRedis()
        serialized_user = serializers.TherapistSerializer(therapist_obj).data

        # save redis username
        redis_obj.set('therapist:{}'.format(therapist_obj.email), json.dumps(serialized_user))

        # save redis with token
        return redis_obj.set('therapist:{}'.format(token), json.dumps(serialized_user))
    except models.Therapist.DoesNotExist as e:
        raise e


def clear_redis_user(token, therapist_id):
    """
    clear therapist record in redis
    :param therapist_id
    :param token:
    :return:
    """
    try:
        therapist_obj = models.Therapist.objects.get(pk=therapist_id)
        redis_obj = redis.StrictRedis()

        # delete mobile redis user
        redis_obj.delete('therapist:{}'.format(therapist_obj.email))

        return redis_obj.delete('therapist:{}'.format(token))
    except models.Therapist.DoesNotExist as e:
        raise e
    except Exception as e:
        raise e
