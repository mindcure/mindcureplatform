import json

from django.conf import settings

from pymongo import MongoClient, ASCENDING, DESCENDING


class MongoService(object):

    __client__ = MongoClient(host=settings.MONGODB_HOST, port=settings.MONGODB_PORT)
    __db__ = __client__[settings.MONGODB_DATABASE]

    @classmethod
    def record_activity(cls, identifier, actor, verb, _object, object_type, browser, device, device_type, os, timestamp):
        """
        record activity into mongodb
        :return:
        """
        try:
            activities = cls.__db__.activities
            record = {
                'id': identifier,
                'actor': actor,
                'verb': verb,
                'object': _object,
                'object_type': object_type,
                'browser': browser,
                'device': device,
                'device_type': device_type,
                'os': os,
                'timestamp': timestamp
            }
            activities.insert_one(record)
            return True
        except Exception, e:
            # app.log.error('[ACTOR: %s][VERB: %s][ACTION: %s][STATUS: FAILED]' % (actor, verb, _object))
            # app.log.error(e)
            return False

    @classmethod
    def get_next_sequence(cls, collection_name):
        return cls.__db__[collection_name].find().count()+1

    @classmethod
    def record_data(cls, collection_name, identifier, timestamp, **kwargs):
        """
         record data
        :param collection_name
        :param identifier:
        :param timestamp:
        :param kwargs:
        :return:
        """
        try:
            records = cls.__db__.get_collection(collection_name)
            record = {
                'id': identifier,
                'timestamp': timestamp
            }
            record.update(kwargs)
            return records.insert_one(record)
        except Exception, e:
            # app.logger.error('[IDENTIFIER: %s][TIMESTAMP: %s][STATUS: FAILED]' % (identifier, timestamp))
            # app.logger.error(e)
            return False

    @classmethod
    def update_record(cls, collection_name, identifier, **kwargs):
        """
        update mongo record
        :param collection_name
        :param identifier
        :return
        """
        try:
            records = cls.__db__.get_collection(collection_name)
            return records.update_one({"id": identifier}, {
                '$set': kwargs
            })
        except Exception, e:
            # app.logger.error('[IDENTIFIER: %s][ACTION: UPDATE][STATUS: FAILED][BODY: %s]' % (identifier, json.dumps(kwargs)))
            # app.logger.error(e)
            return False

    @classmethod
    def delete_record(cls, collection_name, identifier):
        """
        delete mongo record
        :param collection_name
        :param identifier
        :return
        """
        try:
            records = cls.__db__.get_collection(collection_name)
            return records.delete_many({"id": identifier})
        except Exception, e:
            # app.logger.error('[IDENTIFIER: %s][ACTION: DELETE][STATUS: FAILED]' % identifier)
            # app.logger.error(e)
            return False

    @classmethod
    def retrieve_item(cls, collection_name, **kwargs):
        """
        query notifications based on data
        :param collection_name
        :param kwargs
        :return
        """
        try:
            data = cls.__db__[collection_name].find(kwargs)
            return next((i for i in data))
        except Exception, e:
            raise e

    @classmethod
    def query_collection(cls, collection_name, first_only=False, page=1, size=20, excludes=[], **kwargs):
        """
        query notifications based on data
        :param collection_name
        :param first_only
        :param page
        :param size
        :param excludes
        :param kwargs
        :return
        """
        try:
            filter_data = kwargs.copy()
            if len(excludes) > 0:
                for item in excludes:
                    for key, value in item.iteritems():
                        filter_data[key] = {"$ne" : value}

            data = cls.__db__[collection_name].find(filter_data).sort('$natural', -1)
            response = next((i for i in data)) if first_only else [i for i in data]
            return cls.build_pagination(response, page=page, size=size)
        except Exception, e:
            raise e

    @classmethod
    def aggregate_collection(cls, collection_name, key, sort_key, fields, page=1, size=20, **kwargs):
        """
        query notifications based on data
        :param collection_name
        :param key
        :param page
        :param size
        :param fields
        :param sort_key
        :param kwargs
        :return
        """
        from bson.son import SON
        try:
            # fields.append('_id')
            params = [
                {'$sort': {'timestamp': -1}},
                {'$match': {k: v for k, v in kwargs.iteritems()}},
                {'$group':  {
                    '_id': {key: '${}'.format(key)},
                    'items': {'$push': {j: '${}'.format(j) for j in fields}}
                }},
                {'$group': {
                    '_id': {key: '$_id.{}'.format(key)},
                    'data': {'$push': {'items': '$items'}}
                }}
                # {'$sort': SON([('_id.{}'.format(sort_key), -1)])}
            ]
            data = cls.__db__[collection_name].aggregate(params)
            response = list(data)
            return cls.build_pagination(response, page=page, size=size)
        except Exception, e:
            raise e

    @classmethod
    def build_pagination(cls, list_objs, page=1, size=20):
        """
        paginates data
        :param list_objs:
        :param page:
        :param size:
        :return:
        """
        from operator import itemgetter

        total = len(list_objs)
        pages = (total / size) + min(1, total % size)

        prev = max(1, page - 1) if page > 1 else None
        next_ = min(pages, page + 1) if page < pages else None

        # new_list = sorted(list_objs, key=itemgetter('timestamp'))

        page_ind = page - 1

        start = page_ind * size
        end = start + size

        items_list = list_objs[start:end] if page <= pages else []

        return {"page": page, "items": items_list, "total": total, "size": size, "prev": prev, "next": next_,
                "pages": pages}


