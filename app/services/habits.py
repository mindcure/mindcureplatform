"""
habits service
"""
from __future__ import division
from datetime import timedelta, date, datetime
from django.core.mail import send_mail
from django.conf import settings
from django.template.loader import render_to_string

from app import models, tasks
from mindcure import celery_app


def create(user_id, name, target_days, start_date, positive_response, negative_response):
    """
    create habit service
    :param user_id:
    :param name:
    :param target_days:
    :param start_date:
    :param positive_response:
    :param negative_response:
    :return:
    """
    try:
        user = models.Account.objects.get(pk=user_id)

        target_date = start_date + timedelta(days=target_days)

        habit = models.Habit.objects.create(name=name, target_date=target_date, positive_response=positive_response,
                                            negative_response=negative_response, user=user, start_date=start_date)

        return habit
    except models.Account.DoesNotExist as e:
        raise e
    except Exception as e:
        raise e


def record_check_in(user_id, habit_id, positive, negative):
    """
    record check in response
    :param user_id:
    :param habit_id:
    :param positive:
    :param negative:
    :return:
    """
    try:
        user = models.Account.objects.get(pk=user_id)

        habit = models.Habit.objects.get(pk=habit_id)

        return models.CheckIn.objects.create(user=user, habit=habit, positive=positive, negative=negative)

    except models.Account.DoesNotExist as e:
        raise e
    except models.Habit.DoesNotExist as e:
        raise e


def restart_habit(habit_id, start_date, target_days):
    """
    restart habit
    :param habit_id:
    :param start_date
    :param target_days
    :return:
    """
    try:
        habit = models.Habit.objects.get(pk=habit_id)

        # clear previous check ins
        for check_in in habit.check_ins.all():
            check_in.delete()

        # save new habit information
        habit.start_date = start_date
        habit.target_date = start_date + timedelta(days=target_days)
        habit.save()

        return habit

    except models.Habit.DoesNotExist as e:
        raise e


def check_owner(user_id, habit_id):
    """
    check model owner against user
    :param user_id:
    :param habit_id:
    :return:
    """
    try:
        habit = models.Habit.objects.get(pk=habit_id)
        return habit.user_id == user_id
    except models.Habit.DoesNotExist as e:
        raise e


# def add_companion(habit_id, email):
#     """
#     add habit companion
#     :param habit_id:
#     :param email
#     :return:
#     """
#     try:
#         habit = models.Habit.objects.get(pk=habit_id)

#         # add new habit companion
#         models.HabitCompanion.objects.create(habit=habit, email=email)

#         return habit

#     except models.Habit.DoesNotExist as e:
#         raise e
def get_habit(habit_id):
    """
    get habit 
    :param habit_id:
    :return:
    """
    try:
        habit = models.Habit.objects.get(pk=habit_id)
        return habit

    except models.Habit.DoesNotExist as e:
        raise e


def get_companion(habit_id):
    """
    get habit companion
    :param habit_id:
    :return:
    """
    try:
        habit = models.Habit.objects.get(pk=habit_id)

        return models.HabitCompanion.objects.filter(habit=habit.id).first()

    except models.Habit.DoesNotExist as e:
        raise e


def add_companion(habit_id, companion_id):
    """
    add habit companion
    :param habit_id:
    :param companion_id
    :return:
    """
    try:
        habit = models.Habit.objects.get(pk=habit_id)

        companion = models.Account.objects.get(pk=companion_id)

        # add new habit companion
        models.HabitCompanion.objects.create(habit=habit, companion=companion)

        tasks.companion_request_accepted.delay(habit.id, companion.id)

        # schedule for 1 week from now
        target = date.today() + timedelta(days=7)

        if habit.target_date <=  target:
            target = habit.target_date

        # set next target date and time (10:00)
        target_datetime = datetime(
            year=target.year,
            month=target.month,
            day=target.day,
            hour=10,
            minute=0
        )

        tasks.send_habit_report.apply_async(args=[habit.id], eta=target_datetime)

        return habit

    except models.Habit.DoesNotExist as e:
        raise e


def load_checkins(habit_id, start_date, end_date):
    """
    load checkins between period start_date and end_date
    :param habit_id:
    :param start_date:
    :param end_date:
    :return:
    """
    try:
        habit = models.Habit.objects.get(pk=habit_id)

        check_list = models.CheckIn.objects.filter(habit_id=habit.id, created_at__range=[start_date, end_date])

        negative_count = models.CheckIn.objects.filter(habit_id=habit.id, negative=True).count()
        positive_count = models.CheckIn.objects.filter(habit_id=habit.id, positive=True).count()

        return habit, check_list.all(), negative_count, positive_count
    except models.Habit.DoesNotExist as e:
        raise e