from rest_framework import exceptions, permissions

from app import models


class IsSuperAdmin(permissions.BasePermission):
    """
    Allows access only to admin users.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_superuser