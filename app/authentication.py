from django.core.exceptions import PermissionDenied
from django.contrib.auth import authenticate
from django.db.models import Q

from . import models


def authenticate_user(username, password):
    """
        authenticate user via username
        :param username
        :param password
    """
    user = models.Account.objects.filter(Q(username=username) | Q(email=username)).first()

    if user:
        return authenticate(username=user.username, password=password)
    return None
