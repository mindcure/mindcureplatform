# import json
#
# from django.template import Library
# from django.core.serializers import serialize
# from django.utils import simplejson
# from django.db.models.query import QuerySet, ValuesListQuerySet
#
# register = Library()
#
#
# @register.filter(is_safe=True)
# def jsonify(object):
#
#      if isinstance(object, ValuesListQuerySet):
#         return json.dumps(list(object))
#
#     if isinstance(object, QuerySet):
#         return serialize('json', object)
#     return json.dumps(object)
