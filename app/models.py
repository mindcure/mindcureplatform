from __future__ import unicode_literals

import os
import binascii
from datetime import date, datetime

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from .services import utils


class UpdateMixin(object):
    """
    simple mixin to update model object
    """
    def update(self, **kwargs):
        """
        update model object
        :param kwargs:
        :return:
        """
        if self._state.adding:
            raise self.DoesNotExist

        for key, value in kwargs.items():

            if hasattr(self, key):
                setattr(self, key, value)
        self.save(update_fields=kwargs.keys())


class Abstract(models.Model, UpdateMixin):

    class Meta:
        abstract = True
        ordering = ('-created_at',)

    created_at = models.DateTimeField(default=timezone.now)
    updated_at = models.DateTimeField(default=timezone.now)


class Qualification(Abstract):
    name = models.TextField(null=False, blank=False)
    code = models.CharField(max_length=255, unique=True)


class Country(Abstract):
    class Meta(object):
        """docstring for Address"""
        verbose_name = _('country')
        verbose_name_plural = _('countries')
    name = models.TextField(null=False, blank=False)
    code = models.CharField(max_length=150, blank=False, null=False)
    is_enabled = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class State(Abstract):
    name = models.TextField(null=False, blank=False)
    code = models.CharField(max_length=150, blank=False, null=False)
    is_enabled = models.BooleanField(default=False)
    country = models.ForeignKey(Country)

    def __str__(self):
        return self.name


class City(Abstract):
    class Meta(object):
        """docstring for City"""
        verbose_name = _('city')
        verbose_name_plural = _('cities')
            
    name = models.TextField(null=False, blank=False)
    code = models.CharField(max_length=150, blank=False, null=False)
    state = models.ForeignKey(State)

    def __str__(self):
        return self.name


class Address(Abstract):
    class Meta(object):
        """docstring for Address"""
        verbose_name = _('address')
        verbose_name_plural = _('addresses')
    phone = models.CharField(max_length=50, blank=True, null=True)
    line1 = models.TextField(blank=False, null=False)
    line2 = models.TextField(blank=True, null=True)
    # city = models.ForeignKey(City, null=True, blank=True)
    city = models.TextField(blank=True, null=True)
    state = models.ForeignKey(State, null=True)
    country = models.ForeignKey(Country)

    def __str__(self):
        return self.get_full_address()

    def __repr__(self):
        return '<Address: %s>' % self.get_full_address()

    @property
    def full_address(self):
        """
        full address of an address object
        :return:
        """
        return self.get_full_address()

    def get_full_address(self):
        return '%s %s %s' % (self.line1, '{},'.format(self.city) if self.city else '', '{},'.format(
            self.state.name) if self.state else '')


class School(Abstract):
    name = models.TextField(null=False, blank=False)
    code = models.CharField(max_length=255, null=False, blank=False)
    city = models.TextField(null=True, blank=True)
    state = models.ForeignKey(State)
    country = models.ForeignKey(Country)


class Education(Abstract):
    year = models.IntegerField(null=False, blank=False)
    school = models.ForeignKey(School)
    degree = models.TextField(null=False, blank=False)
    study_field = models.TextField(null=False, blank=False)
    grade = models.CharField(max_length=255, null=False, blank=False)


class Account(Abstract, User):
    class Meta(object):
        """docstring for City"""
        verbose_name = _('account')
        verbose_name_plural = _('accounts')

    address = models.ForeignKey(Address, null=True)
    date_of_birth = models.DateField(null=True, blank=True)
    is_verified = models.BooleanField(default=False)
    bio = models.TextField(null=True, blank=True)
    headline = models.CharField(max_length=255, null=True, blank=True)
    age = models.IntegerField(null=True, blank=True)
    gender = models.CharField(max_length=100, blank=True, null=True)
    country = models.CharField(max_length=200, null=True, blank=True)

    def change_password(self, password):
        """
        reset password and send password reset email
        """
        self.set_password(password)
        self.save()

        return self


class TherapistCategory(Abstract):
    """
    therapist experience
    """
    title = models.TextField()
    slug = models.SlugField()

    def save(self, raw=False, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.slug = slugify(self.title)

        super(TherapistCategory, self).save()


class Language(Abstract):
    """
    therapist experience
    """
    title = models.TextField()
    slug = models.SlugField()

    def save(self, raw=False, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.slug = slugify(self.title)

        super(Language, self).save()


class RequestInfoQuestion(Abstract):
    is_active = models.BooleanField(default=True)
    body = models.TextField(null=False, blank=False)
    is_multi_select = models.BooleanField(default=True)
    number = models.IntegerField(unique=True)

    def __repr__(self):
        return "%s" % (self.body)

    def __str__(self):
        return "%s" % (self.body)

    def __unicode__(self):
        return u'{f}'.format(f=self.body)


class Therapist(Abstract):
    class Meta(object):
        """docstring for City"""
        verbose_name = _('therapist')
        verbose_name_plural = _('therapists')

    name = models.TextField(_('name'), blank=False, null=False)
    email = models.EmailField(_('email address'), unique=True)
    password = models.CharField(blank=False, null=False, max_length=255)
    address = models.ForeignKey(Address, null=True)
    profile_image = models.ImageField(upload_to='images')
    qualification = models.ForeignKey(Qualification)
    bio = models.TextField(null=False, blank=False)
    resume = models.FileField(upload_to='resumes')
    is_activated = models.BooleanField(default=False)
    rating = models.PositiveIntegerField(default=1)
    headline = models.TextField(null=True, blank=True)
    languages = models.ManyToManyField(Language)
    categories = models.ManyToManyField(TherapistCategory)
    is_verified = models.BooleanField(default=False)

    def reviews_count(self):
        """
        returns reviews count
        :return:
        """
        return self.reviews.count()

    def sessions_count(self):
        """
        returns reviews count
        :return:
        """
        return self.sessions.count()

    def get_billings_sum(self):
        """
        returns sum of billings
        :return:
        """
        amount_sum = self.billings.aggregate(models.Sum('sub_transaction__amount'))
        return float(amount_sum.get('sub_transaction__amount__sum')) if amount_sum.get('sub_transaction__amount__sum') else 0

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class RequestInfo(Abstract):
    """"""
    cover_letter = models.TextField(null=False, blank=False)
    therapist = models.OneToOneField(Therapist, null=True, blank=True)


class RequestInfoResponse(Abstract):
    request_info = models.ForeignKey(RequestInfo)
    body = models.TextField(unique=False)
    request_info_question = models.ForeignKey(RequestInfoQuestion)


class Certification(Abstract):
    therapist = models.ForeignKey(Therapist)
    issuer = models.TextField(null=False, blank=False)
    date = models.DateField(null=False)
    description = models.TextField(null=True)
    name = models.TextField(null=False)


class Category(Abstract):
    class Meta(object):
        """docstring for Address"""
        verbose_name = _('category')
        verbose_name_plural = _('categories')
    name = models.CharField(max_length=100, null=False, blank=False)
    slug = models.SlugField(max_length=50, editable=False)

    def save(self):
        """
        save slug on category create
        :return:
        """
        if not self.id:
            self.slug = slugify(self.name)

        super(Category, self).save()

    def __str__(self):
        return self.name


class Product(Abstract):
    name = models.TextField(null=False, blank=False)
    description = models.TextField(null=True, blank=True)
    sku = models.TextField(null=False, blank=False, editable=False)
    categories = models.ManyToManyField(Category)
    unit_price = models.DecimalField(max_digits=10, decimal_places=2)
    available_quantity = models.IntegerField(default=0)
    cover_image = models.ImageField(upload_to='products')
    resource = models.FileField(upload_to='products')
    external_url = models.URLField(null=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id:
            sku = utils.random_generator(20)
            while Product.objects.filter(sku=sku).exists():
                sku = utils.random_generator(20)
            self.sku = utils.random_generator()

        super(Product, self).save()

    def __str__(self):
        return self.name


# class Variant(Abstract):
#     name = models.TextField(null=False, blank=False)
#     slug = models.SlugField(null=False, blank=False, editable=False)
#     product = models.ForeignKey(Product, related_name='variants')
#     cover_image = models.ImageField(upload_to='products')
#
#     def save(self, force_insert=False, force_update=False, using=None,
#              update_fields=None):
#         if not self.id:
#             self.slug = slugify(self.name)
#             self.cover_image = self.product.cover_image if not self.cover_image else self.cover_image
#
#         super(Variant, self).save()


class OrderStatus(Abstract):
    class Meta(object):
        """docstring for Address"""
        verbose_name = _('order status')
        verbose_name_plural = _('order statuses')
    name = models.CharField(max_length=200)
    code = models.CharField(max_length=50)


class PaymentStatus(Abstract):
    class Meta(object):
        """docstring for Address"""
        verbose_name = _('payment status')
        verbose_name_plural = _('payment statuses')
    name = models.CharField(max_length=200)
    code = models.CharField(max_length=50)


class Cart(Abstract):
    user = models.ForeignKey(Account, null=True)
    checked_out = models.BooleanField(default=False, verbose_name=_('checked out'))
    checkout_date = models.DateField(null=True, blank=True)

    class Meta:
        verbose_name = _('cart')
        verbose_name_plural = _('carts')

    def __unicode__(self):
        return unicode(self.created_at)

    def total_price(self):
        return sum([c.total_price for c in Item.objects.filter(cart_id=self.id).all()])

    total_price = property(total_price)


class Order(Abstract):
    cart = models.ForeignKey(Cart, related_name='carts')
    user = models.ForeignKey(Account)
    reference_code = models.CharField(max_length=150, unique=True)
    payment_status = models.ForeignKey(PaymentStatus)
    order_status = models.ForeignKey(OrderStatus)
    amount = models.DecimalField(max_digits=18, decimal_places=2, verbose_name=_('total price'))


class OrderItem(Abstract):
    user = models.ForeignKey(Account)
    order = models.ForeignKey(Order, verbose_name=_('order'), related_name='order_items')
    quantity = models.PositiveIntegerField(verbose_name=_('quantity'))
    unit_price = models.DecimalField(max_digits=18, decimal_places=2, verbose_name=_('unit price'))
    product = models.ForeignKey(Product)
    total_price = models.DecimalField(max_digits=18, decimal_places=2, verbose_name=_('total price'))


class TransactionStatus(Abstract):
    class Meta(object):
        """docstring for Address"""
        verbose_name = _('transaction status')
        verbose_name_plural = _('transaction statuses')
    name = models.CharField(max_length=200)
    code = models.CharField(max_length=50)


class Transaction(Abstract):
    order = models.ForeignKey(Order)
    user = models.ForeignKey(Account)
    reference_code = models.TextField(null=False)
    txRef = models.TextField(null=True)
    amount = models.FloatField(default=0.0)
    transaction_status = models.ForeignKey(TransactionStatus)


class SubscriptionTransaction(Abstract):
    user = models.ForeignKey(Account)
    txRef = models.TextField(null=True)
    amount = models.DecimalField(decimal_places=2, max_digits=10)
    deducted_amount = models.DecimalField(decimal_places=2, max_digits=10)
    total_amount = models.DecimalField(decimal_places=2, max_digits=10)
    reference_code = models.TextField(null=False)
    session = models.ForeignKey('Sessions', null=True)
    duration = models.PositiveIntegerField(null=False, blank=False)
    transaction_status = models.ForeignKey(TransactionStatus)
    start_date = models.DateField(null=False, blank=False)


class Item(Abstract):
    user = models.ForeignKey(Account, null=True)
    cart = models.ForeignKey(Cart, verbose_name=_('cart'), related_name='items')
    quantity = models.PositiveIntegerField(verbose_name=_('quantity'))
    unit_price = models.DecimalField(max_digits=18, decimal_places=2, verbose_name=_('unit price'))
    product = models.ForeignKey(Product)
    # # product as generic relation
    # content_type = models.ForeignKey(ContentType)
    # object_id = models.PositiveIntegerField()
    #
    # objects = ItemManager()

    class Meta:
        verbose_name = _('item')
        verbose_name_plural = _('items')
        ordering = ('cart',)

    def __unicode__(self):
        return u'%d unit(s) of %s' % (self.quantity, self.product.__name__)

    def total_price(self):
        return self.quantity * self.unit_price
    total_price = property(total_price)

    def __str__(self):
        return self.product.name

    def __repr__(self):
        return self.product.name

    # # product
    # def get_product(self):
    #     return self.content_type.get_object_for_this_type(pk=self.object_id)
    #
    # def set_product(self, product):
    #     self.content_type = ContentType.objects.get_for_model(type(product))
    #     self.object_id = product.pk

    # product = property(get_product, set_product)


class Resources(Abstract):
    class Meta(object):
        """docstring for Address"""
        verbose_name = _('resource')
        verbose_name_plural = _('resources')
    user = models.ForeignKey(Account)
    resource_item = models.FileField(upload_to='resources')
    order_item = models.ForeignKey(OrderItem)
    name = models.CharField(max_length=250)


class AssessmentTest(Abstract):
    title = models.TextField()
    slug = models.SlugField(editable=False)
    banner_image = models.ImageField(upload_to='images')
    description = models.TextField(null=True, blank=True)
    low_score_response = models.TextField(null=True, blank=True)
    good_score_response = models.TextField(null=True, blank=True)
    low_score_threshold = models.PositiveIntegerField(null=True, blank=True)

    def save(self, raw=False, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.slug = slugify(self.title)

        super(AssessmentTest, self).save()

    def __str__(self):
        return self.title


class QuestionResponse(Abstract):
    name = models.TextField(unique=True)
    slug = models.SlugField(editable=False)
    score = models.PositiveIntegerField(default=1)

    def save(self, raw=False, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.slug = slugify(self.name)

        super(QuestionResponse, self).save()

    def __str__(self):
        return self.name


class PreTherapyResponse(Abstract):
    name = models.TextField(unique=True)
    slug = models.SlugField(editable=False)

    def save(self, raw=False, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.id:
            self.slug = slugify(self.name)

        super(PreTherapyResponse, self).save()

    def __str__(self):
        return self.name


class PreTherapyQuestion(Abstract):
    is_active = models.BooleanField(default=True)
    body = models.TextField(null=False, blank=False)
    responses = models.ManyToManyField(PreTherapyResponse, blank=True)

    def __repr__(self):
        return "%s" % (self.body)

    def __str__(self):
        return "%s" % (self.body)

    def __unicode__(self):
        return u'{f}'.format(f=self.body)


class UserPreTherapy(Abstract):
    user = models.OneToOneField(Account, related_name='pre_therapy')
    is_completed = models.BooleanField(default=False)
    # score = models.IntegerField(default=0)


class UserPreTherapyResponse(Abstract):
    user = models.ForeignKey(Account)
    pre_assessment = models.ForeignKey(UserPreTherapy, related_name='responses', on_delete=models.CASCADE)
    response = models.TextField(null=False, blank=False)
    question = models.TextField(null=False, blank=False)
    code = models.CharField(max_length=255)


class Question(Abstract):
    # number = models.IntegerField(null=True, editable=False, blank=True)
    is_active = models.BooleanField(default=True)
    body = models.TextField(null=False, blank=False)
    assessment_test = models.ForeignKey(AssessmentTest, related_name='questions')
    responses = models.ManyToManyField(QuestionResponse)

    def __repr__(self):
        return "%s" % (self.body)

    def __str__(self):
        return "%s" % (self.body)

    def __unicode__(self):
        return u'{f}'.format(f=self.body)


class UserAssessment(Abstract):
    user = models.ForeignKey(Account, related_name='user_assessments')
    assessment_test = models.ForeignKey(AssessmentTest)
    is_completed = models.BooleanField(default=False)
    score = models.IntegerField(default=0)


class UserResponse(Abstract):
    user = models.ForeignKey(Account)
    assessment = models.ForeignKey(UserAssessment, related_name='responses')
    response = models.TextField(null=False, blank=False)
    question = models.TextField(null=False, blank=False)


class Testimonial(Abstract):
    body = models.TextField(null=False, blank=False)
    is_active = models.BooleanField(default=True)
    name = models.TextField()
    is_therapist = models.BooleanField(default=False)

    def __str__(self):
        return 'Testimonial: {}'.format(self.name)


class OnBoarding(Abstract):
    user = models.ForeignKey(Account, related_name='onboarding')
    pre_therapy_step_completed = models.BooleanField(default=False)
    plan_step_completed = models.BooleanField(default=False)
    therapist_step_completed = models.BooleanField(default=False)
    # session_step_completed = models.BooleanField(default=False)


class Rule(Abstract):
    """
    plan rules
    """
    name = models.CharField(max_length=200, null=False, blank=False)
    duration = models.PositiveIntegerField(null=False, blank=False)
    is_continuous = models.BooleanField(default=False)
    price = models.DecimalField(null=False, blank=False, decimal_places=2, max_digits=18)
    description = models.TextField(null=True, blank=True)


class PlanType(Abstract):
    """
        plan type models
    """
    name = models.CharField(max_length=200, null=False, blank=False)
    code = models.CharField(max_length=150, null=False, blank=False)
    price = models.DecimalField(null=False, blank=False, decimal_places=2, max_digits=18)
    description = models.TextField(null=True, blank=True)
    rules = models.ManyToManyField(Rule)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.id:
            self.code = slugify(self.name)
            
        super(PlanType, self).save()


class Review(Abstract):
    """
    therapy reviews
    """
    therapist = models.ForeignKey(Therapist, related_name='reviews')
    user = models.ForeignKey(Account)
    title = models.TextField(null=False, blank=False)
    body = models.TextField(null=False, blank=False)

    # def save(self, force_insert=False, force_update=False, using=None,
    #          update_fields=None):
    #     if not self.id:
    #         self.therapist.increment_reviews_count()


class RatingEntry(Abstract):
    therapist = models.ForeignKey(Therapist)
    score = models.PositiveIntegerField(default=1)


class PreTherapy(Abstract):
    title = models.TextField()
    slug = models.SlugField(editable=False)
    description = models.TextField(null=True, blank=True)

    def save(self, raw=False, force_insert=False,
                  force_update=False, using=None, update_fields=None):
        if not self.id:
            self.slug = slugify(self.title)

        super(PreTherapy, self).save()

    def __str__(self):
        return self.title


class Plan(Abstract):
    price = models.PositiveIntegerField()
    title = models.CharField(null=False, blank=False, max_length=255)
    description = models.TextField(null=False, blank=False)
    monthly_price = models.PositiveIntegerField(null=True, blank=True)
    duration = models.PositiveIntegerField(null=False, blank=False)

    def __str__(self):
        return self.title


class Sessions(Abstract):
    user = models.ForeignKey(Account, related_name='sessions')
    therapist = models.ForeignKey(Therapist, related_name='sessions')

    last_active_date = models.DateTimeField(null=True, blank=True)   # most recent activity

    billed_date = models.DateField(null=True, blank=True)   # date session was paid for
    termination_date = models.DateField(null=True, blank=True)  # date session was terminated
    can_extend = models.BooleanField(default=False)             # signifies if a terminated session can be extended
    extended_session = models.ForeignKey('self', null=True, blank=True)     # extended session details

    rolled_over_duration = models.IntegerField(default=0)
    duration = models.IntegerField(default=0)           # duration of current session
    start_date = models.DateField(null=True, blank=True)        # date session starts
    expiration_date = models.DateField(null=True, blank=True)   # date session ends

    new_plan = models.ForeignKey(Plan, null=True, blank=True)

    def has_expired(self):
        if self.expiration_date:
            return self.expiration_date >= date.today()
        return False

    def has_started(self):
        if self.start_date:
            return self.start_date <= date.today()
        return False

    def is_active(self):
        """
        started, not expired, not terminated
        :return:
        """
        if self.start_date and self.expiration_date:
            return self.start_date <= date.today() and self.expiration_date >= date.today() and \
               self.termination_date is None
        return False

# class PreTherapyResponse(Abstract):
#     user = models.OneToOneField(Account)
#     response = models.TextField(null=False, blank=False)
#     question = models.TextField(null=False, blank=False)


class Profile(Abstract):
    last_billed_date = models.DateField(null=True, blank=True)
    current_therapist = models.ForeignKey(Therapist, null=True, blank=True)
    current_plan = models.ForeignKey(Plan, null=True, blank=True)
    user = models.OneToOneField(Account)
    counter_conversation = models.IntegerField(default=0)
    counter_call = models.IntegerField(default=0)

    def get_current_session(self):
        """
        returns user's current session
        :return:
        """
        today = date.today()
        user = self.user

        # billed not expired
        if Sessions.objects.filter(user=user, billed_date__isnull=False, expiration_date__gte=today).first():
            return Sessions.objects.filter(user=user, billed_date__isnull=False, expiration_date__gte=today).first()

        # billed but terminated and extendable
        elif Sessions.objects.filter(user=user, billed_date__isnull=False, termination_date__isnull=False,
                                     can_extend=True).first():
            return Sessions.objects.filter(user=user, billed_date__isnull=False, termination_date__isnull=False,
                                           can_extend=True).first()

        # not started and not terminated
        elif Sessions.objects.filter(user=user, termination_date__isnull=True, expiration_date__isnull=True,
                                     billed_date__isnull=True).first():
            return Sessions.objects.filter(user=user, termination_date__isnull=True, expiration_date__isnull=True,
                                           billed_date__isnull=True).first()

        return None


class Token(models.Model):
    key = models.CharField(max_length=40, primary_key=True)
    therapist = models.ForeignKey(Therapist, null=False, blank=False)
    expires = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    is_verification = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('TherapistToken')
        verbose_name_plural = _('TherapistTokens')

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
            self.expires = datetime(2038, 12, 31, 23, 59)
        return super(Token, self).save(*args, **kwargs)

    @staticmethod
    def generate_key():
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key

    def expire(self):
        tz_info = self.expires.tzinfo
        self.expires = datetime.now(tz_info)
        self.save()

        return self

    @property
    def is_expired(self):
        """
        check if a subscription has expired
        """
        tz_info = self.expires.tzinfo
        return datetime.now(tz_info) > self.expires if self.expires else True


class Billing(Abstract):
    user = models.ForeignKey(Account, related_name='billings')
    sub_transaction = models.ForeignKey(SubscriptionTransaction)
    therapist = models.ForeignKey(Therapist, related_name='billings')


class SystemMessage(Abstract):
    """
    system messages
    """

    class Meta:
        verbose_name = 'system_message'

    html_text = models.TextField(null=False, blank=False)
    sms_text = models.TextField(null=False, blank=False)
    tag = models.CharField(default='birthday', max_length=50)


class AuthorizationToken(Abstract):
    user = models.ForeignKey(Account, on_delete=models.CASCADE, blank=False, null=False)
    code = models.TextField(null=False, blank=False)
    is_expired = models.BooleanField(default=False)
    is_verification = models.BooleanField(default=True)
    is_password_change = models.BooleanField(default=False)

    def __str__(self):
        return self.code


class TherapistOnBoarding(Abstract):
    therapist = models.OneToOneField(Therapist, related_name='onboarding')
    payment_step_completed = models.BooleanField(default=False)
    chat_step_completed = models.BooleanField(default=False)


class TherapistBillingSettings(Abstract):
    therapist = models.OneToOneField(Therapist, related_name='billing_settings')
    subaccount_code = models.TextField(null=False, blank=False)
    settlement_schedule = models.CharField(max_length=50)
    settlement_bank = models.CharField(max_length=200)
    payment_gateway_id = models.IntegerField(null=False, blank=False)
    payment_gateway_integration = models.IntegerField(null=False, blank=False)
    business_name = models.TextField(null=False, blank=False)
    percentage_charge = models.FloatField(default=30.0)


class DailyTips(Abstract):
    """
    daily tips mode
    """
    class Meta:
        verbose_name = 'daily tip'
    cover_image = models.ImageField(upload_to='images', blank=True, null=True)
    target_date = models.DateField(unique=True, null=False, blank=False)
    body = models.TextField(null=False, blank=False)
    author = models.CharField(blank=True, null=True, max_length=255)

    def __str__(self):
        return 'Daily Tip: {}'.format(self.body)


class Habit(Abstract):
    """
    habit model
    """
    user = models.ForeignKey(Account, null=False, blank=False)
    name = models.CharField(max_length=255, null=False, blank=False)
    positive_response = models.CharField(max_length=255, null=False, blank=False)
    negative_response = models.CharField(max_length=255, null=False, blank=False)
    start_date = models.DateField(null=False, blank=False)
    target_date = models.DateField(null=False, blank=False)


class HabitCompanion(Abstract):
    """
    habit companion model
    """
    habit = models.OneToOneField(Habit, related_name='companion')
    # email = models.EmailField(null=False, blank=False)
    companion = models.ForeignKey(Account)


class CheckIn(Abstract):
    """
    check-in model
    """
    user = models.ForeignKey(Account, related_name='check_ins')
    habit = models.ForeignKey(Habit, related_name='check_ins')
    positive = models.BooleanField()
    negative = models.BooleanField()


class AccountNotification(Abstract):
    """
    account notification model
    """
    user = models.ForeignKey(Account, related_name='account_notification')
    player_id = models.TextField(null=False, blank=False)
    push_token = models.TextField(null=False, blank=False)


class TherapyFeedback(Abstract):
    """
    therapy session feedback
    """
    therapy_session = models.OneToOneField(Sessions)
    body = models.TextField(null=False, blank=False)
    subject = models.TextField(null=False, blank=False)

    def __repr__(self):
        return 'Therapy Session by {}'.format(self.therapy_session.therapist.name)

    def __str__(self):
        return 'Therapy Session by {}'.format(self.therapy_session.therapist.name)


class CompanionToken(models.Model):
    email = models.EmailField(_('email address'), null=False, blank=False)
    key = models.CharField(max_length=40, primary_key=True)
    expires = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    habit = models.ForeignKey(Habit)

    class Meta:
        verbose_name = _('CompanionToken')
        verbose_name_plural = _('CompanionToken')

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
            self.expires = datetime(2038, 12, 31, 23, 59)
        return super(CompanionToken, self).save(*args, **kwargs)

    @staticmethod
    def generate_key():
        return binascii.hexlify(os.urandom(20)).decode()

    def __str__(self):
        return self.key

    def expire(self):
        tz_info = self.expires.tzinfo
        self.expires = datetime.now(tz_info)
        self.save()

        return self

    @property
    def is_expired(self):
        """
        check if a subscription has expired
        """
        tz_info = self.expires.tzinfo
        return datetime.now(tz_info) > self.expires if self.expires else True