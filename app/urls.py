from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^home$', views.index, name='home'),
    url(r'^about$', views.AboutView.as_view(), name='about'),
    url(r'^search$', views.SearchView.as_view(), name='search'),
    url(r'^therapists$', views.TherapistsView.as_view(), name='therapists'),
    url(r'^therapists/(?P<pk>[-\d]+)$', views.TherapistDetailPageView.as_view(), name='therapist_detail'),
    url(r'^contact$', views.ContactView.as_view(), name='contact'),
    url(r'^faq$', views.FaqView.as_view(), name='faq'),
    url(r'^services$', views.ServiceView.as_view(), name='services'),
    url(r'^therapist-guide$', views.TherapistGuideView.as_view(), name='therapist_guide'),
    url(r'^setup-practise', views.SetupTherapistView.as_view(), name='setup-practise'),
    url(r'^get-started$', views.OnboardingView.as_view(), name='get_started'),
    url(r'^refund-policy$', views.refund_policy, name='refund_policy'),
    url(r'self-help$', views.self_help, name='self_help'),
    url(r'^self-help/(?P<slug>[-\w]+)$', views.SelfAssessmentView.as_view(), name='self_assessment'),
    url(r'^result/(?P<slug>[-\w]+)$', views.assessment_result, name='assessment_result'),
    url(r'^plan-selection$', views.SelectPlanView.as_view(), name='plan_selection'),
    url(r'^therapist-selection$', views.SelectTherapistView.as_view(), name='therapist_selection'),
    url(r'^subscriptions/payment$', views.SubscribeView.as_view(), name='subscription_payment'),
    url(r'^pre-therapy$', views.PreTherapyView.as_view(), name='pre_therapy'),
    url(r'^process-therapy$', views.process_pre_therapy, name='process_therapy'),

    # shopping
    url(r'^shop$', views.ShopView.as_view(), name='shop'),
    url(r'^shop/(?P<slug>[-\w]+)$', views.product_detail, name='product_detail'),
    url(r'^cart$', views.CartView.as_view(), name='cart'),
    url(r'^checkout$', views.CheckoutView.as_view(), name='checkout'),

    # user account urls
    url(r'^profile$', views.ProfileView.as_view(), name='profile'),
    url(r'^sessions$', views.SessionView.as_view(), name='sessions'),
    url(r'^call/(?P<pk>[-\d]+)$', views.voice_call, name='voice_call'),
    url(r'^assessments$', views.get_assessments, name='assessments'),
    url(r'^assessments/(?P<pk>[-\d]+)$', views.get_assessment, name='assessment'),
    url(r'^orders$', views.OrderView.as_view(), name='orders'),
    url(r'^orders/(?P<code>[-\w]+)$', views.order_item, name='order_item'),
    url(r'^resources$', views.ResourceView.as_view(), name='resources'),
    url(r'^settings$', views.SettingView.as_view(), name='settings'),
    url(r'^billings', views.view_billings, name='billings'),
    url(r'^habits/(?P<pk>[-\d]+)$', views.view_habit, name='view_habit'),
    url(r'^habits/(?P<pk>[-\d]+)/companion/accept$', views.accept_companion_request, name='accept_companion_request'),
    url(r'^habits/(?P<pk>[-\d]+)/companion/decline$', views.decline_companion_request, name='decline_companion_request'),

    url(r'^messages$', views.MessageView.as_view(), name='messages'),
    url(r'^messages/(?P<pk>[-\d]+)$', views.get_messages, name='therapist_messages'),
    # url(r'^messages/new$', views.NewMessageView.as_view(), name='new_message'),
    url(r'^notifications$', views.NotificationView.as_view(), name='notifications'),
    url(r'^plans/change$', views.ChangePlanView.as_view(), name='change_plan'),
    url(r'^therapists/change$', views.ChangeTherapistView.as_view(), name='change_therapist'),


    # The ones i created
    url(r'^messages/(?P<pk>[-\d]+)/new$', views.TherapyMessageView.as_view(), name='therapy_messages'),
    url(r'^therapy-chats/(?P<pk>[-\d]+)$', views.TherapyChatView.as_view(), name='therapy_chats'),

    url(r'^privacy$', views.privacy_policy, name='privacy'),
    url(r'^terms$', views.terms_of_use, name='terms'),
    url(r'^emails/test$', views.test_email_template)
]