# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-05-24 13:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0050_auto_20180516_2218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='onboarding',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='onboarding', to='app.Account'),
        ),
    ]
