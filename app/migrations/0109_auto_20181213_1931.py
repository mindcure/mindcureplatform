# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-12-13 18:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0108_auto_20181213_1831'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='subscriptiontransaction',
            name='duration',
        ),
        migrations.AddField(
            model_name='subscriptiontransaction',
            name='session_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Sessions'),
            preserve_default=False,
        ),
    ]
