# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-12-10 16:46
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0104_auto_20181209_1811'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='TherapistOnBaording',
            new_name='TherapistOnBoarding',
        ),
    ]
