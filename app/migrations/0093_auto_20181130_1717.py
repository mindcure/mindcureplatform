# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-11-30 16:17
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0092_auto_20181118_0926'),
    ]

    operations = [
        migrations.CreateModel(
            name='DailyTips',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('cover_image', models.ImageField(upload_to='images')),
                ('target_date', models.DateField(unique=True)),
                ('body', models.TextField()),
            ],
            options={
                'ordering': ('-created_at',),
                'abstract': False,
            },
        ),
        migrations.AlterField(
            model_name='order',
            name='cart',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='carts', to='app.Cart'),
        ),
    ]
