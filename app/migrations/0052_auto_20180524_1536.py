# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-05-24 14:36
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0051_auto_20180524_1429'),
    ]

    operations = [
        migrations.CreateModel(
            name='SubscriptionTransaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('reference_code', models.TextField()),
                ('txRef', models.TextField(null=True)),
                ('amount', models.FloatField(default=0.0)),
                ('transaction_status', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.TransactionStatus')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app.Account')),
            ],
            options={
                'ordering': ('-created_at',),
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='onboarding',
            name='session_step_completed',
        ),
    ]
