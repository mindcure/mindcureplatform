# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-27 17:05
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0020_question_is_active'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='checkout_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='userresponse',
            name='assessment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='responses', to='app.UserAssessment'),
        ),
    ]
