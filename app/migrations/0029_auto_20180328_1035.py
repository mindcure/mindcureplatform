# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-28 09:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0028_auto_20180328_1034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='assessmenttest',
            name='title',
            field=models.TextField(),
        ),
    ]
