# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-11-16 16:54
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0085_auto_20181116_1556'),
    ]

    operations = [
        migrations.CreateModel(
            name='TherapistBillingSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('subaccount_code', models.TextField()),
                ('settlement_schedule', models.CharField(max_length=50)),
                ('settlement_bank', models.CharField(max_length=200)),
                ('payment_gateway_id', models.IntegerField()),
                ('payment_gateway_integration', models.IntegerField()),
                ('business_name', models.TextField()),
                ('percentage_charge', models.FloatField(default=30.0)),
                ('therapist', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='billing_settings', to='app.Therapist')),
            ],
            options={
                'ordering': ('-created_at',),
                'abstract': False,
            },
        ),
    ]
