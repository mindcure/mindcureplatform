# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-03-28 10:50
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0030_auto_20180328_1137'),
    ]

    operations = [
        migrations.CreateModel(
            name='QuestionResponse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now)),
                ('name', models.CharField(choices=[('always', 'Always'), ('mostly', 'Mostly'), ('sometimes', 'Sometimes/Rarely'), ('never', 'Never')], max_length=50)),
            ],
            options={
                'ordering': ('-created_at',),
                'abstract': False,
            },
        ),
        migrations.RemoveField(
            model_name='question',
            name='response',
        ),
        migrations.AddField(
            model_name='question',
            name='responses',
            field=models.ManyToManyField(to='app.QuestionResponse'),
        ),
    ]
