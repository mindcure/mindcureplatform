# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2019-02-12 07:07
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0118_therapyfeedback'),
    ]

    operations = [
        migrations.AddField(
            model_name='sessions',
            name='new_plan_id',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='app.Plan'),
        ),
    ]
