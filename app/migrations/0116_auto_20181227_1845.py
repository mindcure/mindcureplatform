# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-12-27 17:45
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0115_auto_20181219_1708'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userassessment',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_assessments', to='app.Account'),
        ),
    ]
