# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2019-02-13 16:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0120_auto_20190213_0931'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='habitcompanion',
            name='email',
        ),
        migrations.AddField(
            model_name='habitcompanion',
            name='companion',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='app.Account'),
            preserve_default=False,
        ),
    ]
