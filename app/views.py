import json
import warnings
from datetime import datetime, date

from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404, resolve_url
from django.views.generic import TemplateView
from django.contrib import messages
from django.contrib.auth import forms as auth_forms, login as auth_login, authenticate, mixins, views as auth_views, get_user_model
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.core.mail import send_mail
from django.core.cache import cache
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import ensure_csrf_cookie, csrf_protect, csrf_exempt, requires_csrf_token
from django.contrib.auth.decorators import login_required
from django_user_agents.utils import get_user_agent
from django.contrib.auth.tokens import default_token_generator
from django.template.response import TemplateResponse
from django.urls import reverse, reverse_lazy
from django.utils.deprecation import (
    RemovedInDjango20Warning, RemovedInDjango21Warning,
)
from django.template import RequestContext
from django.utils.encoding import force_text
from django.utils.http import is_safe_url, urlsafe_base64_decode
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.debug import sensitive_post_parameters

from . import forms, models, serializers
from .services import account, mixins as custom_mixins, utils, cart, therapists as therapists_service, assessment as \
    assessment_service, habits as habit_service, therapy as therapy_service

from . import tasks


def verify(request, *args, **kwargs):
    """
    verify user account
    :param request:
    :return:
    """
    token = request.GET['token']
    if not token:
        return redirect('index')

    auth_token = models.AuthorizationToken.objects.filter(code=token, is_verification=True).first()

    if not auth_token:
        messages.error(request, 'Invalid Token')
        return redirect('login')

    if auth_token.is_expired:
        messages.error(request, 'Token has expired')
        return redirect('login')

    # update user verification status
    user_account = models.Account.objects.filter(user_ptr_id=auth_token.user.id).first()
    user_account.is_verified = True
    user_account.save()

    # update expiration date for token
    auth_token.is_expired = True
    auth_token.save()

    messages.success(request, 'Your account has been verified')

    return redirect('login')


def therapist_verify(request, *args, **kwargs):
    """
    verify therapist account
    :param request:
    :return:
    """
    try:
        token = request.GET['token']
        if not token:
            return redirect('index')

        auth_token = models.Token.objects.filter(key=token, is_verification=True).first()

        if not auth_token:
            messages.error(request, 'Invalid Token')
            return redirect('index')

        if auth_token.is_expired:
            messages.error(request, 'Token has expired')
            return redirect('therapist_login_redirect')

        # update user verification status
        therapist = models.Therapist.objects.get(pk=auth_token.therapist.id)
        therapist.is_verified = True
        therapist.save()

        # update expiration date for token
        auth_token.expires = datetime.now()
        auth_token.save()

        messages.success(request, 'Your account has been verified')

        return redirect('therapist_login_redirect')
    except models.Therapist.DoesNotExist as e:
        messages.error(request, 'Invalid Token')
        return redirect('index')


def therapist_login_redirect(request):
    """
    landing page before routing to therapist app
    :param request:
    :return:
    """
    therapist_login_url = settings.THERAPIST_URL
    return utils.render_with_locals('auth/therapist_login_redirect.html', {}, **locals())


@ensure_csrf_cookie
def index(request, *args, **kwargs):
    """
    home view
    :param request:
    :param args:
    :param kwargs:
    :return:
    """
    therapists = models.Therapist.objects.all().order_by('-id')[:8]
    testimonials = models.Testimonial.objects.filter(is_therapist=False).all()

    return utils.render_with_locals('index.html', {
        'title': 'MindCure'
    }, **locals())


class AboutView(TemplateView):
    context = {
        'title': 'MindCure | About Us'
    }
    template_name = 'about.html'

    def get(self, request, *args, **kwargs):
        return utils.render_with_locals(self.template_name, self.context, **locals())


class FaqView(TemplateView):
    context = {
        'title': 'MindCure | FAQ'
    }
    template_name = 'faq.html'

    def get(self, request, *args, **kwargs):
        return utils.render_with_locals(self.template_name, self.context, **locals())


class ContactView(TemplateView):
    context = {
        'title': 'MindCure | Contact'
    }
    template_name = 'contact_us.html'

    def get(self, request, *args, **kwargs):
        return utils.render_with_locals(self.template_name, self.context, **locals())

    def post(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        form = forms.ContactForm(request.POST, request.FILES)

        if form.is_valid():
            send_mail(form.cleaned_data['subject'], message=form.cleaned_data['message'],
                      from_email=form.cleaned_data['email'], recipient_list=[settings.TEAM_EMAIL], fail_silently=True)

            messages.success(request, "Message successfully sent")

            return redirect('contact')

        return utils.render_with_locals(self.template_name, self.context, **locals())


class ServiceView(TemplateView):
    context = {
        'title': 'MindCure | Services'
    }
    template_name = 'services.html'

    def get(self, request, *args, **kwargs):
        return utils.render_with_locals(self.template_name, self.context, **locals())


class TherapistGuideView(TemplateView):
    context = {
        'title': 'MindCure | Become A Therapist'
    }
    template_name = 'therapist_guide.html'

    def get(self, request, *args, **kwargs):
        testimonials = models.Testimonial.objects.filter(is_therapist=True).all()
        return utils.render_with_locals(self.template_name, self.context, **locals())


class SetupTherapistView(TemplateView):
    context = {
        'title': 'MindCure | Therapist Registration'
    }
    template_name = 'create_therapist.html'

    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        title = 'MindCure | Therapist Registration'
        states = models.State.objects.order_by('name')
        qualifications = models.Qualification.objects.order_by('id')
        countries = models.Country.objects.order_by('name')
        questions = models.RequestInfoQuestion.objects.order_by('number')

        return render(request, self.template_name, locals())

    def post(self, request, *args, **kwargs):
        """
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        form = forms.TherapistForm(request.POST, request.FILES)

        if form.is_valid():

            therapist = therapists_service.create_therapist_account(**form.cleaned_data)
            if therapist:
                return redirect(settings.THERAPIST_URL)

        title = 'MindCure | Therapist Registration'
        states = models.State.objects.order_by('name')
        qualifications = models.Qualification.objects.order_by('id')
        countries = models.Country.objects.order_by('name')
        questions = models.RequestInfoQuestion.objects.order_by('number')

        return utils.render_with_locals(self.template_name, self.context, **locals())


class SearchView(TemplateView):
    context = {
        'title': 'MindCure | About Us'
    }
    template_name = 'search.html'

    def get(self, request, *args, **kwargs):
        return utils.render_with_locals(self.template_name, self.context, **locals())


class TherapistsView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Therapists'
    }
    template_name = 'therapists.html'

    def get(self, request, *args, **kwargs):

        categories = models.TherapistCategory.objects.all()
        languages = models.Language.objects.all()
        page = request.GET.get('page', 1)
        order_by = request.GET.get('order_by', 'rating')

        query = models.Therapist.objects.order_by(order_by)
        if request.GET.get('q'):
            q = request.GET.get('q')
            query = query.filter(Q(qualification__name__icontains=q) | Q(name__icontains=q) | Q(email__icontains=q))

        if request.GET.get('language'):
            query = query.filter(languages__slug=request.GET.get('language'))

        if request.GET.get('categories'):
            query = query.filter(categories__slug=request.GET.get('categories'))

        filtered_query = query.select_related().filter(is_activated=True, is_verified=True, billing_settings__isnull=False).all()
        paginator = Paginator(filtered_query, 20)

        try:
            therapists = paginator.page(page)
        except PageNotAnInteger:
            therapists = paginator.page(1)
        except EmptyPage:
            therapists = paginator.page(paginator.num_pages)

        profile = request.user.account.profile

        selected = None
        if profile.current_therapist:
            selected = profile.current_therapist.id

        return utils.render_with_locals(self.template_name, self.context, **locals())


class TherapistDetailPageView(TemplateView):
    context = {
        'title': 'MindCure | Therapist Detail'
    }
    template_name = 'therapistDetail.html'

    def get(self, request, pk, *args, **kwargs):
        therapist = get_object_or_404(models.Therapist, pk=pk)

        similar_therapists = models.Therapist.objects.filter(Q(qualification__name__contains=
                                                               therapist.qualification.name), ~Q(id=therapist.id))

        page = request.GET.get('page', 1)
        order_by = request.GET.get('order_by', 'created_at')

        query = models.Review.objects.filter(therapist_id=therapist.pk).order_by(order_by)

        filtered_query = query.all()
        paginator = Paginator(filtered_query, 20)

        try:
            reviews = paginator.page(page)
        except PageNotAnInteger:
            reviews = paginator.page(1)
        except EmptyPage:
            reviews = paginator.page(paginator.num_pages)

        title = "{}'s bio".format(therapist.name)

        url = '{}{}/therapists/{}'.format(settings.SCHEME, settings.DOMAIN, therapist.pk)

        return utils.render_with_locals(self.template_name, self.context, **locals())


def logout(request, next_page='/accounts/login'):
    """
    logout user
    :param request:
    :param next_page:
    :return:
    """
    # if 'cart' in request.session:
    #     for key in request.session.keys():
    #         del request.session[key]

    # flush session
    account.clear_redis_user('user:w_{}'.format(request.user.username),
                             'user:w_{}'.format(request.COOKIES.get('csrftoken')))
    request.session.flush()

    response = auth_views.logout(request, next_page=next_page)

    for cookie_key in request.COOKIES.keys():
        response.delete_cookie(cookie_key, domain=settings.DOMAIN_BASE)

    response.delete_cookie('express.sid', domain='mindcureglobal.com')
    response.delete_cookie('mindcure_community', domain='mindcureglobal.com')

    return response


def login(request, *args, **kwargs):
    """
    login view
    :param request:
    :param args:
    :param kwargs:
    :return:
    """
    if request.user.is_authenticated():
        return redirect('index')

    form = auth_forms.AuthenticationForm()
    ua = get_user_agent(request)
    quotes = []
    with open('setup/quotes.json') as f:
        u_quotes = json.loads(f.read())
        quotes = [c.encode('utf-8') for c in u_quotes]

    next_url = request.GET.get('next') or request.session.get('next') or '/'
    register_url = '/accounts/register/?next={}'.format(next_url)

    return utils.render_with_locals('auth/login.html', {
        'title': 'MindCure | Sign In'
    }, **locals())


class RegistrationView(custom_mixins.AnonymousMixin, TemplateView):
    context = {
        'title': 'MindCure | Join Us'
    }
    template_name = 'auth/register.html'

    def get(self, request, *args, **kwargs):
        form = forms.RegistrationForm()
        ua = get_user_agent(request)

        next_url = request.GET.get('next') or request.session.get('next') or '/'
        login_url = '/accounts/login/?next={}'.format(next_url)

        return utils.render_with_locals(self.template_name, self.context, **locals())

    def post(self, request, *args, **kwargs):
        """
            register account
        """
        form = forms.RegistrationForm(request.POST)

        if form.is_valid():

            if account.check_username(form.cleaned_data['username']):
                messages.error(request, 'An account with this username already exists')
                return render(request, self.template_name, locals())

            if account.check_email(form.cleaned_data['email']):
                messages.error(request, 'An account with this email already exists')
                return render(request, self.template_name, locals())

            acct = account.register_account(**form.cleaned_data)

            if acct:
                # log user in
                auth_login(request, acct.user_ptr)

                next_url = request.session.get('next') or request.GET.get('next') or 'index'

                return redirect(next_url)

        return utils.render_with_locals(self.template_name, self.context, **locals())


class ProfileView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Profile'
    }
    template_name = 'account/profile.html'

    def get(self, request, *args, **kwargs):
        return utils.render_with_locals(self.template_name, self.context, **locals())

    def post(self, request, *args, **kwargs):
        """
        update current user's profile
        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        form = forms.ProfileForm(request.POST)

        if form.is_valid():
            user = account.update_profile(request.user.id, **form.cleaned_data)
            messages.success(request, "Account information updated") if user else \
                messages.error(request, "Error updating information")

        return utils.render_with_locals(self.template_name, self.context, **locals())


class SessionView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Profile'
    }
    template_name = 'account/sessions.html'

    def get(self, request, *args, **kwargs):

        profile = request.user.account.profile

        if not profile.current_therapist:
            return redirect('therapist_selection')

        therapist = profile.current_therapist
        current_session = profile.get_current_session()
        serialized_session = json.dumps(serializers.SessionSerializer(current_session).data)
        serialized_profile = json.dumps(serializers.ProfileSerializer(profile).data)

        return utils.render_with_locals(self.template_name, self.context, **locals())


def voice_call(request, pk, *args, **kwargs):

    profile = request.user.account.profile

    if not profile.current_therapist:
        return redirect('therapist_selection')

    context = {
        'title': 'MindCure | Voice Call'
    }

    therapist = therapists_service.get_therapist(pk)

    return utils.render_with_locals('account/voice.html', **locals())


def get_assessments(request, *args, **kwargs):
    assessments = models.UserAssessment.objects.filter(user_id=request.user.id)
    return utils.render_with_locals('account/assessments.html', {
        'title': 'MindCure | Assessment'
    }, **locals())


def get_assessment(request, pk, *args, **kwargs):
    user_assessment = models.UserAssessment.objects.get(pk=pk)
    assessment = user_assessment.assessment_test
    return utils.render_with_locals('assessment_result.html', {
        'title': 'MindCure | {}'.format(assessment.title)
    }, **locals())


class OrderView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Order'
    }
    template_name = 'account/orders.html'

    def get(self, request, *args, **kwargs):

        query = models.Order.objects.order_by('-created_at').all()

        page = request.GET.get('page', 1)

        paginator = Paginator(query, 40)

        try:
            orders = paginator.page(page)
        except PageNotAnInteger:
            orders = paginator.page(1)
        except EmptyPage:
            orders = paginator.page(paginator.num_pages)

        return utils.render_with_locals(self.template_name, self.context, **locals())


class SettingView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Settings'
    }
    template_name = 'account/settings.html'

    def get(self, request, *args, **kwargs):
        return utils.render_with_locals(self.template_name, self.context, **locals())


class ResourceView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Resources'
    }
    template_name = 'account/resources.html'

    def get(self, request, *args, **kwargs):

        query = models.Resources.objects.order_by('-created_at').all()

        page = request.GET.get('page', 1)

        paginator = Paginator(query, 40)

        try:
            resources = paginator.page(page)
        except PageNotAnInteger:
            resources = paginator.page(1)
        except EmptyPage:
            resources = paginator.page(paginator.num_pages)

        return utils.render_with_locals(self.template_name, self.context, **locals())


class NotificationView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Notifications'
    }
    template_name = 'account/notifications.html'

    def get(self, request, *args, **kwargs):
        return utils.render_with_locals(self.template_name, self.context, **locals())


class TherapyMessageView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Messages'
    }
    template_name = 'account/new_message.html'

    def get(self, request, pk, *args, **kwargs):
        therapist = get_object_or_404(models.Therapist, pk=pk)
        return utils.render_with_locals(self.template_name, self.context, **locals())


class TherapyChatView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Messages'
    }
    template_name = 'account/sessions.html'

    def get(self, request, pk, *args, **kwargs):
        therapist = get_object_or_404(models.Therapist, pk=pk)
        user = request.user.account

        profile = user.profile
        current_session = profile.get_current_session()
        serialized_session = json.dumps(serializers.SessionSerializer(current_session).data)
        serialized_profile = json.dumps(serializers.ProfileSerializer(profile).data)

        return utils.render_with_locals(self.template_name, self.context, **locals())


class MessageView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Messages'
    }
    template_name = 'account/messages.html'

    def get(self, request, *args, **kwargs):
        return utils.render_with_locals(self.template_name, self.context, **locals())


class NewMessageView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Messages'
    }
    template_name = 'account/new_message.html'

    def get(self, request, *args, **kwargs):
        return utils.render_with_locals(self.template_name, self.context, **locals())


class ShopView(TemplateView):
    context = {
        'title': 'MindCure | Shop'
    }
    template_name = 'shop/products.html'

    def get(self, request, *args, **kwargs):
        categories = models.Category.objects.all()

        page = request.GET.get('page', 1)
        category = request.GET.get('category', 'all')
        order_by = request.GET.get('order_by', 'created_at')

        query = models.Product.objects.order_by(order_by)
        if request.GET.get('q'):
            q = request.GET.get('q')
            query = query.filter(Q(description__contains=q) | Q(name__contains=q) | Q(sku__contains=q))

        filtered_query = query.all() if category == 'all' else query.filter(categories__slug=category).all()
        paginator = Paginator(filtered_query, 18)

        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            products = paginator.page(1)
        except EmptyPage:
            products = paginator.page(paginator.num_pages)

        return utils.render_with_locals(self.template_name, self.context, **locals())


def product_detail(request, slug):
    """
    self help page
    :param request:
    :return:
    """
    product = get_object_or_404(models.Product, sku=slug)
    return utils.render_with_locals('shop/product_detail.html', {'title': product.name}, **locals())


class CartView(TemplateView):
    context = {
        'title': 'MindCure | Cart'
    }
    template_name = 'shop/cart.html'

    def get(self, request, *args, **kwargs):

        if request.user.is_authenticated:
            if request.user.is_superuser:
                return redirect('index')

        user_cart = cart.get_current_cart(request)
        return utils.render_with_locals(self.template_name, self.context, **locals())


class CheckoutView(TemplateView):
    context = {
        'title': 'MindCure | Cart'
    }
    template_name = 'shop/checkout.html'

    def get(self, request, *args, **kwargs):
        user_cart = cart.get_current_cart(request)
        return utils.render_with_locals(self.template_name, self.context, **locals())


class OnboardingView(TemplateView):
    context = {
        'title': 'MindCure | Onboarding'
    }
    template_name = 'get_started.html'

    def get(self, request, *args, **kwargs):

        if not request.user.is_authenticated():
            return utils.render_with_locals(self.template_name, self.context, **locals())

        if request.user.is_authenticated() and not request.user.is_staff:

            on_board = models.OnBoarding.objects.filter(user=request.user.account).first()

            # step by step check on onboarding items
            if not on_board:
                on_board = models.OnBoarding.objects.create(user=request.user.account)

            if on_board.pre_therapy_step_completed and on_board.plan_step_completed and \
                    on_board.therapist_step_completed:
                return redirect('sessions')

        return utils.render_with_locals(self.template_name, self.context, **locals())


class SelfAssessmentView(TemplateView):
    context = {
        'title': 'MindCure | Self Assessment'
    }
    template_name = 'assessment.html'

    def get(self, request, slug):

        if request.user.is_superuser or request.user.is_staff:
            messages.error(request, "You need to login as a regular user to access this")
            return redirect('self_help')

        if request.session.get('assessment_data'):
            del request.session['assessment_data']

        test = get_object_or_404(models.AssessmentTest, slug=slug)
        questions = models.Question.objects.filter(is_active=True, assessment_test=test).all()

        questions_serialized = json.dumps([serializers.QuestionSerializer(c).data for c in questions])

        return utils.render_with_locals(self.template_name, self.context, **locals())

    def post(self, request, slug, *args, **kwargs):
        """
        submit assessment
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        form_data = request.POST.copy()
        if form_data.has_key('csrftoken'):
            form_data.pop('csrftoken')

        assessment = models.AssessmentTest.objects.filter(slug=slug).first()

        next_url = '/result/{}'.format(assessment.slug)

        if not request.user.is_authenticated():
            request.session['assessment_data'] = form_data
            request.session['next'] = next_url

            messages.error(request, "You need to login to continue")

            return redirect('login')

        # save assessment result
        assessment_service.save_user_response(request.user.account.id, assessment.id, **form_data)

        return utils.render_with_locals('assessment_result.html', self.context, **locals())


def assessment_result(request, slug):
    """
    refund policy page
    :param request:
    :return:
    """

    assessment = models.AssessmentTest.objects.filter(slug=slug).first()
    responses = request.session['assessment_data']

    user_assessment = assessment_service.save_user_response(request.user.account.id, assessment.id, **responses)

    # scores = 0
    # user_assessment = models.UserAssessment.objects.create(user=request.user.account, assessment_test=assessment)
    #
    # for question_id, response_name in responses.iteritems():
    #     question = models.Question.objects.get(pk=question_id)
    #     response = models.QuestionResponse.objects.filter(slug=response_name).first()
    #     user_response = models.UserResponse.objects.create(user=request.user.account, question=question.body,
    #                                                        response=response.name, assessment=user_assessment)
    #     scores += response.score
    #
    # user_assessment.is_completed = True
    # user_assessment.score = scores
    # user_assessment.save()

    return utils.render_with_locals('assessment_result.html', {}, **locals())


def process_pre_therapy(request):
    """
    process pre therapy questionnaire for newly authenticated user
    :param request:
    :return:
    """
    responses = request.session['pre_therapy_data']

    # update onboarding steps
    on_board = models.OnBoarding.objects.filter(user=request.user).first()

    if not on_board:
        on_board = models.OnBoarding.objects.create(user=request.user.account)

    if on_board.pre_therapy_step_completed:
        messages.error(request, "You have previously completed the pre-therapy questionnaire")
        return redirect('get_started')

    # record pre_therapy responses
    account.update_user_pre_therapy_responses(request.user.account.id, **responses)

    # update pre_therapy step completed state
    on_board.pre_therapy_step_completed = True
    on_board.save()

    return redirect('therapist_selection')


def refund_policy(request):
    """
    refund policy page
    :param request:
    :return:
    """
    return render(request, 'refund_policy.html', **locals())


def self_help(request):
    """
    self help page
    :param request:
    :return:
    """
    tests = models.AssessmentTest.objects.all()
    return utils.render_with_locals('self_help.html', {'title': 'Self Help'}, **locals())


class PreTherapyView(TemplateView):
    context = {
        'title': 'MindCure | Pre Therapy'
    }
    template_name = 'pre_therapy.html'

    def get(self, request, *args, **kwargs):

        country_file = open('setup/countries.json')
        countries = json.loads(country_file.read())

        if request.user.is_staff or request.user.is_superuser:
            messages.error(request, "You are not permitted to answer these questions.")
            return redirect('get_started')

        if not request.user.is_authenticated():
            return utils.render_with_locals(self.template_name, self.context, **locals())

        on_board = models.OnBoarding.objects.filter(user=request.user).first()

        if not on_board:
            on_board = models.OnBoarding.objects.create(user=request.user.account)
            return utils.render_with_locals(self.template_name, self.context, **locals())

        if on_board.pre_therapy_step_completed:
            messages.error(request, "You have previously completed the pre-therapy questionnaire")
            return redirect('get_started')

        return utils.render_with_locals(self.template_name, self.context, **locals())

    def post(self, request, *args, **kwargs):
        """
        submit assessment
        :param request:
        :param args:
        :param kwargs:
        :return:
        """

        form = forms.WebPreTherapyForm(request.POST)

        if not form.is_valid():
            return utils.render_with_locals(self.template_name, self.context, **locals())

        next_url = '/process-therapy'

        # if form_data.get('gender') == 'Others':
        #     form_data['gender'] = form_data['gender_others']
        #     form_data.pop('gender_others')

        if not request.user.is_authenticated():
            request.session['pre_therapy_data'] = form.cleaned_data
            request.session['next'] = next_url
            messages.error(request, "You need to login to continue")
            return redirect('login')

        data = form.cleaned_data.copy()
        data['qualities'] = ', '.join(data['qualities'])

        # record pre_therapy responses
        account.update_user_pre_therapy_responses(request.user.account.id, **form.cleaned_data)

        # update onboarding steps
        on_board = models.OnBoarding.objects.filter(user=request.user).first()

        if not on_board:
            on_board = models.OnBoarding.objects.create(user=request.user.account)

        if not on_board.pre_therapy_step_completed:
            on_board.pre_therapy_step_completed = True
            on_board.save()

        if not on_board.therapist_step_completed:
            return redirect('therapist_selection')

        if not on_board.plan_step_completed:
            return redirect('plan_selection')

        return utils.render_with_locals('index.html', self.context, **locals())


class SelectPlanView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Select Plan'
    }
    template_name = 'select_plan.html'

    def get(self, request, *args, **kwargs):
        plans = models.Plan.objects

        on_board = models.OnBoarding.objects.filter(user=request.user.account).first()

        if not on_board:
            on_board = models.OnBoarding.objects.create(user=request.user.account)
            return utils.render_with_locals(self.template_name, self.context, **locals())

        if not on_board.therapist_step_completed:
            messages.error(request, "You have not selected a therapist")
            return redirect('get_started')

        return utils.render_with_locals(self.template_name, self.context, **locals())

    def post(self, request, *args, **kwargs):
        """
        plan selection flow
        """

        # update onboarding steps
        on_board = models.OnBoarding.objects.filter(user=request.user).first()

        if not on_board:
            return redirect('pre_therapy')

        form = forms.SelectPlanForm(request.POST)

        if form.is_valid():
            therapy_service.change_plan(request.user.account.id, form.cleaned_data['plan_id'])

            return redirect('sessions')

        return utils.render_with_locals(self.template_name, self.context, **locals())


class SelectTherapistView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Select Therapist'
    }
    template_name = 'therapists.html'

    def get(self, request, *args, **kwargs):

        on_board = models.OnBoarding.objects.filter(user=request.user.account).first()

        if not on_board:
            messages.error(request, "You have not answered the prerequisite pre-therapy questions.")
            return redirect('pre_therapy')

        if not on_board.pre_therapy_step_completed:
            messages.error(request, "You have not answered the prerequisite pre-therapy questions.")
            return redirect('pre_therapy')

        # load defaults
        languages = models.Language.objects.all()
        categories = models.TherapistCategory.objects.all()

        page = request.GET.get('page', 1)
        order_by = request.GET.get('order_by', 'rating')
        language = request.GET.get('language', 'english')
        categories_query = request.GET.get('categories', 'all')

        q = request.GET.get('q', None)
        if q:
            query = models.Therapist.objects.filter(Q(name__icontains=q) | Q(email__icontains=q))
        else:
            query = models.Therapist.objects.filter(languages__slug__in=[language], is_activated=True, billing_settings__isnull=False)

            if categories_query != 'all':
                query = query.filter(categories__slug__in=[categories_query])

        filtered_query = query.filter(is_activated=True, is_verified=True).order_by(order_by)
        paginator = Paginator(filtered_query, 10)

        try:
            therapists = paginator.page(page)
        except PageNotAnInteger:
            therapists = paginator.page(1)
        except EmptyPage:
            therapists = paginator.page(paginator.num_pages)

        profile = request.user.account.profile

        selected = None
        if profile.current_therapist:
            selected = profile.current_therapist.id

        return utils.render_with_locals(self.template_name, self.context, **locals())

    def post(self, request, *args, **kwargs):
        """
        plan selection flow
        """
        form = forms.SelectTherapistForm(request.POST)

        if form.is_valid():
            therapy_service.change_therapist(request.user.account.id, form.cleaned_data['therapist_id'])

            # update onboarding steps
            on_board = models.OnBoarding.objects.filter(user=request.user).first()

            if not on_board:
                return redirect('pre_therapy')

            if not on_board.plan_step_completed:
                messages.success(request, "You have selected a suitable therapist. Select a plan below to start "
                                          "counseling.")
                return redirect('sessions')

            return redirect('get_started')

        return utils.render_with_locals(self.template_name, self.context, **locals())


class SubscribeView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Subscription Payment'
    }
    template_name = 'subscription_payment.html'

    def get(self, request, *args, **kwargs):

        on_board = models.OnBoarding.objects.filter(user=request.user.account).first()

        # step by step check on onboarding items
        if not on_board:
            on_board = models.OnBoarding.objects.create(user=request.user.account)

        if not on_board.therapist_step_completed:
            return redirect('therapist_selection')

        profile = request.user.account.profile

        return utils.render_with_locals(self.template_name, self.context, **locals())


class ChangeTherapistView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Change Therapist'
    }
    template_name = 'therapists.html'

    def get(self, request, *args, **kwargs):

        on_board = models.OnBoarding.objects.filter(user=request.user.account).first()

        if not on_board:
            messages.error(request, "You have not answered the prerequisite pre-therapy questions.")
            return redirect('pre_therapy')

        if not on_board.pre_therapy_step_completed:
            messages.error(request, "You have not answered the prerequisite pre-therapy questions.")
            return redirect('pre_therapy')

        page = request.GET.get('page', 1)
        order_by = request.GET.get('order_by', 'rating')

        query = models.Therapist.objects.order_by(order_by)
        if request.GET.get('q'):
            q = request.GET.get('q')
            query = query.filter(Q(qualification__name__icontains=q) | Q(name__icontains=q) | Q(email__icontains=q))

        if request.GET.get('language'):
            query = query.filter(languages__slug=request.GET.get('language'))

        if request.GET.get('categories'):
            query = query.filter(categories__slug=request.GET.get('categories'))

        filtered_query = query.filter(is_activated=True, is_verified=True, billing_settings__isnull=False).all()
        paginator = Paginator(filtered_query, 50)

        try:
            therapists = paginator.page(page)
        except PageNotAnInteger:
            therapists = paginator.page(1)
        except EmptyPage:
            therapists = paginator.page(paginator.num_pages)

        profile = request.user.account.profile

        selected = None
        if profile.current_therapist:
            selected = profile.current_therapist.id

        return utils.render_with_locals(self.template_name, self.context, **locals())

    def post(self, request, *args, **kwargs):
        """
        plan selection flow
        """
        form = forms.SelectTherapistForm(request.POST)

        if form.is_valid():
            therapy_service.change_therapist(request.user.account.id, form.cleaned_data['therapist_id'])

            # update onboarding steps
            on_board = models.OnBoarding.objects.filter(user=request.user).first()

            if not on_board:
                return redirect('pre_therapy')

            if not on_board.plan_step_completed:
                messages.success(request, "You have selected a suitable therapist. Select a plan below to start "
                                          "counseling.")
                return redirect('plan_selection')

            return redirect('get_started')

        return utils.render_with_locals(self.template_name, self.context, **locals())


class ChangePlanView(mixins.LoginRequiredMixin, TemplateView):
    context = {
        'title': 'MindCure | Change Plan'
    }
    template_name = 'account/change_plan.html'

    def get(self, request, *args, **kwargs):
        plans = models.Plan.objects

        on_board = models.OnBoarding.objects.filter(user=request.user.account).first()

        if not on_board:
            on_board = models.OnBoarding.objects.create(user=request.user.account)
            return utils.render_with_locals(self.template_name, self.context, **locals())

        if not on_board.therapist_step_completed:
            messages.error(request, "You have not selected a therapist")
            return redirect('get_started')

        return utils.render_with_locals(self.template_name, self.context, **locals())

    def post(self, request, *args, **kwargs):
        """
        plan selection flow
        """

        # update onboarding steps
        on_board = models.OnBoarding.objects.filter(user=request.user).first()

        if not on_board:
            return redirect('pre_therapy')

        form = forms.SelectPlanForm(request.POST)

        if form.is_valid():
            therapy_service.change_plan(request.user.account.id, form.cleaned_data['plan_id'])

            return redirect('subscription_payment')

        return utils.render_with_locals(self.template_name, self.context, **locals())


def terms_of_use(request):
    """
    terms of use page
    :return:
    """
    title = 'Terms of Use | MindCure'
    return render(request, 'terms.html', locals())


def privacy_policy(request):
    """
    privacy policy page
    :return:
    """
    title = 'Privacy Policy | MindCure'
    return render(request, 'privacy.html', locals())


def get_messages(request, pk, *args, **kwargs):
    """
    get therapist messages
    :param request:
    :param pk:
    :param args:
    :param kwargs:
    :return:
    """
    therapist = models.Therapist.objects.get(pk=pk)

    if not therapist:
        raise models.Therapist.DoesNotExist

    return utils.render_with_locals('account/therapist_messages.html', {
        'title': 'MindCure | Therapist Messages'
    }, **locals())


def view_billings(request):
    """
    view billings
    :param request:
    :return:
    """
    query = models.Billing.objects.order_by('-created_at')

    page = request.GET.get('page', 1)

    paginator = Paginator(query, 40)

    try:
        billings = paginator.page(page)
    except PageNotAnInteger:
        billings = paginator.page(1)
    except EmptyPage:
        billings = paginator.page(paginator.num_pages)

    return utils.render_with_locals('account/billings.html', {
        'title': 'MindCure | Billings'
    }, **locals())


def order_item(request, code):
    """
    view order item
    :param request:
    :param code:
    :return:
    """
    order = get_object_or_404(models.Order, reference_code=code)

    return utils.render_with_locals('account/order_item.html', {
        'title': 'MindCure | Order - %s' % order.reference_code
    }, **locals())


def test_email_template(request):
    """
    test email templates
    :param request:
    :return:
    """
    habit = models.Habit.objects.first()

    user = habit.companion

    today = date.today()

    year = today.year

    target_days = (habit.target_date - habit.start_date).days + 1

    negative_count = models.CheckIn.objects.filter(habit_id=habit.id, negative=True).count()
    positive_count = models.CheckIn.objects.filter(habit_id=habit.id, positive=True).count()

    total_days = (today - habit.start_date).days + 1 if habit.target_date > today else target_days

    not_checked = total_days - (positive_count + negative_count)

    success_percentage = positive_count/total_days

    domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

    return utils.render_with_locals('emails/users/add_habit_companion.html',  {
        'title': 'MindCure | Test Email Template'
    }, **locals())


def view_habit(request, pk):
    """
    test email templates
    :param request:
    :return:
    """
    habit = get_object_or_404(models.Habit, pk=pk)

    today = date.today()

    year = today.year

    target_days = (habit.target_date - habit.start_date).days + 1

    negative_count = models.CheckIn.objects.filter(habit_id=habit.id, negative=True).count()
    positive_count = models.CheckIn.objects.filter(habit_id=habit.id, positive=True).count()

    total_days = (today - habit.start_date).days + 1 if habit.target_date > today else target_days

    not_checked = total_days - (positive_count + negative_count)

    success_percentage = positive_count/total_days

    domain = '{}{}'.format(settings.SCHEME, settings.DOMAIN)

    return utils.render_with_locals('account/habit_report.html',  {
        'title': 'MindCure | Test Email Template'
    }, **locals())


def accept_companion_request(request, pk):
    """
    accept habit companion request
    :param request
    :param pk
    """
    habit = get_object_or_404(models.Habit, pk=pk)

    if not request.user.is_authenticated():
        token = request.GET['token']
        request.session['next'] = '/habits/{}/companion/accept?token={}'.format(habit.id, token)

        messages.error(request, "You need to login to continue")
        return redirect('login')

    token = request.GET['token']
    if not token:
        messages.error(request, 'Invalid Token')
        return redirect('profile')

    companion_token = models.CompanionToken.objects.filter(key=token, habit_id=habit.id).first()

    if not companion_token:
        messages.error(request, 'Invalid Token')
        return redirect('profile')

    if companion_token.is_expired:
        messages.error(request, 'Token has expired')
        return redirect('profile')

    # add companion to habit
    habit = habit_service.add_companion(habit.id, request.user.id)

    # expire token
    companion_token.expire()

    messages.success(request, 'Habit Companion Request successfully accepted')
    return redirect('profile')


def decline_companion_request(request, pk):
    """
    decline habit companion request
    :param request
    :param pk
    """
    habit = get_object_or_404(models.Habit, pk=pk)

    token = request.GET['token']
    if not token:
        return redirect('index')

    companion_token = models.CompanionToken.objects.filter(key=token, habit_id=habit.id).first()

    if not companion_token:
        messages.error(request, 'Invalid Token')
        return redirect('login')

    if companion_token.is_expired:
        messages.error(request, 'Token has expired')
        return redirect('login')

    # expire token
    companion_token.expire()

    # decline companion request
    tasks.companion_request_declined.delay(habit.id, companion_token.email)

    messages.success(request, 'Habit Companion Request rejected')
    return redirect('index')


@auth_views.deprecate_current_app
@csrf_exempt
# @csrf_protect
def password_reset(request,
                   template_name='registration/password_reset_form.html',
                   email_template_name='registration/password_reset_email.html',
                   subject_template_name='registration/password_reset_subject.txt',
                   password_reset_form=auth_forms.PasswordResetForm,
                   token_generator=default_token_generator,
                   post_reset_redirect=None,
                   from_email=None,
                   html_email_template_name=None,
                   extra_email_context=None):
    warnings.warn("The password_reset() view is superseded by the "
                  "class-based PasswordResetView().",
                  RemovedInDjango21Warning, stacklevel=2)

    if post_reset_redirect is None:
        post_reset_redirect = reverse('password_reset_done')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    if request.method == "POST":
        form = password_reset_form(request.POST)
        if form.is_valid():
            opts = {
                'use_https': request.is_secure(),
                'token_generator': token_generator,
                'from_email': from_email,
                'email_template_name': email_template_name,
                'subject_template_name': subject_template_name,
                'request': request,
                'html_email_template_name': html_email_template_name,
                'extra_email_context': extra_email_context,
            }
            form.save(**opts)
            return HttpResponseRedirect(post_reset_redirect)
    else:
        form = password_reset_form()


    requestContext = RequestContext(request)
    context = {
        'form': form,
        'title': _('Password reset'),
    }

    return render(request, template_name, context)


# Doesn't need csrf_protect since no-one can guess the URL
@sensitive_post_parameters()
@never_cache
@auth_views.deprecate_current_app
@csrf_exempt
def password_reset_confirm(request, uidb64=None, token=None,
                           template_name='registration/password_reset_confirm.html',
                           token_generator=default_token_generator,
                           set_password_form=auth_forms.SetPasswordForm,
                           post_reset_redirect=None,
                           extra_context=None):
    """
    View that checks the hash in a password reset link and presents a
    form for entering a new password.
    """
    warnings.warn("The password_reset_confirm() view is superseded by the "
                  "class-based PasswordResetConfirmView().",
                  RemovedInDjango21Warning, stacklevel=2)
    assert uidb64 is not None and token is not None  # checked by URLconf

    UserModel = get_user_model()

    if post_reset_redirect is None:
        post_reset_redirect = reverse('password_reset_complete')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    try:
        # urlsafe_base64_decode() decodes to bytestring on Python 3
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = UserModel._default_manager.get(pk=uid)
    except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    if user is not None and token_generator.check_token(user, token):
        validlink = True
        title = _('Enter new password')
        if request.method == 'POST':
            form = set_password_form(user, request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(post_reset_redirect)
        else:
            form = set_password_form(user)
    else:
        validlink = False
        form = None
        title = _('Password reset unsuccessful')
    context = {
        'form': form,
        'title': title,
        'validlink': validlink,
    }

    return TemplateResponse(request, template_name, context)