import inspect
import sys

from django.contrib import admin
from django.apps import apps
# from rest_framework.authtoken.admin import TokenAdmin

from . import models


# TokenAdmin.raw_id_fields = ('user',)


class AdminSchema(object):
    @classmethod
    def create_schema(cls, model_object, form_class=None, readonly=(), list_filters=(), exclude=(), model_fields=()):
        """
        create schema for model object
        :param model_object:
        :param form_class:
        :param readonly:
        :param list_filters:
        :param exclude
        :param model_fields
        :return:
        """

        class BaseSchema(object):
            _fields = model_fields if len(model_fields) else model_object._meta.get_fields()
            _fields = filter(lambda x: x not in exclude, _fields)
            resource_class = ResourceFactory.create_resource(getattr(models, model_object._meta.object_name), exclude,
                                                             _fields)
            list_display = model_object._meta.get_fields()
            list_filter = list_filters
            readonly_fields = ('date_created', 'date_modified') + readonly
            date_hierarchy = 'date_created'
            empty_value_display = '-----'
            __name__ = '%sAdmin' % model_object.__name__
            if form_class:
                form = form_class

        admin.site.register(model_object)

        return BaseSchema


class ResourceFactory(object):

    @classmethod
    def create_resource(cls, model_name, exclude, model_fields):
        __MODEL_NAME__ = model_name
        __EXCLUDE__ = exclude
        __FIELDS__ = model_fields

        class BaseResource(object):
            class Meta:
                exclude = __EXCLUDE__
                model = __MODEL_NAME__
                if len(model_fields):
                    fields = __FIELDS__

        return BaseResource


app = apps.get_app_config('app')

for name, table in app.models.items():
    # form_class = None
    # readonly = ()
    # list_filter = list()
    # model_fields = list()
    # exclude = list()
    # AdminSchema.create_schema(table, form_class, readonly, list_filter, model_fields=model_fields, exclude=exclude)
    if table.__name__ not in ['Question_responses', 'Product_categories', 'PlanType_rules', 'OnBoarding',
                              'PreTherapyQuestion_responses', 'Question_responses', 'Token', 'RequestInfoResponse',
                              'RequestInfo_request_info_responses']:
        admin.site.register(table)

# tables = filter(lambda x: x._meta.abstract is False, [getattr(models, m[0]) for m in
#                                                       inspect.getmembers(models, inspect.isclass)
#                                                       if m[1].__module__ == 'app.models' and
#                                                       hasattr(getattr(models, m[0]), '_meta')])


# for table in tables:
#     form_class = None
#     readonly = ()
#     list_filter = list()
#     model_fields = list()
#     exclude = list()
#     AdminSchema.create_schema(table, form_class, readonly, list_filter, model_fields=model_fields, exclude=exclude)
