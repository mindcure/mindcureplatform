import json
import random

from django.core.management import BaseCommand, CommandError

import bcrypt

from app import models
from app.services import utils


class Command(BaseCommand):
    help = "setting up application"

    @staticmethod
    def create_qualifications():
        """
        create qualifications
        :return:
        """
        try:
            _file = open('setup/qualifications.json')
            data = json.loads(_file.read())
            for sx in data:
                models.Qualification.objects.create(code=sx['code'], name=sx['name'])
        except CommandError as e:
            raise e

    @staticmethod
    def create_countries():
        """
        create states
        :return:
        """
        try:
            _file = open('setup/countries.json')
            data = json.loads(_file.read())
            for c in data:
                models.Country.objects.create(code=c['name'].lower(), name=c['name'])

        except CommandError as e:
            raise e

    @staticmethod
    def create_states():
        """
        create states
        :return:
        """
        try:
            _file = open('setup/states.json')
            data = json.loads(_file.read())
            c = models.Country.objects.create(code='nigeria', name='Nigeria')
            for s in data:
                sx = s['state']
                state = models.State.objects.create(code=sx['name'].lower(), name=sx['name'], country=c)
                for local in sx['locals']:
                    models.City.objects.create(state=state, code=local['name'].lower(), name=local['name'])

        except CommandError as e:
            raise e

    @staticmethod
    def create_payment_statuses():
        """
        create states
        :return:
        """
        try:
            _file = open('setup/payment_statuses.json')
            data = json.loads(_file.read())
            for s in data:
                models.PaymentStatus.objects.create(code=s['code'], name=s['name'])

        except CommandError as e:
            raise e

    @staticmethod
    def create_order_statuses():
        """
        create states
        :return:
        """
        try:
            _file = open('setup/order_statuses.json')
            data = json.loads(_file.read())
            for s in data:
                models.OrderStatus.objects.create(code=s['code'], name=s['name'])

        except CommandError as e:
            raise e

    @staticmethod
    def create_transaction_statuses():
        """
        create states
        :return:
        """
        try:
            _file = open('setup/transaction_statuses.json')
            data = json.loads(_file.read())
            for s in data:
                models.TransactionStatus.objects.create(code=s['code'], name=s['name'])

        except CommandError as e:
            raise e

    @staticmethod
    def create_plan_types():
        """
        create states
        :return:
        """
        try:
            _file = open('setup/plan_types.json')
            data = json.loads(_file.read())
            for s in data:
                models.PlanType.objects.create(code=s['code'], name=s['name'])

        except CommandError as e:
            raise e

    @staticmethod
    def create_plans():
        """
        create states
        :return:
        """
        try:
            _file = open('setup/plan.json')
            data = json.loads(_file.read())
            for s in data:
                models.Plan.objects.create(title=s['title'], description=s['description'], price=s['price'])

        except CommandError as e:
            raise e

    @staticmethod
    def setup_categories():
        """
        create states
        :return:
        """
        try:
            _file = open('setup/categories.json')
            data = json.loads(_file.read())
            for s in data:
                models.TherapistCategory.objects.create(title=s['title'])

            # models.Language.objects.create(title='English')

        except CommandError as e:
            raise e

    @staticmethod
    def create_dummy_therapists():
        """
        dummy therapist accounts
        """

        state = models.State.objects.filter(code='lagos').first()

        if not state:
            raise Exception('State not Found')

        country = models.Country.objects.filter(code='nigeria').first()

        if not country:
            raise Exception('Country not Found')

        qualification_codes = ['masters_psychology', 'diploma_psychology', 'certification_psychology',
                               'masters_social', 'neurolinguist', 'hypnotherapy', 'life', 'career', 'human_resources']

        for therapist in xrange(50):
            address = models.Address.objects.create(phone=utils.number_generator(11),
                                                    line1=utils.random_generator(100), line2='', state=state,
                                                    country=country)

            password = bcrypt.hashpw('password'.encode('utf-8'), bcrypt.gensalt(12))

            qualification = models.Qualification.objects.filter(code=qualification_codes[random.randint(0, 8)]).first()
            data = dict(name=utils.random_generator(12), bio=utils.random_generator(100), qualification=qualification,
                        address=address, password=password, email='{}@mind.cure'.format(utils.random_generator(5)),
                        is_activated=True)
            models.Therapist.objects.create(**data)

    @staticmethod
    def reset_site_domain():
        """
        reset value of site domain to match current host address
        :return:
        """
        from django.contrib.sites.models import Site
        from django.conf import settings

        site = Site.objects.first()
        site.domain = settings.DOMAIN
        site.name = settings.DOMAIN

        site.save()

        return

    @staticmethod
    def clean_dummy_therapist_language():
        """
        add english to dummy therapists
        :return:
        """
        language = models.Language.objects.first()
        for m in models.Therapist.objects.all():
            m.languages.add(language)
            m.save()

        return

    @staticmethod
    def add_plan_duration():
        """
        update plans with corresponding duration
        :return:
        """
        for plan in models.Plan.objects.all():
            plan.duration = 7
            plan.save()

        return

    def handle(self, *args, **options):
        """
        setup commands for initializing app
        :param args:
        :param options:
        :return:
        """
        # self.create_plans()
        # # self.create_qualifications()
        # self.create_states()
        # self.create_order_statuses()
        # self.create_payment_statuses()
        # self.create_transaction_statuses()
        # self.create_dummy_therapists()
        # self.setup_categories()
        # self.create_countries()
        # self.reset_site_domain()
        # self.clean_dummy_therapist_language()
        # self.add_plan_duration()
