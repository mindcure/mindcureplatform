from __future__ import division

from datetime import date

from rest_framework.serializers import ModelSerializer, SerializerMethodField

from . import models


class SerializerFactory(object):

    @classmethod
    def create_serializer(cls, model_object, exclude_fields=tuple()):
        class BaseSerializer(ModelSerializer):
            class Meta:
                model = model_object
                exclude = exclude_fields
                # fields = model_object._meta.get_fields()

        return BaseSerializer


StateSerializer = SerializerFactory.create_serializer(models.State)
CountrySerializer = SerializerFactory.create_serializer(models.Country)
CategorySerializer = SerializerFactory.create_serializer(models.Category)

BaseAccountSerializer = SerializerFactory.create_serializer(models.Account, exclude_fields=('password',))
OnboardingSerializer = SerializerFactory.create_serializer(models.OnBoarding)
BaseAddressSerializer = SerializerFactory.create_serializer(models.Address)
QualificationSerializer = SerializerFactory.create_serializer(models.Qualification)

BaseProductSerializer = SerializerFactory.create_serializer(models.Product, exclude_fields=('resource',))
BaseItemSerializer = SerializerFactory.create_serializer(models.Item)
ResourceSerializer = SerializerFactory.create_serializer(models.Resources)

BaseQuestionSerializer = SerializerFactory.create_serializer(models.Question)
QuestionResponseSerializer = SerializerFactory.create_serializer(models.QuestionResponse)

PlanSerializer = SerializerFactory.create_serializer(models.Plan)
BaseSubscriptionTransactionSerializer = SerializerFactory.create_serializer(models.SubscriptionTransaction)

BaseCartSerializer = SerializerFactory.create_serializer(models.Cart)
BaseOrderSerializer = SerializerFactory.create_serializer(models.Order)
BaseOrderItemSerializer = SerializerFactory.create_serializer(models.OrderItem)
BaseTransactionSerializer = SerializerFactory.create_serializer(models.Transaction)

BaseAssessmentSerializer = SerializerFactory.create_serializer(models.AssessmentTest)
BaseUserAssessmentSerializer = SerializerFactory.create_serializer(models.UserAssessment)
UserResponseSerializer = SerializerFactory.create_serializer(models.UserResponse)

PaymentStatusSerializer = SerializerFactory.create_serializer(models.PaymentStatus)
OrderStatusSerializer = SerializerFactory.create_serializer(models.OrderStatus)

RatingSerializer = SerializerFactory.create_serializer(models.RatingEntry)
BaseReviewSerializer = SerializerFactory.create_serializer(models.Review)

TherapistOnBoardingSerializer = SerializerFactory.create_serializer(models.TherapistOnBoarding)
TherapistBillingSettingsSerializer = SerializerFactory.create_serializer(models.TherapistBillingSettings)

DailyTipSerializer = SerializerFactory.create_serializer(models.DailyTips)
BaseHabitSerializer = SerializerFactory.create_serializer(models.Habit)
CheckInSerializer = SerializerFactory.create_serializer(models.CheckIn)
AccountNotificationSerializer = SerializerFactory.create_serializer(models.AccountNotification)
TherapistCategorySerializer = SerializerFactory.create_serializer(models.TherapistCategory)
LanguageSerializer = SerializerFactory.create_serializer(models.Language)
UserPreTherapyResponseSerializer = SerializerFactory.create_serializer(models.UserPreTherapyResponse)
BaseUserPreTherapySerializer = SerializerFactory.create_serializer(models.UserPreTherapy)
HabitCompanionSerializer = SerializerFactory.create_serializer(models.HabitCompanion)


class UserPreTherapySerializer(BaseUserPreTherapySerializer):
    responses = UserPreTherapyResponseSerializer(many=True)


class SubscriptionTransactionSerializer(BaseSubscriptionTransactionSerializer):
    deducted_amount = SerializerMethodField()
    amount = SerializerMethodField()
    total_amount = SerializerMethodField()

    def get_deducted_amount(self, obj):
        """
        retrieve object deducted amount field
        :param obj
        :return:
        """
        return float(obj.deducted_amount) if obj.deducted_amount else 0.0

    def get_amount(self, obj):
        """
        retrieve object amount field
        :param obj
        :return:
        """
        return float(obj.amount) if obj.amount else 0.0

    def get_total_amount(self, obj):
        """
        retrieve object total amount field
        :param obj
        :return:
        """
        return float(obj.total_amount) if obj.total_amount else 0.0


class AddressSerializer(BaseAddressSerializer):
    country = CountrySerializer()
    state = StateSerializer()


class AccountSerializer(BaseAccountSerializer):
    onboarding = OnboardingSerializer(many=True)
    account_notification = AccountNotificationSerializer(many=True)


class ApiAccountSerializer(BaseAccountSerializer):
    pre_therapy = UserPreTherapySerializer()


class ProductSerializer(BaseProductSerializer):
    categories = CategorySerializer(many=True)


class ItemSerializer(BaseItemSerializer):
    """
    item serializer
    """
    product = ProductSerializer(read_only=True)
    total_price = SerializerMethodField()

    def get_total_price(self, obj):
        """
        retrieve object total price field
        :param obj
        :return:
        """
        return float(obj.total_price) if obj.total_price else 0.0


class CartSerializer(BaseCartSerializer):
    items = ItemSerializer(many=True, read_only=True)
    user = AccountSerializer()


class ApiCartSerializer(BaseCartSerializer):
    items = ItemSerializer(many=True, read_only=True)
    total_price = SerializerMethodField()

    def get_total_price(self, obj):
        """
        retrieve object total price field
        :param obj
        :return:
        """
        return float(obj.total_price) if obj.total_price else 0.0


class OrderItemSerializer(BaseOrderItemSerializer):
    order = BaseOrderSerializer()
    product = ProductSerializer()


class OrderSerializer(BaseOrderSerializer):
    items = ItemSerializer(many=True, read_only=True)
    payment_status = PaymentStatusSerializer()
    order_status = OrderStatusSerializer()
    order_items = OrderItemSerializer(many=True)


class QuestionSerializer(BaseQuestionSerializer):
    responses = QuestionResponseSerializer(many=True, read_only=True)


class AssessmentSerializer(BaseAssessmentSerializer):
    questions = QuestionSerializer(many=True, read_only=True)


class UserAssessmentSerializer(BaseUserAssessmentSerializer):
    responses = UserResponseSerializer(many=True, read_only=True)
    assessment_test = BaseAssessmentSerializer(read_only=True)


class TherapistSerializer(ModelSerializer):
    class Meta:
        model = models.Therapist
        fields = ('id', 'name', 'email', 'address', 'profile_image', 'qualification', 'bio', 'resume', 'reviews_count',
                  'rating', 'headline', 'is_activated', 'sessions_count', 'revenue', 'onboarding', 'billing_settings')
    address = AddressSerializer(read_only=True)
    qualification = QualificationSerializer(read_only=True)
    onboarding = TherapistOnBoardingSerializer(read_only=True)
    billing_settings = TherapistBillingSettingsSerializer(read_only=True)
    revenue = SerializerMethodField()
    reviews_count = SerializerMethodField()
    sessions_count = SerializerMethodField()

    def get_revenue(self, obj):
        return obj.get_billings_sum()

    def get_reviews_count(self, obj):
        return obj.reviews_count()

    def get_sessions_count(self, obj):
        return obj.sessions_count()


class SessionSerializer(ModelSerializer):

    class Meta:
        model = models.Sessions
        fields = '__all__'

    therapist = TherapistSerializer()


class TherapySessionSerializer(ModelSerializer):

    class Meta:
        model = models.Sessions
        fields = '__all__'

    user = AccountSerializer()


class ProfileSerializer(ModelSerializer):

    class Meta:
        model = models.Profile
        fields = '__all__'

    current_therapist = TherapistSerializer()
    current_plan = PlanSerializer()
    user = AccountSerializer()
    current_session = SerializerMethodField()

    def get_current_session(self, obj):
        current = obj.get_current_session()
        return SessionSerializer(current).data if current else None


class TherapistBillingSerializer(ModelSerializer):
    class Meta:
        model = models.TherapistBillingSettings

    therapist = TherapistSerializer()
    # user = AccountSerializer()


class BillingSerializer(ModelSerializer):
    class Meta:
        model = models.Billing
        fields = ('sub_transaction', 'user', 'therapist')
    sub_transaction = SubscriptionTransactionSerializer()
    therapist = TherapistSerializer()
    user = AccountSerializer()


class TransactionSerializer(BaseTransactionSerializer):
    user = AccountSerializer()


class ReviewSerializer(BaseReviewSerializer):
    user = AccountSerializer()


class HabitSerializer(BaseHabitSerializer):
    check_ins = CheckInSerializer(many=True)
    companion = HabitCompanionSerializer()
    success_percentage = SerializerMethodField()

    def get_success_percentage(self, obj):
        """
        return success percentage
        :return:
        """
        positive_count = models.CheckIn.objects.filter(habit_id=obj.pk, positive=True).count()

        if not positive_count:
            return 0

        today = date.today()

        if obj.target_date > today:
            base = (today - obj.start_date).days + 1
        else:
            base = (obj.target_date - obj.start_date).days + 1

        return positive_count/base * 100
