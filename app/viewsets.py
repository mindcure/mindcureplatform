from django.apps import apps

from rest_framework import permissions, viewsets

import models
import serializers
from .services import permissions as custom_permissions


class ViewSetfactory(object):
    @classmethod
    def create_instance(cls, serializer_class, model_class=None, permission_classes=tuple()):
        class BaseSet(viewsets.ModelViewSet):
            pass

        BaseSet.model_class = model_class or serializer_class.Meta.model
        BaseSet.serializer_class = serializer_class
        BaseSet.queryset = BaseSet.model_class.objects.all()
        BaseSet.permission_classes = permission_classes

        return BaseSet


AccountViewSet = ViewSetfactory.create_instance(serializers.AccountSerializer, models.Account,
                                                permission_classes=(permissions.IsAuthenticated,
                                                                    custom_permissions.IsSuperAdmin))
