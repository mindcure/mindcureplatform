from django.db.models.signals import post_save
from django.dispatch import Signal, receiver

from . import models, tasks

new_user_registered = Signal(providing_args=["account", "request"])


@receiver(post_save, sender=models.Account)
def create_user_profile(sender, instance, created, **kwargs):
    """
    create user profile
    :param sender:
    :param instance:
    :param created:
    :param kwargs:
    :return:
    """
    if created:
        print('creating user...')
        models.Profile.objects.create(user=instance)
        models.OnBoarding.objects.create(user=instance)

        # contact new user
        tasks.contact_new_user.delay(instance.id)


@receiver(post_save, sender=models.Therapist)
def create_therapist_profile(sender, instance, created, **kwargs):
    """
    create therapist onboarding
    :param sender:
    :param instance:
    :param created:
    :param kwargs:
    :return:
    """
    if created:
        models.TherapistOnBoarding.objects.create(therapist=instance)

        # send registered email to therapist
        tasks.send_mail_new_therapist(instance.id)


