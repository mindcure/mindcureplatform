import json

from django.contrib import messages as flash_messages
from django.conf import settings

from django_user_agents.utils import get_user_agent

from .services import cart


def main(request, *args, **kwargs):

    user = request.user if request.user.is_authenticated() else None
    flashes = flash_messages.get_messages(request)
    notifications = list()
    messages = list()

    ua = get_user_agent(request)

    session_cart = None

    profile = None
    csrf_token = None

    if request.user.is_authenticated and not request.user.is_superuser:
        profile = user.account.profile
        session_cart = cart.get_session_cart(request)

        # retrieve csrf_token value
        csrf_token = request.session.get('cookie_token')

    return dict(user=user, flashes=flashes, settings=settings, messages=messages, notifications=notifications, ua=ua,
                session_cart=session_cart, profile=profile, csrf_token=csrf_token)



