import json
from datetime import datetime, timedelta
import os

from django.contrib import messages
from django.db.models import Q
from django.http import Http404, HttpResponseNotAllowed
from django.core.paginator import PageNotAnInteger, Paginator, EmptyPage
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.contrib.auth import login, authenticate

from rest_framework import decorators, authentication, permissions
from rest_framework.authtoken.models import Token

from .services import encoders, account, therapists, cart, orders, mongo, subscription, therapy as therapy_service
from . import models, serializers, forms, tasks

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "mindcure.settings.{}".format(os.getenv('DJANGO_SETTINGS_NAME')))


def check_username(request):
    if not request.is_ajax():
        raise Http404

    try:
        q = request.GET.get('q')
        if not q:
            return encoders.JSONResponse({'response': False})
        return encoders.JSONResponse({'response': account.check_username(q)})
    except Exception as e:
        raise e


def check_email(request):
    if not request.is_ajax():
        raise Http404

    try:
        q = request.GET.get('q')
        if not q:
            return encoders.JSONResponse({'response': False})
        return encoders.JSONResponse({'response': account.check_email(q)})
    except Exception as e:
        raise e


def check_therapist(request):

    if not request.is_ajax():
        raise Http404

    try:
        q = request.GET.get('q')
        if not q:
            return encoders.JSONResponse({'response': False})
        return encoders.JSONResponse({'response': therapists.check_email(q)})
    except Exception as e:
        raise e


def filter_products(request):
    """
    filter products
    :param request:
    :return:
    """
    page = request.GET.get('page', 1)
    category = request.GET.get('category', 'all')
    order_by = request.GET.get('order_by', 'created_at')

    query = models.Product.objects.order_by(order_by)
    filtered_query = query.all() if category == 'all' else query.filter(categories__slug=category).all()
    paginator = Paginator(filtered_query, 18)

    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)

    return encoders.JSONResponse({'data': {
        'items':  [serializers.ProductSerializer(c).data for c in products.object_list],
        'count': paginator.count, 'page_range': list(products.paginator.page_range),
        'start_index': products.start_index(), 'has_other_pages': products.has_other_pages(),
        'end_index': products.end_index()
    }})


def paginate_therapists(request):
    """
    paginate therapists
    :param request:
    :return:
    """
    page = request.GET.get('page', 1)
    order_by = request.GET.get('order_by', 'created_at')

    query = models.Therapist.objects.filter(is_activated=True, is_verified=True, billing_settings__isnull=False)

    q = request.GET.get('q')
    if q:
        query = models.Therapist.objects.filter(Q(name__icontains=q) | Q(email__icontains=q))

    language = request.GET.get('language')
    if language:
        if language != 'all':
            query = query.filter(languages__slug=language)

    categories = request.GET.get('categories')
    if categories:
        if categories != 'all':
            query = query.filter(categories__slug=request.GET.get('categories'))

    filtered_query = query.order_by(order_by).all()
    paginator = Paginator(filtered_query, 20)

    try:
        therapist_list = paginator.page(page)
    except PageNotAnInteger:
        therapist_list = paginator.page(1)
    except EmptyPage:
        therapist_list = paginator.page(paginator.num_pages)

    return encoders.JSONResponse(
        {'data': {'items': [serializers.TherapistSerializer(c).data for c in therapist_list.object_list],
                  'count': paginator.count, 'page_range': list(therapist_list.paginator.page_range),
                  'start_index': therapist_list.start_index(),
                  'has_other_pages': therapist_list.has_other_pages(),
                  'end_index': therapist_list.end_index()
                  }})


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def add_to_cart(request):
    """
    add product to cart
    :param request:
    :return:
    """
    form = forms.CartItemForm(request.data)

    if form.is_valid():

        product = models.Product.objects.get(pk=form.cleaned_data['product_id'])

        if not product:
            return encoders.JSONResponse({'message': 'Product not found'}, status=400)

        quantity = form.cleaned_data['quantity']

        session_cart = models.Cart.objects.get(pk=form.cleaned_data['cart_id'])

        try:
            item = models.Item.objects.filter(cart_id=session_cart.id, product_id=product.id).first()

            if not item:
                models.Item.objects.create(cart_id=session_cart.id, product=product, quantity=quantity,
                                           unit_price=product.unit_price)

                json_cart = json.dumps(serializers.ApiCartSerializer(session_cart).data, cls=encoders.DateTimeEncoder)
                request.session['cart'] = json_cart
                return encoders.JSONResponse(
                    {'message': "{} successfully added to cart".format(product.name), 'data': json_cart})

            if (item.quantity + form.cleaned_data['quantity']) > product.available_quantity:
                return encoders.JSONResponse({'message': 'Quantity not available'}, status=400)

            item.quantity += quantity
            item.save()

            json_cart = json.dumps(serializers.ApiCartSerializer(session_cart).data, cls=encoders.DateTimeEncoder)
            request.session['cart'] = json_cart
            return encoders.JSONResponse({'message': "{} successfully added to cart".format(product.name),
                                          'data': json_cart})
        except Exception as e:
            return encoders.JSONResponse({'message': e}, status=400)

    return encoders.JSONResponse({'message': form.errors}, status=400)


@csrf_exempt
def remove_item(request, pk):
    """
    add product to cart
    :param request:
    :return:
    """
    if not request.is_ajax():
        raise Http404

    if not request.method == 'DELETE':
        return encoders.JSONResponse('Method not Allowed', status=405)

    try:
        item = models.Item.objects.get(pk=pk)
        item.delete()
        session_cart = models.Cart.objects.get(pk=item.cart_id)

        json_cart = json.dumps(serializers.CartSerializer(session_cart).data, cls=encoders.DateTimeEncoder)
        request.session['cart'] = json_cart
        return encoders.JSONResponse(
            {'message': "Successfully removed item", 'data': json_cart}, status=200)
    except Exception as e:
        return encoders.JSONResponse({'message': e}, status=400)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def initiate_transaction(request):
    """
    initiate transaction
    :param request:
    :return:
    """
    session_cart = cart.get_current_cart(request)

    transaction = orders.process_order(request.user.account.id, session_cart.id)

    if transaction:
        return encoders.JSONResponse({'message': "order successful.",
                                      'transaction': serializers.TransactionSerializer(transaction).data})
    # except Exception as e:
    #     return encoders.JSONResponse({'message': e}, status=400)

    return encoders.JSONResponse({'message': 'failed'}, status=400)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def close_transaction(request):
    """
    close transaction
    :param request:
    :return:
    """
    form = forms.CloseTransactionForm(request.data)

    if form.is_valid():

        transaction = orders.close_transaction(form.cleaned_data['transaction_reference'], form.cleaned_data['tx_ref'],
                                               form.cleaned_data['status'])

        if transaction:
            new_cart = cart.checkout_cart(request, form.cleaned_data['cart_id'])

            json_cart = json.dumps(serializers.CartSerializer(new_cart).data, cls=encoders.DateTimeEncoder)
            request.session['cart'] = json_cart
            return encoders.JSONResponse({'data': {'message': "payment successful.", 'cart': json_cart,
                                          'transaction': serializers.TransactionSerializer(transaction).data}})

        return encoders.JSONResponse({'message': 'failed'}, status=400)

    return encoders.JSONResponse({'message': form.errors}, status=400)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def create_message(request):
    """
    send message
    :param request:
    :return:
    """
    form = forms.ComposeMessageForm(request.data)

    if form.is_valid():
        data = form.cleaned_data.copy()
        #
        # therapist = models.Therapist.objects.get(pk=data['therapist_id'])
        #
        # if not therapist:
        #     return encoders.JSONResponse({'error': {'message': 'Invalid therapist account'}}, status=404)

        data["user_id"] = request.user.account.id
        data["username"] = request.user.account.username
        data["user_read"] = True
        data["therapist_replied"] = False
        data["therapist_read"] = False
        data["user_replied"] = False
        timestamp = (datetime.today() - datetime(1970, 1, 1)).total_seconds()
        identifier = mongo.MongoService.get_next_sequence('messages')
        resp = mongo.MongoService.record_data('messages', identifier, timestamp, **data)

        return encoders.JSONResponse({'data': resp.inserted_id}, status=201)
    else:
        return encoders.JSONResponse({'error': {'message': form.errors}}, status=400)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def subscribe_user(request):
    """
    subscribe user
    :param request:
    :return:
    """

    form = forms.SubscriptionTransactionForm(request.data)

    if form.is_valid():
        transaction = subscription.create_transaction(request.user.account.id, **form.cleaned_data)

        if transaction:
            return encoders.JSONResponse({'message': "transaction initiated",
                                          'transaction': serializers.SubscriptionTransactionSerializer(transaction).data})

        return encoders.JSONResponse({'message': 'failed'}, status=400)

    return encoders.JSONResponse({'error': {'message': form.errors}}, status=400)


@decorators.api_view(['POST'])
@decorators.authentication_classes((authentication.TokenAuthentication, authentication.SessionAuthentication))
@decorators.permission_classes((permissions.IsAuthenticated,))
def complete_subscription(request):
    """
    close subscription transaction
    :param request:
    :return:
    """
    form = forms.CloseSubTransactionForm(request.data)

    if form.is_valid():

        transaction = subscription.close_transaction(**form.cleaned_data)

        return encoders.JSONResponse({'data': serializers.SubscriptionTransactionSerializer(transaction).data})

    return encoders.JSONResponse({'error': {'message': form.errors}}, status=400)


def login_user(request):
    """
    login user
    :param request:
    :return:
    """

    form = forms.LoginForm(request.POST)

    if form.is_valid():

        user = authenticate(request, **form.cleaned_data)
        if user:

            # if not hasattr(user, 'account'):
            #     try:
            #         models.Account.objects.create(user_ptr_id=user.id)

            # check if user is verified
            if not user.account.is_verified:
                tasks.contact_new_user.delay(user.account.id)
                return encoders.JSONResponse({'message': "Account not verified, check your mail for verification link"},
                                             status=401)

            # login user
            login(request, user)

            # merge cart if found
            if 'cart' in request.session:
                cart.merge_cart(request)

            next_url = request.session.get('next') or request.GET.get('next') or '/'

            # token = 'user:w_{}'.format(request.COOKIES.get('csrftoken'))
            # token = request.COOKIES.get('csrftoken')
            # token, _ = Token.objects.get_or_create(user=user)
            cookie_value = account.encode_jwt_token(user.account.id)
            account.save_web_redis_user(cookie_value, user.account.id)
            token = 'w_{}'.format(cookie_value)
            request.session['cookie_token'] = token

            response = encoders.JSONResponse({'next_url': next_url, 'token': token})

            max_age = 14 * 24 * 60 * 60  # two weeks
            expires = datetime.strftime(datetime.utcnow() + timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
            response.set_cookie('mindcure_community', cookie_value, httponly=True, max_age=max_age, expires=expires,
                                domain='mindcureglobal.com')

            return response

        return encoders.JSONResponse({'message': "Invalid username/password combination"}, status=401)

    return encoders.JSONResponse({'message': form.errors}, status=400)


def register_user(request):
    """
    register user account
    :param request
    :return
    """
    form = forms.RegistrationForm(request.POST)

    if form.is_valid():

        if account.check_username(form.cleaned_data['username'].lower()):
            return encoders.JSONResponse({'message': 'An account with this username already exists'}, status=400)

        if account.check_email(form.cleaned_data['email'].lower()):
            return encoders.JSONResponse({'message': 'An account with this email already exists'}, status=400)

        acct = account.register_account(**form.cleaned_data)

        if acct:
            # login user
            # login(request, acct.user_ptr)

            next_url = '/accounts/login'

            # contact new user account
            # tasks.contact_new_user.delay(acct.id)
            # next_url = request.session.get('next') or request.GET.get('next') or '/accounts/login'

            return encoders.JSONResponse({'next_url': next_url})

        return encoders.JSONResponse({'message': "Invalid username/password combination"}, status=400)

    return encoders.JSONResponse({'message': form.errors}, status=400)


def add_rating(request):
    """
    add new rating to therapist
    :param request
    :param kwargs:
    :return:
    """

    form = forms.RatingForm(request.POST)

    if form.is_valid():
        rating = therapists.add_rating(**form.cleaned_data)

        return encoders.JSONResponse({'data': serializers.RatingSerializer(rating).data})

    return encoders.JSONResponse({'error': form.errors}, status=400)


def add_review(request):
    """
    add new review
    :param request
    :return:
    """
    form = forms.ReviewForm(request.POST)

    if form.is_valid():
        review = therapists.add_review(**form.cleaned_data)

        return encoders.JSONResponse({'data': serializers.ReviewSerializer(review).data})

    return encoders.JSONResponse({'error': form.errors}, status=400)


@csrf_exempt
def therapist_send_message(request):

    if request.method == "POST":
        form = forms.ComposeMessageForm(request.POST)

        if form.is_valid():
            data = form.cleaned_data.copy()

            data["user_read"] = False
            data["therapist_replied"] = False
            data["therapist_read"] = True
            data["user_replied"] = False
            timestamp = (datetime.today() - datetime(1970, 1, 1)).total_seconds()
            identifier = mongo.MongoService.get_next_sequence('messages')
            resp = mongo.MongoService.record_data('messages', identifier, timestamp, **data)

            return encoders.JSONResponse({'data': resp.inserted_id}, status=201)
        else:
            return encoders.JSONResponse({'error': {'message': form.errors}}, status=400)
    else:
        raise HttpResponseNotAllowed

