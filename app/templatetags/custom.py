import random
import json

from django import template
from django.utils.safestring import mark_safe
from django.conf import settings

from app import models

register = template.Library()


@register.simple_tag()
def random_quote():
    """
    return random quotes
    :return:
    """
    with open('setup/quotes.json', 'rb') as quotes_file:
        quotes = json.loads(quotes_file.read())

    return mark_safe(random.choice(quotes))

#
# @register.simple_tag()
# def age(user_id):
#     """
#     return user's pre-therapy response age
#     :param user_id
#     :return:
#     """
#     try:
#         user = models.Account.objects.get(pk=user_id)
#
#         if not hasattr(user, 'pre_therapy'):
#             return None
#
#         responses = user.pre_therapy.responses.all()
#
#         age_question = next(i for i in responses if i.question == settings.PT_QUESTIONS_AGE)
#
#         return age_question.response
#     except Exception as e:
#         return None
