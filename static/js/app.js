/**
 * Created by tm30 on 3/29/18.
 */

 // formatDate
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
};

// list cookies
function listCookies() {
    var theCookies = document.cookie.split(';');
    var aString = '';
    for (var i = 1 ; i <= theCookies.length; i++) {
        aString += i + ' ' + theCookies[i-1] + "\n";
    }
    return aString;
};

// goTo previous window
function goBack() {
    window.history.back();
}

// Logs an action (text) and the time when it happened on the console.
function trace(text) {
    text = text.trim();
    const now = (window.performance.now() / 1000).toFixed(3);

    console.log(now, text);
};

function errorMsg(msg, error) {
    const errorElement = document.querySelector('#errorMsg');
    errorElement.innerHTML += `<p>${msg}</p>`;
    if (typeof error !== 'undefined') {
        console.error(error);
    }
}

// var socketUrl = 'http://localhost:5060';
var socketUrl = 'https://mindcureglobal.com:5070';
var socket = io.connect(socketUrl, {
    autoConnect: false,
    transports: ['websocket']
});

socket.on('log', function(array) {
    console.log.apply(console, array);
});

// setup video 
var turnserver = { iceServers: [{
        urls: "turn:mindcureglobal.com:3478",
        username: "mindcure",
        credential: "e@volution1ary"
    }]
};

// let turnserver = null;

let localStream;
let localVideo;
let peerConnection;
let audio;
let timer;
var sec = 0;

function pad(val) {
    var valString = val + '';
    if (valString.length < 2) {
        return "0" + valString;
    }
    return valString;
}

function clearTimer() {
    clearInterval(timer);
}

var app = angular.module('mindcureApp', ['ngResource', 'ngCookies', 'ngToast', 'angularMoment', 'ngStorage', 'swxSessionStorage']);

app.constant('TEST_SECRET_KEY', 'sk_live_8843e484bfb9be1a1200f26762a11d1189f85241');
app.constant('PUBLIC_KEY', 'pk_live_dbcff800894dc820301169ead2684283561ab8ca');
app.constant('MAX_FILE_SIZE', 10000000);
app.constant('BASE_AWS_URL', 'https://s3.amazonaws.com/mindcure');
app.constant('AWS_ACCESS_KEY_ID', 'AKIAIMDMRNBQCXIAUIAQ');
app.constant('AWS_ACCESS_KEY_SECRET', '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');

app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});

app.factory('Product', function ($resource) {
    return $resource('api/products/:id');
});

app.factory('Cart', function ($resource) {
    return $resource('api/carts/:id');
});

app.factory('Assessment', function ($resource) {
    return $resource('api/assessments/:id');
});

app.factory('Inbox', function ($resource) {
    return $resource('api/user/inbox/:id');
});

app.factory('Sent', function ($resource) {
    return $resource('api/user/sent/:id');
});

app.factory('Message', function ($resource) {
    return $resource('api/user/messages/:id');
});

app.service('S3UploadService', ['$q', 'AWS_ACCESS_KEY_ID', 'AWS_ACCESS_KEY_SECRET', function ($q, AWS_ACCESS_KEY_ID, AWS_ACCESS_KEY_SECRET) {
    // Us standard region
    AWS.config.region = 'us-east-1';
    AWS.config.update({ accessKeyId: AWS_ACCESS_KEY_ID, secretAccessKey: AWS_ACCESS_KEY_SECRET });

    var bucket = new AWS.S3({ params: { Bucket: 'mindcure', maxRetries: 10 }, httpOptions: { timeout: 360000 } });

    this.Progress = 0;
    this.Upload = function (file, file_name) {

        var deferred = $q.defer();
        var params = { Bucket: 'mindcure', Key: file_name, ContentType: file.type, Body: file };
        var options = {
            // Part Size of 10mb
            partSize: 10 * 1024 * 1024,
            queueSize: 1,
            // Give the owner of the bucket full control
            ACL: 'bucket-owner-full-control'
        };
        var uploader = bucket.upload(params, options, function (err, data) {
            if (err) {
                deferred.reject(err);
            }
            deferred.resolve();
        });
        uploader.on('httpUploadProgress', function (event) {
            deferred.notify(event);
        });

        return deferred.promise;
    };
}]);

app.filter('timeAgo', function() {
    return function(x) {
        return moment.unix(x).minutesFromNow();
    }
})

app.filter('therapist_name', function () {
    return function (x) {

        if (x == null) {
            return 'Therapist'
        }

        let v = x.split('-')
        if (v.length >= 2) {
            return v[2]
        }
        return 'Therapist';
    }
})


app.controller('BaseController', function ($scope, $rootScope, $cookies, ngToast, PUBLIC_KEY) {

    $scope.init = function (data) {
        $rootScope.cart = data;
        $scope.data = {transaction: null}
    };

    $scope.logout = function () {
        swal({
            title: 'Logging out',
            type: 'info',
            html: 'logging out...',
            onOpen: () => {
                swal.showLoading()
              },
              onClose: () => {
              }
        });

        skygear.auth.logout().then(function () {
            swal.close();
            location.href = '/logout';
        }, function (error) {
            console.error(error);
        });
    };

    $scope.selectPlan = function() {
        var formData = $('#planForm').serializeArray();
        $('#planNext').prop("disabled", true);

        $.ajax({
            url: '/api/plans/select',
            method: 'POST',
            data: formData,
            success: function (resp) {
                console.log(resp);
                swal(
                    'Plan Selected!',
                    'Successfully selected a plan. You will be redirected to the payment confirmation page',
                    'success'
                ).then((result) => {
                    location.href = 'subscriptions/payment'
                });
            },
            error: function (err) {
                swal(
                    'Error',
                    err.message,
                    'error'
                );
            }
        });
    }

    $scope.init_therapist = function () {
        $scope.current_therapist_id = null;
    };

    $scope.changeTherapist = function (therapist_id) {
        $scope.current_therapist_id = therapist_id;
        $scope.therapist_error = null;
        document.getElementById("therapist_id").value = $scope.current_therapist_id;
        $('#myDIV').submit();
    };

    $scope.assign = function() {
        swal({
            title: 'Assigning Therapist',
            type: 'info',
          //   input: 'text',
            inputAttributes: {
              autocapitalize: 'off'
            },
            html: '<i class="ion-ios-timer f-15"></i>&nbsp;Loading...<br>' + 'Sieving through the list to assign an appropriate therapist',
            // html: 'Sieving through the list to assign an appropriate therapist <br><br><br>',
            showCloseButton: false,
            showConfirmButton: false,
            showCancelButton: false,
            onOpen: function () {
              console.log('loading data...')
                $.ajax({
                    url: '/api/therapists/assign',
                    method: 'POST',
                    data: {
                        csrfmiddlewaretoken: $cookies.get('csrftoken'),
                        body: '',
                        send_therapist: false
                    },
                    success: function (resp) {
                        swal.close();
                        location.href = '/sessions';
                    },
                    error: function (err) {
                        swal(
                            'Error',
                            err.message,
                            'error'
                        );
                    }
                });
            }
        });
    }

    $scope.skip = function() {
        swal({
          title: 'Assigning Therapist',
          type: 'info',
          html:
            '<textarea id="body" name="body" class="swal2-input" style="height: 10em" placeholder="Enter reason for changing therapist"></textarea>' +
            '<input id="send_therapist" type="checkbox" style="width: auto; display: inline-block; height: auto; margin-right: 10px" value="true" checked name="send_therapist" class="swal2-input">' +
            '<label style="display: inline-block">Share reason with current therapist</label>',
          preConfirm: function () {
            return new Promise(function (resolve) {
            resolve({'body': $('#body').val(), send_therapist: $('#send_therapist').is(":checked")})
            })
          },
          onOpen: function () {
            $('#body').focus()
          },
        //   input: 'text',
        //   inputAttributes: {
        //     autocapitalize: 'off'
        //   },
          //html: '<i class="ion-ios-timer f-30"></i>&nbsp;Loading...<br>' + 'Sieving through the list to assign an appropriate therapist',
        //   html: 'Sieving through the list to assign an appropriate therapist <br><br><br>',
        //   showCloseButton: false,
        //   showConfirmButton: false,
        //   showCancelButton: false,
          focusConfirm: false
        }).then(function (result) {
            if (result.value) {
                $.ajax({
                    url: '/api/therapists/assign',
                    method: 'POST',
                    data: {
                        csrfmiddlewaretoken: $cookies.get('csrftoken'),
                        body: result.value.body,
                        send_therapist: result.value.send_therapist
                    },
                    success: function (resp) {
                        swal.close();
                        location.href = '/sessions';
                    },
                    error: function (err) {
                        swal(
                            'Error',
                            err.message,
                            'error'
                        );
                    }
                });
            }
        }).catch(swal.noop)

    };

    var initiateRave = function (amount, txRef) {
        getpaidSetup({
            customer_email: "Mindcureglobal@gmail.com",
            amount: amount,
            txref: txRef,
            PBFPubKey: "FLWPUBK-cb8ba0eca16be29c796f723e7bdf3693-X",
            meta: [{transaction_reference: txRef}],
            onclose: function () {
                displayToast('<span>You cancelled the transaction</span>', 'warning');
            },
            callback: function (d) {
                console.log(d);
            }
        });
    };

    var closeSubscription = function(reference, status) {
        $.ajax({
            url: '/api/subscribe/close',
            method: 'POST',
            data:  {
                csrfmiddlewaretoken: $cookies.get('csrftoken'),
                status: status,
                tx_ref: reference,
                transaction_reference: $scope.payloadData.transaction.reference_code
            },
            success: function (resp) {
                swal(
                    'Payment Successful',
                    'You have successfully paid for your therapy session',
                    'info'
                ).then((result) => {
                    location.href = '/sessions'
                });;
            },
            error: function (err) {
                displayToast('<span>' + err.responseJSON.error.message + '</span>', 'warning');
            }
        });
    }

    var initiatePaystack = function(callback_fxn) {
        var handler = PaystackPop.setup({
           key: PUBLIC_KEY,
           email: $scope.payloadData.email,
           amount: $scope.payloadData.amount * 100,
           currency: 'NGN',
           bearer: 'account',
           subaccount: $scope.payloadData.subaccount_number,
           ref:  $scope.payloadData.transaction.transaction_reference,
           callback: function(response){
                return callback_fxn(response.reference, response.status)
            },
            onClose: function(){
                swal(
                    'Payment Stopped',
                    'You stopped the payment process',
                    'info'
                );
            }
         });
         handler.openIframe();
    }

    let closeTransaction = function(reference, status) {
         $.ajax({
            url: '/api/close/transaction',
            method: 'POST',
            data: {
                csrfmiddlewaretoken: $cookies.get('csrftoken'),
                transaction_reference: $scope.data.transaction.reference_code,
                status: status,
                tx_ref: reference,
                cart_id: $rootScope.cart.id
            },
            success: function (resp) {
                let response_data = resp.data
                $rootScope.cart = response_data.cart
                swal(
                    'Sent!',
                    'Payment for your order is successful',
                    'success'
                ).then((result) => {
                    location.href = '/'
                });
            },
            error: function (err) {
                displayToast('<span>' + err.responseJSON.message + '</span>', 'warning');
            }
        });
    }

    $scope.processPayment = function () {

        $.ajax({
            url: '/api/initiate/transaction',
            method: 'POST',
            data: {csrfmiddlewaretoken: $cookies.get('csrftoken')},
            success: function (resp) {
                $scope.data.transaction = resp.transaction;
                console.log($scope.data.transaction);
                var handler = PaystackPop.setup({
                   key: PUBLIC_KEY,
                   email: $scope.data.transaction.user.email,
                   amount: $scope.data.transaction.amount * 100,
                   currency: 'NGN',
                   bearer: 'account',
                   ref:  $scope.data.transaction.transaction_reference,
                   callback: function(response){
                        return closeTransaction(response.trans, response.status)
                    },
                    onClose: function(){
                        swal(
                            'Payment Stopped',
                            'You stopped the payment process',
                            'info'
                        );
                    }
                 });
                 handler.openIframe();
            },
            error: function (err) {
                swal(
                    'Error!',
                    err.responseJSON.message,
                    'error'
                );
            }
        });
    }

    $scope.initSub = function(plan_id, therapist_id, amount, email, subaccount_number) {

        $scope.payloadData = {
            email: email,
            subaccount_number: subaccount_number,
            amount: amount
        }
        $scope.start_date = null;
        $scope.amount = amount + '-5';
        $scope.payFormData = {
            plan_id: plan_id,
            therapist_id: therapist_id,
            amount: amount,
            start_date: null,
            csrfmiddlewaretoken: $cookies.get('csrftoken'),
        }
        $scope.now_date = formatDate(new Date());
        $scope.computeDuration();
    }

    $scope.computeDuration = function() {
        var amount_split = $scope.amount.split('-')
        $scope.payFormData.amount = amount_split[0]
        $scope.payFormData.duration = amount_split[1]
    }

    $scope.subscribeUser = function () {
        $scope.payFormData.start_date = formatDate($scope.start_date);
        $.ajax({
            url: '/api/subscribe/user',
            method: 'POST',
            data:  $scope.payFormData,
            success: function (resp) {
                displayToast('<span>' + resp.message + '</span>', 'success');
                $scope.payloadData.transaction = resp.transaction
                $scope.payloadData.amount = $scope.payFormData.amount
                return initiatePaystack(closeSubscription);
            },
            error: function (err) {
                displayToast('<span>' + err.responseJSON.error.message + '</span>', 'warning');
            }
        });

    };

    var displayToast = function (text, toast_type) {
        ngToast.create({
            className: toast_type,
            content: text
        });
    };

    $scope.itemAvailable = function (product_id) {
        var product = $rootScope.cart.items.find(function (elem) {
            return elem.product.id === product_id
        });

        return [undefined, null].includes(product);
    };

    $scope.addToCart = function (product_id) {
        var data = {
            cart_id: $rootScope.cart.id,
            product_id: product_id,
            quantity: 1,
            csrfmiddlewaretoken: $cookies.get('csrftoken')
        };

        $.ajax({
            url: '/api/carts/add',
            method: 'POST',
            data: data,
            success: function (resp) {
                $scope.$apply(function () {
                    $rootScope.cart = JSON.parse(resp.data);
                });
                displayToast('<span>' + resp.message + '</span>', 'success');
            },
            error: function (err) {
                displayToast('<span>' + err.responseJSON.message + '</span>', 'warning');
            }
        });
    };

    $scope.removeItem = function (item_id) {
        $.ajax({
            url: '/api/items/' + item_id + '/remove',
            method: 'DELETE',
            success: function (resp) {
                $scope.$apply(function () {
                    $rootScope.cart = JSON.parse(resp.data);
                });
                displayToast('<span>' + resp.message + '</span>', 'success');
            },
            error: function (err) {
                displayToast('<span>' + err.responseJSON.message + '</span>', 'warning');
            }
        });
    };

    $scope.init_pre_therapy = function() {
        $scope.responses = { current_question: 1, current_response: '', next_active: false, prev_active: false};
        $scope.questions = [
            {'number': 1, response: null},
            {'number': 2, response: null},
            {'number': 3, response: null},
            {'number': 4, response: null},
            {'number': 5, response: null},
            {'number': 6, response: null},
            {'number': 7, response: null},
            {'number': 8, response: null},
            {'number': 9, response: null},
            {'number': 10, response: null},
        ]
    }

    $scope.init_assessment = function(questions) {
        $scope.questions = questions;
        $scope.questions.map(function(elx) {
            elx.response = null
            return elx
        })
        $scope.responses = { current_question: 0, current_response: '', next_active: false, prev_active: false};
    }

    $scope.findQuestion = function(id) {
       return $scope.questions.find(function(ipx) {
            return ipx.id == id
        })
    }

    $scope.next_pt_question = function() {
        $scope.responses.current_question += 1;
        $scope.responses.current_response = null;
    }

    $scope.prev_pt_question = function() {
        $scope.responses.current_question -= 1;
        $scope.responses.current_response = null;
    }
});

app.controller('AuthController', function ($scope, $cookies) {

    $scope.init = function () {
        $scope.authData = {
            username: null,
            password: null,
            email: null
        };
        $scope.error = {messages: [], form: {}};
        $scope.next_url = null;
    };

    var processError = function (error) {
        $scope.$apply(function () {
            $scope.error.messages = error;
        })
    };

//    let socketLogin = function() {
//        var cookie_token = $cookies.get('cookie_token')
//        socket.emit('login', `user:w_${cookie_token}`);
////        $scope.next_url = data.next_url
//        return;
//    }

    let skylogin = function(resp) {
        skygear.auth.loginWithUsername($scope.authData.username, $scope.authData.username).then(function (user) {
            location.href = resp.next_url
//            return socketLogin(resp)
        }, function (error) {
            if (error.error.code === skygear.ErrorCodes.InvalidCredentials) {
                processError(["Invalid username/password combination."]);
//                        location.href = '/logout';
                return;
            }
            else if (error.error.code === skygear.ErrorCodes.ResourceNotFound) {
                skygear.auth.signupWithUsername($scope.authData.username, $scope.authData.username).then(function (user) {
//                    return socketLogin(resp);
                      location.href = resp.next_url
                }, function (err) {
                    processError(["Invalid username/password combination."]);
//                            location.href = '/logout';
                    return;
                });
            }
            else {
                swal("Invalid username/password combination.").then(function() {
                    location.href = '/logout';
                })
//                $timeout(function() {
//                    location.href = '/logout';
//                }, 1000)
                return;
            }
        });
    }


    $scope.login = function () {
        $("button[type=submit]").attr("disabled", true);
        $.ajax({
            url: '/api/web/login' + location.search,
            method: 'POST',
            data: {
                csrfmiddlewaretoken: $cookies.get('csrftoken'),
                username: $scope.authData.username,
                password: $scope.authData.password
            },
            success: function (resp) {
                skylogin(resp);
            },
            error: function (err) {
                processError([err.responseJSON.message]);
                $("button[type=submit]").attr("disabled", false);
            }
        });
    };

    $scope.signUp = function () {
        $("button[type=submit]").attr("disabled", true);
        $.ajax({
            url: '/api/web/register',
            method: 'POST',
            data: {
                csrfmiddlewaretoken: $cookies.get('csrftoken'),
                username: $scope.authData.username,
                password: $scope.authData.password,
                email: $scope.authData.email
            },
            success: function (response) {
                skygear.auth.signupWithUsername($scope.authData.username, $scope.authData.username).then(function (user) {
                    skygear.auth.loginWithUsername($scope.authData.username, $scope.authData.username).then(function (user) {
                        location.href = response.next_url;
                    }, function (error) {
                        console.log(error)
                        if (error.error.code === skygear.ErrorCodes.InvalidCredentials) {
                            processError(["Invalid credentials."]);
//                            location.href = '/logout';
                            return;
                        }
                        else {
                            processError(["Signup error"]);
//                            location.href = '/logout';
                            return;
                        }
                    });
                }, function (err) {
                    processError([err]);
//                    location.href = '/logout';
                });
            },
            error: function (err) {
                $scope.$apply(function () {
                    $scope.error.form = err.responseJSON.message;
                });
                $("button[type=submit]").attr("disabled", false);
            }
        });

    }
});

app.controller('SessionController', function($scope, $rootScope, MAX_FILE_SIZE, $timeout, $cookies, $q, BASE_AWS_URL, S3UploadService) {

    const now = new Date();
    now.setHours(0,0,0);

    var secondsSpan = document.getElementById("seconds");
    var minutesSpan = document.getElementById('minutes');

    var startTimer = () => {
        sec = 0;
        timer = setInterval(() => {
            ++sec;
            secondsSpan.innerHTML = pad(sec%60);
            minutesSpan.innerHTML = pad(parseInt(sec/60))
        }, 1000);
    }

    var scrollBottom = function() {
        items = document.querySelectorAll("#scrolled");
        last = items[items.length-1];
        if (last) {
            last.scrollIntoView();
        }
    }

    $rootScope.hideModal = function() {
        modal.style.display = "none";
    }

    function showModal() {
        modal.style.display = "block";
    }

    async function increment_conversation_count() {
        $.ajax({
            url: '/api/increment/conversation',
            method: 'POST',
            data: {csrfmiddlewaretoken: $cookies.get('csrftoken')},
            success: function (resp) {
                $scope.$apply(function() {
                    $scope.conv_diff = $scope.conversation_limit - resp.data.counter_conversation
                    if ($scope.conv_diff <= 0) {
                        deactivateConversation();
                    }
                })
            },
            error: function (err) {
                console.log(err);
            }
        });
    }

    skygearChat.subscribe(function (payload) {
        $scope.$apply(function () {
            switch (payload.record_type) {
                case 'message':

                    let msg = payload.record;

                    if (payload.event_type === 'delete') {
                        let index = $rootScope.conversation.messages.indexOf(msg);
                        if (index > 0) {
                            $rootScope.conversation.messages.splice(index, 1);
                        }
                    }
                    else if (payload.event_type === 'create') {
                        $rootScope.conversation.messages.push(msg);
                        $rootScope.conversation.last_message = msg;
                        $timeout(function() {
                            scrollBottom();
                            if ($scope.current_session.therapist.email !== $scope.conv_data.therapist_email) {
                                increment_conversation_count();
                            }
                        })
                    }
                    break;
                default:
                    console.log(payload.record);
                    break;
            }
        })
    });

    var makeUnavailable = function() {
        $rootScope.conversation.available = false;
    }

    var deactivateConversation = function() {
        $("#chatting").attr('disabled','disabled');
    }

    var activateConversation = function() {
        $("#chatting").prop('disabled', false);
        $rootScope.conversation.available = true;
//        $scope.$apply(function() {
//            $rootScope.conversation.available = true;
//        })
    }

    var setMessages = function(messages) {
        $scope.$apply(function() {
            $rootScope.conversation.messages = messages.reverse().concat($rootScope.conversation.messages);
            $scope.loading.more = false;
        })
    };

    var getMessages = function (conversation) {

        let currentTime = new Date();

        if (conversation.hasOwnProperty('messages')) {
            if (conversation.messages.length > 0) {
                currentTime = conversation.messages[0].createdAt;
            }
        }

        skygearChat.getMessages(conversation.data, conversation.data.per_page, currentTime, '_created_at').then(function (messages) {
            return setMessages(messages);
        }, function(err) {
            deactivateConversation();
        });
    };

    var setConversation = function(conversation_data) {
        $scope.$apply(function () {
            $rootScope.conversation.data = conversation_data;
            $rootScope.conversation.data.per_page = 50;
            $rootScope.conversation.available = true;
            getMessages($rootScope.conversation);
        })
    }

    var createConversation = function() {
        var participants = [skygear.auth.currentUser];
        const conversationOptions = {
            distinctByParticipants: true
        };

        var User = skygear.Record.extend('user');
        var q = new skygear.Query(User);
        q.equalTo('email', $scope.conv_data.therapist_email);
        skygear.publicDB.query(q).then(function (result) {
            console.log('result', result);
            var response = Array.from(result);
            if (response.length == 0) {
                deactivateConversation();
                return;
            }
            participants.push(response[0]);

            skygearChat.createConversation(participants, $scope.conv_data.conversation_title, $scope.conv_data).then(function (conversation) {
                setConversation(conversation)
            }, function (err) {
                deactivateConversation();
                return;
            });

        });
    };

    var getConversation = function() {
        var Conversation = skygear.Record.extend('conversation');
        var q = new skygear.Query(Conversation);
        q.equalTo('title', $scope.conv_data.conversation_title);
        skygear.publicDB.query(q).then(function (result) {
            var data = Array.from(result);
            if (data.length > 0) {
                 setConversation(data[0])
            }
            else {
                 createConversation();
            }
        }, function (errr) {
            deactivateConversation();
        });
    }

    $('#contents').scroll(function() {
        if ( $('#contents').scrollTop() <= 0 ) {

            $scope.$apply(function() {
                $scope.loading.more = true;
            })
            getMessages($rootScope.conversation);
        }
    });

    $scope.sendMessage = function (message, conversation) {
        if (message) {
            $scope.text_msg = null;
            skygearChat.createMessage(conversation, message).then(function (resp) {
                
            }, function (err) {
                console.log(err)
            })
        }
    };

    var getUser = function(email) {
        var User = skygear.Record.extend('user');
        var q = new skygear.Query(User);
        q.equalTo('email', email);
        skygear.publicDB.query(q).then(function (result) {
            var response = Array.from(result);
            if (response.length > 0) {
                $q.all([getConversation(), socket.emit('login', $scope.cookie_token.data, 'user')]).then(function(success) {
                    $scope.loading.data = false;
                }, function(err){
                })
            }
            else {
                deactivateConversation();
                makeUnavailable();
                swal.close();
            }
        }).catch((err) => {
            deactivateConversation();
            makeUnavailable();
            console.log(err);
        });
    }

    let attachFile = function(file) {
        $rootScope.file = file;
        var timestamp = Math.round((new Date()).getTime() / 1000);
        var file_name = `mindcure_desktop_user_${timestamp}_${file.name}`;
        S3UploadService.Upload($rootScope.file, file_name).then(function (result) {
            // Mark as success
            $scope.text_msg = null;
            skygearChat.createMessage($rootScope.conversation.data, `${BASE_AWS_URL}/${file_name}`, { is_attachment: true }, null).then(function (resp) {
                $rootScope.file.Success = true;
            }).catch((err) => {
                console.warn(err);
            })
        }, function (error) {
            // Mark the error
            ons.notification.toast(error, {animation: 'fall', timeout: 2000});
        }, function (progress) {
            // Write the progress as a percentage
            $rootScope.file.Progress = (progress.loaded / progress.total) * 100
        });
    };

    $('#upload_file').change(function() {
        if (document.getElementById('upload_file').files.length > 0) {
            let uploaded_file = document.getElementById('upload_file').files[0];

            if (uploaded_file.size >= MAX_FILE_SIZE) {
                swal("Oh noes!", "The maximum allowed file is " + MAX_FILE_SIZE/1000000 + ' MB.', "error");
                return;
            }
            attachFile(uploaded_file);
            // $scope.sendAttachment('', $rootScope.conversation.data, uploaded_file);
        }
    })

    $scope.sendAttachment = function (message, conversation, uploaded_file) {
        $scope.text_msg = null;
        skygearChat.createMessage(conversation, message, null, uploaded_file).then(function (resp) {
            // if ($scope.current_session.therapist.email !== $scope.conv_data.therapist_email) {
            //     increment_conversation_count();
            // }
        }, function (err) {
            console.log(err)
        })
    };

    $('#chatting').keydown(function (event) {
        if (event.key === "Enter") {
            $scope.sendMessage($scope.text_msg, $rootScope.conversation.data);
        }
    });

    let compareDates = function(comparator) {
        const comp = new Date(comparator)
        comp.setHours(0,0,0)

        return comp >= now
    }

    socket.on('message', (message) => {
        if (message.type == 'bye') {
            clearTimer();
            peerConnection.close();
            peerConnection = null;
            $scope.$apply(() => {
                $scope.voice.ringing = false;
                $scope.voice.answered = false;
                $scope.voice.remote_socket = null;
                $scope.voice.terminated = true;
            })
            stopRinging();
            trace('Ending call.');
        }

        if (message.type == 'answer') {
            $scope.$apply(() => {
                $scope.voice.answered = true;
                $scope.voice.ringing = false;
                stopRinging();
            })
            if (peerConnection) {
                peerConnection.setRemoteDescription(new RTCSessionDescription(message));
            }
            startTimer();
        }

        if (message.type == 'candidate') {
            var candidate = new RTCIceCandidate({
                sdpMLineIndex: message.label,
                candidate: message.candidate
            });
            if (peerConnection) {
                peerConnection.addIceCandidate(candidate);   
            }
        }
    })

    socket.on('contact_status', (status, socket) => {
        console.log('contact status: ', status);
        if (!status) {
            swal('Therapist unavailable').then(function() {
                if (peerConnection) {
                    peerConnection.close();
                    peerConnection = null;
                }
        
                // change voice flags
               $scope.$apply(() => {
                    $scope.voice.ringing = false;
                    $scope.voice.answered = false;
                    $scope.voice.calling = false;
                    $scope.voice.terminated = true;
               })
        
                stopRinging();
                trace('Ending call.');
            })
        }
        else {
            $scope.$apply(() => {
                $scope.voice.ringing = true;
                $scope.voice.remote_socket = socket;
            })
            ring();
        }
    })

    let constructDate = function(date_object) {
        const comp = new Date(date_object)
        comp.setHours(0,0,0)
        return comp
    }

    $rootScope.toggleMute = function() {
        console.log('mute clicked');
        $timeout(() => {
            $scope.$apply(function() {
                $scope.voice.muted = !$scope.voice.muted;
                localStream.getAudioTracks()[0].enabled = $scope.voice.muted;
            })
        })
    }

    $rootScope.toggleCamera = function() {
        console.log('camera clicked');
        $timeout(() => {
            $scope.$apply(function() {
                $scope.voice.video_muted = !$scope.voice.video_muted;
                localStream.getVideoTracks()[0].enabled = $scope.voice.video_muted;
            })
        })
    }


    function hangupCall() {
        clearTimer();
        if (peerConnection) {
            peerConnection.close();
            peerConnection = null;
        }

        // change voice flags
        $scope.voice.ringing = false;
        $scope.voice.answered = false;
        $scope.voice.calling = false;

        if ($scope.voice.remote_socket) {
            sendMessage({
                type: 'bye', 
                socket: $scope.voice.remote_socket
            });
        }
        stopRinging();
        trace('Ending call.');
        $rootScope.hideModal();
    }

    $rootScope.hangUp = function() {
        hangupCall();
    }

    // When the user clicks on <span> (x), close the modal
    close.onclick = function() {
        hangupCall();
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
      if (event.target == modal) {
        $rootScope.hideModal();
      }
    }

    function handleIceCandidate(event) {
        if (event.candidate) {
            sendMessage({
              type: 'candidate',
              label: event.candidate.sdpMLineIndex,
              id: event.candidate.sdpMid,
              candidate: event.candidate.candidate
            });
        } else {
            console.warn('End of candidates.');
        }
    }
 
    function handleRemoteStreamAdded(event) {
        localVideo.srcObject = event.stream;
        $scope.voice.muted = localVideo.srcObject.getAudioTracks()[0].enabled;
        $scope.voice.video_muted = localVideo.srcObject.getVideoTracks()[0].enabled;
    }

    function handleRemoteStreamRemoved() {
        localVideo.srcObject = ''
        console.log('Remote stream removed');
    }

    function handleTrackAdded(track) {
        console.log('track added: ', track);
    }

    function handleTrackRemoved(track) {
        console.log('track removed: ', track);
    }

    function sendMessage(message) {
        socket.emit('message', message);
    }

    window.onbeforeunload = function() {
        if ($scope.voice.remote_socket) {
            sendMessage({
                type: 'bye',
                socket: $scope.voice.remote_socket
            });
        }
    };

    socket.on('bye', function() {
        clearTimer();
        peerConnection.close();
        peerConnection = null;
        $scope.$apply(() => {
            $scope.voice.ringing = false;
            $scope.voice.answered = false;
            $scope.voice.remote_socket = null;
            $scope.voice.terminated = true;
        })
        stopRinging();
    })

    function ring() {
        audio = new Audio('/static/sound/old_telephone.mp3');
        audio.addEventListener('ended', function() {
            this.currentTime = 0;
            this.play();
        }, false);
        audio.play();
    }

    function stopRinging() {
        if (audio) {
            audio.pause();
            audio.currentTime = 0;
        }
    }

    // create offer to remote connection 
    // set localdescription on peerconnection
    // send message to remote connection
    function setLocalAndSendMessage(sessionDescription) {
        peerConnection.setLocalDescription(sessionDescription);
        sendMessage({
            type: 'offer',
            data: sessionDescription,
            user_type: 'therapist',
            email: $scope.conv_data.therapist_email,
            token: $scope.cookie_token.data
        });
    }

    function handleCreateOfferError(error) {
        console.warn('createOffer() error: ', error)
        swal('Unable to connect').then(() => {
            hangupCall();
        })
    }

    function doCall() {
        $scope.$apply(function() {
            $scope.voice.terminated = false;
        })
        peerConnection.createOffer(setLocalAndSendMessage, handleCreateOfferError);
    }      

    function startCall() {
        try {
            // show calling flag
            $scope.voice.calling = true;
            peerConnection.addStream(localStream);
            doCall();
        } catch(e) {
            console.log(e.message);
            swal('Unable to connect!').then(() => {
                hangupCall();
            })
        }
    }

    function enablePeerListeners() {
        try {
            peerConnection = new RTCPeerConnection(turnserver);
            peerConnection.onicecandidate = handleIceCandidate;
            peerConnection.onaddstream = handleRemoteStreamAdded;
            peerConnection.onremovestream = handleRemoteStreamRemoved;
            peerConnection.onaddtrack = handleTrackAdded;
            peerConnection.onremovetrack = handleTrackRemoved;
            startCall();
        } catch (e) {
            console.warn('Failed to create PeerConnection, exception: ' + e.message);
            swal('Cannot create connection. Check your network connection').then(() => {
                hangupCall();
            });
            return;
        }
    }

    // handle stream responses
    function handleStream(stream) {
        localStream = stream;
        console.log(localStream);
        // localVideo.srcObject = stream;
        showModal();
        enablePeerListeners();
    }

    async function init(e) {
        e.disabled = true;
        trace('localVideo: ', localVideo);

        const constraints = window.constraints = {
            audio: true,
            video: true
        };
        const stream = await navigator.mediaDevices.getUserMedia(constraints);
        handleStream(stream);
    }

    document.querySelector('#call').addEventListener('click', e => init(e));


    $scope.init = function(therapist_id, profile, therapist_email, therapist_name, username, current_session, cookie_token, conversation_limit) {

        // connect to socket
        socket.open();
        $scope.cookie_token = {data: cookie_token};

        swal({
            title: 'Syncing',
            type: 'info',
            html: 'syncing messages...<br><br><br>',
            onOpen: () => {
                swal.showLoading()
              },
              onClose: () => {
              }
        });

        deactivateConversation();
        $scope.current_session = JSON.parse(current_session);
        $scope.account = {data: JSON.parse(profile)}
        $scope.skyUser = skygear.auth.currentUser
        $scope.conversation_limit = conversation_limit;
        $scope.conv_diff = $scope.conversation_limit - $scope.account.data.counter_conversation
        $scope.showMsg = {limit: false, active_session: false, can_call: false};

        $scope.loading = {more: false, data: true}
        $scope.conv_data = {
            therapist_email: therapist_email,
            therapist_name: therapist_name,
            username: username,
            conversation_title: 'user:' +  username + '-' + 'therapist:' + therapist_email
        };

        getUser($scope.conv_data.therapist_email);
    }

    let addressLimit = function() {
        console.log('conversations: ', $scope.account.data.counter_conversation);
        if ($scope.account.data.counter_conversation >= $scope.conversation_limit) {
            $scope.showMsg.limit = true;
            return deactivateConversation()
        }
        // show conversation limit
        $scope.showMsg.limit = true;
        return activateConversation();
    }

    var unbind = $scope.$watch('conversation.messages', function(e) {
        console.log('loading: ', $scope.loading.data);
        // if user has a current session
            // if user has a current therapist
                // if session is active
                    // if session therapist === current therapist
                        // activateConversation();
                    // else
                        // if conversation limit exceeded
                            // deactivateConversation();
                        // else
                            // activateConversation();
                            // show conversation limit
                // else
                     // if conversation limit exceeded
                        // deactivateConversation();
                    // else
                        // activateConversation();
                        // show conversation limit
            // else
                // if conversation limit exceeded
                    // deactivateConversation();
                // else
                    // activateConversation();
                    // show conversation limit
        // else
            // if conversation limit exceeded
                // deactivateConversation();
            // else
                // activateConversation();
                // show conversation limit

        if (!$scope.loading.data) {
//             $scope.showMsg.active_session = constructDate($scope.current_session.start_date) > now || constructDate($scope.current_session.expiration_date) < now || $scope.current_session.termination_date !== null
             $scope.showMsg.active_session = constructDate($scope.current_session.start_date) <= now && constructDate($scope.current_session.expiration_date) >= now && $scope.current_session.termination_date === null
             $timeout(function() {
                scrollBottom();
                if ($scope.current_session) {
                if ($scope.current_session.therapist) {
                    if ($scope.showMsg.active_session) {
                        if ($scope.current_session.therapist.email == $scope.conv_data.therapist_email) {
                            $scope.$apply(() => {
                                $scope.showMsg.can_call = true;
                            })
                            activateConversation();
                        }
                        else {
                            addressLimit();
                        }
                    } else {
                        addressLimit();
                    }
                }
                else {
                    addressLimit();
                }
                }
                else {
                    addressLimit();
                }
                unbind();
             })

        }
    })

//    var unbind = $scope.$watch('conversation.messages', function() {
//
//        if ($rootScope.conversation.messages.length > 0) {
//            $timeout(function(){
//                scrollBottom();
//
//                // check if session has not expired
////                if (constructDate($scope.current_session.expiration_date) < now) {
////                    $("#chatting").attr('disabled','disabled');
//                unbind();
//            })
//        }
//
//        // check if session is active
//        if (constructDate($scope.current_session.start_date) > now || constructDate($scope.current_session.expiration_date) < now || $scope.current_session.termination_date !== null) {
//            $("#chatting").attr('disabled','disabled');
//        }
//
//        else {
//            $("#chatting").prop('disabled', false);
//        }
//
//        else {
//            if ($scope.current_session) {
//                if ($scope.current_session.therapist && $rootScope.conversation.data.metadata) {
//                    if ($scope.current_session.therapist.email !== $rootScope.conversation.data.metadata.therapist_email) {
//                         $("#chatting").prop('disabled', false);
//                    }
//                }
//            }
//        }
//    })

});

app.controller('ShopController', function ($scope, Product) {
    $scope.data = {
        has_other_pages: false,
        count: 0,
        page_range: [],
        data: [],
        start_index: 0,
        end_index: 0
    };
    Product.get(function (response) {
        $scope.data = {
            has_other_pages: response.has_other_pages,
            count: response.count,
            page_range: response.page_range,
            data: response.data,
            start_index: response.start_index,
            end_index: response.end_index
        };
    })

});

app.controller('ProfileController', function ($scope, $q, $http, $rootScope, $cookies, S3UploadService) {

    // login error listener
    socket.on('login_error', function(message) {
        swal('Session Timed out').then(function() {
            $scope.logout()
        })
    });

    $rootScope.hideModal = function() {
        modal.style.display = "none";
    }

    // login successful listener
    socket.on('login_successful', function(user) {
        swal.close();
    });

    moment.fn.minutesFromNow = function() {
        return Math.floor((+new Date() - (+this))/60000) + ' mins ago';
    }

     $scope.logout = function () {

        swal({
            title: 'Logging out',
            type: 'info',
            html: 'logging out...',
            onOpen: () => {
                swal.showLoading()
              },
              onClose: () => {
              }
        });

        skygear.auth.logout().then(function () {
            socket.disconnect();
            swal.close();
            location.href = '/logout';
        }, function (error) {
            console.error(error);
        });
    };

    var setNotifications = function (data) {
        $rootScope.notifications = data;
    };

    var loadNotifications = function () {
        $http({
            method: 'GET',
            url: '/api/user/notifications'
        }).then(function successCallback(response) {
            setNotifications(response.data.data);
        }, function errorCallback(err) {
            console.log(err);
        });
    };

    var setMessages = function (data) {
        $rootScope.user_messages = data;
        if ($scope.user_messages.total > 0) {
            $scope.current_message = $scope.user_messages.items[0]
        }
    };

    $rootScope.changeCurrent = function(msg) {
        $scope.current_message = msg
    }

    var loadMessages = function () {
        $http({
            method: 'GET',
            url: '/api/user/messages'
        }).then(function successCallback(response) {
            setMessages(response.data.data);
        }, function errorCallback(err) {
            console.log(err);
        });
    };

    function initVoice() {
        // setup video configurations
        $scope.voice = {
            ringing: false,
            calling: true,
            answered: false,
            terminated: false,
            muted: false,
            video_muted: false,
            offerOptions: {
              offerToReceiveVideo: true,
              offerToReceiveAudio: true
            }
        }

        localVideo = document.getElementById('videoElement');
    }

    $scope.init = function (csrf_token) {

        $rootScope.notifications = {items: [], total: 0, page: 1};
        $rootScope.user_messages = {items: [], total: 0, page: 1};
        $scope.current_message = null;
        $rootScope.conversation = {messages: [], data: {}, available: true};

        var defer = $q.defer();

        $q.all([loadMessages(), loadNotifications(), initVoice()]).then(function (resp) {
        }, function (err_response) {
            console.log(err_response)
        });

        return defer.promise;
    }

    $scope.quitTherapy = function() {
 
        swal({
          title: 'Are you sure?',
          text: "Kindly provide feedback on why you want to terminate this therapy session",
        //   text: "Once deleted, your session will be terminated!",
          type: 'warning',
          input: 'textarea',
        //   showCancelButton: true,
        //   confirmButtonColor: '#3085d6',
        //   cancelButtonColor: '#d33',
        //   confirmButtonText: 'Yes, terminate it!'
        }).then((result) => {
          if (result.dismiss) {
            swal("You cancelled the termination request!");
          }
          else {
            $.ajax({
                url: '/api/therapists/quit',
                method: 'POST',
                data:  {
                    csrfmiddlewaretoken: $cookies.get('csrftoken'),
                    body: result.value
                },
                success: function (resp) {
                    swal({
                        title: 'Success',
                        text: "Poof! Your therapy session has been terminated!"
                    }).then((result) => {
                        location.reload()
                    })
                },
                error: function (err) {
                    swal({
                        title: 'Error',
                        text: err.responseJSON.error.message
                    }).then((result) => {
                    })
                }
            });
          }
        })
    }

});

app.controller('MessageController', function($scope, $rootScope, Message, $http, $cookies) {

    var setMessages = function(data) {
        $scope.messages = data;
        if ($scope.messages.length > 0) {
            $scope.current_message = $scope.messages[0]
        }

    };

    $scope.init = function() {
        $scope.messages = {items: [], total: 0, pages: 0};
        const loader = document.getElementById('loader');
        loader.classList.add('fadeOut');
        Message.get(function (data) {
            setMessages(data.data);
        })
    };

    $scope.initCompose = function(user_id, username, therapist_id, therapist_name) {
        $scope.message = {
            subject: null, body: null, therapist_id: therapist_id, csrfmiddlewaretoken: $cookies.get('csrftoken'),
            recipient: therapist_name, sender: username, do_not_reply: false, username: username, user_id: user_id,
            thread_id: '' + String(therapist_id) + '-' + username + '-' + therapist_name
        };
    };

    $scope.sendMessage = function() {

        $.ajax({
            url: '/api/messages/send',
            method: 'POST',
            data: $scope.message,
            success: function (resp) {
                swal(
                    'Sent!',
                    'Message successfully sent to therapist',
                    'success'
                ).then((result) => {
                    location.href = '/messages'
                });
            },
            error: function (err) {
                swal(
                    'Error',
                    err.message,
                    'error'
                );
            }
        });
    }
});